/*
 * ilc_task.c
 *
 *  Created on: Aug 23, 2017
 *      Author: okoc
 */

#include "table_tennis_common.h"
#include "sl_interface_ilc.h"

void add_ilc_task(void);
static int change_ilc_task(void);
static int init_ilc_task(void);
static int run_ilc_task(void);
static void display_environment(void);
static void display_sim_env(void);
static void compute_torques();
static void check_safety();
static int goto_posture(int posture);
static int goto_center_posture();
static int goto_right_posture();
static int goto_left_posture();
static void turn_off_fb();
static void turn_on_fb();

/*
 * Adds the task to the task menu
 */
void add_ilc_task(void) {
    int i;
    char varname[30];

    addTask("ILC task", init_ilc_task, run_ilc_task, change_ilc_task);
}

/*
 * Changes task parameters
 */
static int change_ilc_task(void) {
    int i, j;
    return TRUE;
}

/*
 * Initialization for task
 *
 */
static int init_ilc_task(void) {

    int i;
    int posture, ready, turn_off_pd; // flags

    for (i = 1; i <= N_DOFS; i++) {
        joint_des_state[i].th = joint_state[i].th;
        joint_des_state[i].thd = 0.0;
        joint_des_state[i].thdd = 0.0;
        joint_des_state[i].uff = 0.0;
    }

    turn_on_fb();

    /* check whether any other task is running */
    if (strcmp(current_task_name, NO_TASK) != 0) {
        printf("Task can only be run if no other task is running!\n");
        return FALSE;
    }

    /* go to a save posture */
    get_int("Which posture? 0 = CENTRE, 1 = RIGHT, 2 = LEFT.\n", 0, &posture);
    if (!goto_posture(posture))
        return FALSE;

    get_int("Turn off PD? 0 = NO, 1 = YES.\n", 0, &turn_off_pd);

    reset_experiments(joint_state);

    // for real setup
    setDefaultEndeffector();
    endeff[RIGHT_HAND].x[_Z_] = .3;

    /* ready to go */
    ready = 999;
    while (ready == 999) {
        if (!get_int("Enter 1 to start or anything else to abort ...", ready,
                &ready))
            return FALSE;
    }
    if (ready != 1)
        return FALSE;

    // turn on/off real time
    changeRealTime(TRUE);
    if (turn_off_pd)
        turn_off_fb();
    return TRUE;

}

/*
 * Runs the task from the task servo: REAL TIME requirements!
 *
 */
static int run_ilc_task(void) {

    experiment(joint_state, joint_des_state);

    // reset and draw ball in simulation
    display_environment();

    // do not compute inverse dynamics !
    check_safety();

    return TRUE;
}

/*
 * Switch between motor servo feedback and task servo feedback
 * MAYBE DELAYED?
 * Can be used before starting trajectory tracking
 */
static void turn_off_fb() {

    // to change feedback
    static int firsttime = TRUE;
    static Vector zerogain;

    if (firsttime) {
        firsttime = FALSE;
        zerogain = my_vector(1, 7);
    }
    changePIDGains(zerogain, zerogain, zerogain);
}

/*
 * Switch between motor servo feedback and task servo feedback
 * MAYBE DELAYED?
 * Can be used before starting trajectory tracking
 */
static void turn_on_fb() {

    static int firsttime = TRUE;

    // to change feedback
    static Vector pgain, dgain, igain;

    if (firsttime) {
        firsttime = FALSE;
        pgain = my_vector(1, 7);
        dgain = my_vector(1, 7);
        igain = my_vector(1, 7);
        read_gains(config_files[GAINS], pgain, dgain, igain);
    }

    changePIDGains(pgain, dgain, igain);
}

/*
 * Displays the table tennis environment (static, no ball)
 */
static void display_environment() {

    // for resetting
    static int firsttime = TRUE;
    static int reset_sim = FALSE;
    static double time_passed;
    static double time_last;

    if (firsttime) {
        firsttime = FALSE;
        time_last = get_time();
    }

    // calculate the racket orientation from endeffector
    calc_racket(&racket_state, &racket_orient, cart_state[RIGHT_HAND],
            cart_orient[RIGHT_HAND]);

    display_sim_env();
}

/*
 * Draws the table and the racket every 2 ms for simulation
 */
static void display_sim_env(void) {

    double pos[14];

    pos[4] = racket_state.x[_X_];
    pos[5] = racket_state.x[_Y_];
    pos[6] = racket_state.x[_Z_];
    pos[7] = racket_orient.q[_Q0_];
    pos[8] = racket_orient.q[_Q1_];
    pos[9] = racket_orient.q[_Q2_];
    pos[10] = racket_orient.q[_Q3_];

    //no need to send ball
    pos[11] = 0.0;
    pos[12] = 0.0;
    pos[13] = 0.0;

    sendUserGraphics("env", &(pos[0]), 14 * sizeof(double));
}

/*
 * Check the safety of the desired joint states explicitly
 * and then calculate the u_ff with inverse dynamics
 *
 * If friction compensation is turned on, then add some compensation on top of u_ff
 *
 */
static void compute_torques() {

    check_safety();

    // control the robot
    // calculate the feedforward commands with inverse dynamics
    SL_InvDyn(NULL, joint_des_state, endeff, &base_state, &base_orient);
    /*if (friction_comp) {
     addFrictionModel(joint_des_state);
     }*/

}

/*
 * Checks the safety of the calculated desired state
 * explicitly
 *
 */
static void check_safety() {

    if (!check_joint_limits(joint_state, SLACK)) {
        printf("Joint limits are exceeding limits! Freezing...\n");
        turn_on_fb();
        freeze();
    }
    if (!check_des_joint_limits(joint_des_state, SLACK)) {
        printf("Joint des limits are exceeding limits! Freezing...\n");
        turn_on_fb();
        freeze();
    }
    if (!check_range(joint_des_state)) {
        printf("Exceeding torque limits! Freezing...\n");
        turn_on_fb();
        freeze();
    }
    if (collision_detection(racket_state)) {
        printf("Collision with table detected!\n");
        turn_on_fb();
        freeze();
    }
}

/*
 * Initialize robot posture at right, center and left sides
 */
static int goto_posture(int posture) {

    int i;
    if (posture == 1)
        goto_right_posture();
    else if (posture == 0)
        goto_center_posture();
    else if (posture == 2)
        goto_left_posture();
    else { // unrecognized input
        printf("Unrecognized posture. Quitting...\n");
        return FALSE;
    }

    for (i = 1; i <= N_DOFS; i++) {
        joint_des_state[i].th = init_joint_state[i].th;
        joint_des_state[i].thd = 0.0;
        joint_des_state[i].thdd = 0.0;
    }
    return TRUE;
}

/*
 * Robot goes to a center posture
 */
static int goto_center_posture() {

    int i;
    bzero((char *) &(init_joint_state[1]),
            N_DOFS * sizeof(init_joint_state[1]));
    init_joint_state[1].th = 0.0;
    init_joint_state[2].th = 0.0;
    init_joint_state[3].th = 0.0;
    init_joint_state[4].th = 1.5;
    init_joint_state[5].th = -1.75;
    init_joint_state[6].th = 0.0;
    init_joint_state[7].th = 0.0;

    go_target_wait(init_joint_state);
    return FALSE;

    return TRUE;
}

/*
 * Robot waits on the right-hand side
 */
static int goto_right_posture() {

    int i;
    bzero((char *) &(init_joint_state[1]),
            N_DOFS * sizeof(init_joint_state[1]));
    init_joint_state[1].th = 1.0;
    init_joint_state[2].th = -0.2;
    init_joint_state[3].th = -0.1;
    init_joint_state[4].th = 1.8;
    init_joint_state[5].th = -1.57;
    init_joint_state[6].th = 0.1;
    init_joint_state[7].th = 0.3;

    go_target_wait(init_joint_state);
    if (!go_target_wait_ID(init_joint_state))
        return FALSE;
    return TRUE;
}

/*
 * Robot waits on the left-hand side
 */
static int goto_left_posture() {

    int i;
    bzero((char *) &(init_joint_state[1]),
            N_DOFS * sizeof(init_joint_state[1]));
    init_joint_state[1].th = -1.0;
    init_joint_state[2].th = 0.0;
    init_joint_state[3].th = 0.0;
    init_joint_state[4].th = 1.5;
    init_joint_state[5].th = -1.57;
    init_joint_state[6].th = 0.1;
    init_joint_state[7].th = 0.3;

    go_target_wait(init_joint_state);
    if (!go_target_wait_ID(init_joint_state))
        return FALSE;
    return TRUE;
}
