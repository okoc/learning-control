Do the following: 

1. include the header files in the include/ path (table_tennis_common.h table.h)
2. copy the display_env() function inside table_tennis_graphics.c to initUserGraphics.c
3. add the following line to initUserGraphics.c inside initUserGraphics() function :

addToUserGraphics("env", "Display Table and Racket", &(display_env), 14*sizeof(double));
