/**
 * @file traj.h
 *
 * @brief Declarations for trajectory optimization and learning controllers.
 *
 *
 *  Created on: Jun 7, 2016
 *  Modified: August 14, 2017
 *      Author: okoc
 */

#ifndef TRAJ_H_
#define TRAJ_H_

#include "constants.h"

using namespace arma;

namespace Traj {

/**
 * @brief 3rd order spline parameters returned from optimizer.
 *
 * If optimization is still running, player does not update trajectories.
 * IF optimization was successful (update is TRUE) and
 * it has terminated (running is FALSE), then player class can update trajectories.
 */
struct spline_params {
    mat a = zeros < mat > (NDOF, 4); //!< strike poly params of 3rd order
    mat b = zeros < mat > (NDOF, 4); //!< return poly params of 3rd order
    double time2hit = 1.0; //!< free-final time (for hitting ball)
};

/**
 * @brief Desired/actual joint positions, velocities, accelerations and torques.
 *
 * Main Player function play() updates desired joint values
 * every 2ms for Barrett WAM.
 */
struct joint {
    vec7 q = zeros < vec > (NDOF); //!< desired/actual joint pos
    vec7 qd = zeros < vec > (NDOF); //!< desired/actual joint vels
    vec7 qdd = zeros < vec > (NDOF); //!< desired/actual joint accs
    vec7 u = zeros < vec > (NDOF); //!< calculated (desired) torques

    /**
     * @brief Constructor for joint structure holding q,qd,qdd and uff values.
     * @param ndof Number of degrees of freedom of the system
     */
    joint(const unsigned ndof = NDOF);
};

/**
 * @brief Trajectories in joint space for the robot.
 *
 * Normally references can be generated incrementally, but this structure
 * is useful for ILC since in ILC we do not really have real-time constraints
 * for traj. generation.
 * (excluding table tennis constraints of course).
 */
struct traj {

    bool filtered = false;
    mat Q;
    mat Qd;
    mat Qdd;
    mat us; //!< feedforward controls

    /** @brief Set the size of the joints and controls */
    traj(const unsigned N = 1);

    /** @brief Similar to constructor, but after default constructor initialized mats. */
    void enlarge(const unsigned N);

    /** @brief Only keep a portion of the trajectories (if N < current length). */
    void cut(const unsigned N);

    /** @brief Fill the corresponding element of the joint matrices */
    void fill(const joint & qnow, unsigned elem);

    /**
     * @brief Slow down a reference
     *
     * @param lambda is the relaxation parameter, i.e.
     * lambda < 1 speeds up the trajectory while
     * lambda > 1 slows down the trajectory.
     *
     * Ugly code since armadillo does not support matrix interpolation.
     * If us were not computed (e.g. inverse dynamics) or zeroed before, then leaves them be.
     */
    void relax(const double lambda);

    /** @brief Append a new traj to the current one. */
    void join(const traj & traj2);

    /** @brief Filter trajectory with zero-phase 2nd order Butterworth filter. */
    void filter(const bool filter_pos, const double & cutoff_freq);

    /**
     * @brief Calculates the Root Mean Squared (RMS) error of a joint striking trajectory
     * w.r.t a reference.
     *
     * Both position and velocity errors are taken into account till timestep N.
     */
    double calc_rms_error(const traj & ref,
                                const int N) const;

    /**
     * @brief Calculate final hitting error norm
     * @param strike
     * @return Error norm of final hit
     */
    double calc_final_error(const traj & strike) const;

    /**
     * @brief Find the difference between current traj and last traj
     * @param traj_last Last q,qd,qdd pair
     * @param oper Return only q, [q;qd] or [q;qd;qdd] pairs (1-2-3 resp.)
     * @param error Return the matrix of [q], [q;qd] or [q;qd;qdd] errors
     */
    void subtract(const traj & traj_last, const int oper, mat & error) const;

};

/**
 * @brief Calculate polynomial coefficients and final time.
 *
 * If the optimizers finished running and terminated successfully,
 * then generates the striking and returning polynomial coefficients
 * from qf, qfdot and T, and given the actual joint states qact, qactdot
 *
 */
void calc_coeffs(const joint & qact,
                const vec::fixed<15> & strike_params,
                const vec::fixed<8> & rest_params,
                spline_params & p);

// three version for traj generation: first one is full while
// second loads strike/return trajectories separately
// third version cuts the generated strike trajectory and adjusts return

/**
 * @brief Generate random 3rd order hitting and returning polynomials for tabletennis
 *
 * Using a lookup table to load 3rd order polynomial hitting parameters qf, qfdot, T.
 * Then generating the whole (batch) desired trajectory (hitting and returning)
 * from the actual joint positions and velocities.
 *
 */
void gen_random_tabletennis_traj(const joint & qact,
                                const vec7 & qrest,
                                const double time2return,
                                traj & ref);
/**
 * @brief Generate random 3rd order hitting and returning polynomials for tabletennis
 *
 * Using a lookup table to load 3rd order polynomial hitting parameters qf, qfdot, T.
 * Then generating hitting and returning trajectories separately.
 */
void gen_random_tabletennis_traj(const joint & qact,
                                const vec7 & qrest,
                                const double time2return,
                                traj & strike,
                                traj & returning);

/**
 * @brief Generate random 3rd order hitting and returning polynomials for tabletennis.
 * Cuts the generated strike trajectory and adjusts return.
 *
 * Using a lookup table to load 3rd order polynomial hitting parameters qf, qfdot, T.
 * Then generating hitting and returning trajectories separately, where the hitting traj.
 * is cut (decided by the parameter cut_strike).
 *
 * @return Spline parameters for hitting and returning.
 *
 */
spline_params gen_random_tabletennis_traj(const joint & qact,
                                        const vec7 & qrest,
                                        const double time2return,
                                        const double cut_strike,
                                        traj & strike,
                                        traj & returning);

/**
 * @brief Optimize 3rd order hitting and returning polynomials for table tennis
 * @param ball_state Suitable ball state, should be feasible for hit/return.
 * @param qact Actual robot state
 * @param qrest Desired rest state
 * @param ball_land_des Desired landing positions (2d)
 * @param time2return Desired time to return to rest after hit
 * @param time_land_des Desired landing time
 * @param strike
 * @param returning
 * @return generate TRUE if optimized successfully!
 */
bool gen_random_tabletennis_traj(const vec6 & ball_state,
                                 const joint & qact,
                                 const vec7 & qrest,
                                 const vec2 & ball_land_des,
                                 const double time2return,
                                 const double time_land_des,
                                 traj & strike,
                                 traj & returning,
                                 Traj::spline_params & poly);

/**
 * @brief Generate strike and return traj. incrementally
 *
 * Given polynomial parameters saved in poly,
 * move on to the NEXT desired state only (joint pos,vel,acc).
 * @param poly Polynomial parameters updated in OPTIM classes
 * @param q_rest_des FIXED desired resting posture
 * @param time2return FIXED time to return to rest posture after hit
 * @param t The time passed already following trajectory
 * @param qdes Update pos,vel,acc values of this desired joints structure
 * @return flag If still moving set to true.
 */
bool update_next_state(const spline_params & poly,
                       const vec7 & q_rest_des,
                       const double time2return,
                       double & t,
                       joint & qdes);

/** @brief Initialize robot posture on the right size of the robot */
void init_right_posture(vec7 & q0);

/** @brief Initialize robot posture at right, center and left sides */
void init_posture(const int posture, vec7 & q0);

// joint limit checking

/**
 * @brief Check pos,vel,acc,torque limits for whole trajectory.
 *
 * Here we check that the joint position, velocity and acceleration
 * as well as torque limits
 * are always satisfied (i.e. for the whole trajectory).
 * @param ref Trajectory structure (pos,vel,acc,ff torques)
 * @return true if all checks are satisfied for whole trajectory
 *
 * WARNING: Inverse dynamics must have been computed before!
 *
 */
bool check_all_limits(const traj & ref);

/**
 * @brief Only check torque limits for feedforward controls
 *
 * Useful in ILC loop, where the feedforward commands are
 * updated.
 * @param u_ff
 */
bool check_torque_limits(const mat & u_ff);

/**
 * @brief Clamp online the full applied torque to the limits
 * @param u_full Feedforward + feedback torques at each time step
 * @return If any value was clamped return TRUE
 */
bool clamp_torque(vec & u_full);

/**
 * @brief Reads joint limits from file.
 *
 * Limits file is located in the config/ folder of the main repo.
 *
 * @param lb lower bound values to be loaded.
 * @param ub upper bound values to be loaded.
 */
void read_joint_limits(vec7 & lb, vec7 & ub,
                        vec7 & vel_lim, vec7 & acc_lim,
                        vec7 & torque_lim);
}

#endif /* OPTIMPOLY_H_ */
