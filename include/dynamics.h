/**
 * @file dynamics.h
 *
 * @brief Forward and inverse dynamics functions as well as parameter loading routines are here.
 *
 *  Created on: Aug 15, 2017
 *      Author: okoc
 */

#ifndef INCLUDE_DYNAMICS_H_
#define INCLUDE_DYNAMICS_H_

using arma::vec3;
using arma::vec;
using arma::zeros;
using arma::mat;
using arma::mat33;

const int TAG_FORW_DYN_JAC = 1;
const int TAG_INV_DYN_JAC = 2;

// definitions
const double ZSFE = 0.346; //!< z height of SAA axis above ground
const double ZHR = 0.505; //!< length of upper arm until 4.5cm before elbow link
const double YEB = 0.045;  //!< elbow y offset
const double ZEB = 0.045;  //!< elbow z offset
const double YWR = -0.045;  //!< elbow y offset (back to forewarm)
const double ZWR = 0.045;  //!< elbow z offset (back to forearm)
const double ZWFE = 0.255; //!< forearm length (minus 4.5cm)

const double g = 9.81; //TODO: why is this positive?

namespace dyn {

/**
 * @brief Base cartesian positions and orientations (quaternion).
 *
 * Base is not modified anywhere, only used as a static structure
 * in forward/inverse dynamics functions.
 */
struct base {
    vec3 x = zeros < vec > (3); //!< Cartesian position
    vec3 xd = zeros < vec > (3); //!< Cartesian velocity
    vec3 xdd = zeros < vec > (3); //!< Cartesian acceleration
    vec4 q = { 0.0, 1.0, 0.0, 0.0 }; //!< Orientation as quaternion
    vec3 ad = zeros < vec > (3); //!< Angular Velocity [alpha,beta,gamma]
    vec3 add = zeros < vec > (3); //!< Angular Accelerations
};

/**
 * @brief External forces and torques.
 *
 * External forces are not modified anywhere, only used as a static structure
 * in forward/inverse dynamics computations.
 */
struct force_ex {
    vec3 base_force = zeros < vec > (3);
    vec3 base_torque = zeros < vec > (3);
    mat joint_force = zeros < mat > (7, 3);
    mat joint_torque = zeros < mat > (7, 3);
};

/**
 * @brief Endeffector parameters that are necessary (mass, position, ...).
 *
 * A racket is attached to the endeffector whose center is 30 cm away
 * from endeffector in the z-direction.
 * Used as a static structure in forward/inverse dynamics computations.
 */
struct end_effector {
    double mass = 0.0; //!< mass
    vec3 mcm = zeros<vec> (3); //!< mass times center of mass
    vec3 x = { 0.0, 0.0, 0.30 }; //!< Cartesian position
    vec3 a = zeros<vec>(3); //!< Euler angles
};

/**
 * @brief Link parameters, loaded from files. Passed as argument to forward/inverse dyn.
 *
 * Normally 1 for the base and 7 for the joints: 8 link structures are needed.
 * load_link_params() function loads the parameters from a file.
 */
struct link {
    double mass = 0.0; //!< mass of the link
    vec3 mcm = zeros<vec>(3); //!< mass times center of mass of link
    mat33 inertia = zeros<mat>(3,3); //!< inertia matrix corresponding to link

    /**
     * @brief Vectorizes link parameters such that autodiff can be used.
     * @return vector of link parameters (particular link)
     */
    vec vectorise() const;

};

typedef void (*f_dyn)(const link link_param[NDOF + 1], Traj::joint & Q); //!< forward and inv. dyn. typedef

/**
 * @brief Loads the link parameters from config file to array of links structure.
 *
 * The link parameters are located in the config/ folder of main repo.
 * Loaded format: mass (1), mcm (3), symmetric inertia matrix params (6)
 */
void load_link_params(const std::string & filename,
                    link param[NDOF + 1]);
/**
 * @brief Loads the PD gains from file
 *
 * PD values are loaded from config/ folder in main repo
 */
void load_PD_feedback_gains(mat & fb);

/**
 * @brief Load LQR matrix (calc. in MATLAB) into given matrix
 * @param fb
 */
void load_LQR_feedback(mat & fb, int version);

// link related functions

/**
 * @brief Randomize array of link parameters by perturbing them with uniform distr.
 *
 * Disturbance is multiplicative.
 *
 * @param std Standard deviation of perturbations
 * @param link_param Links whose parameters are already loaded
 * @param new_links Perturbed links
 */
void randomize_links(const double & std,
                    const link link_param[NDOF + 1],
                    link new_links[NDOF + 1]);

/**
 * @brief Randomize array of link parameters by drawing samples from normal distr.
 *
 * Disturbance is additive.
 *
 * @param Sigma variance of the link parameters
 * @param link_param mean of the link parameters
 * @param new_links Perturbed links
 */
void randomize_links(const mat & Sigma,
                    const link link_param[NDOF + 1],
                    link new_links[NDOF + 1]);

/**
 * @brief Vectorize all link parameters (to one matrix)
 * @param link_params
 * @param link_mat
 */
void vectorise_links(const link link_params[NDOF+1], mat & link_mat);

/**
 * @brief Aggregate matrix of sys. id. link parameters into link structure
 * @param link_vec
 * @param link_params
 */
void aggregate_links(const mat & link_mat, link link_params[NDOF+1]);

// dynamics based control computations

/** @brief Use inverse dynamics to calculate torques for trajectory */
void calc_inv_dynamics_for_traj(f_dyn f_inv, const link link_param[NDOF + 1],
                                Traj::traj & ref);

/**
 * @brief Linearize dynamics around a reference by using numerical diff.
 *
 * Using 'TWO-SIDED-SECANT' to do a stable linearization
 * Ugly code to preserve simple syntax for forward dynamics
 *
 * @param f Forward dynamics
 * @param link_param Link parameters for dynamics model
 * @param ref The reference trajectory around which we linearize dynamics
 * @param downsample This is the 'DOWNSAMPLE' size for reference
 * @param h This is the stepsize used for numerical diff.
 * @param A linearized LTV drift matrices around traj.
 * @param B linearized LTV control matrices around traj.
 */
void linearize_dynamics(f_dyn forward,
                        const link link_param[NDOF + 1],
                        const Traj::traj & ref,
                        const int downsample,
                        const double h,
                        cube & A,
                        cube & B);

/**
 * @brief Linearize dynamics around a reference by using autodiff (ADOLC)
 *
 * Using ADOL-C library to do automatic differentiation
 *
 * TODO: Pass a function pointer to forward dynamics autodiff version
 *
 * @param link_param Link parameters for dynamics model
 * @param ref The reference trajectory around which we linearize dynamics
 * @param downsample This is the 'DOWNSAMPLE' size for reference
 * @param A linearized LTV drift matrices around traj.
 * @param B linearized LTV control matrices around traj.
 */
void linearize_dynamics(const link link_param[NDOF + 1],
                        const Traj::traj & ref,
                        const int downsample,
                        cube & A,
                        cube & B);

/**
 * @brief Finite Horizon LQR computation for LTV model.
 * @param Ad LTV model matrices
 * @param Bd
 * @param Q Weighting matrix (for running costs)
 * @param Qf Final weighting matrix
 * @param R Penalty matrix for inputs
 * @param K Feedback matrix (output)
 * @param P Ricatti matrices (output)
 */
void lqr_fin_horizon_ltv(const cube & Ad,
                        const cube & Bd,
                        const mat & Q,
                        const mat & Qf,
                        const mat & R,
                        cube & K,
                        cube & P);

/**
 * @brief Discretize continuous matrices A,B for dt time step
 * @param dt time step
 * @param A Continuous drift matrices
 * @param B Continuous control matrices
 * @param Ad Output of discretization for drift term
 * @param Bd Output of discretization for control matrix
 */
void discretize_dynamics(const double dt,
                        const cube & A,
                        const cube & B,
                        cube & Ad,
                        cube & Bd);
/**
 * @brief Lift the discretized dynamics matrices A(t), B(t) for each t
 * to form one big input-to-output plant matrix F
 */
void lift_dynamics(f_dyn forward,
                    const link link_param[NDOF + 1],
                    const Traj::traj & ref,
                    const int downsample,
                    const double h,
                    mat & F);

// barrett wam dynamics

/**
 * @brief Barrett WAM forward dynamics for evolving (simulating) system trajectories.
 *
 * Articulate Dynamics taken from SL:
 * shared/barrett/math/ForDynArt_declare.h
 * shared/barrett/math/ForDynArt_math.h
 * shared/barrett/math/ForDynArt_functions.h
 *
 * These are called from shared/barrett/src/SL_dynamics.c
 *
 */
void barrett_wam_dynamics_art(const link param[NDOF + 1], Traj::joint &);

/**
 * @brief Barrett WAM inverse dynamics for control
 *
 * Newton-Euler based Inverse Dynamics taken from SL:
 * shared/barrett/math/InvDynNE_declare.h
 * shared/barrett/math/InvDynNE_math.h
 * shared/barrett/math/InvDynNE_functions.h
 *
 * These are called from shared/barrett/src/SL_dynamics.c
 *
 * @param Q joint positions,velocities and acc. given, u updated
 */
void barrett_wam_inv_dynamics_ne(const link param[NDOF + 1], Traj::joint &);

/** @brief Barrett WAM inv. dyn. for autodiff (ADOLC library)*/
template<class type> void inv_dyn_ne_auto(const type *link_param,
                                        const vec7 & q,
                                        const vec7 & qd,
                                        const vec7 & qdd,
                                        type *u);
/** @brief Barrett WAM forward dynamics for autodiff (ADOLC library)*/
template<class type>
void barrett_wam_dynamics_art_auto(const link link_param[NDOF+1],
                                  type* x,
                                  type* qdd); // for autodiff

// autodiff features

/** @brief Initialize ADOLC operations for computing forward dynamics diff */
void generate_forw_dyn_tape(const int m,
                           const int n,
                           const link link_param[NDOF+1],
                           const vec & x);
/** @brief Initialize ADOLC operations */
void generate_inv_dyn_tape(const int m,
                            const int n,
                            const Traj::joint & Q);
/**
 * @brief Autodiff jacobian calculator that supports Armadillo matrix for Jacobian.
 *
 * Overloads the large_jacobian() function of ADOL-C library.
 */
void auto_jacobian(const int & tag, vec & x, mat & jact);

// evolving wam dynamics in simulation

/**
 * @brief Evolve dynamics while tracking reference trajectory in joint space.
 *
 * Using symplectic euler to evolve the system.
 *
 */
void evolve(f_dyn forward,
            const link param[NDOF + 1],
            const Traj::traj & ref,
            const mat & u_ff,
            const mat & fb,
            const Traj::joint & qinit,
            Traj::traj & traj_act);

/**
 * @brief Evolve dynamics while tracking reference trajectory in joint space.
 *
 * Using symplectic euler to evolve the system.
 * Overloaded version of evolve() where we have now time varying fb matrices,
 * as in LQR.
 *
 */
void evolve(f_dyn forward,
            const link link_param[NDOF + 1], // overloaded evolve for LQR fb
            const Traj::traj & ref,
            const mat & u_ff,
            const cube & fb,
            const Traj::joint & qinit,
            Traj::traj & traj_act);

}

#endif /* INCLUDE_DYNAMICS_H_ */
