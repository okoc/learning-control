/**
 * @file filters.h
 *
 * @brief Butterworth filters and its zero phase version is included here.
 *
 *  Created on: Aug 14, 2017
 *      Author: okoc
 */

#ifndef INCLUDE_FILTERS_H_
#define INCLUDE_FILTERS_H_

using arma::mat;
using arma::vec;

/**
 * @brief Zero phase filtering with 2nd order butterworth filter.
 *
 * Applying the same filter to each row of the input matrix.
 *
 * Note: the non-causal filter does NOT have real-time constraints!
 * so efficiency was not the issue in the implementation.
 *
 * @param signals Input matrix with multiple rows to be filtered independently
 * @param cutoff Normalized cutoff percentage (0 = just mean, 100 = keep signal)
 * @return Filtered signals
 */
void filtfilt2(const mat & signals, const unsigned cutoff, mat & out);
void filtfilt2(const vec & signal, const unsigned cutoff, vec & out);

#endif /* INCLUDE_FILTERS_H_ */
