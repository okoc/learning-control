/**
 * @file ilc.h
 *
 * @brief The main ILC class (model based and simple PD-type) is located here.
 *
 *  Created on: Aug 22, 2017
 *      Author: okoc
 */

#ifndef INCLUDE_ILC_H_
#define INCLUDE_ILC_H_

#include <boost/property_tree/ptree.hpp>
#include <armadillo>
#include "traj.h"
#include "dynamics.h"
#include "ilc.h"
#include "adaptation.h"

const std::string home = std::getenv("HOME");
const std::string config_file = home + "/learning-control/" + "ilc.cfg";

using arma::vec;
using arma::mat;
using arma::cube;
namespace pt = boost::property_tree;

/**
 * @brief ILC options loaded from a config file.
 */
struct ilc_options {

    // model settings
    bool verbose = false; //!< verbosity derived from experiment class
    bool use_fb = true; //!< for batch model based or model free ILC lqr is an option
    bool use_model = true; //!< model based ILC or simple PD ILC
    bool recursive = true; //!< recursive model based ILC vs. batch lifted form
    bool adaptive = false; //!< adapt LTV matrices based on last trial errors
    bool cautious = true; //!< incorporate caution using covariances of LTV model matrices
    double R_weight = 0.05; //!< scaling of R, the regularization matrix
    double step_len = 0.1; //!< step length used in batch form of ILC
    double init_ltv_precision = 1e4; //!< initial precision used in LBR
    vec2 pd = { 0.1, 0.1 }; //!< PD values used in model-free ILC
    std::vector<bool> mask = { 0, 0, 0, 0, 0, 0, 0 }; //!< masks certain joints
};

/**
 * @brief ILC class for performing Learning Control updates.
 */
class ILC {

private:

    ilc_options options; //!< adjustable settings loaded/saved to/from file
    int idx_update = 0; //!< number of updates so far

    // model matrices and weighting used for LQR and ILC
    LTV ltv; //!< Stores time-varying model matrices and their uncertainties
    adapt::Adapt adapt; //!< Adaptation laws for ILC contained in this class
    cube P = zeros<cube>(1, 1, 1); //!< Ricatti matrices
    cube Q = zeros<cube>(1, 1, 1); //!< (varying) weight matrix for LQR
    mat R = zeros<mat>(1, 1); //!< (constant) penalty matrix for LQR


    mat F = zeros<mat>(1, 1); //!< lifted model matrix for batch form of ILC
    Traj::traj ref; //!< reference trajectory to be tracked

    /**
     * @brief Load ILC parameters and settings from config file.
     * @param config_file
     */
    void load(const std::string & config_file);

    /**
     * @brief Masks joint updates if corresponding options.mask flag is TRUE.
     *
     * Masking means that that joint's uff is not updated. In this case
     * it is reversed.
     * @param uff_last
     */
    void mask_update(const mat & uff_last);

public:

    cube K = zeros<cube>(1, 1, 1); //!< LQR matrices for fixed traj.
    mat uff; //!< total feedforward inputs

    /**
     * @brief ILC initialization for experiments with given config file.
     *
     * If either model-based updates are used or LQR is to be used along
     * (possibly model-free) ILC updates, then computes (discrete) linearized models.
     * Also prepares their variances and lifts them if batch model-based update is to be used.
     */
    ILC(dyn::f_dyn forward,
        dyn::link link_param[NDOF + 1],
        const Traj::traj & ref,
        const std::string & config_file = "ilc.cfg",
        const bool verbose = false);
    /** @brief ILC initialization with specified options. */
    ILC(dyn::f_dyn forward,
        dyn::link link_param[NDOF + 1],
        const Traj::traj & ref,
        const ilc_options & i_opt,
        const adapt::adapt_options & a_opt,
        const bool verbose = false);

    /**
     * @brief In case initial condition assumption is violated, adapt trajectory
     * @param qdes
     * @param idx
     */
    void update_ref(const Traj::joint & qdes, const int idx);

    /**
     * @brief ILC update law is applied here to update feedforward inputs uff
     *
     * IF model based update is turned on (options.use_model) then it calls model based update
     */
    void update(const std::vector<Traj::traj> & joint_data);

    /**
     * @brief If ILC is also responsible for adding computed feedback internally
     *
     */
    bool add_feedback() const;

    /**
     * @brief Dumping parameters loaded from a config file and the address of saved joints
     * @param tree Boost property tree instance
     */
    void dump(pt::ptree & tree) const;

};

/**
 * @brief Incremental pseudo-inverse form of model based ILC
 *
 * Running disturbance rejection LQR on the errors
 *
 * @param Ad    LTV drift matrices
 * @param Bd    LTV control matrices
 * @param P     Ricatti matrices
 * @param Q     Weighting matrices
 * @param R     Regularizer matrices
 * @param e     Error along fixed trajectory
 * @param uff   Feedforward control input compensations
 */
void recursive_ilc(const cube & Ad,
                    const cube & Bd,
                    const cube & P,
                    const cube & Q,
                    const mat & R,
                    const mat & e,
                    mat & uff);

/**
 * @brief Recursive Cautious Model-Based ILC
 *
 * Adjusts feedback as well as feedforward commands for next iteration.
 *
 */
void recursive_cautious_ilc(const LTV & ltv,
                            const cube & Q,
                            const mat & R,
                            const mat & e,
                            cube & K,
                            mat & uff,
                            mat & ufb);

/**
 * @brief Batch model based ILC using direct lifted form.
 *
 * Model-based norm-optimal batch ILC, using direct lifted-vector form.
 * The implementation uses pseudoinverse with const set tolerance.
 *
 * Only applying ILC on the striking segment of the whole reference trajectory.
 * The trajectory is downsampled and the corrections are made in downsampled space.
 * The total feedforward commands are then upsampled.
 *
 * TODO: we're not adding current iteration ILC which stabilizes this form.
 *
 * @param err Errors along trajectory (positions and velocities)
 * @param F The nominal linearized (downsampled) dynamics matrix
 * @param downsample The amount of downsampling along traj
 * @param uff Feedforward control inputs
 */
void batch_ilc(const mat & err,
                const mat & F,
                const int downsample,
                const double step_len,
                mat & uff);

/**
 * @brief Estimate the covariance matrices used in cautious ILC
 *
 * Adds to the Phi, Psi, M matrices the regularization terms
 * that come from the covariance of A,B matrices.
 *
 * Phi,Psi,M matrices are used in the Ricatti equation calculations.
 *
 * @param Sigma
 * @param P
 * @param Phi
 * @param Psi
 * @param M
 */
void estimate_cautious_covar(const mat & Sigma,
                             const mat & P,
                             mat & Phi,
                             mat & Psi,
                             mat & M);

#endif /* INCLUDE_ILC_H_ */
