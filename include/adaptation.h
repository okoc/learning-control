/*
 * adaptation.h
 *
 *  Created on: Feb 16, 2018
 *      Author: okoc
 */

#ifndef INCLUDE_ADAPTATION_H_
#define INCLUDE_ADAPTATION_H_

#include <boost/property_tree/ptree.hpp>
#include "dynamics.h"

namespace pt = boost::property_tree;

using arma::cube;
using arma::zeros;

/**
 * @brief Linear Time Varying matrices and their variances/precisions used in learning control.
 *
 * It can hold discrete or continuous matrices and their covariances/precisions.
 */
struct LTV {

    bool discrete = true;
    cube A = zeros<cube>(1,1,1); //!< mean values of x-derivative of f(x,u) (discr/cts.)
    cube B = zeros<cube>(1,1,1); //!< mean values of u-derivative of f(x,u) (discr/cts.)

    cube Sigmas = zeros<cube>(1,1,1); //!< cross-variance of A and B (discr/cts.)
    cube Gammas = zeros<cube>(1,1,1); //!< cross-precision of A and B (discr/cts.)

    /** @brief Default constructor needed for ILC class */
    LTV() {};

    /**
     * @brief Initialize LTV models.
     * @param m: dimension of control input
     * @param n: dimension of state
     * @param k: dimension of output (possibly subset of state)
     * @param N: size of LTV horizon
     */
    LTV(int m, int n, int k, int N);

    /**
     * @brief Initialize covariance and precision mats using a diagonal matrix.
     *
     * Scalar times identity is used to initialize all precision and cov. mats.
     * @param init_precision
     */
    void init_cov(const double & init_precision);

    /**
     * @brief Downsample LTV models conveniently.
     * @param down
     * @param ltv_down
     */
    void downsample(const int down, LTV & ltv) const;

    /**
     * @brief Upsample LTV models conveniently.
     * @param up
     * @param ltv_up
     */
    void upsample(LTV & ltv) const;

    /**
     * @brief If discrete flag is false, then discretize
     *
     * Discretize from qdd = f(q,qdot,u) linearized df to [q,qd] = F(q,qd,u) linearized dF
     * @param dt
     * @param ltv Discrete set of A,B matrices and their covariances
     */
    void discretize(const double & dt, LTV & ltv) const;
};

namespace adapt {

/** @brief Algorithm type used in adaptation. */
enum algo {
    DISC_LBR, //!< discrete LTV adaptation with LBR
    CTS_LBR, //!< continuous LTV adaptation with LBR
    LINK_LBR, //!< adapt link parameters with LBR
    BROYDEN //!< use Broyden's method
};

/**
 * @brief Adaptation class settings used to select/vary learning method for LTV adaptation
 */
struct adapt_options {
    bool use_window = false; //!< use sliding window
    bool trust_region_weighting = false; //!< use trust region based adaptation
    int size_window = 10; //!< sliding window length
    int downsample = 10; //!< learn only downsampled A,B mats and then linearly interp.
    double init_link_precision = 1e10; //!< factor for link precision
    double forgetting_factor = 1.0; //!< use exponential weighting for LBR
    double mc_samples = 100; //!< number of samples to approx. mean/vars of mats from links
    algo method = DISC_LBR; //!< set the method used for adaptation
};

class Adapt {

private:
    adapt_options options;
    vec link_params = zeros<vec>(10*(NDOF+1));
    mat link_precision = zeros<mat>(10*(NDOF+1),10*(NDOF+1));
    LTV ltv_cts; //!< continuous ltv models if adapt_cts = true

public:

    /**
     * @brief Initialize Adaptation class without link parameters.
     * @param config_file
     */
    Adapt(const std::string & config_file);

    /**
     * @brief Initialize Adaptation class with link nominal parameter values
     * @param config_file
     * @param link_params
     */
    Adapt(const std::string & config_file,
          dyn::link link_params[NDOF+1]);

    /**
     * @brief Initialize Adaptation class with specified options
     * @param opts Adaptation options
     * @param link_params
     */
    Adapt(const adapt_options & opt, dyn::link link_params[NDOF+1]);

    /**
     * @brief Initialize continuous model matrices and their covariances.
     *
     * Using [qd;qdd] = A*[q;qd] + B*u to init qdd = Ac*[q;qd] + Bc*u
     * Covariances are initialized using the discrete covariances and DT discretization.
     * @param A
     * @param B
     * @param Sigmas_discr
     * @param Gammas_discr
     */
    void init_cts_ltv(const cube & A,
                      const cube & B,
                      const double & discr_precision);

    /**
     * @brief Main learning function to update LTV parameters
     * @param joint_data History of recorded joint pos and velocities
     * @param ref Reference trajectory to be tracked
     * @param ltv LTV structure containing Ad,Bd mats and their variances
     */
    void learn(const std::vector<Traj::traj> & joint_data,
               const Traj::traj & ref,
               LTV & ltv);

    /**
     * @brief Load adaptation parameters and settings from config file.
     * @param config_file
     */
    void load(const std::string & config_file);

    /**
     * @brief Dump adaptation parameters
     * @param tree
     */
    void dump(pt::ptree & tree) const;
};

/**
 * @brief Adapt LTV model matrices A,B with Broyden's method
 *
 * As in solving system of nonlinear equations, we here use
 * Broyden's method to update the linearized model matrices A,B.
 * We also get covariance estimates with Broyden's method!
 *
 * @param du
 * @param de
 * @param Sigmas
 * @param A
 * @param B
 */
void est_ltv_discr_broyden(const mat & du,
                            const mat & de,
                            LTV & ltv);

/**
 * @brief Adapt LTV discrete matrices Ad and Bd with LBR.
 *
 * @param du: Difference in controls
 * @param de: Difference in q,qd errors along traj.
 * @param s2: Noise in observation model (likelihood)
 * @param lambda: Forgetting factor
 * @param downsample: Downsampling to speed up updates
 * @param LTV: ltv model matrices and their covariances
 */
void est_ltv_discr_lbr(const mat & du,
                        const mat & de,
                        const double & s2,
                        const double & lambda,
                        const int & downsample,
                        LTV & ltv);

/**
 * @brief Adapt LTV continuous matrices A and B with LBR.
 *
 * @param du: Difference in controls
 * @param de: Difference in q,qd,qdd errors along traj.
 * @param s2: Noise in observation model (likelihood)
 * @param lambda: Forgetting factor
 * @param downsample: Downsampling to speed up updates
 * @param LTV: ltv model matrices and their covariances
 */
void est_ltv_cts_lbr(const mat & du,
                        const mat & de,
                        const double & s2,
                        const double & lambda,
                        const int & downsample,
                        LTV & ltv);

/**
 * @brief Perform trust region like adaptation of weights for norm-optimal ILC
 *
 * Using step costs to adjust the weights.
 *
 * If predicted reduction in future step cost is much higher than
 * actual reduction (or even increase!) then the weights will be reduced.
 *
 * Algorithm 4.1 taken from Nocedal's book on Numerical Optimization
 * is adapted here. The major difference is that the candidate step cannot
 * actually be evaluated in reinforcement learning.
 *
 * [q_min,q_max]: overall bounds on the weights
 *
 * TODO: does this ever help?
 *
 */
void trust_region_weighting(const cube & A,
                            const cube & B,
                            const cube & K,
                            const mat & uff,
                            const vec & e0,
                            const mat & e_cur,
                            const mat & e_last,
                            cube & Q);

/**
 * @brief Linear Bayes Regression (LBR) that updates mean and precision mat.
 *
 * Instead of updating the covariance, directly updates the precision mat.
 *
 * TODO: there are no hyperparameters a,b!
 * @param X
 * @param y
 * @param s2
 * @param Gamma
 * @param mu
 */
void linear_bayes_regression(const mat & Xmat,
                            const vec & y,
                            const double & s2,
                            mat & Gamma,
                            vec & mu);

/**
 * @brief Form regressor matrix for system id. using autodiff
 *
 * Used for inverse dynamics based adaptive control.
 * Actual value of theta doesn't matter since inv. dyn is linear
 *
 */
void form_regressor(const Traj::traj & traj_act,
                    vec & theta,
                    mat & Xmat,
                    vec & y);

/**
 * @brief Upsampling the input matrix
 *
 * Armadillo does not support extrapolation (linear) so appending one element
 * in the beginning of the downsampled signal by reflecting the 2nd element around the first
 *
 * @param len Upsample total size
 * @param Downsampled signal
 * @param Upsampled signal
 */
void upsample(const mat & M_down, mat & M_up);

/**
 * @brief Upsample a 3D-array (seen as column-stacked vectorized mats).
 * @param C_down
 * @param C_up
 */
void upsample(const cube & C_down, cube & C_up);

/**
 * @brief Compute mean & variance of dynamics derivatives around a reference.
 *
 * Using ADOL-C library to do automatic differentiation.
 * Downsampling is critical to reduce run-time since using Monte Carlo sampling!
 *
 */
void linearize_dynamics(const dyn::link link_param[NDOF + 1],
                        const mat & Sigma_link,
                        const Traj::traj & ref,
                        const int N_sample,
                        const int downsample,
                        LTV & ltv);

}

#endif /* INCLUDE_ADAPTATION_H_ */
