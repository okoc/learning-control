/**
 * @file experiment.h
 *
 * @brief Includes an experiment class that orchestrates Learning Control experiments.
 *
 *  Created on: Feb 21, 2018
 *      Author: okoc
 */

#ifndef INCLUDE_EXPERIMENT_H_
#define INCLUDE_EXPERIMENT_H_

#include <boost/property_tree/ptree.hpp>
#include <armadillo>
#include "traj.h"
#include "dynamics.h"
#include "ilc.h"
#include "tabletennis.h"

namespace pt = boost::property_tree;

/**
 * @brief Experiment options loaded from a config file.
 */
struct experiment_options {
    bool lookup = true; //!< look up or optimize trajectory (online)
    bool save_joint_data = false; //!< dump all trajectory data before deconstruction
    bool detach_ilc = false; //!< if true then detach ILC
    bool verbose = true; //!< if turned on print RMS of actual traj
    bool check_torque = true; //!< check torque limits after ILC update
    bool turn_on_filter = false; // zero-phase filter
    bool filter_position = false; //!< filter position to calculate velocity
    int random_seed = 3; //!< for random traj generation
    int max_num_iter = 0; //!< max number of ilc iterations
    double cut_traj = 1.0; //!< cut part of strike
    double relax_traj = 1.0; //!< slow down traj. if value < 1
    double moving_threshold = 1e-3; //!< move if rest pose error < threshold
    double time2return = 1.0; //!< time to return after strike to rest posture
    double cutoff_freq = 50; //!< zero-phase filter cutoff frequency
    std::string link_param_file; //!< file to load dynamics model parameters
    std::string save_file_name; //!< save file with time stamp for dumping joint data
};

/**
 * @brief All class specific options can be fed to a suitable constructor.
 *
 * Useful for unit testing.
 */
struct all_options {
    experiment_options e_opt;
    ilc_options i_opt;
    adapt::adapt_options a_opt;
};

/**
 * @brief Table tennis task requirements and evaluations are stored here.
 *
 */
struct task {

    player::TableTennis tt = player::TableTennis(false,false); //!< Table tennis class for prediction

    /** Initialize Table tennis ballgun and set ball init state */
    task();

    /** @return initial task state */
    arma::vec6 get_state();

    /**
     * @brief Evaluate rollout for task
     * @return String printing LAND if ball lands legally
     */
    std::string evaluate_performance(Traj::traj & act);
};

/**
 * @brief Main class for initializing learning control experiments.
 */
class Experiment {

private:

    task tt = task(); //!< Table tennis task for evaluating performance
    Traj::spline_params poly;
    bool load_config_file = true; //!< true if constructor with all_options not called
    bool track_traj = false; //!< if turned on we can apply next update
    experiment_options options; //!< options that are loaded/saved to config file
    int idx_ilc_iter = 0; //!< ilc iterations performed already
    bool fresh = false; //!< if traj is prepared but no learning performed
    bool moving = false; //!< if robot is executing reference traj.
    Traj::joint qrest; //!< rest posture needed in SL
    arma::mat pd = arma::zeros<arma::mat>(NDOF,2*NDOF); //!< pd values for returning polynomial
    ILC* ilc = nullptr; //!< Iterative Learning Control updates are done here

    std::vector<Traj::traj> joint_data; //!< saving actual trajectory data
    dyn::f_dyn f_forw; //!< pointer to forward dynamics model
    dyn::f_dyn f_inv; //!< pointer to inverse dynamics model

    /** @brief Prepare reference trajectory and also ILC for that ref. */
    void prepare(const arma::vec7 & qact, const all_options & opts);
    /** @brief Prepare reference trajectory and also ILC for that ref. */
    void prepare(const arma::vec7 & qact);

    /**
     * @brief Load experimental parameters and settings from config file.
     * @param config_file
     */
    void load(const std::string & config_file);

    /** @brief Dumps saved joint data values as well as loaded configuration parameters. */
    void save_data();

    /**
     * @brief Run ILC after possibly filtering the actual trajectory.
     * @param traj_act
     */
    void run_ilc(Traj::traj & traj_act);

public:

    Traj::traj strike; //!< Reference trajectories
    dyn::link link_param[NDOF + 1]; //!< link parameters for dynamics model

    /**
     * @brief Initializes experiments based on given configurations.
     *
     * Settings are used without loading config file,
     * generates random reference from a lookup table
     * and then initializes ILC.
     */
    Experiment(const arma::vec7 & qact,
                const all_options & opts,
                dyn::f_dyn forw, dyn::f_dyn inv);

    /**
     * @brief Class constructor for initializing and running ILC experiments.
     *
     * Loads settings from a file, generates random reference from a lookup table
     * and then initializes ILC.
     */
    Experiment(const arma::vec7 & qact, dyn::f_dyn forw, dyn::f_dyn inv);

    /**
     * @brief Deconstructor for the Experiment class.
     * Frees the pointer to ILC class.
     */
    ~Experiment();

    /**
     * @brief Calls ILC update.
     *
     * Only updates if number of ILC experiments is less than max.
     * TODO: here we can also call system identification.
     */
    void update(Traj::traj & traj_act);

    /**
     * @brief Return to rest posture using a line segment in joint space and stay there.
     */
    void return_to_rest(const Traj::joint & qact,
                        const int idx_return,
                        Traj::joint & qdes);

    /**
     * @brief Perform a rollout of ILC experiments.
     * @param qact Actual trajectory pos, vel, acc and total inputs (for storing).
     * @param qdes Desired trajectory pos, vel, acc and total inputs to be applied.
     */
    void rollout(Traj::joint & qact, Traj::joint & qdes);

    /**
     * @brief Simulate the actual robot experiment using 'actual' dynamics model
     *
     * Taking time varying feedback matrices.
     */
    double simulate(dyn::f_dyn,
                    const dyn::link link_param_act[NDOF + 1],
                    const Traj::joint & qinit,
                    const arma::cube & K,
                    Traj::traj & traj_act) const;
    /** Taking a constant feedback matrix. @see simulate */
    double simulate(dyn::f_dyn,
                    const dyn::link link_param_act[NDOF + 1],
                    const Traj::joint & qinit,
                    const arma::mat & fb,
                    Traj::traj & traj_act) const;
    /** Directly applying (internal) ILC updated feedback */
    double simulate(dyn::f_dyn,
                    const dyn::link link_param_act[NDOF + 1],
                    const Traj::joint & qinit,
                    Traj::traj & traj_act) const;

    /**
     * @brief Detach is private so setting it here in public method
     *
     * For unit tests, detach should be set to false.
     */
    void set_detach(bool detach);

    /**
     * @brief If ILC updates are feasible, e.g. torques are within bounds,
     * return TRUE.
     *
     * If the updates are terminated (if detached) AND torques are within bounds
     * then the next ILC iteration can be attempted.
     *
     */
    bool ready(const Traj::joint & qact) const;

    /**
     * @brief If ILC updates are feasible AND actual resting posture is close to des. resting posture
     * return TRUE.
     *
     * If the updates are terminated (if detached)
     * AND torques are withing bounds
     * AND resting posture is close to desired
     * then the next ILC iteration can be attempted.
     *
     */
    bool ready() const;

    /** @brief Returns the maximum number of ILC iterations. */
    int get_num_exp() const;

    /** @brief Returns length of strike and return traj. */
    void get_len_traj(int & n_strike, int & n_return) const;

    /**
     * @brief Dumping parameters loaded from a config file and the address of saved joints
     * @param tree Boost property tree instance
     */
    void dump(pt::ptree & tree) const;

};

/**
 * @brief Check for difference between actual posture and desired rest posture.
 * @param qact Actual posture
 * @param qdes Desired resting posture
 * @param thresh Threshold for moving
 * @return True if less than threshold
 */
bool posture_is_stable(const Traj::joint & qact,
                       const Traj::joint & qrest,
                       const double & thresh);

#endif /* INCLUDE_EXPERIMENT_H_ */
