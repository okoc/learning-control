/**
 * @file sl_interface_ilc.h
 *
 * @brief Interface to SL. Exposes experiment() for ILC experiments.
 *
 */

#ifndef SL_INTERF_ILC_H
#define SL_INTERF_ILC_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief C-Interface to the robot for making ILC experiments
 * @param joint_state Actual joint states coming from sensors
 * @param joint_des_state Desired joint states commanded
 */
extern void experiment(const SL_Jstate joint_state[],
                        SL_DJstate joint_des_state[]);

/**
 * @brief Reset experiment and start a new one given current posture
 * @param joint_state current posture used as the desired resting
 * posture for the hitting movement.
 */
extern void reset_experiments(const SL_Jstate joint_state[]);

#ifdef __cplusplus
} // extern "C"
#endif

/*
#ifdef __cplusplus

// internal C++ functions

#endif
*/

#endif /* SL_INTERF_ILC_H */
