#!/bin/sh
# Shell script to transform the MATLAB forward/inverse dynamics files back to C++
#
# Funny thing is, the MATLAB files were transformed before from C! (old SL files)

# make sure all comments are //

cmd='s:%://:g' #change comment
cmd="$cmd; s:power:pow:g" #change power to pow
#cmd="$cmd; s:.m :.mass:g" 
#cmd="$cmd; s:m*:.mass*:g" 
#cmd="$cmd; s:.m;:.mass;:g"
# transform global structures
cmd="$cmd; s:link0:link_param[0]:g"
cmd="$cmd; s:eff(1):endeff_param:g"
for i in 1 2 3
do 
  cmd="$cmd; s:uex0.f:force_ex_param.base_force:g"
  cmd="$cmd; s:uex0.t:force_ex_param.base_torque:g"
  cmd="$cmd; s:basec:base_param:g"
  cmd="$cmd; s:baseo:base_param:g"
  for j in 1 2 3 4 5 6 7
  do
  	cmd="$cmd; s:uex($j).f($i):force_ex_param.joint_force($j,$i):g"
	cmd="$cmd; s:uex($j).t($i):force_ex_param.joint_torque($j,$i):g"
	done
done

for i in 1 2 3 4 5 6 7
do
  cmd="$cmd; s:links($i):link_param[$i]:g"
done

for i in 1 2 3 4 5 6 7 #change indices back to 0-indexing
do 
  k=$((i-1))
	cmd="$cmd; s:($i):($k):g"
	for j in 1 2 3 4 5 6 7
  do 
		l=$((j-1))
		cmd="$cmd; s:($i,$j):($k,$l):g"
  done
done

echo $cmd
sed "$cmd" <~/traj-gen-and-tracking/Dynamics/barrettWamInvDynamicsNE.m >~/learning-control/barrettWamInvDynamicsNE.txt


#sed 's:%://:g
#s:(1):(0):g' <example.txt >output.txt



