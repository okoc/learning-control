# Use SED to transform the link parameters structure in MATLAB 
# to a vector
# so that we can use autodiff features

cmd='s:barrett_wam_inv_dynamics_ne(const links link_param\[NDOF+1\]:barrett_wam_inv_dyn_ne_auto(const double \*link_param:g'
declare -r num_param=10
for i in 0 1 2 3 4 5 6 7
do
	idx=$((i))*num_param
	idx0=$(($idx+0))
	cmd="$cmd; s:link_param\[$i\].mass\*:link_param\[$idx0\]\*:g"
	cmd="$cmd; s:link_param\[$i\].mass +:link_param\[$idx0\] +:g"
	for j in 0 1 2
	do	
		val=$(($idx+$j+1))
		cmd="$cmd; s:link_param\[$i\].mcm($j):link_param\[$val\]:g"
	done

	idx5=$(($idx+4))
	idx6=$(($idx+5))
	idx7=$(($idx+6))
	idx8=$(($idx+7))
	idx9=$(($idx+8))
	idx10=$(($idx+9))
	cmd="$cmd; s:link_param\[$i\].inertia(0,0):link_param\[$idx5\]:g"
	cmd="$cmd; s:link_param\[$i\].inertia(0,1):link_param\[$idx6\]:g"
	cmd="$cmd; s:link_param\[$i\].inertia(0,2):link_param\[$idx7\]:g"
	cmd="$cmd; s:link_param\[$i\].inertia(1,1):link_param\[$idx8\]:g"
	cmd="$cmd; s:link_param\[$i\].inertia(1,2):link_param\[$idx9\]:g"
	cmd="$cmd; s:link_param\[$i\].inertia(2,2):link_param\[$idx10\]:g"	
done

for i in 0 1 2 3 4 5
do
	for j in 0 1 2 3 4 5 6 7 8
	do
		cmd="$cmd; s:fnet$j($i):fnet$j\[$i\]:g"
		cmd="$cmd; s:f$j($i):f$j\[$i\]:g"
	done
done

for j in 0 1 2 3 4 5 6 7 8
do 
	cmd="$cmd; s:vec6 fnet$j:type fnet$j[6]:g"
	cmd="$cmd; s:vec6 f$j:type f$j[6]:g"
done

echo $cmd
sed "$cmd" <~/learning-control/src/inverse_dynamics.cpp >~/learning-control/barrettWamInvDynamicsAuto.txt
