# Use SED to transform forward dynamics to autodiff form with ADOL-C

cmd='s:barrett_wam_dynamics_art(const link link_param\[NDOF+1\], joint \& Q):barrett_wam_dynamics_art(const link link_param[NDOF+1], const type\* x, type\* qdd):g'
cmd="$cmd; s:static double :static type\*:g"
cmd="$cmd;
echo $cmd
sed "$cmd" <~/learning-control/src/forward_dynamics.cpp >~/learning-control/barrettWamForwDynamicsAuto.txt
