# Use SED to transform the link parameters structure in MATLAB 
# to a vector
# so that we can use autodiff features

cmd='s:barrettWamInvDynamicsNE(q,qd,qdd,PAR):barrettWamInvDynamicsNEAuto(q,qd,qdd,link_vec,PAR):g'
declare -r num_param=10
for i in 1 2 3 4 5 6 7
do
	idx=$((i-1))*num_param
	idx1=$(($idx+1))
	cmd="$cmd; s:links($i).m\*:link_vec($idx1)\*:g"
	cmd="$cmd; s:links($i).m +:link_vec($idx1) +:g"
	for j in 1 2 3
	do	
		val=$(($idx+$j+1))
		cmd="$cmd; s:links($i).mcm($j):link_vec($val):g"
	done

	idx5=$(($idx+5))
	idx6=$(($idx+6))
	idx7=$(($idx+7))
	idx8=$(($idx+8))
	idx9=$(($idx+9))
	idx10=$(($idx+10))
	cmd="$cmd; s:links($i).inertia(1,1):link_vec($idx5):g"
	cmd="$cmd; s:links($i).inertia(1,2):link_vec($idx6):g" # they dont appear in forw. dyn
	#cmd="$cmd; s:links($i).inertia(2,1):link_vec($idx6):g"
	cmd="$cmd; s:links($i).inertia(1,3):link_vec($idx7):g"
	#cmd="$cmd; s:links($i).inertia(3,1):link_vec($idx+7):g"
	cmd="$cmd; s:links($i).inertia(2,2):link_vec($idx8):g"
	cmd="$cmd; s:links($i).inertia(2,3):link_vec($idx9):g"
	#cmd="$cmd; s:links($i).inertia(3,2):link_vec($idx+9):g"
	cmd="$cmd; s:links($i).inertia(3,3):link_vec($idx10):g"	
done

echo $cmd
sed "$cmd" <~/learning-control/matlab/wam/barrettWamInvDynamicsNE.m >~/learning-control/barrettWamInvDynamicsNE.txt
