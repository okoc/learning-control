/*
 * ex_comm_adolc_sl.h
 *
 *  Created on: Apr 1, 2018
 *      Author: okoc
 */

#ifndef EXAMPLES_INCLUDE_EX_COMM_ADOLC_SL_H_
#define EXAMPLES_INCLUDE_EX_COMM_ADOLC_SL_H_

#ifdef __cplusplus
extern "C" {
#endif

// Interface for Player
extern void calc_autodiff();

#ifdef __cplusplus
} // extern "C"
#endif

/*
#ifdef __cplusplus

// internal C++ functions

#endif
*/



#endif /* EXAMPLES_INCLUDE_EX_COMM_ADOLC_SL_H_ */
