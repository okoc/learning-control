/*
 * ex_auto_diff.cpp
 *
 * Example testing autodiff of inverse dynamics using ADOLC library
 *
 *  Created on: Mar 22, 2018
 *      Author: okoc
 */

#include <adolc/adouble.h>            // use of active doubles
#include <adolc/drivers/drivers.h>    // use of "Easy to Use" drivers
#include <adolc/taping.h>             // use of taping
#include <armadillo>
#include "stdio.h"
#include "math.h"

using namespace arma;

#define TAG_JAC 2
const int m = 3;
const int n = 2;

typedef void (*fun)(const unsigned m,
                    const unsigned n,
                    const adouble *x,
                    adouble *result);

void generate_tape(fun f, int m, int n);
template<class type>
void myfnc(const unsigned m,
           const unsigned n,
           const type *x,
           type *result);
void auto_jacobian(const vec & x, mat & jact);
void calc_raw_jac(fun f, const double x[], const int m, const int n);
void calc_arma_jac(fun f, const vec & x, const int m, const int n);

int main() {

    fun f = myfnc;
    double x[n];

    cout << "Calculating raw jacobian...\n";
    calc_raw_jac(f,x,m,n);

    cout << "Calculating jacobian using ARMADILLO matrix...\n";
    vec2 x_ = {0.5, 1.5};
    calc_arma_jac(f,x_,m,n);

    return 1;
}

void calc_raw_jac(fun f, const double x[], const int m, const int n) {

    double **jac = new double*[m];
    for (int i = 0; i < m; i++)
        jac[i] = new double[n];

    generate_tape(f,m,n);

    // evaluate
    jacobian(TAG_JAC,m,n,x,jac);

    cout.precision(4);
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            std::cout << jac[i][j] << "\t";
        }
        std::cout << "\n";
    }

    for (int i = 0; i < m; i++)
        delete[] jac[i];
    delete[] jac;
}

void calc_arma_jac(fun f, const vec & x, const int m, const int n) {

    generate_tape(f,m,n);

    mat jact = zeros<mat>(n,m);
    auto_jacobian(x,jact);
    cout.precision(4);
    cout << "Jac:\n" << jact.t() << endl;
}

/**
 * @brief Autodiff jacobian calculator that supports Armadillo matrix for Jacobian.
 *
 * Overloads the jacobian() function of ADOL-C library.
 * TODO: doesn't generalize to arbitrary m!
 */
void auto_jacobian(const vec & x, mat & jact) {

    double *cjac[m] = {jact.colptr(0), jact.colptr(1), jact.colptr(2)};

    jacobian(TAG_JAC,m,n,x.memptr(),cjac);
}

/*
 * Initialize ADOLC operations
 */
void generate_tape(fun f, int m, int n) {

    adouble *x_auto = new adouble[n];
    adouble *g_auto = new adouble[m];
    double *x = new double[n];
    double dummy;

    trace_on(TAG_JAC);
    for (int i = 0; i < n; i++)
        x_auto[i] <<= x[i];

    f(m,n,x_auto,g_auto);

    for(int i = 0; i < m; i++)
        g_auto[i] >>= dummy;
    trace_off();

    delete[] x_auto;
    delete[] x;
    delete[] g_auto;


}

/*
 * Vector function template
 */
template<class type>
void myfnc(const unsigned m,
           const unsigned n,
           const type *x,
           type *result) {

    result[0] = 1 - x[0]*x[0]*x[1];
    result[1] = x[1]*x[0];
    result[2] = x[0] - x[1];
}
