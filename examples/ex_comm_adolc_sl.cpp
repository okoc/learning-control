/*
 * ex_comm_adolc_sl.cpp
 *
 *  Created on: Apr 1, 2018
 *      Author: okoc
 */

#include <adolc/adouble.h>            // use of active doubles
#include <adolc/drivers/drivers.h>    // use of "Easy to Use" drivers
#include <adolc/taping.h>             // use of taping
#include <armadillo>
#include "stdio.h"
#include "math.h"
#include "ex_comm_adolc_sl.h"

using namespace arma;

#define TAG_GRAD 1
#define TAG_JAC 2
const int m = 3;
const int n = 2;

typedef void (*fun)(const unsigned m,
                    const unsigned n,
                    const adouble *x,
                    adouble *result);

template<class type>
void myfnc(const unsigned m,
           const unsigned n,
           const type *x,
           type *result);

static void calc_raw_jac(fun f, const double x[], const int m, const int n);
static void calc_raw_grad();
static void generate_tape(fun f, int m, int n);

void calc_autodiff() {

    fun f = myfnc;
    double x[n] = {0.0};

    cout << "Calculating gradient...\n";
    calc_raw_grad();

    //cout << "Calculating raw jacobian...\n";
    //calc_raw_jac(f,x,m,n);

}

static void calc_raw_grad() {

    double xp[n]; // = new double[n];
    double yp = 0.0;
    adouble *x = new adouble[n];
    adouble y = 1;
    for (int i = 0; i < n; i++)
        xp[i] = (i+1.0)/(2.0+i);
    trace_on(TAG_GRAD);
    for (int i = 0; i < n; i++) {
        x[i] <<= xp[i];
        y *= x[i];
    }
    y >>= yp;
    delete[] x;
    trace_off();
    double* grad = new double[n];
    gradient(1,n,xp,grad);

    cout.precision(4);
    for (int i = 0; i < n; i++) {
            std::cout << grad[i] << "\t";
        }
        std::cout << "\n";
    delete[] grad;

}

static void calc_raw_jac(fun f, const double x[], const int m, const int n) {

    double **jac = new double*[m];
    for (int i = 0; i < m; i++)
        jac[i] = new double[n];

    generate_tape(f,m,n);

    // evaluate
    jacobian(TAG_JAC,m,n,x,jac);

    cout.precision(4);
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            std::cout << jac[i][j] << "\t";
        }
        std::cout << "\n";
    }

    for (int i = 0; i < m; i++)
        delete[] jac[i];
    delete[] jac;
}

/*
 * Initialize ADOLC operations
 */
static void generate_tape(fun f, int m, int n) {

    adouble *x_auto = new adouble[n];
    adouble *g_auto = new adouble[m];
    double *x = new double[n];
    double dummy;

    trace_on(TAG_JAC);
    for (int i = 0; i < n; i++)
        x_auto[i] <<= x[i];

    f(m,n,x_auto,g_auto);

    for(int i = 0; i < m; i++)
        g_auto[i] >>= dummy;
    trace_off();

    delete[] x_auto;
    delete[] x;
    delete[] g_auto;


}

/*
 * Vector function template
 */
template<class type>
void myfnc(const unsigned m,
           const unsigned n,
           const type *x,
           type *result) {

    result[0] = 1 - x[0]*x[0]*x[1];
    result[1] = x[1]*x[0];
    result[2] = x[0] - x[1];
}
