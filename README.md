# learning-control

Learning Control for Table Tennis

This repository explores applying learning control algorithms to track
Table Tennis striking movements.

We simply lookup table tennis striking (and returning) 3rd order
polynomials in joint space. Iterative Learning Control
(ILC) provides a general framework to learn to track the striking trajectory.

Inverse Dynamics based on a nominal model can be used to calculate initial torques to track these polynomials. Forward Dynamics using an 'actual' model can be used to simulate the learning performance of ILC in (unit) tests. Further instructions are found in the Doxygen generated documentation. 

# installation

For your convenience, an installation script is located in the main folder. Simply run ./install.sh
