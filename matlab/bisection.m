%% Bisection to find a x-des for f-des
%
% Used to find the right amount of perturbation
% that has the required matrix 2-norm
%
% f is the function that has positive values and monotonic
% tol is the tolerance of the solution in terms of f-value
%
% xmin and xmax must bracket the value!
function xbest = bisection(xmin,xmax,f,fdes,tol)

assert(f(xmin) < fdes && f(xmax) > fdes, ...
       'xmin and xmax must bracket the value');
xbest = xmin/2 + xmax/2;
fbest = f(xbest);

while abs(fbest - fdes) > tol
    
    if fbest > fdes
        xmax = xbest;
    else
        xmin = xbest;
    end
    xbest = xmin/2 + xmax/2;
    fbest = f(xbest);
end


end