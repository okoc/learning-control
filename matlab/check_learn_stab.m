% Function that tests for monotonic stability of the learning update
% that inverts the nominal plant
% Assumes knowledge of the actual plant dynamics

function [P,MC] = check_learn_stab(act,nom,L,K,traj,verbose)

    F = nom;
    Z = act;
    
    E = Z - F;
    % due to feedback
    Ks = lift_fb_mat(K);
    M = eye(size(F,1)) - Z*Ks;
    P = eye(size(F,1)) - M\(Z*L);
    mc_rad = norm(P);
    fprintf('Norm(E) = %.2f, mc_rad = %.2f\n', ...
             norm(E), mc_rad);
    
    if verbose
        if (mc_rad < 1)
            MC = true;
            disp('Model is monotonically convergent!');
        else
            disp('Model may not be monotonically convergent!');
            MC = false;
        end

        err_min_norm = norm((eye(size(F,1)) - Z*pinv(Z))*traj(:),2);
        % M = I - Z(LZ)^-1L
        v = Z*((L*Z)\(L*traj(:)));
        err_min_nom_norm = norm(traj(:) - v,2);

        fprintf('e_min[act,nom] = [%.2f,%.2f]\n', err_min_norm, err_min_nom_norm);      
        fprintf('Condition of model matrix: %f\n', cond(F));
    end

end

% Lift the feedback matrices into one big lifted matrix
function Mat = lift_fb_mat(K) 

    n = size(K,2);
    m = size(K,1);
    N = size(K,3);
    Mat = zeros(m*N,n*N);

    for i = 2:N
        vec_x = ((i-1)*m + 1):(i*m);
        vec_y = ((i-1)*n + 1):(i*n);
        Mat(vec_x,vec_y) = K(:,:,i);
    end

end