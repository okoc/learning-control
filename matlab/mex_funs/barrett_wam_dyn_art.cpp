/*
 * For unit testing with ARMADILLO random matrices
 * it is nice to compare with MATLAB code with some existing
 * test functions.
 *
 *
 */

#include <armadillo>
#include "armaMex.hpp"

#define NDOF 7

// definitions
const double ZSFE  =  0.346; //!< z height of SAA axis above ground
const double ZHR  =  0.505;  //!< length of upper arm until 4.5cm before elbow link
const double YEB  =  0.045;  //!< elbow y offset
const double ZEB  =  0.045;  //!< elbow z offset
const double YWR  = -0.045;  //!< elbow y offset (back to forewarm)
const double ZWR  =  0.045;  //!< elbow z offset (back to forearm)
const double ZWFE  =  0.255; //!< forearm length (minus 4.5cm)

const double g = 9.81; //TODO: why is this positive?

using namespace arma;

/**
 * @brief Desired/actual joint positions, velocities, accelerations and torques.
 *
 * Main Player function play() updates desired joint values
 * every 2ms for Barrett WAM.
 */
struct joint {
	vec7 q = zeros<vec>(NDOF); //!< desired/actual joint pos
	vec7 qd = zeros<vec>(NDOF); //!< desired/actual joint vels
	vec7 qdd = zeros<vec>(NDOF); //!< desired/actual joint accs
	vec7 u = zeros<vec>(NDOF); //!< calculated (desired) torques
    
    joint(vec7 & q_, vec7 & qd_, vec7 & u_) : q(q_), qd(qd_), u(u_) {};
};

/**
 * @brief Link parameters
 *
 * Normally 1 for the base and 7 for the joints: 8 link structures are needed.
 * We can load them from a file.
 */
struct link {
	double mass = 0.0; //!< mass of the link
	vec3 mcm = zeros<vec>(3); //!< mass times center of mass of link
	mat33 inertia = zeros<mat>(3,3); //!< inertia matrix corresponding to link
};

/**
 * @brief Base cartesian position and orientation (quaternion)
 */
struct base {
	vec3 x = zeros<vec>(3); //!< Cartesian position
	vec3 xd = zeros<vec>(3); //!< Cartesian velocity
	vec3 xdd = zeros<vec>(3); //!< Cartesian acceleration
	vec4 q = {0.0, 1.0, 0.0, 0.0}; //!< Orientation as quaternion
	vec3 ad = zeros<vec>(3); //!< Angular Velocity [alpha,beta,gamma]
	vec3 add = zeros<vec>(3); //!< Angular Accelerations
};

/**
 * @brief External forces and torques
 */
struct force_ex {
	vec3 base_force = zeros<vec>(3);
	vec3 base_torque = zeros<vec>(3);
	mat joint_force = zeros<mat>(7,3);
	mat joint_torque = zeros<mat>(7,3);
};

/**
 * @brief Endeffector parameters that are necessary (mass, position, ...)
 */
struct end_effector {
	double mass = 0.0; //!< mass
	vec3 mcm = zeros<vec>(3); //!< mass times center of mass
	vec3 x = {0.0, 0.0, 0.30}; //!< Cartesian position
	vec3 a = zeros<vec>(3); //!< Euler angles
};

void barrett_wam_dynamics_art(const link link_param[NDOF+1], joint & Q);
void load_link_params(const std::string & filename, link link_param[NDOF+1]);

/**
 * @brief Loads the link parameters from config file to global array of links structure.
 */
void load_link_params(const std::string & filename, link link_param[NDOF+1]) {

	using namespace std;
	string env = getenv("HOME");
	string fullname = env + "/learning-control/config/" + filename;
	mat link_mat;
	link_mat.load(fullname);
	//cout << link_mat;
	mat33 inertia;

	for (int i = 0; i < link_mat.n_rows; i++) {
		link_param[i].mass = link_mat(i,0);
		link_param[i].mcm(0) = link_mat(i,1);
		link_param[i].mcm(1) = link_mat(i,2);
		link_param[i].mcm(2) = link_mat(i,3);
		inertia(0,0) = link_mat(i,4); //element 11
		inertia(0,1) = link_mat(i,5); //12 and 21
		inertia(0,2) = link_mat(i,6);
		inertia(1,1) = link_mat(i,7);
		inertia(1,2) = link_mat(i,8);
		inertia(2,2) = link_mat(i,9);
		link_param[i].inertia = symmatu(inertia);
	}
}

/* Barrett WAM forward dynamics for evolving (simulating) system
 * trajectories
 *
 * Articulate Dynamics taken from SL:
 * shared/barrett/math/ForDynArt_declare.h
 * shared/barrett/math/ForDynArt_math.h
 * shared/barrett/math/ForDynArt_functions.h
 *
 * these are called from shared/barrett/src/SL_dynamics.c
 *
 */
void barrett_wam_dynamics_art(const link link_param[NDOF+1], joint & Q) {

static base base_param;
static force_ex force_ex_param;
static end_effector endeff_param;

// system states are X  =  (q(0),q(6),qd(0),,qd(6));
vec7 q  =  Q.q;
vec7 qd  =  Q.qd;
vec7 u = Q.u;
vec7 qdd;

static double  ss1th;
static double  cs1th;
static double  ss2th;
static double  cs2th;
static double  ss3th;
static double  cs3th;
static double  ss4th;
static double  cs4th;
static double  ss5th;
static double  cs5th;
static double  ss6th;
static double  cs6th;
static double  ss7th;
static double  cs7th;

static double  rseff1a1;
static double  rceff1a1;
static double  rseff1a2;
static double  rceff1a2;
static double  rseff1a3;
static double  rceff1a3;

static mat33 S00;
static mat33 S10;
static mat33 S21;
static mat33 S32;
static mat33 S43;
static mat33 S54;
static mat33 S65;
static mat33 S76;
static mat33 S87;

static mat33 Si00;
static mat33 Si01;
static mat33 Si12;
static mat33 Si23;
static mat33 Si34;
static mat33 Si45;
static mat33 Si56;
static mat33 Si67;
static mat33 Si78;

static mat33 SG10;
static mat33 SG20;
static mat33 SG30;
static mat33 SG40;
static mat33 SG50;
static mat33 SG60;
static mat33 SG70;
static mat33 SG80;

static vec6 v0;
static vec6 v1;
static vec6 v2;
static vec6 v3;
static vec6 v4;
static vec6 v5;
static vec6 v6;
static vec6 v7;
static vec6 v8;

static vec6 c1;
static vec6 c2;
static vec6 c3;
static vec6 c4;
static vec6 c5;
static vec6 c6;
static vec6 c7;

static vec6 pv0;
static vec6 pv1;
static vec6 pv2;
static vec6 pv3;
static vec6 pv4;
static vec6 pv5;
static vec6 pv6;
static vec6 pv7;
static vec6 pv8;

static mat66 JA0;
static mat66 JA1;
static mat66 JA2;
static mat66 JA3;
static mat66 JA4;
static mat66 JA5;
static mat66 JA6;
static mat66 JA7;
static mat66 JA8;

static vec6 h1;
static vec6 h2;
static vec6 h3;
static vec6 h4;
static vec6 h5;
static vec6 h6;
static vec6 h7;

static vec8 d;

static mat66 T101;
static mat66 T112;
static mat66 T123;
static mat66 T134;
static mat66 T145;
static mat66 T156;
static mat66 T167;
static mat66 T178;

static mat66 T01;
static mat66 T12;
static mat66 T23;
static mat66 T34;
static mat66 T45;
static mat66 T56;
static mat66 T67;
static mat66 T78;

static vec6 p0;
static vec6 p1;
static vec6 p2;
static vec6 p3;
static vec6 p4;
static vec6 p5;
static vec6 p6;
static vec6 p7;
static vec6 p8;

static vec6 pmm1;
static vec6 pmm2;
static vec6 pmm3;
static vec6 pmm4;
static vec6 pmm5;
static vec6 pmm6;
static vec6 pmm7;
static vec6 pmm8;

static vec6 pm1;
static vec6 pm2;
static vec6 pm3;
static vec6 pm4;
static vec6 pm5;
static vec6 pm6;
static vec6 pm7;
static vec6 pm8;

static vec6 a1;
static vec6 a2;
static vec6 a3;
static vec6 a4;
static vec6 a5;
static vec6 a6;
static vec6 a7;
static vec6 a8;


// sine and cosine precomputation
ss1th  =  sin(q(0));
cs1th  =  cos(q(0));
ss2th  =  sin(q(1));
cs2th  =  cos(q(1));
ss3th  =  sin(q(2));
cs3th  =  cos(q(2));
ss4th  =  sin(q(3));
cs4th  =  cos(q(3));
ss5th  =  sin(q(4));
cs5th  =  cos(q(4));
ss6th  =  sin(q(5));
cs6th  =  cos(q(5));
ss7th  =  sin(q(6));
cs7th  =  cos(q(6));

// endeffector orientations

rseff1a1  =  sin(endeff_param.a(0));
rceff1a1  =  cos(endeff_param.a(0));
rseff1a2  =  sin(endeff_param.a(1));
rceff1a2  =  cos(endeff_param.a(1));
rseff1a3  =  sin(endeff_param.a(2));
rceff1a3  =  cos(endeff_param.a(2));

// includes the functions one by one

//wam_ForDynArtfunc1

// rotation matrices
S00(0,0) = -1 + 2*pow(base_param.q(0),2) + 2*pow(base_param.q(1),2);
S00(0,1) = 2*(base_param.q(1)*base_param.q(2) + base_param.q(0)*base_param.q(3));
S00(0,2) = 2*(-(base_param.q(0)*base_param.q(2)) + base_param.q(1)*base_param.q(3));
S00(1,0) = 2*(base_param.q(1)*base_param.q(2) - base_param.q(0)*base_param.q(3));
S00(1,1) = -1 + 2*pow(base_param.q(0),2) + 2*pow(base_param.q(2),2);
S00(1,2) = 2*(base_param.q(0)*base_param.q(1) + base_param.q(2)*base_param.q(3));
S00(2,0) = 2*(base_param.q(0)*base_param.q(2) + base_param.q(1)*base_param.q(3));
S00(2,1) = 2*(-(base_param.q(0)*base_param.q(1)) + base_param.q(2)*base_param.q(3));
S00(2,2) = -1 + 2*pow(base_param.q(0),2) + 2*pow(base_param.q(3),2);

S10(0,0) = cs1th;
S10(0,1) = ss1th;
S10(1,0) = -ss1th;
S10(1,1) = cs1th;

S21(0,1) = ss2th;
S21(0,2) = cs2th;
S21(1,1) = cs2th;
S21(1,2) = -ss2th;

S32(0,1) = ss3th;
S32(0,2) = -cs3th;
S32(1,1) = cs3th;
S32(1,2) = ss3th;

S43(0,1) = ss4th;
S43(0,2) = cs4th;
S43(1,1) = cs4th;
S43(1,2) = -ss4th;

S54(0,1) = ss5th;
S54(0,2) = -cs5th;
S54(1,1) = cs5th;
S54(1,2) = ss5th;

S65(0,1) = ss6th;
S65(0,2) = cs6th;
S65(1,1) = cs6th;
S65(1,2) = -ss6th;

S76(0,1) = ss7th;
S76(0,2) = -cs7th;
S76(1,1) = cs7th;
S76(1,2) = ss7th;

S87(0,0) = rceff1a2*rceff1a3;
S87(0,1) = rceff1a3*rseff1a1*rseff1a2 + rceff1a1*rseff1a3;
S87(0,2) = -(rceff1a1*rceff1a3*rseff1a2) + rseff1a1*rseff1a3;
S87(1,0) = -(rceff1a2*rseff1a3);
S87(1,1) = rceff1a1*rceff1a3 - rseff1a1*rseff1a2*rseff1a3;
S87(1,2) = rceff1a3*rseff1a1 + rceff1a1*rseff1a2*rseff1a3;
S87(2,0) = rseff1a2;
S87(2,1) = -(rceff1a2*rseff1a1);
S87(2,2) = rceff1a1*rceff1a2;

//wam_ForDynArtfunc2

// inverse rotation matrices
Si00(0,0) = -1 + 2*pow(base_param.q(0),2) + 2*pow(base_param.q(1),2);
Si00(0,1) = 2*(base_param.q(1)*base_param.q(2) - base_param.q(0)*base_param.q(3));
Si00(0,2) = 2*(base_param.q(0)*base_param.q(2) + base_param.q(1)*base_param.q(3));
Si00(1,0) = 2*(base_param.q(1)*base_param.q(2) + base_param.q(0)*base_param.q(3));
Si00(1,1) = -1 + 2*pow(base_param.q(0),2) + 2*pow(base_param.q(2),2);
Si00(1,2) = 2*(-(base_param.q(0)*base_param.q(1)) + base_param.q(2)*base_param.q(3));
Si00(2,0) = 2*(-(base_param.q(0)*base_param.q(2)) + base_param.q(1)*base_param.q(3));
Si00(2,1) = 2*(base_param.q(0)*base_param.q(1) + base_param.q(2)*base_param.q(3));
Si00(2,2) = -1 + 2*pow(base_param.q(0),2) + 2*pow(base_param.q(3),2);

Si01(0,0) = cs1th;
Si01(0,1) = -ss1th;
Si01(1,0) = ss1th;
Si01(1,1) = cs1th;

Si12(1,0) = ss2th;
Si12(1,1) = cs2th;
Si12(2,0) = cs2th;
Si12(2,1) = -ss2th;

Si23(1,0) = ss3th;
Si23(1,1) = cs3th;
Si23(2,0) = -cs3th;
Si23(2,1) = ss3th;

Si34(1,0) = ss4th;
Si34(1,1) = cs4th;
Si34(2,0) = cs4th;
Si34(2,1) = -ss4th;

Si45(1,0) = ss5th;
Si45(1,1) = cs5th;
Si45(2,0) = -cs5th;
Si45(2,1) = ss5th;

Si56(1,0) = ss6th;
Si56(1,1) = cs6th;
Si56(2,0) = cs6th;
Si56(2,1) = -ss6th;

Si67(1,0) = ss7th;
Si67(1,1) = cs7th;
Si67(2,0) = -cs7th;
Si67(2,1) = ss7th;

Si78(0,0) = rceff1a2*rceff1a3;
Si78(0,1) = -(rceff1a2*rseff1a3);
Si78(0,2) = rseff1a2;
Si78(1,0) = rceff1a3*rseff1a1*rseff1a2 + rceff1a1*rseff1a3;
Si78(1,1) = rceff1a1*rceff1a3 - rseff1a1*rseff1a2*rseff1a3;
Si78(1,2) = -(rceff1a2*rseff1a1);
Si78(2,0) = -(rceff1a1*rceff1a3*rseff1a2) + rseff1a1*rseff1a3;
Si78(2,1) = rceff1a3*rseff1a1 + rceff1a1*rseff1a2*rseff1a3;
Si78(2,2) = rceff1a1*rceff1a2;

//wam_ForDynArtfunc3

// rotation matrices fr.massglobal to link coordinates
SG10(0,0) = S00(0,0)*S10(0,0) + S00(1,0)*S10(0,1);
SG10(0,1) = S00(0,1)*S10(0,0) + S00(1,1)*S10(0,1);
SG10(0,2) = S00(0,2)*S10(0,0) + S00(1,2)*S10(0,1);
SG10(1,0) = S00(0,0)*S10(1,0) + S00(1,0)*S10(1,1);
SG10(1,1) = S00(0,1)*S10(1,0) + S00(1,1)*S10(1,1);
SG10(1,2) = S00(0,2)*S10(1,0) + S00(1,2)*S10(1,1);
SG10(2,0) = S00(2,0);
SG10(2,1) = S00(2,1);
SG10(2,2) = S00(2,2);

SG20(0,0) = S21(0,1)*SG10(1,0) + S21(0,2)*SG10(2,0);
SG20(0,1) = S21(0,1)*SG10(1,1) + S21(0,2)*SG10(2,1);
SG20(0,2) = S21(0,1)*SG10(1,2) + S21(0,2)*SG10(2,2);
SG20(1,0) = S21(1,1)*SG10(1,0) + S21(1,2)*SG10(2,0);
SG20(1,1) = S21(1,1)*SG10(1,1) + S21(1,2)*SG10(2,1);
SG20(1,2) = S21(1,1)*SG10(1,2) + S21(1,2)*SG10(2,2);
SG20(2,0) = -SG10(0,0);
SG20(2,1) = -SG10(0,1);
SG20(2,2) = -SG10(0,2);

SG30(0,0) = S32(0,1)*SG20(1,0) + S32(0,2)*SG20(2,0);
SG30(0,1) = S32(0,1)*SG20(1,1) + S32(0,2)*SG20(2,1);
SG30(0,2) = S32(0,1)*SG20(1,2) + S32(0,2)*SG20(2,2);
SG30(1,0) = S32(1,1)*SG20(1,0) + S32(1,2)*SG20(2,0);
SG30(1,1) = S32(1,1)*SG20(1,1) + S32(1,2)*SG20(2,1);
SG30(1,2) = S32(1,1)*SG20(1,2) + S32(1,2)*SG20(2,2);
SG30(2,0) = SG20(0,0);
SG30(2,1) = SG20(0,1);
SG30(2,2) = SG20(0,2);

SG40(0,0) = S43(0,1)*SG30(1,0) + S43(0,2)*SG30(2,0);
SG40(0,1) = S43(0,1)*SG30(1,1) + S43(0,2)*SG30(2,1);
SG40(0,2) = S43(0,1)*SG30(1,2) + S43(0,2)*SG30(2,2);
SG40(1,0) = S43(1,1)*SG30(1,0) + S43(1,2)*SG30(2,0);
SG40(1,1) = S43(1,1)*SG30(1,1) + S43(1,2)*SG30(2,1);
SG40(1,2) = S43(1,1)*SG30(1,2) + S43(1,2)*SG30(2,2);
SG40(2,0) = -SG30(0,0);
SG40(2,1) = -SG30(0,1);
SG40(2,2) = -SG30(0,2);

SG50(0,0) = S54(0,1)*SG40(1,0) + S54(0,2)*SG40(2,0);
SG50(0,1) = S54(0,1)*SG40(1,1) + S54(0,2)*SG40(2,1);
SG50(0,2) = S54(0,1)*SG40(1,2) + S54(0,2)*SG40(2,2);
SG50(1,0) = S54(1,1)*SG40(1,0) + S54(1,2)*SG40(2,0);
SG50(1,1) = S54(1,1)*SG40(1,1) + S54(1,2)*SG40(2,1);
SG50(1,2) = S54(1,1)*SG40(1,2) + S54(1,2)*SG40(2,2);
SG50(2,0) = SG40(0,0);
SG50(2,1) = SG40(0,1);
SG50(2,2) = SG40(0,2);

SG60(0,0) = S65(0,1)*SG50(1,0) + S65(0,2)*SG50(2,0);
SG60(0,1) = S65(0,1)*SG50(1,1) + S65(0,2)*SG50(2,1);
SG60(0,2) = S65(0,1)*SG50(1,2) + S65(0,2)*SG50(2,2);
SG60(1,0) = S65(1,1)*SG50(1,0) + S65(1,2)*SG50(2,0);
SG60(1,1) = S65(1,1)*SG50(1,1) + S65(1,2)*SG50(2,1);
SG60(1,2) = S65(1,1)*SG50(1,2) + S65(1,2)*SG50(2,2);
SG60(2,0) = -SG50(0,0);
SG60(2,1) = -SG50(0,1);
SG60(2,2) = -SG50(0,2);

SG70(0,0) = S76(0,1)*SG60(1,0) + S76(0,2)*SG60(2,0);
SG70(0,1) = S76(0,1)*SG60(1,1) + S76(0,2)*SG60(2,1);
SG70(0,2) = S76(0,1)*SG60(1,2) + S76(0,2)*SG60(2,2);
SG70(1,0) = S76(1,1)*SG60(1,0) + S76(1,2)*SG60(2,0);
SG70(1,1) = S76(1,1)*SG60(1,1) + S76(1,2)*SG60(2,1);
SG70(1,2) = S76(1,1)*SG60(1,2) + S76(1,2)*SG60(2,2);
SG70(2,0) = SG60(0,0);
SG70(2,1) = SG60(0,1);
SG70(2,2) = SG60(0,2);

SG80(0,0) = S87(0,0)*SG70(0,0) + S87(0,1)*SG70(1,0) + S87(0,2)*SG70(2,0);
SG80(0,1) = S87(0,0)*SG70(0,1) + S87(0,1)*SG70(1,1) + S87(0,2)*SG70(2,1);
SG80(0,2) = S87(0,0)*SG70(0,2) + S87(0,1)*SG70(1,2) + S87(0,2)*SG70(2,2);
SG80(1,0) = S87(1,0)*SG70(0,0) + S87(1,1)*SG70(1,0) + S87(1,2)*SG70(2,0);
SG80(1,1) = S87(1,0)*SG70(0,1) + S87(1,1)*SG70(1,1) + S87(1,2)*SG70(2,1);
SG80(1,2) = S87(1,0)*SG70(0,2) + S87(1,1)*SG70(1,2) + S87(1,2)*SG70(2,2);
SG80(2,0) = S87(2,0)*SG70(0,0) + S87(2,1)*SG70(1,0) + S87(2,2)*SG70(2,0);
SG80(2,1) = S87(2,0)*SG70(0,1) + S87(2,1)*SG70(1,1) + S87(2,2)*SG70(2,1);
SG80(2,2) = S87(2,0)*SG70(0,2) + S87(2,1)*SG70(1,2) + S87(2,2)*SG70(2,2);

//wam_ForDynArtfunc4

// velocity vectors
v0(0) = base_param.ad(0)*S00(0,0) + base_param.ad(1)*S00(0,1) + base_param.ad(2)*S00(0,2);
v0(1) = base_param.ad(0)*S00(1,0) + base_param.ad(1)*S00(1,1) + base_param.ad(2)*S00(1,2);
v0(2) = base_param.ad(0)*S00(2,0) + base_param.ad(1)*S00(2,1) + base_param.ad(2)*S00(2,2);
v0(3) = base_param.xd(0)*S00(0,0) + base_param.xd(1)*S00(0,1) + base_param.xd(2)*S00(0,2);
v0(4) = base_param.xd(0)*S00(1,0) + base_param.xd(1)*S00(1,1) + base_param.xd(2)*S00(1,2);
v0(5) = base_param.xd(0)*S00(2,0) + base_param.xd(1)*S00(2,1) + base_param.xd(2)*S00(2,2);

v1(0) = v0(0)*S10(0,0) + v0(1)*S10(0,1);
v1(1) = v0(0)*S10(1,0) + v0(1)*S10(1,1);
v1(2) = qd(0) + v0(2);
v1(3) = ZSFE*v0(1)*S10(0,0) + v0(3)*S10(0,0) - ZSFE*v0(0)*S10(0,1) + v0(4)*S10(0,1);
v1(4) = ZSFE*v0(1)*S10(1,0) + v0(3)*S10(1,0) - ZSFE*v0(0)*S10(1,1) + v0(4)*S10(1,1);
v1(5) = v0(5);

v2(0) = v1(1)*S21(0,1) + v1(2)*S21(0,2);
v2(1) = v1(1)*S21(1,1) + v1(2)*S21(1,2);
v2(2) = qd(1) - v1(0);
v2(3) = v1(4)*S21(0,1) + v1(5)*S21(0,2);
v2(4) = v1(4)*S21(1,1) + v1(5)*S21(1,2);
v2(5) = -v1(3);

v3(0) = v2(1)*S32(0,1) + v2(2)*S32(0,2);
v3(1) = v2(1)*S32(1,1) + v2(2)*S32(1,2);
v3(2) = qd(2) + v2(0);
v3(3) = ZHR*v2(2)*S32(0,1) + v2(4)*S32(0,1) - ZHR*v2(1)*S32(0,2) + v2(5)*S32(0,2);
v3(4) = ZHR*v2(2)*S32(1,1) + v2(4)*S32(1,1) - ZHR*v2(1)*S32(1,2) + v2(5)*S32(1,2);
v3(5) = v2(3);

v4(0) = v3(1)*S43(0,1) + v3(2)*S43(0,2);
v4(1) = v3(1)*S43(1,1) + v3(2)*S43(1,2);
v4(2) = qd(3) - v3(0);
v4(3) = v3(4)*S43(0,1) + v3(5)*S43(0,2) + v3(0)*(-(ZEB*S43(0,1)) + YEB*S43(0,2));
v4(4) = v3(4)*S43(1,1) + v3(5)*S43(1,2) + v3(0)*(-(ZEB*S43(1,1)) + YEB*S43(1,2));
v4(5) = -(ZEB*v3(1)) + YEB*v3(2) - v3(3);

v5(0) = v4(1)*S54(0,1) + v4(2)*S54(0,2);
v5(1) = v4(1)*S54(1,1) + v4(2)*S54(1,2);
v5(2) = qd(4) + v4(0);
v5(3) = ZWR*v4(2)*S54(0,1) + v4(4)*S54(0,1) + YWR*v4(0)*S54(0,2) - ZWR*v4(1)*S54(0,2) + v4(5)*S54(0,2);
v5(4) = ZWR*v4(2)*S54(1,1) + v4(4)*S54(1,1) + YWR*v4(0)*S54(1,2) - ZWR*v4(1)*S54(1,2) + v4(5)*S54(1,2);
v5(5) = -(YWR*v4(2)) + v4(3);

v6(0) = v5(1)*S65(0,1) + v5(2)*S65(0,2);
v6(1) = v5(1)*S65(1,1) + v5(2)*S65(1,2);
v6(2) = qd(5) - v5(0);
v6(3) = -(ZWFE*v5(0)*S65(0,1)) + v5(4)*S65(0,1) + v5(5)*S65(0,2);
v6(4) = -(ZWFE*v5(0)*S65(1,1)) + v5(4)*S65(1,1) + v5(5)*S65(1,2);
v6(5) = -(ZWFE*v5(1)) - v5(3);

v7(0) = v6(1)*S76(0,1) + v6(2)*S76(0,2);
v7(1) = v6(1)*S76(1,1) + v6(2)*S76(1,2);
v7(2) = qd(6) + v6(0);
v7(3) = v6(4)*S76(0,1) + v6(5)*S76(0,2);
v7(4) = v6(4)*S76(1,1) + v6(5)*S76(1,2);
v7(5) = v6(3);

v8(0) = v7(0)*S87(0,0) + v7(1)*S87(0,1) + v7(2)*S87(0,2);
v8(1) = v7(0)*S87(1,0) + v7(1)*S87(1,1) + v7(2)*S87(1,2);
v8(2) = v7(0)*S87(2,0) + v7(1)*S87(2,1) + v7(2)*S87(2,2);
v8(3) = v7(3)*S87(0,0) + v7(4)*S87(0,1) + v7(2)*(-(endeff_param.x(1)*S87(0,0)) + endeff_param.x(0)*S87(0,1)) + v7(5)*S87(0,2) + v7(1)*(endeff_param.x(2)*S87(0,0) - endeff_param.x(0)*S87(0,2)) + v7(0)*(-(endeff_param.x(2)*S87(0,1)) + endeff_param.x(1)*S87(0,2));
v8(4) = v7(3)*S87(1,0) + v7(4)*S87(1,1) + v7(2)*(-(endeff_param.x(1)*S87(1,0)) + endeff_param.x(0)*S87(1,1)) + v7(5)*S87(1,2) + v7(1)*(endeff_param.x(2)*S87(1,0) - endeff_param.x(0)*S87(1,2)) + v7(0)*(-(endeff_param.x(2)*S87(1,1)) + endeff_param.x(1)*S87(1,2));
v8(5) = v7(3)*S87(2,0) + v7(4)*S87(2,1) + v7(2)*(-(endeff_param.x(1)*S87(2,0)) + endeff_param.x(0)*S87(2,1)) + v7(5)*S87(2,2) + v7(1)*(endeff_param.x(2)*S87(2,0) - endeff_param.x(0)*S87(2,2)) + v7(0)*(-(endeff_param.x(2)*S87(2,1)) + endeff_param.x(1)*S87(2,2));


//wam_ForDynArtfunc5

// c-misc vectors
c1(0) = qd(0)*v1(1);
c1(1) = -(qd(0)*v1(0));
c1(3) = qd(0)*v1(4);
c1(4) = -(qd(0)*v1(3));

c2(0) = qd(1)*v2(1);
c2(1) = -(qd(1)*v2(0));
c2(3) = qd(1)*v2(4);
c2(4) = -(qd(1)*v2(3));

c3(0) = qd(2)*v3(1);
c3(1) = -(qd(2)*v3(0));
c3(3) = qd(2)*v3(4);
c3(4) = -(qd(2)*v3(3));

c4(0) = qd(3)*v4(1);
c4(1) = -(qd(3)*v4(0));
c4(3) = qd(3)*v4(4);
c4(4) = -(qd(3)*v4(3));

c5(0) = qd(4)*v5(1);
c5(1) = -(qd(4)*v5(0));
c5(3) = qd(4)*v5(4);
c5(4) = -(qd(4)*v5(3));

c6(0) = qd(5)*v6(1);
c6(1) = -(qd(5)*v6(0));
c6(3) = qd(5)*v6(4);
c6(4) = -(qd(5)*v6(3));

c7(0) = qd(6)*v7(1);
c7(1) = -(qd(6)*v7(0));
c7(3) = qd(6)*v7(4);
c7(4) = -(qd(6)*v7(3));

//wam_ForDynArtfunc6

// articulated body inertias and misc variables
// pv-misc vectors
pv0(0) = -(link_param[0].mcm(0)*pow(v0(1),2)) - link_param[0].mcm(0)*pow(v0(2),2) + v0(0)*(link_param[0].mcm(1)*v0(1) + link_param[0].mcm(2)*v0(2)) - link_param[0].mass*v0(2)*v0(4) + link_param[0].mass*v0(1)*v0(5) - force_ex_param.base_force(0)*S00(0,0) - force_ex_param.base_force(1)*S00(0,1) + g*link_param[0].mass*S00(0,2) - force_ex_param.base_force(2)*S00(0,2);
pv0(1) = -(link_param[0].mcm(1)*pow(v0(0),2)) - link_param[0].mcm(1)*pow(v0(2),2) + v0(1)*(link_param[0].mcm(0)*v0(0) + link_param[0].mcm(2)*v0(2)) + link_param[0].mass*v0(2)*v0(3) - link_param[0].mass*v0(0)*v0(5) - force_ex_param.base_force(0)*S00(1,0) - force_ex_param.base_force(1)*S00(1,1) + g*link_param[0].mass*S00(1,2) - force_ex_param.base_force(2)*S00(1,2);
pv0(2) = -(link_param[0].mcm(2)*pow(v0(0),2)) - link_param[0].mcm(2)*pow(v0(1),2) + (link_param[0].mcm(0)*v0(0) + link_param[0].mcm(1)*v0(1))*v0(2) - link_param[0].mass*v0(1)*v0(3) + link_param[0].mass*v0(0)*v0(4) - force_ex_param.base_force(0)*S00(2,0) - force_ex_param.base_force(1)*S00(2,1) + g*link_param[0].mass*S00(2,2) - force_ex_param.base_force(2)*S00(2,2);
pv0(3) = (-(link_param[0].mcm(1)*v0(1)) - link_param[0].mcm(2)*v0(2))*v0(3) + (link_param[0].mcm(0)*v0(2) + link_param[0].mass*v0(4))*v0(5) + v0(4)*(link_param[0].mcm(0)*v0(1) - link_param[0].mass*v0(5)) + v0(0)*(link_param[0].mcm(1)*v0(4) + link_param[0].mcm(2)*v0(5) - v0(2)*link_param[0].inertia(0,1) + v0(1)*link_param[0].inertia(0,2)) + v0(1)*(-(link_param[0].mcm(0)*v0(4)) - v0(2)*link_param[0].inertia(1,1) + v0(1)*link_param[0].inertia(1,2)) + v0(2)*(-(link_param[0].mcm(0)*v0(5)) - v0(2)*link_param[0].inertia(1,2) + v0(1)*link_param[0].inertia(2,2)) - force_ex_param.base_torque(0)*S00(0,0) - force_ex_param.base_torque(1)*S00(0,1) - force_ex_param.base_torque(2)*S00(0,2) - g*link_param[0].mcm(2)*S00(1,2) + g*link_param[0].mcm(1)*S00(2,2);
pv0(4) = (-(link_param[0].mcm(0)*v0(0)) - link_param[0].mcm(2)*v0(2))*v0(4) + (link_param[0].mcm(1)*v0(2) - link_param[0].mass*v0(3))*v0(5) + v0(3)*(link_param[0].mcm(1)*v0(0) + link_param[0].mass*v0(5)) + v0(0)*(-(link_param[0].mcm(1)*v0(3)) + v0(2)*link_param[0].inertia(0,0) - v0(0)*link_param[0].inertia(0,2)) + v0(1)*(link_param[0].mcm(0)*v0(3) + link_param[0].mcm(2)*v0(5) + v0(2)*link_param[0].inertia(0,1) - v0(0)*link_param[0].inertia(1,2)) + v0(2)*(-(link_param[0].mcm(1)*v0(5)) + v0(2)*link_param[0].inertia(0,2) - v0(0)*link_param[0].inertia(2,2)) + g*link_param[0].mcm(2)*S00(0,2) - force_ex_param.base_torque(0)*S00(1,0) - force_ex_param.base_torque(1)*S00(1,1) - force_ex_param.base_torque(2)*S00(1,2) - g*link_param[0].mcm(0)*S00(2,2);
pv0(5) = (link_param[0].mcm(2)*v0(1) + link_param[0].mass*v0(3))*v0(4) + v0(3)*(link_param[0].mcm(2)*v0(0) - link_param[0].mass*v0(4)) + (-(link_param[0].mcm(0)*v0(0)) - link_param[0].mcm(1)*v0(1))*v0(5) + v0(0)*(-(link_param[0].mcm(2)*v0(3)) - v0(1)*link_param[0].inertia(0,0) + v0(0)*link_param[0].inertia(0,1)) + v0(1)*(-(link_param[0].mcm(2)*v0(4)) - v0(1)*link_param[0].inertia(0,1) + v0(0)*link_param[0].inertia(1,1)) + v0(2)*(link_param[0].mcm(0)*v0(3) + link_param[0].mcm(1)*v0(4) - v0(1)*link_param[0].inertia(0,2) + v0(0)*link_param[0].inertia(1,2)) - g*link_param[0].mcm(1)*S00(0,2) + g*link_param[0].mcm(0)*S00(1,2) - force_ex_param.base_torque(0)*S00(2,0) - force_ex_param.base_torque(1)*S00(2,1) - force_ex_param.base_torque(2)*S00(2,2);

pv1(0) = -(link_param[1].mcm(0)*pow(v1(1),2)) - link_param[1].mcm(0)*pow(v1(2),2) + v1(0)*(link_param[1].mcm(1)*v1(1) + link_param[1].mcm(2)*v1(2)) - link_param[1].mass*v1(2)*v1(4) + link_param[1].mass*v1(1)*v1(5) - force_ex_param.joint_force(0,0)*SG10(0,0) - force_ex_param.joint_force(0,1)*SG10(0,1) + g*link_param[1].mass*SG10(0,2) - force_ex_param.joint_force(0,2)*SG10(0,2);
pv1(1) = -(link_param[1].mcm(1)*pow(v1(0),2)) - link_param[1].mcm(1)*pow(v1(2),2) + v1(1)*(link_param[1].mcm(0)*v1(0) + link_param[1].mcm(2)*v1(2)) + link_param[1].mass*v1(2)*v1(3) - link_param[1].mass*v1(0)*v1(5) - force_ex_param.joint_force(0,0)*SG10(1,0) - force_ex_param.joint_force(0,1)*SG10(1,1) + g*link_param[1].mass*SG10(1,2) - force_ex_param.joint_force(0,2)*SG10(1,2);
pv1(2) = -(link_param[1].mcm(2)*pow(v1(0),2)) - link_param[1].mcm(2)*pow(v1(1),2) + (link_param[1].mcm(0)*v1(0) + link_param[1].mcm(1)*v1(1))*v1(2) - link_param[1].mass*v1(1)*v1(3) + link_param[1].mass*v1(0)*v1(4) - force_ex_param.joint_force(0,0)*SG10(2,0) - force_ex_param.joint_force(0,1)*SG10(2,1) + g*link_param[1].mass*SG10(2,2) - force_ex_param.joint_force(0,2)*SG10(2,2);
pv1(3) = (-(link_param[1].mcm(1)*v1(1)) - link_param[1].mcm(2)*v1(2))*v1(3) + (link_param[1].mcm(0)*v1(2) + link_param[1].mass*v1(4))*v1(5) + v1(4)*(link_param[1].mcm(0)*v1(1) - link_param[1].mass*v1(5)) + v1(0)*(link_param[1].mcm(1)*v1(4) + link_param[1].mcm(2)*v1(5) - v1(2)*link_param[1].inertia(0,1) + v1(1)*link_param[1].inertia(0,2)) + v1(1)*(-(link_param[1].mcm(0)*v1(4)) - v1(2)*link_param[1].inertia(1,1) + v1(1)*link_param[1].inertia(1,2)) + v1(2)*(-(link_param[1].mcm(0)*v1(5)) - v1(2)*link_param[1].inertia(1,2) + v1(1)*link_param[1].inertia(2,2)) - force_ex_param.joint_torque(0,0)*SG10(0,0) - force_ex_param.joint_torque(0,1)*SG10(0,1) - force_ex_param.joint_torque(0,2)*SG10(0,2) - g*link_param[1].mcm(2)*SG10(1,2) + g*link_param[1].mcm(1)*SG10(2,2);
pv1(4) = (-(link_param[1].mcm(0)*v1(0)) - link_param[1].mcm(2)*v1(2))*v1(4) + (link_param[1].mcm(1)*v1(2) - link_param[1].mass*v1(3))*v1(5) + v1(3)*(link_param[1].mcm(1)*v1(0) + link_param[1].mass*v1(5)) + v1(0)*(-(link_param[1].mcm(1)*v1(3)) + v1(2)*link_param[1].inertia(0,0) - v1(0)*link_param[1].inertia(0,2)) + v1(1)*(link_param[1].mcm(0)*v1(3) + link_param[1].mcm(2)*v1(5) + v1(2)*link_param[1].inertia(0,1) - v1(0)*link_param[1].inertia(1,2)) + v1(2)*(-(link_param[1].mcm(1)*v1(5)) + v1(2)*link_param[1].inertia(0,2) - v1(0)*link_param[1].inertia(2,2)) + g*link_param[1].mcm(2)*SG10(0,2) - force_ex_param.joint_torque(0,0)*SG10(1,0) - force_ex_param.joint_torque(0,1)*SG10(1,1) - force_ex_param.joint_torque(0,2)*SG10(1,2) - g*link_param[1].mcm(0)*SG10(2,2);
pv1(5) = (link_param[1].mcm(2)*v1(1) + link_param[1].mass*v1(3))*v1(4) + v1(3)*(link_param[1].mcm(2)*v1(0) - link_param[1].mass*v1(4)) + (-(link_param[1].mcm(0)*v1(0)) - link_param[1].mcm(1)*v1(1))*v1(5) + v1(0)*(-(link_param[1].mcm(2)*v1(3)) - v1(1)*link_param[1].inertia(0,0) + v1(0)*link_param[1].inertia(0,1)) + v1(1)*(-(link_param[1].mcm(2)*v1(4)) - v1(1)*link_param[1].inertia(0,1) + v1(0)*link_param[1].inertia(1,1)) + v1(2)*(link_param[1].mcm(0)*v1(3) + link_param[1].mcm(1)*v1(4) - v1(1)*link_param[1].inertia(0,2) + v1(0)*link_param[1].inertia(1,2)) - g*link_param[1].mcm(1)*SG10(0,2) + g*link_param[1].mcm(0)*SG10(1,2) - force_ex_param.joint_torque(0,0)*SG10(2,0) - force_ex_param.joint_torque(0,1)*SG10(2,1) - force_ex_param.joint_torque(0,2)*SG10(2,2);

pv2(0) = -(link_param[2].mcm(0)*pow(v2(1),2)) - link_param[2].mcm(0)*pow(v2(2),2) + v2(0)*(link_param[2].mcm(1)*v2(1) + link_param[2].mcm(2)*v2(2)) - link_param[2].mass*v2(2)*v2(4) + link_param[2].mass*v2(1)*v2(5) - force_ex_param.joint_force(1,0)*SG20(0,0) - force_ex_param.joint_force(1,1)*SG20(0,1) + g*link_param[2].mass*SG20(0,2) - force_ex_param.joint_force(1,2)*SG20(0,2);
pv2(1) = -(link_param[2].mcm(1)*pow(v2(0),2)) - link_param[2].mcm(1)*pow(v2(2),2) + v2(1)*(link_param[2].mcm(0)*v2(0) + link_param[2].mcm(2)*v2(2)) + link_param[2].mass*v2(2)*v2(3) - link_param[2].mass*v2(0)*v2(5) - force_ex_param.joint_force(1,0)*SG20(1,0) - force_ex_param.joint_force(1,1)*SG20(1,1) + g*link_param[2].mass*SG20(1,2) - force_ex_param.joint_force(1,2)*SG20(1,2);
pv2(2) = -(link_param[2].mcm(2)*pow(v2(0),2)) - link_param[2].mcm(2)*pow(v2(1),2) + (link_param[2].mcm(0)*v2(0) + link_param[2].mcm(1)*v2(1))*v2(2) - link_param[2].mass*v2(1)*v2(3) + link_param[2].mass*v2(0)*v2(4) - force_ex_param.joint_force(1,0)*SG20(2,0) - force_ex_param.joint_force(1,1)*SG20(2,1) + g*link_param[2].mass*SG20(2,2) - force_ex_param.joint_force(1,2)*SG20(2,2);
pv2(3) = (-(link_param[2].mcm(1)*v2(1)) - link_param[2].mcm(2)*v2(2))*v2(3) + (link_param[2].mcm(0)*v2(2) + link_param[2].mass*v2(4))*v2(5) + v2(4)*(link_param[2].mcm(0)*v2(1) - link_param[2].mass*v2(5)) + v2(0)*(link_param[2].mcm(1)*v2(4) + link_param[2].mcm(2)*v2(5) - v2(2)*link_param[2].inertia(0,1) + v2(1)*link_param[2].inertia(0,2)) + v2(1)*(-(link_param[2].mcm(0)*v2(4)) - v2(2)*link_param[2].inertia(1,1) + v2(1)*link_param[2].inertia(1,2)) + v2(2)*(-(link_param[2].mcm(0)*v2(5)) - v2(2)*link_param[2].inertia(1,2) + v2(1)*link_param[2].inertia(2,2)) - force_ex_param.joint_torque(1,0)*SG20(0,0) - force_ex_param.joint_torque(1,1)*SG20(0,1) - force_ex_param.joint_torque(1,2)*SG20(0,2) - g*link_param[2].mcm(2)*SG20(1,2) + g*link_param[2].mcm(1)*SG20(2,2);
pv2(4) = (-(link_param[2].mcm(0)*v2(0)) - link_param[2].mcm(2)*v2(2))*v2(4) + (link_param[2].mcm(1)*v2(2) - link_param[2].mass*v2(3))*v2(5) + v2(3)*(link_param[2].mcm(1)*v2(0) + link_param[2].mass*v2(5)) + v2(0)*(-(link_param[2].mcm(1)*v2(3)) + v2(2)*link_param[2].inertia(0,0) - v2(0)*link_param[2].inertia(0,2)) + v2(1)*(link_param[2].mcm(0)*v2(3) + link_param[2].mcm(2)*v2(5) + v2(2)*link_param[2].inertia(0,1) - v2(0)*link_param[2].inertia(1,2)) + v2(2)*(-(link_param[2].mcm(1)*v2(5)) + v2(2)*link_param[2].inertia(0,2) - v2(0)*link_param[2].inertia(2,2)) + g*link_param[2].mcm(2)*SG20(0,2) - force_ex_param.joint_torque(1,0)*SG20(1,0) - force_ex_param.joint_torque(1,1)*SG20(1,1) - force_ex_param.joint_torque(1,2)*SG20(1,2) - g*link_param[2].mcm(0)*SG20(2,2);
pv2(5) = (link_param[2].mcm(2)*v2(1) + link_param[2].mass*v2(3))*v2(4) + v2(3)*(link_param[2].mcm(2)*v2(0) - link_param[2].mass*v2(4)) + (-(link_param[2].mcm(0)*v2(0)) - link_param[2].mcm(1)*v2(1))*v2(5) + v2(0)*(-(link_param[2].mcm(2)*v2(3)) - v2(1)*link_param[2].inertia(0,0) + v2(0)*link_param[2].inertia(0,1)) + v2(1)*(-(link_param[2].mcm(2)*v2(4)) - v2(1)*link_param[2].inertia(0,1) + v2(0)*link_param[2].inertia(1,1)) + v2(2)*(link_param[2].mcm(0)*v2(3) + link_param[2].mcm(1)*v2(4) - v2(1)*link_param[2].inertia(0,2) + v2(0)*link_param[2].inertia(1,2)) - g*link_param[2].mcm(1)*SG20(0,2) + g*link_param[2].mcm(0)*SG20(1,2) - force_ex_param.joint_torque(1,0)*SG20(2,0) - force_ex_param.joint_torque(1,1)*SG20(2,1) - force_ex_param.joint_torque(1,2)*SG20(2,2);

pv3(0) = -(link_param[3].mcm(0)*pow(v3(1),2)) - link_param[3].mcm(0)*pow(v3(2),2) + v3(0)*(link_param[3].mcm(1)*v3(1) + link_param[3].mcm(2)*v3(2)) - link_param[3].mass*v3(2)*v3(4) + link_param[3].mass*v3(1)*v3(5) - force_ex_param.joint_force(2,0)*SG30(0,0) - force_ex_param.joint_force(2,1)*SG30(0,1) + g*link_param[3].mass*SG30(0,2) - force_ex_param.joint_force(2,2)*SG30(0,2);
pv3(1) = -(link_param[3].mcm(1)*pow(v3(0),2)) - link_param[3].mcm(1)*pow(v3(2),2) + v3(1)*(link_param[3].mcm(0)*v3(0) + link_param[3].mcm(2)*v3(2)) + link_param[3].mass*v3(2)*v3(3) - link_param[3].mass*v3(0)*v3(5) - force_ex_param.joint_force(2,0)*SG30(1,0) - force_ex_param.joint_force(2,1)*SG30(1,1) + g*link_param[3].mass*SG30(1,2) - force_ex_param.joint_force(2,2)*SG30(1,2);
pv3(2) = -(link_param[3].mcm(2)*pow(v3(0),2)) - link_param[3].mcm(2)*pow(v3(1),2) + (link_param[3].mcm(0)*v3(0) + link_param[3].mcm(1)*v3(1))*v3(2) - link_param[3].mass*v3(1)*v3(3) + link_param[3].mass*v3(0)*v3(4) - force_ex_param.joint_force(2,0)*SG30(2,0) - force_ex_param.joint_force(2,1)*SG30(2,1) + g*link_param[3].mass*SG30(2,2) - force_ex_param.joint_force(2,2)*SG30(2,2);
pv3(3) = (-(link_param[3].mcm(1)*v3(1)) - link_param[3].mcm(2)*v3(2))*v3(3) + (link_param[3].mcm(0)*v3(2) + link_param[3].mass*v3(4))*v3(5) + v3(4)*(link_param[3].mcm(0)*v3(1) - link_param[3].mass*v3(5)) + v3(0)*(link_param[3].mcm(1)*v3(4) + link_param[3].mcm(2)*v3(5) - v3(2)*link_param[3].inertia(0,1) + v3(1)*link_param[3].inertia(0,2)) + v3(1)*(-(link_param[3].mcm(0)*v3(4)) - v3(2)*link_param[3].inertia(1,1) + v3(1)*link_param[3].inertia(1,2)) + v3(2)*(-(link_param[3].mcm(0)*v3(5)) - v3(2)*link_param[3].inertia(1,2) + v3(1)*link_param[3].inertia(2,2)) - force_ex_param.joint_torque(2,0)*SG30(0,0) - force_ex_param.joint_torque(2,1)*SG30(0,1) - force_ex_param.joint_torque(2,2)*SG30(0,2) - g*link_param[3].mcm(2)*SG30(1,2) + g*link_param[3].mcm(1)*SG30(2,2);
pv3(4) = (-(link_param[3].mcm(0)*v3(0)) - link_param[3].mcm(2)*v3(2))*v3(4) + (link_param[3].mcm(1)*v3(2) - link_param[3].mass*v3(3))*v3(5) + v3(3)*(link_param[3].mcm(1)*v3(0) + link_param[3].mass*v3(5)) + v3(0)*(-(link_param[3].mcm(1)*v3(3)) + v3(2)*link_param[3].inertia(0,0) - v3(0)*link_param[3].inertia(0,2)) + v3(1)*(link_param[3].mcm(0)*v3(3) + link_param[3].mcm(2)*v3(5) + v3(2)*link_param[3].inertia(0,1) - v3(0)*link_param[3].inertia(1,2)) + v3(2)*(-(link_param[3].mcm(1)*v3(5)) + v3(2)*link_param[3].inertia(0,2) - v3(0)*link_param[3].inertia(2,2)) + g*link_param[3].mcm(2)*SG30(0,2) - force_ex_param.joint_torque(2,0)*SG30(1,0) - force_ex_param.joint_torque(2,1)*SG30(1,1) - force_ex_param.joint_torque(2,2)*SG30(1,2) - g*link_param[3].mcm(0)*SG30(2,2);
pv3(5) = (link_param[3].mcm(2)*v3(1) + link_param[3].mass*v3(3))*v3(4) + v3(3)*(link_param[3].mcm(2)*v3(0) - link_param[3].mass*v3(4)) + (-(link_param[3].mcm(0)*v3(0)) - link_param[3].mcm(1)*v3(1))*v3(5) + v3(0)*(-(link_param[3].mcm(2)*v3(3)) - v3(1)*link_param[3].inertia(0,0) + v3(0)*link_param[3].inertia(0,1)) + v3(1)*(-(link_param[3].mcm(2)*v3(4)) - v3(1)*link_param[3].inertia(0,1) + v3(0)*link_param[3].inertia(1,1)) + v3(2)*(link_param[3].mcm(0)*v3(3) + link_param[3].mcm(1)*v3(4) - v3(1)*link_param[3].inertia(0,2) + v3(0)*link_param[3].inertia(1,2)) - g*link_param[3].mcm(1)*SG30(0,2) + g*link_param[3].mcm(0)*SG30(1,2) - force_ex_param.joint_torque(2,0)*SG30(2,0) - force_ex_param.joint_torque(2,1)*SG30(2,1) - force_ex_param.joint_torque(2,2)*SG30(2,2);

pv4(0) = -(link_param[4].mcm(0)*pow(v4(1),2)) - link_param[4].mcm(0)*pow(v4(2),2) + v4(0)*(link_param[4].mcm(1)*v4(1) + link_param[4].mcm(2)*v4(2)) - link_param[4].mass*v4(2)*v4(4) + link_param[4].mass*v4(1)*v4(5) - force_ex_param.joint_force(3,0)*SG40(0,0) - force_ex_param.joint_force(3,1)*SG40(0,1) + g*link_param[4].mass*SG40(0,2) - force_ex_param.joint_force(3,2)*SG40(0,2);
pv4(1) = -(link_param[4].mcm(1)*pow(v4(0),2)) - link_param[4].mcm(1)*pow(v4(2),2) + v4(1)*(link_param[4].mcm(0)*v4(0) + link_param[4].mcm(2)*v4(2)) + link_param[4].mass*v4(2)*v4(3) - link_param[4].mass*v4(0)*v4(5) - force_ex_param.joint_force(3,0)*SG40(1,0) - force_ex_param.joint_force(3,1)*SG40(1,1) + g*link_param[4].mass*SG40(1,2) - force_ex_param.joint_force(3,2)*SG40(1,2);
pv4(2) = -(link_param[4].mcm(2)*pow(v4(0),2)) - link_param[4].mcm(2)*pow(v4(1),2) + (link_param[4].mcm(0)*v4(0) + link_param[4].mcm(1)*v4(1))*v4(2) - link_param[4].mass*v4(1)*v4(3) + link_param[4].mass*v4(0)*v4(4) - force_ex_param.joint_force(3,0)*SG40(2,0) - force_ex_param.joint_force(3,1)*SG40(2,1) + g*link_param[4].mass*SG40(2,2) - force_ex_param.joint_force(3,2)*SG40(2,2);
pv4(3) = (-(link_param[4].mcm(1)*v4(1)) - link_param[4].mcm(2)*v4(2))*v4(3) + (link_param[4].mcm(0)*v4(2) + link_param[4].mass*v4(4))*v4(5) + v4(4)*(link_param[4].mcm(0)*v4(1) - link_param[4].mass*v4(5)) + v4(0)*(link_param[4].mcm(1)*v4(4) + link_param[4].mcm(2)*v4(5) - v4(2)*link_param[4].inertia(0,1) + v4(1)*link_param[4].inertia(0,2)) + v4(1)*(-(link_param[4].mcm(0)*v4(4)) - v4(2)*link_param[4].inertia(1,1) + v4(1)*link_param[4].inertia(1,2)) + v4(2)*(-(link_param[4].mcm(0)*v4(5)) - v4(2)*link_param[4].inertia(1,2) + v4(1)*link_param[4].inertia(2,2)) - force_ex_param.joint_torque(3,0)*SG40(0,0) - force_ex_param.joint_torque(3,1)*SG40(0,1) - force_ex_param.joint_torque(3,2)*SG40(0,2) - g*link_param[4].mcm(2)*SG40(1,2) + g*link_param[4].mcm(1)*SG40(2,2);
pv4(4) = (-(link_param[4].mcm(0)*v4(0)) - link_param[4].mcm(2)*v4(2))*v4(4) + (link_param[4].mcm(1)*v4(2) - link_param[4].mass*v4(3))*v4(5) + v4(3)*(link_param[4].mcm(1)*v4(0) + link_param[4].mass*v4(5)) + v4(0)*(-(link_param[4].mcm(1)*v4(3)) + v4(2)*link_param[4].inertia(0,0) - v4(0)*link_param[4].inertia(0,2)) + v4(1)*(link_param[4].mcm(0)*v4(3) + link_param[4].mcm(2)*v4(5) + v4(2)*link_param[4].inertia(0,1) - v4(0)*link_param[4].inertia(1,2)) + v4(2)*(-(link_param[4].mcm(1)*v4(5)) + v4(2)*link_param[4].inertia(0,2) - v4(0)*link_param[4].inertia(2,2)) + g*link_param[4].mcm(2)*SG40(0,2) - force_ex_param.joint_torque(3,0)*SG40(1,0) - force_ex_param.joint_torque(3,1)*SG40(1,1) - force_ex_param.joint_torque(3,2)*SG40(1,2) - g*link_param[4].mcm(0)*SG40(2,2);
pv4(5) = (link_param[4].mcm(2)*v4(1) + link_param[4].mass*v4(3))*v4(4) + v4(3)*(link_param[4].mcm(2)*v4(0) - link_param[4].mass*v4(4)) + (-(link_param[4].mcm(0)*v4(0)) - link_param[4].mcm(1)*v4(1))*v4(5) + v4(0)*(-(link_param[4].mcm(2)*v4(3)) - v4(1)*link_param[4].inertia(0,0) + v4(0)*link_param[4].inertia(0,1)) + v4(1)*(-(link_param[4].mcm(2)*v4(4)) - v4(1)*link_param[4].inertia(0,1) + v4(0)*link_param[4].inertia(1,1)) + v4(2)*(link_param[4].mcm(0)*v4(3) + link_param[4].mcm(1)*v4(4) - v4(1)*link_param[4].inertia(0,2) + v4(0)*link_param[4].inertia(1,2)) - g*link_param[4].mcm(1)*SG40(0,2) + g*link_param[4].mcm(0)*SG40(1,2) - force_ex_param.joint_torque(3,0)*SG40(2,0) - force_ex_param.joint_torque(3,1)*SG40(2,1) - force_ex_param.joint_torque(3,2)*SG40(2,2);

pv5(0) = -(link_param[5].mcm(0)*pow(v5(1),2)) - link_param[5].mcm(0)*pow(v5(2),2) + v5(0)*(link_param[5].mcm(1)*v5(1) + link_param[5].mcm(2)*v5(2)) - link_param[5].mass*v5(2)*v5(4) + link_param[5].mass*v5(1)*v5(5) - force_ex_param.joint_force(4,0)*SG50(0,0) - force_ex_param.joint_force(4,1)*SG50(0,1) + g*link_param[5].mass*SG50(0,2) - force_ex_param.joint_force(4,2)*SG50(0,2);
pv5(1) = -(link_param[5].mcm(1)*pow(v5(0),2)) - link_param[5].mcm(1)*pow(v5(2),2) + v5(1)*(link_param[5].mcm(0)*v5(0) + link_param[5].mcm(2)*v5(2)) + link_param[5].mass*v5(2)*v5(3) - link_param[5].mass*v5(0)*v5(5) - force_ex_param.joint_force(4,0)*SG50(1,0) - force_ex_param.joint_force(4,1)*SG50(1,1) + g*link_param[5].mass*SG50(1,2) - force_ex_param.joint_force(4,2)*SG50(1,2);
pv5(2) = -(link_param[5].mcm(2)*pow(v5(0),2)) - link_param[5].mcm(2)*pow(v5(1),2) + (link_param[5].mcm(0)*v5(0) + link_param[5].mcm(1)*v5(1))*v5(2) - link_param[5].mass*v5(1)*v5(3) + link_param[5].mass*v5(0)*v5(4) - force_ex_param.joint_force(4,0)*SG50(2,0) - force_ex_param.joint_force(4,1)*SG50(2,1) + g*link_param[5].mass*SG50(2,2) - force_ex_param.joint_force(4,2)*SG50(2,2);
pv5(3) = (-(link_param[5].mcm(1)*v5(1)) - link_param[5].mcm(2)*v5(2))*v5(3) + (link_param[5].mcm(0)*v5(2) + link_param[5].mass*v5(4))*v5(5) + v5(4)*(link_param[5].mcm(0)*v5(1) - link_param[5].mass*v5(5)) + v5(0)*(link_param[5].mcm(1)*v5(4) + link_param[5].mcm(2)*v5(5) - v5(2)*link_param[5].inertia(0,1) + v5(1)*link_param[5].inertia(0,2)) + v5(1)*(-(link_param[5].mcm(0)*v5(4)) - v5(2)*link_param[5].inertia(1,1) + v5(1)*link_param[5].inertia(1,2)) + v5(2)*(-(link_param[5].mcm(0)*v5(5)) - v5(2)*link_param[5].inertia(1,2) + v5(1)*link_param[5].inertia(2,2)) - force_ex_param.joint_torque(4,0)*SG50(0,0) - force_ex_param.joint_torque(4,1)*SG50(0,1) - force_ex_param.joint_torque(4,2)*SG50(0,2) - g*link_param[5].mcm(2)*SG50(1,2) + g*link_param[5].mcm(1)*SG50(2,2);
pv5(4) = (-(link_param[5].mcm(0)*v5(0)) - link_param[5].mcm(2)*v5(2))*v5(4) + (link_param[5].mcm(1)*v5(2) - link_param[5].mass*v5(3))*v5(5) + v5(3)*(link_param[5].mcm(1)*v5(0) + link_param[5].mass*v5(5)) + v5(0)*(-(link_param[5].mcm(1)*v5(3)) + v5(2)*link_param[5].inertia(0,0) - v5(0)*link_param[5].inertia(0,2)) + v5(1)*(link_param[5].mcm(0)*v5(3) + link_param[5].mcm(2)*v5(5) + v5(2)*link_param[5].inertia(0,1) - v5(0)*link_param[5].inertia(1,2)) + v5(2)*(-(link_param[5].mcm(1)*v5(5)) + v5(2)*link_param[5].inertia(0,2) - v5(0)*link_param[5].inertia(2,2)) + g*link_param[5].mcm(2)*SG50(0,2) - force_ex_param.joint_torque(4,0)*SG50(1,0) - force_ex_param.joint_torque(4,1)*SG50(1,1) - force_ex_param.joint_torque(4,2)*SG50(1,2) - g*link_param[5].mcm(0)*SG50(2,2);
pv5(5) = (link_param[5].mcm(2)*v5(1) + link_param[5].mass*v5(3))*v5(4) + v5(3)*(link_param[5].mcm(2)*v5(0) - link_param[5].mass*v5(4)) + (-(link_param[5].mcm(0)*v5(0)) - link_param[5].mcm(1)*v5(1))*v5(5) + v5(0)*(-(link_param[5].mcm(2)*v5(3)) - v5(1)*link_param[5].inertia(0,0) + v5(0)*link_param[5].inertia(0,1)) + v5(1)*(-(link_param[5].mcm(2)*v5(4)) - v5(1)*link_param[5].inertia(0,1) + v5(0)*link_param[5].inertia(1,1)) + v5(2)*(link_param[5].mcm(0)*v5(3) + link_param[5].mcm(1)*v5(4) - v5(1)*link_param[5].inertia(0,2) + v5(0)*link_param[5].inertia(1,2)) - g*link_param[5].mcm(1)*SG50(0,2) + g*link_param[5].mcm(0)*SG50(1,2) - force_ex_param.joint_torque(4,0)*SG50(2,0) - force_ex_param.joint_torque(4,1)*SG50(2,1) - force_ex_param.joint_torque(4,2)*SG50(2,2);

pv6(0) = -(link_param[6].mcm(0)*pow(v6(1),2)) - link_param[6].mcm(0)*pow(v6(2),2) + v6(0)*(link_param[6].mcm(1)*v6(1) + link_param[6].mcm(2)*v6(2)) - link_param[6].mass*v6(2)*v6(4) + link_param[6].mass*v6(1)*v6(5) - force_ex_param.joint_force(5,0)*SG60(0,0) - force_ex_param.joint_force(5,1)*SG60(0,1) + g*link_param[6].mass*SG60(0,2) - force_ex_param.joint_force(5,2)*SG60(0,2);
pv6(1) = -(link_param[6].mcm(1)*pow(v6(0),2)) - link_param[6].mcm(1)*pow(v6(2),2) + v6(1)*(link_param[6].mcm(0)*v6(0) + link_param[6].mcm(2)*v6(2)) + link_param[6].mass*v6(2)*v6(3) - link_param[6].mass*v6(0)*v6(5) - force_ex_param.joint_force(5,0)*SG60(1,0) - force_ex_param.joint_force(5,1)*SG60(1,1) + g*link_param[6].mass*SG60(1,2) - force_ex_param.joint_force(5,2)*SG60(1,2);
pv6(2) = -(link_param[6].mcm(2)*pow(v6(0),2)) - link_param[6].mcm(2)*pow(v6(1),2) + (link_param[6].mcm(0)*v6(0) + link_param[6].mcm(1)*v6(1))*v6(2) - link_param[6].mass*v6(1)*v6(3) + link_param[6].mass*v6(0)*v6(4) - force_ex_param.joint_force(5,0)*SG60(2,0) - force_ex_param.joint_force(5,1)*SG60(2,1) + g*link_param[6].mass*SG60(2,2) - force_ex_param.joint_force(5,2)*SG60(2,2);
pv6(3) = (-(link_param[6].mcm(1)*v6(1)) - link_param[6].mcm(2)*v6(2))*v6(3) + (link_param[6].mcm(0)*v6(2) + link_param[6].mass*v6(4))*v6(5) + v6(4)*(link_param[6].mcm(0)*v6(1) - link_param[6].mass*v6(5)) + v6(0)*(link_param[6].mcm(1)*v6(4) + link_param[6].mcm(2)*v6(5) - v6(2)*link_param[6].inertia(0,1) + v6(1)*link_param[6].inertia(0,2)) + v6(1)*(-(link_param[6].mcm(0)*v6(4)) - v6(2)*link_param[6].inertia(1,1) + v6(1)*link_param[6].inertia(1,2)) + v6(2)*(-(link_param[6].mcm(0)*v6(5)) - v6(2)*link_param[6].inertia(1,2) + v6(1)*link_param[6].inertia(2,2)) - force_ex_param.joint_torque(5,0)*SG60(0,0) - force_ex_param.joint_torque(5,1)*SG60(0,1) - force_ex_param.joint_torque(5,2)*SG60(0,2) - g*link_param[6].mcm(2)*SG60(1,2) + g*link_param[6].mcm(1)*SG60(2,2);
pv6(4) = (-(link_param[6].mcm(0)*v6(0)) - link_param[6].mcm(2)*v6(2))*v6(4) + (link_param[6].mcm(1)*v6(2) - link_param[6].mass*v6(3))*v6(5) + v6(3)*(link_param[6].mcm(1)*v6(0) + link_param[6].mass*v6(5)) + v6(0)*(-(link_param[6].mcm(1)*v6(3)) + v6(2)*link_param[6].inertia(0,0) - v6(0)*link_param[6].inertia(0,2)) + v6(1)*(link_param[6].mcm(0)*v6(3) + link_param[6].mcm(2)*v6(5) + v6(2)*link_param[6].inertia(0,1) - v6(0)*link_param[6].inertia(1,2)) + v6(2)*(-(link_param[6].mcm(1)*v6(5)) + v6(2)*link_param[6].inertia(0,2) - v6(0)*link_param[6].inertia(2,2)) + g*link_param[6].mcm(2)*SG60(0,2) - force_ex_param.joint_torque(5,0)*SG60(1,0) - force_ex_param.joint_torque(5,1)*SG60(1,1) - force_ex_param.joint_torque(5,2)*SG60(1,2) - g*link_param[6].mcm(0)*SG60(2,2);
pv6(5) = (link_param[6].mcm(2)*v6(1) + link_param[6].mass*v6(3))*v6(4) + v6(3)*(link_param[6].mcm(2)*v6(0) - link_param[6].mass*v6(4)) + (-(link_param[6].mcm(0)*v6(0)) - link_param[6].mcm(1)*v6(1))*v6(5) + v6(0)*(-(link_param[6].mcm(2)*v6(3)) - v6(1)*link_param[6].inertia(0,0) + v6(0)*link_param[6].inertia(0,1)) + v6(1)*(-(link_param[6].mcm(2)*v6(4)) - v6(1)*link_param[6].inertia(0,1) + v6(0)*link_param[6].inertia(1,1)) + v6(2)*(link_param[6].mcm(0)*v6(3) + link_param[6].mcm(1)*v6(4) - v6(1)*link_param[6].inertia(0,2) + v6(0)*link_param[6].inertia(1,2)) - g*link_param[6].mcm(1)*SG60(0,2) + g*link_param[6].mcm(0)*SG60(1,2) - force_ex_param.joint_torque(5,0)*SG60(2,0) - force_ex_param.joint_torque(5,1)*SG60(2,1) - force_ex_param.joint_torque(5,2)*SG60(2,2);

pv7(0) = -(link_param[7].mcm(0)*pow(v7(1),2)) - link_param[7].mcm(0)*pow(v7(2),2) + v7(0)*(link_param[7].mcm(1)*v7(1) + link_param[7].mcm(2)*v7(2)) - link_param[7].mass*v7(2)*v7(4) + link_param[7].mass*v7(1)*v7(5) - force_ex_param.joint_force(6,0)*SG70(0,0) - force_ex_param.joint_force(6,1)*SG70(0,1) + g*link_param[7].mass*SG70(0,2) - force_ex_param.joint_force(6,2)*SG70(0,2);
pv7(1) = -(link_param[7].mcm(1)*pow(v7(0),2)) - link_param[7].mcm(1)*pow(v7(2),2) + v7(1)*(link_param[7].mcm(0)*v7(0) + link_param[7].mcm(2)*v7(2)) + link_param[7].mass*v7(2)*v7(3) - link_param[7].mass*v7(0)*v7(5) - force_ex_param.joint_force(6,0)*SG70(1,0) - force_ex_param.joint_force(6,1)*SG70(1,1) + g*link_param[7].mass*SG70(1,2) - force_ex_param.joint_force(6,2)*SG70(1,2);
pv7(2) = -(link_param[7].mcm(2)*pow(v7(0),2)) - link_param[7].mcm(2)*pow(v7(1),2) + (link_param[7].mcm(0)*v7(0) + link_param[7].mcm(1)*v7(1))*v7(2) - link_param[7].mass*v7(1)*v7(3) + link_param[7].mass*v7(0)*v7(4) - force_ex_param.joint_force(6,0)*SG70(2,0) - force_ex_param.joint_force(6,1)*SG70(2,1) + g*link_param[7].mass*SG70(2,2) - force_ex_param.joint_force(6,2)*SG70(2,2);
pv7(3) = (-(link_param[7].mcm(1)*v7(1)) - link_param[7].mcm(2)*v7(2))*v7(3) + (link_param[7].mcm(0)*v7(2) + link_param[7].mass*v7(4))*v7(5) + v7(4)*(link_param[7].mcm(0)*v7(1) - link_param[7].mass*v7(5)) + v7(0)*(link_param[7].mcm(1)*v7(4) + link_param[7].mcm(2)*v7(5) - v7(2)*link_param[7].inertia(0,1) + v7(1)*link_param[7].inertia(0,2)) + v7(1)*(-(link_param[7].mcm(0)*v7(4)) - v7(2)*link_param[7].inertia(1,1) + v7(1)*link_param[7].inertia(1,2)) + v7(2)*(-(link_param[7].mcm(0)*v7(5)) - v7(2)*link_param[7].inertia(1,2) + v7(1)*link_param[7].inertia(2,2)) - force_ex_param.joint_torque(6,0)*SG70(0,0) - force_ex_param.joint_torque(6,1)*SG70(0,1) - force_ex_param.joint_torque(6,2)*SG70(0,2) - g*link_param[7].mcm(2)*SG70(1,2) + g*link_param[7].mcm(1)*SG70(2,2);
pv7(4) = (-(link_param[7].mcm(0)*v7(0)) - link_param[7].mcm(2)*v7(2))*v7(4) + (link_param[7].mcm(1)*v7(2) - link_param[7].mass*v7(3))*v7(5) + v7(3)*(link_param[7].mcm(1)*v7(0) + link_param[7].mass*v7(5)) + v7(0)*(-(link_param[7].mcm(1)*v7(3)) + v7(2)*link_param[7].inertia(0,0) - v7(0)*link_param[7].inertia(0,2)) + v7(1)*(link_param[7].mcm(0)*v7(3) + link_param[7].mcm(2)*v7(5) + v7(2)*link_param[7].inertia(0,1) - v7(0)*link_param[7].inertia(1,2)) + v7(2)*(-(link_param[7].mcm(1)*v7(5)) + v7(2)*link_param[7].inertia(0,2) - v7(0)*link_param[7].inertia(2,2)) + g*link_param[7].mcm(2)*SG70(0,2) - force_ex_param.joint_torque(6,0)*SG70(1,0) - force_ex_param.joint_torque(6,1)*SG70(1,1) - force_ex_param.joint_torque(6,2)*SG70(1,2) - g*link_param[7].mcm(0)*SG70(2,2);
pv7(5) = (link_param[7].mcm(2)*v7(1) + link_param[7].mass*v7(3))*v7(4) + v7(3)*(link_param[7].mcm(2)*v7(0) - link_param[7].mass*v7(4)) + (-(link_param[7].mcm(0)*v7(0)) - link_param[7].mcm(1)*v7(1))*v7(5) + v7(0)*(-(link_param[7].mcm(2)*v7(3)) - v7(1)*link_param[7].inertia(0,0) + v7(0)*link_param[7].inertia(0,1)) + v7(1)*(-(link_param[7].mcm(2)*v7(4)) - v7(1)*link_param[7].inertia(0,1) + v7(0)*link_param[7].inertia(1,1)) + v7(2)*(link_param[7].mcm(0)*v7(3) + link_param[7].mcm(1)*v7(4) - v7(1)*link_param[7].inertia(0,2) + v7(0)*link_param[7].inertia(1,2)) - g*link_param[7].mcm(1)*SG70(0,2) + g*link_param[7].mcm(0)*SG70(1,2) - force_ex_param.joint_torque(6,0)*SG70(2,0) - force_ex_param.joint_torque(6,1)*SG70(2,1) - force_ex_param.joint_torque(6,2)*SG70(2,2);

pv8(0) = -(endeff_param.mcm(0)*pow(v8(1),2)) - endeff_param.mcm(0)*pow(v8(2),2) + v8(0)*(endeff_param.mcm(1)*v8(1) + endeff_param.mcm(2)*v8(2)) - endeff_param.mass*v8(2)*v8(4) + endeff_param.mass*v8(1)*v8(5) + endeff_param.mass*g*SG80(0,2);
pv8(1) = -(endeff_param.mcm(1)*pow(v8(0),2)) - endeff_param.mcm(1)*pow(v8(2),2) + v8(1)*(endeff_param.mcm(0)*v8(0) + endeff_param.mcm(2)*v8(2)) + endeff_param.mass*v8(2)*v8(3) - endeff_param.mass*v8(0)*v8(5) + endeff_param.mass*g*SG80(1,2);
pv8(2) = -(endeff_param.mcm(2)*pow(v8(0),2)) - endeff_param.mcm(2)*pow(v8(1),2) + (endeff_param.mcm(0)*v8(0) + endeff_param.mcm(1)*v8(1))*v8(2) - endeff_param.mass*v8(1)*v8(3) + endeff_param.mass*v8(0)*v8(4) + endeff_param.mass*g*SG80(2,2);
pv8(3) = (-(endeff_param.mcm(1)*v8(1)) - endeff_param.mcm(2)*v8(2))*v8(3) - endeff_param.mcm(0)*v8(1)*v8(4) - endeff_param.mcm(0)*v8(2)*v8(5) + (endeff_param.mcm(0)*v8(2) + endeff_param.mass*v8(4))*v8(5) + v8(4)*(endeff_param.mcm(0)*v8(1) - endeff_param.mass*v8(5)) + v8(0)*(endeff_param.mcm(1)*v8(4) + endeff_param.mcm(2)*v8(5)) - g*endeff_param.mcm(2)*SG80(1,2) + g*endeff_param.mcm(1)*SG80(2,2);
pv8(4) = -(endeff_param.mcm(1)*v8(0)*v8(3)) + (-(endeff_param.mcm(0)*v8(0)) - endeff_param.mcm(2)*v8(2))*v8(4) - endeff_param.mcm(1)*v8(2)*v8(5) + (endeff_param.mcm(1)*v8(2) - endeff_param.mass*v8(3))*v8(5) + v8(3)*(endeff_param.mcm(1)*v8(0) + endeff_param.mass*v8(5)) + v8(1)*(endeff_param.mcm(0)*v8(3) + endeff_param.mcm(2)*v8(5)) + g*endeff_param.mcm(2)*SG80(0,2) - g*endeff_param.mcm(0)*SG80(2,2);
pv8(5) = -(endeff_param.mcm(2)*v8(0)*v8(3)) - endeff_param.mcm(2)*v8(1)*v8(4) + (endeff_param.mcm(2)*v8(1) + endeff_param.mass*v8(3))*v8(4) + v8(3)*(endeff_param.mcm(2)*v8(0) - endeff_param.mass*v8(4)) + v8(2)*(endeff_param.mcm(0)*v8(3) + endeff_param.mcm(1)*v8(4)) + (-(endeff_param.mcm(0)*v8(0)) - endeff_param.mcm(1)*v8(1))*v8(5) - g*endeff_param.mcm(1)*SG80(0,2) + g*endeff_param.mcm(0)*SG80(1,2);


//wam_ForDynArtfunc7

JA8(0,1) = endeff_param.mcm(2);
JA8(0,2) = -endeff_param.mcm(1);
JA8(0,3) = endeff_param.mass;
JA8(1,0) = -endeff_param.mcm(2);
JA8(1,2) = endeff_param.mcm(0);
JA8(1,4) = endeff_param.mass;
JA8(2,0) = endeff_param.mcm(1);
JA8(2,1) = -endeff_param.mcm(0);
JA8(2,5) = endeff_param.mass;
JA8(3,4) = -endeff_param.mcm(2);
JA8(3,5) = endeff_param.mcm(1);
JA8(4,3) = endeff_param.mcm(2);
JA8(4,5) = -endeff_param.mcm(0);
JA8(5,3) = -endeff_param.mcm(1);
JA8(5,4) = endeff_param.mcm(0);

T178(0,1) = JA8(0,1);
T178(0,2) = JA8(0,2);
T178(0,3) = JA8(0,3);
T178(1,0) = JA8(1,0);
T178(1,2) = JA8(1,2);
T178(1,4) = JA8(1,4);
T178(2,0) = JA8(2,0);
T178(2,1) = JA8(2,1);
T178(2,5) = JA8(2,5);
T178(3,4) = JA8(3,4);
T178(3,5) = JA8(3,5);
T178(4,3) = JA8(4,3);
T178(4,5) = JA8(4,5);
T178(5,3) = JA8(5,3);
T178(5,4) = JA8(5,4);


T78(0,0) = (-(endeff_param.x(2)*S87(0,1)) + endeff_param.x(1)*S87(0,2))*Si78(0,0)*T178(0,3) + S87(2,0)*(Si78(0,0)*T178(0,2) + Si78(0,1)*T178(1,2)) + (-(endeff_param.x(2)*S87(1,1)) + endeff_param.x(1)*S87(1,2))*Si78(0,1)*T178(1,4) + S87(0,0)*(Si78(0,1)*T178(1,0) + Si78(0,2)*T178(2,0)) + S87(1,0)*(Si78(0,0)*T178(0,1) + Si78(0,2)*T178(2,1)) + (-(endeff_param.x(2)*S87(2,1)) + endeff_param.x(1)*S87(2,2))*Si78(0,2)*T178(2,5);
T78(0,1) = (endeff_param.x(2)*S87(0,0) - endeff_param.x(0)*S87(0,2))*Si78(0,0)*T178(0,3) + S87(2,1)*(Si78(0,0)*T178(0,2) + Si78(0,1)*T178(1,2)) + (endeff_param.x(2)*S87(1,0) - endeff_param.x(0)*S87(1,2))*Si78(0,1)*T178(1,4) + S87(0,1)*(Si78(0,1)*T178(1,0) + Si78(0,2)*T178(2,0)) + S87(1,1)*(Si78(0,0)*T178(0,1) + Si78(0,2)*T178(2,1)) + (endeff_param.x(2)*S87(2,0) - endeff_param.x(0)*S87(2,2))*Si78(0,2)*T178(2,5);
T78(0,2) = (-(endeff_param.x(1)*S87(0,0)) + endeff_param.x(0)*S87(0,1))*Si78(0,0)*T178(0,3) + S87(2,2)*(Si78(0,0)*T178(0,2) + Si78(0,1)*T178(1,2)) + (-(endeff_param.x(1)*S87(1,0)) + endeff_param.x(0)*S87(1,1))*Si78(0,1)*T178(1,4) + S87(0,2)*(Si78(0,1)*T178(1,0) + Si78(0,2)*T178(2,0)) + S87(1,2)*(Si78(0,0)*T178(0,1) + Si78(0,2)*T178(2,1)) + (-(endeff_param.x(1)*S87(2,0)) + endeff_param.x(0)*S87(2,1))*Si78(0,2)*T178(2,5);
T78(0,3) = S87(0,0)*Si78(0,0)*T178(0,3) + S87(1,0)*Si78(0,1)*T178(1,4) + S87(2,0)*Si78(0,2)*T178(2,5);
T78(0,4) = S87(0,1)*Si78(0,0)*T178(0,3) + S87(1,1)*Si78(0,1)*T178(1,4) + S87(2,1)*Si78(0,2)*T178(2,5);
T78(0,5) = S87(0,2)*Si78(0,0)*T178(0,3) + S87(1,2)*Si78(0,1)*T178(1,4) + S87(2,2)*Si78(0,2)*T178(2,5);

T78(1,0) = (-(endeff_param.x(2)*S87(0,1)) + endeff_param.x(1)*S87(0,2))*Si78(1,0)*T178(0,3) + S87(2,0)*(Si78(1,0)*T178(0,2) + Si78(1,1)*T178(1,2)) + (-(endeff_param.x(2)*S87(1,1)) + endeff_param.x(1)*S87(1,2))*Si78(1,1)*T178(1,4) + S87(0,0)*(Si78(1,1)*T178(1,0) + Si78(1,2)*T178(2,0)) + S87(1,0)*(Si78(1,0)*T178(0,1) + Si78(1,2)*T178(2,1)) + (-(endeff_param.x(2)*S87(2,1)) + endeff_param.x(1)*S87(2,2))*Si78(1,2)*T178(2,5);
T78(1,1) = (endeff_param.x(2)*S87(0,0) - endeff_param.x(0)*S87(0,2))*Si78(1,0)*T178(0,3) + S87(2,1)*(Si78(1,0)*T178(0,2) + Si78(1,1)*T178(1,2)) + (endeff_param.x(2)*S87(1,0) - endeff_param.x(0)*S87(1,2))*Si78(1,1)*T178(1,4) + S87(0,1)*(Si78(1,1)*T178(1,0) + Si78(1,2)*T178(2,0)) + S87(1,1)*(Si78(1,0)*T178(0,1) + Si78(1,2)*T178(2,1)) + (endeff_param.x(2)*S87(2,0) - endeff_param.x(0)*S87(2,2))*Si78(1,2)*T178(2,5);
T78(1,2) = (-(endeff_param.x(1)*S87(0,0)) + endeff_param.x(0)*S87(0,1))*Si78(1,0)*T178(0,3) + S87(2,2)*(Si78(1,0)*T178(0,2) + Si78(1,1)*T178(1,2)) + (-(endeff_param.x(1)*S87(1,0)) + endeff_param.x(0)*S87(1,1))*Si78(1,1)*T178(1,4) + S87(0,2)*(Si78(1,1)*T178(1,0) + Si78(1,2)*T178(2,0)) + S87(1,2)*(Si78(1,0)*T178(0,1) + Si78(1,2)*T178(2,1)) + (-(endeff_param.x(1)*S87(2,0)) + endeff_param.x(0)*S87(2,1))*Si78(1,2)*T178(2,5);
T78(1,3) = S87(0,0)*Si78(1,0)*T178(0,3) + S87(1,0)*Si78(1,1)*T178(1,4) + S87(2,0)*Si78(1,2)*T178(2,5);
T78(1,4) = S87(0,1)*Si78(1,0)*T178(0,3) + S87(1,1)*Si78(1,1)*T178(1,4) + S87(2,1)*Si78(1,2)*T178(2,5);
T78(1,5) = S87(0,2)*Si78(1,0)*T178(0,3) + S87(1,2)*Si78(1,1)*T178(1,4) + S87(2,2)*Si78(1,2)*T178(2,5);

T78(2,0) = (-(endeff_param.x(2)*S87(0,1)) + endeff_param.x(1)*S87(0,2))*Si78(2,0)*T178(0,3) + S87(2,0)*(Si78(2,0)*T178(0,2) + Si78(2,1)*T178(1,2)) + (-(endeff_param.x(2)*S87(1,1)) + endeff_param.x(1)*S87(1,2))*Si78(2,1)*T178(1,4) + S87(0,0)*(Si78(2,1)*T178(1,0) + Si78(2,2)*T178(2,0)) + S87(1,0)*(Si78(2,0)*T178(0,1) + Si78(2,2)*T178(2,1)) + (-(endeff_param.x(2)*S87(2,1)) + endeff_param.x(1)*S87(2,2))*Si78(2,2)*T178(2,5);
T78(2,1) = (endeff_param.x(2)*S87(0,0) - endeff_param.x(0)*S87(0,2))*Si78(2,0)*T178(0,3) + S87(2,1)*(Si78(2,0)*T178(0,2) + Si78(2,1)*T178(1,2)) + (endeff_param.x(2)*S87(1,0) - endeff_param.x(0)*S87(1,2))*Si78(2,1)*T178(1,4) + S87(0,1)*(Si78(2,1)*T178(1,0) + Si78(2,2)*T178(2,0)) + S87(1,1)*(Si78(2,0)*T178(0,1) + Si78(2,2)*T178(2,1)) + (endeff_param.x(2)*S87(2,0) - endeff_param.x(0)*S87(2,2))*Si78(2,2)*T178(2,5);
T78(2,2) = (-(endeff_param.x(1)*S87(0,0)) + endeff_param.x(0)*S87(0,1))*Si78(2,0)*T178(0,3) + S87(2,2)*(Si78(2,0)*T178(0,2) + Si78(2,1)*T178(1,2)) + (-(endeff_param.x(1)*S87(1,0)) + endeff_param.x(0)*S87(1,1))*Si78(2,1)*T178(1,4) + S87(0,2)*(Si78(2,1)*T178(1,0) + Si78(2,2)*T178(2,0)) + S87(1,2)*(Si78(2,0)*T178(0,1) + Si78(2,2)*T178(2,1)) + (-(endeff_param.x(1)*S87(2,0)) + endeff_param.x(0)*S87(2,1))*Si78(2,2)*T178(2,5);
T78(2,3) = S87(0,0)*Si78(2,0)*T178(0,3) + S87(1,0)*Si78(2,1)*T178(1,4) + S87(2,0)*Si78(2,2)*T178(2,5);
T78(2,4) = S87(0,1)*Si78(2,0)*T178(0,3) + S87(1,1)*Si78(2,1)*T178(1,4) + S87(2,1)*Si78(2,2)*T178(2,5);
T78(2,5) = S87(0,2)*Si78(2,0)*T178(0,3) + S87(1,2)*Si78(2,1)*T178(1,4) + S87(2,2)*Si78(2,2)*T178(2,5);

T78(3,0) = S87(2,0)*((-(endeff_param.x(2)*Si78(1,0)) + endeff_param.x(1)*Si78(2,0))*T178(0,2) + (-(endeff_param.x(2)*Si78(1,1)) + endeff_param.x(1)*Si78(2,1))*T178(1,2)) + S87(0,0)*((-(endeff_param.x(2)*Si78(1,1)) + endeff_param.x(1)*Si78(2,1))*T178(1,0) + (-(endeff_param.x(2)*Si78(1,2)) + endeff_param.x(1)*Si78(2,2))*T178(2,0)) + S87(1,0)*((-(endeff_param.x(2)*Si78(1,0)) + endeff_param.x(1)*Si78(2,0))*T178(0,1) + (-(endeff_param.x(2)*Si78(1,2)) + endeff_param.x(1)*Si78(2,2))*T178(2,1)) + (-(endeff_param.x(2)*S87(2,1)) + endeff_param.x(1)*S87(2,2))*((-(endeff_param.x(2)*Si78(1,2)) + endeff_param.x(1)*Si78(2,2))*T178(2,5) + Si78(0,0)*T178(3,5) + Si78(0,1)*T178(4,5)) + (-(endeff_param.x(2)*S87(0,1)) + endeff_param.x(1)*S87(0,2))*((-(endeff_param.x(2)*Si78(1,0)) + endeff_param.x(1)*Si78(2,0))*T178(0,3) + Si78(0,1)*T178(4,3) + Si78(0,2)*T178(5,3)) + (-(endeff_param.x(2)*S87(1,1)) + endeff_param.x(1)*S87(1,2))*((-(endeff_param.x(2)*Si78(1,1)) + endeff_param.x(1)*Si78(2,1))*T178(1,4) + Si78(0,0)*T178(3,4) + Si78(0,2)*T178(5,4));
T78(3,1) = S87(2,1)*((-(endeff_param.x(2)*Si78(1,0)) + endeff_param.x(1)*Si78(2,0))*T178(0,2) + (-(endeff_param.x(2)*Si78(1,1)) + endeff_param.x(1)*Si78(2,1))*T178(1,2)) + S87(0,1)*((-(endeff_param.x(2)*Si78(1,1)) + endeff_param.x(1)*Si78(2,1))*T178(1,0) + (-(endeff_param.x(2)*Si78(1,2)) + endeff_param.x(1)*Si78(2,2))*T178(2,0)) + S87(1,1)*((-(endeff_param.x(2)*Si78(1,0)) + endeff_param.x(1)*Si78(2,0))*T178(0,1) + (-(endeff_param.x(2)*Si78(1,2)) + endeff_param.x(1)*Si78(2,2))*T178(2,1)) + (endeff_param.x(2)*S87(2,0) - endeff_param.x(0)*S87(2,2))*((-(endeff_param.x(2)*Si78(1,2)) + endeff_param.x(1)*Si78(2,2))*T178(2,5) + Si78(0,0)*T178(3,5) + Si78(0,1)*T178(4,5)) + (endeff_param.x(2)*S87(0,0) - endeff_param.x(0)*S87(0,2))*((-(endeff_param.x(2)*Si78(1,0)) + endeff_param.x(1)*Si78(2,0))*T178(0,3) + Si78(0,1)*T178(4,3) + Si78(0,2)*T178(5,3)) + (endeff_param.x(2)*S87(1,0) - endeff_param.x(0)*S87(1,2))*((-(endeff_param.x(2)*Si78(1,1)) + endeff_param.x(1)*Si78(2,1))*T178(1,4) + Si78(0,0)*T178(3,4) + Si78(0,2)*T178(5,4));
T78(3,2) = S87(2,2)*((-(endeff_param.x(2)*Si78(1,0)) + endeff_param.x(1)*Si78(2,0))*T178(0,2) + (-(endeff_param.x(2)*Si78(1,1)) + endeff_param.x(1)*Si78(2,1))*T178(1,2)) + S87(0,2)*((-(endeff_param.x(2)*Si78(1,1)) + endeff_param.x(1)*Si78(2,1))*T178(1,0) + (-(endeff_param.x(2)*Si78(1,2)) + endeff_param.x(1)*Si78(2,2))*T178(2,0)) + S87(1,2)*((-(endeff_param.x(2)*Si78(1,0)) + endeff_param.x(1)*Si78(2,0))*T178(0,1) + (-(endeff_param.x(2)*Si78(1,2)) + endeff_param.x(1)*Si78(2,2))*T178(2,1)) + (-(endeff_param.x(1)*S87(2,0)) + endeff_param.x(0)*S87(2,1))*((-(endeff_param.x(2)*Si78(1,2)) + endeff_param.x(1)*Si78(2,2))*T178(2,5) + Si78(0,0)*T178(3,5) + Si78(0,1)*T178(4,5)) + (-(endeff_param.x(1)*S87(0,0)) + endeff_param.x(0)*S87(0,1))*((-(endeff_param.x(2)*Si78(1,0)) + endeff_param.x(1)*Si78(2,0))*T178(0,3) + Si78(0,1)*T178(4,3) + Si78(0,2)*T178(5,3)) + (-(endeff_param.x(1)*S87(1,0)) + endeff_param.x(0)*S87(1,1))*((-(endeff_param.x(2)*Si78(1,1)) + endeff_param.x(1)*Si78(2,1))*T178(1,4) + Si78(0,0)*T178(3,4) + Si78(0,2)*T178(5,4));
T78(3,3) = S87(2,0)*((-(endeff_param.x(2)*Si78(1,2)) + endeff_param.x(1)*Si78(2,2))*T178(2,5) + Si78(0,0)*T178(3,5) + Si78(0,1)*T178(4,5)) + S87(0,0)*((-(endeff_param.x(2)*Si78(1,0)) + endeff_param.x(1)*Si78(2,0))*T178(0,3) + Si78(0,1)*T178(4,3) + Si78(0,2)*T178(5,3)) + S87(1,0)*((-(endeff_param.x(2)*Si78(1,1)) + endeff_param.x(1)*Si78(2,1))*T178(1,4) + Si78(0,0)*T178(3,4) + Si78(0,2)*T178(5,4));
T78(3,4) = S87(2,1)*((-(endeff_param.x(2)*Si78(1,2)) + endeff_param.x(1)*Si78(2,2))*T178(2,5) + Si78(0,0)*T178(3,5) + Si78(0,1)*T178(4,5)) + S87(0,1)*((-(endeff_param.x(2)*Si78(1,0)) + endeff_param.x(1)*Si78(2,0))*T178(0,3) + Si78(0,1)*T178(4,3) + Si78(0,2)*T178(5,3)) + S87(1,1)*((-(endeff_param.x(2)*Si78(1,1)) + endeff_param.x(1)*Si78(2,1))*T178(1,4) + Si78(0,0)*T178(3,4) + Si78(0,2)*T178(5,4));
T78(3,5) = S87(2,2)*((-(endeff_param.x(2)*Si78(1,2)) + endeff_param.x(1)*Si78(2,2))*T178(2,5) + Si78(0,0)*T178(3,5) + Si78(0,1)*T178(4,5)) + S87(0,2)*((-(endeff_param.x(2)*Si78(1,0)) + endeff_param.x(1)*Si78(2,0))*T178(0,3) + Si78(0,1)*T178(4,3) + Si78(0,2)*T178(5,3)) + S87(1,2)*((-(endeff_param.x(2)*Si78(1,1)) + endeff_param.x(1)*Si78(2,1))*T178(1,4) + Si78(0,0)*T178(3,4) + Si78(0,2)*T178(5,4));

T78(4,0) = S87(2,0)*((endeff_param.x(2)*Si78(0,0) - endeff_param.x(0)*Si78(2,0))*T178(0,2) + (endeff_param.x(2)*Si78(0,1) - endeff_param.x(0)*Si78(2,1))*T178(1,2)) + S87(0,0)*((endeff_param.x(2)*Si78(0,1) - endeff_param.x(0)*Si78(2,1))*T178(1,0) + (endeff_param.x(2)*Si78(0,2) - endeff_param.x(0)*Si78(2,2))*T178(2,0)) + S87(1,0)*((endeff_param.x(2)*Si78(0,0) - endeff_param.x(0)*Si78(2,0))*T178(0,1) + (endeff_param.x(2)*Si78(0,2) - endeff_param.x(0)*Si78(2,2))*T178(2,1)) + (-(endeff_param.x(2)*S87(2,1)) + endeff_param.x(1)*S87(2,2))*((endeff_param.x(2)*Si78(0,2) - endeff_param.x(0)*Si78(2,2))*T178(2,5) + Si78(1,0)*T178(3,5) + Si78(1,1)*T178(4,5)) + (-(endeff_param.x(2)*S87(0,1)) + endeff_param.x(1)*S87(0,2))*((endeff_param.x(2)*Si78(0,0) - endeff_param.x(0)*Si78(2,0))*T178(0,3) + Si78(1,1)*T178(4,3) + Si78(1,2)*T178(5,3)) + (-(endeff_param.x(2)*S87(1,1)) + endeff_param.x(1)*S87(1,2))*((endeff_param.x(2)*Si78(0,1) - endeff_param.x(0)*Si78(2,1))*T178(1,4) + Si78(1,0)*T178(3,4) + Si78(1,2)*T178(5,4));
T78(4,1) = S87(2,1)*((endeff_param.x(2)*Si78(0,0) - endeff_param.x(0)*Si78(2,0))*T178(0,2) + (endeff_param.x(2)*Si78(0,1) - endeff_param.x(0)*Si78(2,1))*T178(1,2)) + S87(0,1)*((endeff_param.x(2)*Si78(0,1) - endeff_param.x(0)*Si78(2,1))*T178(1,0) + (endeff_param.x(2)*Si78(0,2) - endeff_param.x(0)*Si78(2,2))*T178(2,0)) + S87(1,1)*((endeff_param.x(2)*Si78(0,0) - endeff_param.x(0)*Si78(2,0))*T178(0,1) + (endeff_param.x(2)*Si78(0,2) - endeff_param.x(0)*Si78(2,2))*T178(2,1)) + (endeff_param.x(2)*S87(2,0) - endeff_param.x(0)*S87(2,2))*((endeff_param.x(2)*Si78(0,2) - endeff_param.x(0)*Si78(2,2))*T178(2,5) + Si78(1,0)*T178(3,5) + Si78(1,1)*T178(4,5)) + (endeff_param.x(2)*S87(0,0) - endeff_param.x(0)*S87(0,2))*((endeff_param.x(2)*Si78(0,0) - endeff_param.x(0)*Si78(2,0))*T178(0,3) + Si78(1,1)*T178(4,3) + Si78(1,2)*T178(5,3)) + (endeff_param.x(2)*S87(1,0) - endeff_param.x(0)*S87(1,2))*((endeff_param.x(2)*Si78(0,1) - endeff_param.x(0)*Si78(2,1))*T178(1,4) + Si78(1,0)*T178(3,4) + Si78(1,2)*T178(5,4));
T78(4,2) = S87(2,2)*((endeff_param.x(2)*Si78(0,0) - endeff_param.x(0)*Si78(2,0))*T178(0,2) + (endeff_param.x(2)*Si78(0,1) - endeff_param.x(0)*Si78(2,1))*T178(1,2)) + S87(0,2)*((endeff_param.x(2)*Si78(0,1) - endeff_param.x(0)*Si78(2,1))*T178(1,0) + (endeff_param.x(2)*Si78(0,2) - endeff_param.x(0)*Si78(2,2))*T178(2,0)) + S87(1,2)*((endeff_param.x(2)*Si78(0,0) - endeff_param.x(0)*Si78(2,0))*T178(0,1) + (endeff_param.x(2)*Si78(0,2) - endeff_param.x(0)*Si78(2,2))*T178(2,1)) + (-(endeff_param.x(1)*S87(2,0)) + endeff_param.x(0)*S87(2,1))*((endeff_param.x(2)*Si78(0,2) - endeff_param.x(0)*Si78(2,2))*T178(2,5) + Si78(1,0)*T178(3,5) + Si78(1,1)*T178(4,5)) + (-(endeff_param.x(1)*S87(0,0)) + endeff_param.x(0)*S87(0,1))*((endeff_param.x(2)*Si78(0,0) - endeff_param.x(0)*Si78(2,0))*T178(0,3) + Si78(1,1)*T178(4,3) + Si78(1,2)*T178(5,3)) + (-(endeff_param.x(1)*S87(1,0)) + endeff_param.x(0)*S87(1,1))*((endeff_param.x(2)*Si78(0,1) - endeff_param.x(0)*Si78(2,1))*T178(1,4) + Si78(1,0)*T178(3,4) + Si78(1,2)*T178(5,4));
T78(4,3) = S87(2,0)*((endeff_param.x(2)*Si78(0,2) - endeff_param.x(0)*Si78(2,2))*T178(2,5) + Si78(1,0)*T178(3,5) + Si78(1,1)*T178(4,5)) + S87(0,0)*((endeff_param.x(2)*Si78(0,0) - endeff_param.x(0)*Si78(2,0))*T178(0,3) + Si78(1,1)*T178(4,3) + Si78(1,2)*T178(5,3)) + S87(1,0)*((endeff_param.x(2)*Si78(0,1) - endeff_param.x(0)*Si78(2,1))*T178(1,4) + Si78(1,0)*T178(3,4) + Si78(1,2)*T178(5,4));
T78(4,4) = S87(2,1)*((endeff_param.x(2)*Si78(0,2) - endeff_param.x(0)*Si78(2,2))*T178(2,5) + Si78(1,0)*T178(3,5) + Si78(1,1)*T178(4,5)) + S87(0,1)*((endeff_param.x(2)*Si78(0,0) - endeff_param.x(0)*Si78(2,0))*T178(0,3) + Si78(1,1)*T178(4,3) + Si78(1,2)*T178(5,3)) + S87(1,1)*((endeff_param.x(2)*Si78(0,1) - endeff_param.x(0)*Si78(2,1))*T178(1,4) + Si78(1,0)*T178(3,4) + Si78(1,2)*T178(5,4));
T78(4,5) = S87(2,2)*((endeff_param.x(2)*Si78(0,2) - endeff_param.x(0)*Si78(2,2))*T178(2,5) + Si78(1,0)*T178(3,5) + Si78(1,1)*T178(4,5)) + S87(0,2)*((endeff_param.x(2)*Si78(0,0) - endeff_param.x(0)*Si78(2,0))*T178(0,3) + Si78(1,1)*T178(4,3) + Si78(1,2)*T178(5,3)) + S87(1,2)*((endeff_param.x(2)*Si78(0,1) - endeff_param.x(0)*Si78(2,1))*T178(1,4) + Si78(1,0)*T178(3,4) + Si78(1,2)*T178(5,4));

T78(5,0) = S87(2,0)*((-(endeff_param.x(1)*Si78(0,0)) + endeff_param.x(0)*Si78(1,0))*T178(0,2) + (-(endeff_param.x(1)*Si78(0,1)) + endeff_param.x(0)*Si78(1,1))*T178(1,2)) + S87(0,0)*((-(endeff_param.x(1)*Si78(0,1)) + endeff_param.x(0)*Si78(1,1))*T178(1,0) + (-(endeff_param.x(1)*Si78(0,2)) + endeff_param.x(0)*Si78(1,2))*T178(2,0)) + S87(1,0)*((-(endeff_param.x(1)*Si78(0,0)) + endeff_param.x(0)*Si78(1,0))*T178(0,1) + (-(endeff_param.x(1)*Si78(0,2)) + endeff_param.x(0)*Si78(1,2))*T178(2,1)) + (-(endeff_param.x(2)*S87(2,1)) + endeff_param.x(1)*S87(2,2))*((-(endeff_param.x(1)*Si78(0,2)) + endeff_param.x(0)*Si78(1,2))*T178(2,5) + Si78(2,0)*T178(3,5) + Si78(2,1)*T178(4,5)) + (-(endeff_param.x(2)*S87(0,1)) + endeff_param.x(1)*S87(0,2))*((-(endeff_param.x(1)*Si78(0,0)) + endeff_param.x(0)*Si78(1,0))*T178(0,3) + Si78(2,1)*T178(4,3) + Si78(2,2)*T178(5,3)) + (-(endeff_param.x(2)*S87(1,1)) + endeff_param.x(1)*S87(1,2))*((-(endeff_param.x(1)*Si78(0,1)) + endeff_param.x(0)*Si78(1,1))*T178(1,4) + Si78(2,0)*T178(3,4) + Si78(2,2)*T178(5,4));
T78(5,1) = S87(2,1)*((-(endeff_param.x(1)*Si78(0,0)) + endeff_param.x(0)*Si78(1,0))*T178(0,2) + (-(endeff_param.x(1)*Si78(0,1)) + endeff_param.x(0)*Si78(1,1))*T178(1,2)) + S87(0,1)*((-(endeff_param.x(1)*Si78(0,1)) + endeff_param.x(0)*Si78(1,1))*T178(1,0) + (-(endeff_param.x(1)*Si78(0,2)) + endeff_param.x(0)*Si78(1,2))*T178(2,0)) + S87(1,1)*((-(endeff_param.x(1)*Si78(0,0)) + endeff_param.x(0)*Si78(1,0))*T178(0,1) + (-(endeff_param.x(1)*Si78(0,2)) + endeff_param.x(0)*Si78(1,2))*T178(2,1)) + (endeff_param.x(2)*S87(2,0) - endeff_param.x(0)*S87(2,2))*((-(endeff_param.x(1)*Si78(0,2)) + endeff_param.x(0)*Si78(1,2))*T178(2,5) + Si78(2,0)*T178(3,5) + Si78(2,1)*T178(4,5)) + (endeff_param.x(2)*S87(0,0) - endeff_param.x(0)*S87(0,2))*((-(endeff_param.x(1)*Si78(0,0)) + endeff_param.x(0)*Si78(1,0))*T178(0,3) + Si78(2,1)*T178(4,3) + Si78(2,2)*T178(5,3)) + (endeff_param.x(2)*S87(1,0) - endeff_param.x(0)*S87(1,2))*((-(endeff_param.x(1)*Si78(0,1)) + endeff_param.x(0)*Si78(1,1))*T178(1,4) + Si78(2,0)*T178(3,4) + Si78(2,2)*T178(5,4));
T78(5,2) = S87(2,2)*((-(endeff_param.x(1)*Si78(0,0)) + endeff_param.x(0)*Si78(1,0))*T178(0,2) + (-(endeff_param.x(1)*Si78(0,1)) + endeff_param.x(0)*Si78(1,1))*T178(1,2)) + S87(0,2)*((-(endeff_param.x(1)*Si78(0,1)) + endeff_param.x(0)*Si78(1,1))*T178(1,0) + (-(endeff_param.x(1)*Si78(0,2)) + endeff_param.x(0)*Si78(1,2))*T178(2,0)) + S87(1,2)*((-(endeff_param.x(1)*Si78(0,0)) + endeff_param.x(0)*Si78(1,0))*T178(0,1) + (-(endeff_param.x(1)*Si78(0,2)) + endeff_param.x(0)*Si78(1,2))*T178(2,1)) + (-(endeff_param.x(1)*S87(2,0)) + endeff_param.x(0)*S87(2,1))*((-(endeff_param.x(1)*Si78(0,2)) + endeff_param.x(0)*Si78(1,2))*T178(2,5) + Si78(2,0)*T178(3,5) + Si78(2,1)*T178(4,5)) + (-(endeff_param.x(1)*S87(0,0)) + endeff_param.x(0)*S87(0,1))*((-(endeff_param.x(1)*Si78(0,0)) + endeff_param.x(0)*Si78(1,0))*T178(0,3) + Si78(2,1)*T178(4,3) + Si78(2,2)*T178(5,3)) + (-(endeff_param.x(1)*S87(1,0)) + endeff_param.x(0)*S87(1,1))*((-(endeff_param.x(1)*Si78(0,1)) + endeff_param.x(0)*Si78(1,1))*T178(1,4) + Si78(2,0)*T178(3,4) + Si78(2,2)*T178(5,4));
T78(5,3) = S87(2,0)*((-(endeff_param.x(1)*Si78(0,2)) + endeff_param.x(0)*Si78(1,2))*T178(2,5) + Si78(2,0)*T178(3,5) + Si78(2,1)*T178(4,5)) + S87(0,0)*((-(endeff_param.x(1)*Si78(0,0)) + endeff_param.x(0)*Si78(1,0))*T178(0,3) + Si78(2,1)*T178(4,3) + Si78(2,2)*T178(5,3)) + S87(1,0)*((-(endeff_param.x(1)*Si78(0,1)) + endeff_param.x(0)*Si78(1,1))*T178(1,4) + Si78(2,0)*T178(3,4) + Si78(2,2)*T178(5,4));
T78(5,4) = S87(2,1)*((-(endeff_param.x(1)*Si78(0,2)) + endeff_param.x(0)*Si78(1,2))*T178(2,5) + Si78(2,0)*T178(3,5) + Si78(2,1)*T178(4,5)) + S87(0,1)*((-(endeff_param.x(1)*Si78(0,0)) + endeff_param.x(0)*Si78(1,0))*T178(0,3) + Si78(2,1)*T178(4,3) + Si78(2,2)*T178(5,3)) + S87(1,1)*((-(endeff_param.x(1)*Si78(0,1)) + endeff_param.x(0)*Si78(1,1))*T178(1,4) + Si78(2,0)*T178(3,4) + Si78(2,2)*T178(5,4));
T78(5,5) = S87(2,2)*((-(endeff_param.x(1)*Si78(0,2)) + endeff_param.x(0)*Si78(1,2))*T178(2,5) + Si78(2,0)*T178(3,5) + Si78(2,1)*T178(4,5)) + S87(0,2)*((-(endeff_param.x(1)*Si78(0,0)) + endeff_param.x(0)*Si78(1,0))*T178(0,3) + Si78(2,1)*T178(4,3) + Si78(2,2)*T178(5,3)) + S87(1,2)*((-(endeff_param.x(1)*Si78(0,1)) + endeff_param.x(0)*Si78(1,1))*T178(1,4) + Si78(2,0)*T178(3,4) + Si78(2,2)*T178(5,4));


//wam_ForDynArtfunc8

JA7(0,0) = T78(0,0);
JA7(0,1) = link_param[7].mcm(2) + T78(0,1);
JA7(0,2) = -link_param[7].mcm(1) + T78(0,2);
JA7(0,3) = link_param[7].mass+ T78(0,3);
JA7(0,4) = T78(0,4);
JA7(0,5) = T78(0,5);

JA7(1,0) = -link_param[7].mcm(2) + T78(1,0);
JA7(1,1) = T78(1,1);
JA7(1,2) = link_param[7].mcm(0) + T78(1,2);
JA7(1,3) = T78(1,3);
JA7(1,4) = link_param[7].mass+ T78(1,4);
JA7(1,5) = T78(1,5);

JA7(2,0) = link_param[7].mcm(1) + T78(2,0);
JA7(2,1) = -link_param[7].mcm(0) + T78(2,1);
JA7(2,2) = T78(2,2);
JA7(2,3) = T78(2,3);
JA7(2,4) = T78(2,4);
JA7(2,5) = link_param[7].mass+ T78(2,5);

JA7(3,0) = link_param[7].inertia(0,0) + T78(3,0);
JA7(3,1) = link_param[7].inertia(0,1) + T78(3,1);
JA7(3,2) = link_param[7].inertia(0,2) + T78(3,2);
JA7(3,3) = T78(3,3);
JA7(3,4) = -link_param[7].mcm(2) + T78(3,4);
JA7(3,5) = link_param[7].mcm(1) + T78(3,5);

JA7(4,0) = link_param[7].inertia(0,1) + T78(4,0);
JA7(4,1) = link_param[7].inertia(1,1) + T78(4,1);
JA7(4,2) = link_param[7].inertia(1,2) + T78(4,2);
JA7(4,3) = link_param[7].mcm(2) + T78(4,3);
JA7(4,4) = T78(4,4);
JA7(4,5) = -link_param[7].mcm(0) + T78(4,5);

JA7(5,0) = link_param[7].inertia(0,2) + T78(5,0);
JA7(5,1) = link_param[7].inertia(1,2) + T78(5,1);
JA7(5,2) = link_param[7].inertia(2,2) + T78(5,2);
JA7(5,3) = -link_param[7].mcm(1) + T78(5,3);
JA7(5,4) = link_param[7].mcm(0) + T78(5,4);
JA7(5,5) = T78(5,5);


h7(0) = JA7(0,2);
h7(1) = JA7(1,2);
h7(2) = JA7(2,2);
h7(3) = JA7(3,2);
h7(4) = JA7(4,2);
h7(5) = JA7(5,2);

d(6) = 1.e-10 + h7(5);

T167(0,0) = -((h7(0)*h7(3))/d(6)) + JA7(0,0);
T167(0,1) = -((h7(0)*h7(4))/d(6)) + JA7(0,1);
T167(0,2) = -((h7(0)*h7(5))/d(6)) + JA7(0,2);
T167(0,3) = -(pow(h7(0),2)/d(6)) + JA7(0,3);
T167(0,4) = -((h7(0)*h7(1))/d(6)) + JA7(0,4);
T167(0,5) = -((h7(0)*h7(2))/d(6)) + JA7(0,5);

T167(1,0) = -((h7(1)*h7(3))/d(6)) + JA7(1,0);
T167(1,1) = -((h7(1)*h7(4))/d(6)) + JA7(1,1);
T167(1,2) = -((h7(1)*h7(5))/d(6)) + JA7(1,2);
T167(1,3) = -((h7(0)*h7(1))/d(6)) + JA7(1,3);
T167(1,4) = -(pow(h7(1),2)/d(6)) + JA7(1,4);
T167(1,5) = -((h7(1)*h7(2))/d(6)) + JA7(1,5);

T167(2,0) = -((h7(2)*h7(3))/d(6)) + JA7(2,0);
T167(2,1) = -((h7(2)*h7(4))/d(6)) + JA7(2,1);
T167(2,2) = -((h7(2)*h7(5))/d(6)) + JA7(2,2);
T167(2,3) = -((h7(0)*h7(2))/d(6)) + JA7(2,3);
T167(2,4) = -((h7(1)*h7(2))/d(6)) + JA7(2,4);
T167(2,5) = -(pow(h7(2),2)/d(6)) + JA7(2,5);

T167(3,0) = -(pow(h7(3),2)/d(6)) + JA7(3,0);
T167(3,1) = -((h7(3)*h7(4))/d(6)) + JA7(3,1);
T167(3,2) = -((h7(3)*h7(5))/d(6)) + JA7(3,2);
T167(3,3) = -((h7(0)*h7(3))/d(6)) + JA7(3,3);
T167(3,4) = -((h7(1)*h7(3))/d(6)) + JA7(3,4);
T167(3,5) = -((h7(2)*h7(3))/d(6)) + JA7(3,5);

T167(4,0) = -((h7(3)*h7(4))/d(6)) + JA7(4,0);
T167(4,1) = -(pow(h7(4),2)/d(6)) + JA7(4,1);
T167(4,2) = -((h7(4)*h7(5))/d(6)) + JA7(4,2);
T167(4,3) = -((h7(0)*h7(4))/d(6)) + JA7(4,3);
T167(4,4) = -((h7(1)*h7(4))/d(6)) + JA7(4,4);
T167(4,5) = -((h7(2)*h7(4))/d(6)) + JA7(4,5);

T167(5,0) = -((h7(3)*h7(5))/d(6)) + JA7(5,0);
T167(5,1) = -((h7(4)*h7(5))/d(6)) + JA7(5,1);
T167(5,2) = -(pow(h7(5),2)/d(6)) + JA7(5,2);
T167(5,3) = -((h7(0)*h7(5))/d(6)) + JA7(5,3);
T167(5,4) = -((h7(1)*h7(5))/d(6)) + JA7(5,4);
T167(5,5) = -((h7(2)*h7(5))/d(6)) + JA7(5,5);


T67(0,0) = T167(2,2);
T67(0,1) = S76(0,1)*T167(2,0) + S76(1,1)*T167(2,1);
T67(0,2) = S76(0,2)*T167(2,0) + S76(1,2)*T167(2,1);
T67(0,3) = T167(2,5);
T67(0,4) = S76(0,1)*T167(2,3) + S76(1,1)*T167(2,4);
T67(0,5) = S76(0,2)*T167(2,3) + S76(1,2)*T167(2,4);

T67(1,0) = Si67(1,0)*T167(0,2) + Si67(1,1)*T167(1,2);
T67(1,1) = S76(0,1)*(Si67(1,0)*T167(0,0) + Si67(1,1)*T167(1,0)) + S76(1,1)*(Si67(1,0)*T167(0,1) + Si67(1,1)*T167(1,1));
T67(1,2) = S76(0,2)*(Si67(1,0)*T167(0,0) + Si67(1,1)*T167(1,0)) + S76(1,2)*(Si67(1,0)*T167(0,1) + Si67(1,1)*T167(1,1));
T67(1,3) = Si67(1,0)*T167(0,5) + Si67(1,1)*T167(1,5);
T67(1,4) = S76(0,1)*(Si67(1,0)*T167(0,3) + Si67(1,1)*T167(1,3)) + S76(1,1)*(Si67(1,0)*T167(0,4) + Si67(1,1)*T167(1,4));
T67(1,5) = S76(0,2)*(Si67(1,0)*T167(0,3) + Si67(1,1)*T167(1,3)) + S76(1,2)*(Si67(1,0)*T167(0,4) + Si67(1,1)*T167(1,4));

T67(2,0) = Si67(2,0)*T167(0,2) + Si67(2,1)*T167(1,2);
T67(2,1) = S76(0,1)*(Si67(2,0)*T167(0,0) + Si67(2,1)*T167(1,0)) + S76(1,1)*(Si67(2,0)*T167(0,1) + Si67(2,1)*T167(1,1));
T67(2,2) = S76(0,2)*(Si67(2,0)*T167(0,0) + Si67(2,1)*T167(1,0)) + S76(1,2)*(Si67(2,0)*T167(0,1) + Si67(2,1)*T167(1,1));
T67(2,3) = Si67(2,0)*T167(0,5) + Si67(2,1)*T167(1,5);
T67(2,4) = S76(0,1)*(Si67(2,0)*T167(0,3) + Si67(2,1)*T167(1,3)) + S76(1,1)*(Si67(2,0)*T167(0,4) + Si67(2,1)*T167(1,4));
T67(2,5) = S76(0,2)*(Si67(2,0)*T167(0,3) + Si67(2,1)*T167(1,3)) + S76(1,2)*(Si67(2,0)*T167(0,4) + Si67(2,1)*T167(1,4));

T67(3,0) = T167(5,2);
T67(3,1) = S76(0,1)*T167(5,0) + S76(1,1)*T167(5,1);
T67(3,2) = S76(0,2)*T167(5,0) + S76(1,2)*T167(5,1);
T67(3,3) = T167(5,5);
T67(3,4) = S76(0,1)*T167(5,3) + S76(1,1)*T167(5,4);
T67(3,5) = S76(0,2)*T167(5,3) + S76(1,2)*T167(5,4);

T67(4,0) = Si67(1,0)*T167(3,2) + Si67(1,1)*T167(4,2);
T67(4,1) = S76(0,1)*(Si67(1,0)*T167(3,0) + Si67(1,1)*T167(4,0)) + S76(1,1)*(Si67(1,0)*T167(3,1) + Si67(1,1)*T167(4,1));
T67(4,2) = S76(0,2)*(Si67(1,0)*T167(3,0) + Si67(1,1)*T167(4,0)) + S76(1,2)*(Si67(1,0)*T167(3,1) + Si67(1,1)*T167(4,1));
T67(4,3) = Si67(1,0)*T167(3,5) + Si67(1,1)*T167(4,5);
T67(4,4) = S76(0,1)*(Si67(1,0)*T167(3,3) + Si67(1,1)*T167(4,3)) + S76(1,1)*(Si67(1,0)*T167(3,4) + Si67(1,1)*T167(4,4));
T67(4,5) = S76(0,2)*(Si67(1,0)*T167(3,3) + Si67(1,1)*T167(4,3)) + S76(1,2)*(Si67(1,0)*T167(3,4) + Si67(1,1)*T167(4,4));

T67(5,0) = Si67(2,0)*T167(3,2) + Si67(2,1)*T167(4,2);
T67(5,1) = S76(0,1)*(Si67(2,0)*T167(3,0) + Si67(2,1)*T167(4,0)) + S76(1,1)*(Si67(2,0)*T167(3,1) + Si67(2,1)*T167(4,1));
T67(5,2) = S76(0,2)*(Si67(2,0)*T167(3,0) + Si67(2,1)*T167(4,0)) + S76(1,2)*(Si67(2,0)*T167(3,1) + Si67(2,1)*T167(4,1));
T67(5,3) = Si67(2,0)*T167(3,5) + Si67(2,1)*T167(4,5);
T67(5,4) = S76(0,1)*(Si67(2,0)*T167(3,3) + Si67(2,1)*T167(4,3)) + S76(1,1)*(Si67(2,0)*T167(3,4) + Si67(2,1)*T167(4,4));
T67(5,5) = S76(0,2)*(Si67(2,0)*T167(3,3) + Si67(2,1)*T167(4,3)) + S76(1,2)*(Si67(2,0)*T167(3,4) + Si67(2,1)*T167(4,4));


//wam_ForDynArtfunc9

JA6(0,0) = T67(0,0);
JA6(0,1) = link_param[6].mcm(2) + T67(0,1);
JA6(0,2) = -link_param[6].mcm(1) + T67(0,2);
JA6(0,3) = link_param[6].mass+ T67(0,3);
JA6(0,4) = T67(0,4);
JA6(0,5) = T67(0,5);

JA6(1,0) = -link_param[6].mcm(2) + T67(1,0);
JA6(1,1) = T67(1,1);
JA6(1,2) = link_param[6].mcm(0) + T67(1,2);
JA6(1,3) = T67(1,3);
JA6(1,4) = link_param[6].mass+ T67(1,4);
JA6(1,5) = T67(1,5);

JA6(2,0) = link_param[6].mcm(1) + T67(2,0);
JA6(2,1) = -link_param[6].mcm(0) + T67(2,1);
JA6(2,2) = T67(2,2);
JA6(2,3) = T67(2,3);
JA6(2,4) = T67(2,4);
JA6(2,5) = link_param[6].mass+ T67(2,5);

JA6(3,0) = link_param[6].inertia(0,0) + T67(3,0);
JA6(3,1) = link_param[6].inertia(0,1) + T67(3,1);
JA6(3,2) = link_param[6].inertia(0,2) + T67(3,2);
JA6(3,3) = T67(3,3);
JA6(3,4) = -link_param[6].mcm(2) + T67(3,4);
JA6(3,5) = link_param[6].mcm(1) + T67(3,5);

JA6(4,0) = link_param[6].inertia(0,1) + T67(4,0);
JA6(4,1) = link_param[6].inertia(1,1) + T67(4,1);
JA6(4,2) = link_param[6].inertia(1,2) + T67(4,2);
JA6(4,3) = link_param[6].mcm(2) + T67(4,3);
JA6(4,4) = T67(4,4);
JA6(4,5) = -link_param[6].mcm(0) + T67(4,5);

JA6(5,0) = link_param[6].inertia(0,2) + T67(5,0);
JA6(5,1) = link_param[6].inertia(1,2) + T67(5,1);
JA6(5,2) = link_param[6].inertia(2,2) + T67(5,2);
JA6(5,3) = -link_param[6].mcm(1) + T67(5,3);
JA6(5,4) = link_param[6].mcm(0) + T67(5,4);
JA6(5,5) = T67(5,5);


h6(0) = JA6(0,2);
h6(1) = JA6(1,2);
h6(2) = JA6(2,2);
h6(3) = JA6(3,2);
h6(4) = JA6(4,2);
h6(5) = JA6(5,2);

d(5) = 1.e-10 + h6(5);

T156(0,0) = -((h6(0)*h6(3))/d(5)) + JA6(0,0);
T156(0,1) = -((h6(0)*h6(4))/d(5)) + JA6(0,1);
T156(0,2) = -((h6(0)*h6(5))/d(5)) + JA6(0,2);
T156(0,3) = -(pow(h6(0),2)/d(5)) + JA6(0,3);
T156(0,4) = -((h6(0)*h6(1))/d(5)) + JA6(0,4);
T156(0,5) = -((h6(0)*h6(2))/d(5)) + JA6(0,5);

T156(1,0) = -((h6(1)*h6(3))/d(5)) + JA6(1,0);
T156(1,1) = -((h6(1)*h6(4))/d(5)) + JA6(1,1);
T156(1,2) = -((h6(1)*h6(5))/d(5)) + JA6(1,2);
T156(1,3) = -((h6(0)*h6(1))/d(5)) + JA6(1,3);
T156(1,4) = -(pow(h6(1),2)/d(5)) + JA6(1,4);
T156(1,5) = -((h6(1)*h6(2))/d(5)) + JA6(1,5);

T156(2,0) = -((h6(2)*h6(3))/d(5)) + JA6(2,0);
T156(2,1) = -((h6(2)*h6(4))/d(5)) + JA6(2,1);
T156(2,2) = -((h6(2)*h6(5))/d(5)) + JA6(2,2);
T156(2,3) = -((h6(0)*h6(2))/d(5)) + JA6(2,3);
T156(2,4) = -((h6(1)*h6(2))/d(5)) + JA6(2,4);
T156(2,5) = -(pow(h6(2),2)/d(5)) + JA6(2,5);

T156(3,0) = -(pow(h6(3),2)/d(5)) + JA6(3,0);
T156(3,1) = -((h6(3)*h6(4))/d(5)) + JA6(3,1);
T156(3,2) = -((h6(3)*h6(5))/d(5)) + JA6(3,2);
T156(3,3) = -((h6(0)*h6(3))/d(5)) + JA6(3,3);
T156(3,4) = -((h6(1)*h6(3))/d(5)) + JA6(3,4);
T156(3,5) = -((h6(2)*h6(3))/d(5)) + JA6(3,5);

T156(4,0) = -((h6(3)*h6(4))/d(5)) + JA6(4,0);
T156(4,1) = -(pow(h6(4),2)/d(5)) + JA6(4,1);
T156(4,2) = -((h6(4)*h6(5))/d(5)) + JA6(4,2);
T156(4,3) = -((h6(0)*h6(4))/d(5)) + JA6(4,3);
T156(4,4) = -((h6(1)*h6(4))/d(5)) + JA6(4,4);
T156(4,5) = -((h6(2)*h6(4))/d(5)) + JA6(4,5);

T156(5,0) = -((h6(3)*h6(5))/d(5)) + JA6(5,0);
T156(5,1) = -((h6(4)*h6(5))/d(5)) + JA6(5,1);
T156(5,2) = -(pow(h6(5),2)/d(5)) + JA6(5,2);
T156(5,3) = -((h6(0)*h6(5))/d(5)) + JA6(5,3);
T156(5,4) = -((h6(1)*h6(5))/d(5)) + JA6(5,4);
T156(5,5) = -((h6(2)*h6(5))/d(5)) + JA6(5,5);


T56(0,0) = T156(2,2) + ZWFE*S65(0,1)*T156(2,3) + ZWFE*S65(1,1)*T156(2,4);
T56(0,1) = -(S65(0,1)*T156(2,0)) - S65(1,1)*T156(2,1) + ZWFE*T156(2,5);
T56(0,2) = -(S65(0,2)*T156(2,0)) - S65(1,2)*T156(2,1);
T56(0,3) = T156(2,5);
T56(0,4) = -(S65(0,1)*T156(2,3)) - S65(1,1)*T156(2,4);
T56(0,5) = -(S65(0,2)*T156(2,3)) - S65(1,2)*T156(2,4);

T56(1,0) = -(Si56(1,0)*T156(0,2)) - Si56(1,1)*T156(1,2) - ZWFE*S65(0,1)*(Si56(1,0)*T156(0,3) + Si56(1,1)*T156(1,3)) - ZWFE*S65(1,1)*(Si56(1,0)*T156(0,4) + Si56(1,1)*T156(1,4));
T56(1,1) = S65(0,1)*(Si56(1,0)*T156(0,0) + Si56(1,1)*T156(1,0)) + S65(1,1)*(Si56(1,0)*T156(0,1) + Si56(1,1)*T156(1,1)) - ZWFE*(Si56(1,0)*T156(0,5) + Si56(1,1)*T156(1,5));
T56(1,2) = S65(0,2)*(Si56(1,0)*T156(0,0) + Si56(1,1)*T156(1,0)) + S65(1,2)*(Si56(1,0)*T156(0,1) + Si56(1,1)*T156(1,1));
T56(1,3) = -(Si56(1,0)*T156(0,5)) - Si56(1,1)*T156(1,5);
T56(1,4) = S65(0,1)*(Si56(1,0)*T156(0,3) + Si56(1,1)*T156(1,3)) + S65(1,1)*(Si56(1,0)*T156(0,4) + Si56(1,1)*T156(1,4));
T56(1,5) = S65(0,2)*(Si56(1,0)*T156(0,3) + Si56(1,1)*T156(1,3)) + S65(1,2)*(Si56(1,0)*T156(0,4) + Si56(1,1)*T156(1,4));

T56(2,0) = -(Si56(2,0)*T156(0,2)) - Si56(2,1)*T156(1,2) - ZWFE*S65(0,1)*(Si56(2,0)*T156(0,3) + Si56(2,1)*T156(1,3)) - ZWFE*S65(1,1)*(Si56(2,0)*T156(0,4) + Si56(2,1)*T156(1,4));
T56(2,1) = S65(0,1)*(Si56(2,0)*T156(0,0) + Si56(2,1)*T156(1,0)) + S65(1,1)*(Si56(2,0)*T156(0,1) + Si56(2,1)*T156(1,1)) - ZWFE*(Si56(2,0)*T156(0,5) + Si56(2,1)*T156(1,5));
T56(2,2) = S65(0,2)*(Si56(2,0)*T156(0,0) + Si56(2,1)*T156(1,0)) + S65(1,2)*(Si56(2,0)*T156(0,1) + Si56(2,1)*T156(1,1));
T56(2,3) = -(Si56(2,0)*T156(0,5)) - Si56(2,1)*T156(1,5);
T56(2,4) = S65(0,1)*(Si56(2,0)*T156(0,3) + Si56(2,1)*T156(1,3)) + S65(1,1)*(Si56(2,0)*T156(0,4) + Si56(2,1)*T156(1,4));
T56(2,5) = S65(0,2)*(Si56(2,0)*T156(0,3) + Si56(2,1)*T156(1,3)) + S65(1,2)*(Si56(2,0)*T156(0,4) + Si56(2,1)*T156(1,4));

T56(3,0) = ZWFE*Si56(1,0)*T156(0,2) + ZWFE*Si56(1,1)*T156(1,2) + T156(5,2) - ZWFE*S65(0,1)*(-(ZWFE*Si56(1,0)*T156(0,3)) - ZWFE*Si56(1,1)*T156(1,3) - T156(5,3)) - ZWFE*S65(1,1)*(-(ZWFE*Si56(1,0)*T156(0,4)) - ZWFE*Si56(1,1)*T156(1,4) - T156(5,4));
T56(3,1) = S65(0,1)*(-(ZWFE*Si56(1,0)*T156(0,0)) - ZWFE*Si56(1,1)*T156(1,0) - T156(5,0)) + S65(1,1)*(-(ZWFE*Si56(1,0)*T156(0,1)) - ZWFE*Si56(1,1)*T156(1,1) - T156(5,1)) - ZWFE*(-(ZWFE*Si56(1,0)*T156(0,5)) - ZWFE*Si56(1,1)*T156(1,5) - T156(5,5));
T56(3,2) = S65(0,2)*(-(ZWFE*Si56(1,0)*T156(0,0)) - ZWFE*Si56(1,1)*T156(1,0) - T156(5,0)) + S65(1,2)*(-(ZWFE*Si56(1,0)*T156(0,1)) - ZWFE*Si56(1,1)*T156(1,1) - T156(5,1));
T56(3,3) = ZWFE*Si56(1,0)*T156(0,5) + ZWFE*Si56(1,1)*T156(1,5) + T156(5,5);
T56(3,4) = S65(0,1)*(-(ZWFE*Si56(1,0)*T156(0,3)) - ZWFE*Si56(1,1)*T156(1,3) - T156(5,3)) + S65(1,1)*(-(ZWFE*Si56(1,0)*T156(0,4)) - ZWFE*Si56(1,1)*T156(1,4) - T156(5,4));
T56(3,5) = S65(0,2)*(-(ZWFE*Si56(1,0)*T156(0,3)) - ZWFE*Si56(1,1)*T156(1,3) - T156(5,3)) + S65(1,2)*(-(ZWFE*Si56(1,0)*T156(0,4)) - ZWFE*Si56(1,1)*T156(1,4) - T156(5,4));

T56(4,0) = ZWFE*T156(2,2) - Si56(1,0)*T156(3,2) - Si56(1,1)*T156(4,2) - ZWFE*S65(0,1)*(-(ZWFE*T156(2,3)) + Si56(1,0)*T156(3,3) + Si56(1,1)*T156(4,3)) - ZWFE*S65(1,1)*(-(ZWFE*T156(2,4)) + Si56(1,0)*T156(3,4) + Si56(1,1)*T156(4,4));
T56(4,1) = S65(0,1)*(-(ZWFE*T156(2,0)) + Si56(1,0)*T156(3,0) + Si56(1,1)*T156(4,0)) + S65(1,1)*(-(ZWFE*T156(2,1)) + Si56(1,0)*T156(3,1) + Si56(1,1)*T156(4,1)) - ZWFE*(-(ZWFE*T156(2,5)) + Si56(1,0)*T156(3,5) + Si56(1,1)*T156(4,5));
T56(4,2) = S65(0,2)*(-(ZWFE*T156(2,0)) + Si56(1,0)*T156(3,0) + Si56(1,1)*T156(4,0)) + S65(1,2)*(-(ZWFE*T156(2,1)) + Si56(1,0)*T156(3,1) + Si56(1,1)*T156(4,1));
T56(4,3) = ZWFE*T156(2,5) - Si56(1,0)*T156(3,5) - Si56(1,1)*T156(4,5);
T56(4,4) = S65(0,1)*(-(ZWFE*T156(2,3)) + Si56(1,0)*T156(3,3) + Si56(1,1)*T156(4,3)) + S65(1,1)*(-(ZWFE*T156(2,4)) + Si56(1,0)*T156(3,4) + Si56(1,1)*T156(4,4));
T56(4,5) = S65(0,2)*(-(ZWFE*T156(2,3)) + Si56(1,0)*T156(3,3) + Si56(1,1)*T156(4,3)) + S65(1,2)*(-(ZWFE*T156(2,4)) + Si56(1,0)*T156(3,4) + Si56(1,1)*T156(4,4));

T56(5,0) = -(Si56(2,0)*T156(3,2)) - Si56(2,1)*T156(4,2) - ZWFE*S65(0,1)*(Si56(2,0)*T156(3,3) + Si56(2,1)*T156(4,3)) - ZWFE*S65(1,1)*(Si56(2,0)*T156(3,4) + Si56(2,1)*T156(4,4));
T56(5,1) = S65(0,1)*(Si56(2,0)*T156(3,0) + Si56(2,1)*T156(4,0)) + S65(1,1)*(Si56(2,0)*T156(3,1) + Si56(2,1)*T156(4,1)) - ZWFE*(Si56(2,0)*T156(3,5) + Si56(2,1)*T156(4,5));
T56(5,2) = S65(0,2)*(Si56(2,0)*T156(3,0) + Si56(2,1)*T156(4,0)) + S65(1,2)*(Si56(2,0)*T156(3,1) + Si56(2,1)*T156(4,1));
T56(5,3) = -(Si56(2,0)*T156(3,5)) - Si56(2,1)*T156(4,5);
T56(5,4) = S65(0,1)*(Si56(2,0)*T156(3,3) + Si56(2,1)*T156(4,3)) + S65(1,1)*(Si56(2,0)*T156(3,4) + Si56(2,1)*T156(4,4));
T56(5,5) = S65(0,2)*(Si56(2,0)*T156(3,3) + Si56(2,1)*T156(4,3)) + S65(1,2)*(Si56(2,0)*T156(3,4) + Si56(2,1)*T156(4,4));


//wam_ForDynArtfunc10

JA5(0,0) = T56(0,0);
JA5(0,1) = link_param[5].mcm(2) + T56(0,1);
JA5(0,2) = -link_param[5].mcm(1) + T56(0,2);
JA5(0,3) = link_param[5].mass+ T56(0,3);
JA5(0,4) = T56(0,4);
JA5(0,5) = T56(0,5);

JA5(1,0) = -link_param[5].mcm(2) + T56(1,0);
JA5(1,1) = T56(1,1);
JA5(1,2) = link_param[5].mcm(0) + T56(1,2);
JA5(1,3) = T56(1,3);
JA5(1,4) = link_param[5].mass+ T56(1,4);
JA5(1,5) = T56(1,5);

JA5(2,0) = link_param[5].mcm(1) + T56(2,0);
JA5(2,1) = -link_param[5].mcm(0) + T56(2,1);
JA5(2,2) = T56(2,2);
JA5(2,3) = T56(2,3);
JA5(2,4) = T56(2,4);
JA5(2,5) = link_param[5].mass+ T56(2,5);

JA5(3,0) = link_param[5].inertia(0,0) + T56(3,0);
JA5(3,1) = link_param[5].inertia(0,1) + T56(3,1);
JA5(3,2) = link_param[5].inertia(0,2) + T56(3,2);
JA5(3,3) = T56(3,3);
JA5(3,4) = -link_param[5].mcm(2) + T56(3,4);
JA5(3,5) = link_param[5].mcm(1) + T56(3,5);

JA5(4,0) = link_param[5].inertia(0,1) + T56(4,0);
JA5(4,1) = link_param[5].inertia(1,1) + T56(4,1);
JA5(4,2) = link_param[5].inertia(1,2) + T56(4,2);
JA5(4,3) = link_param[5].mcm(2) + T56(4,3);
JA5(4,4) = T56(4,4);
JA5(4,5) = -link_param[5].mcm(0) + T56(4,5);

JA5(5,0) = link_param[5].inertia(0,2) + T56(5,0);
JA5(5,1) = link_param[5].inertia(1,2) + T56(5,1);
JA5(5,2) = link_param[5].inertia(2,2) + T56(5,2);
JA5(5,3) = -link_param[5].mcm(1) + T56(5,3);
JA5(5,4) = link_param[5].mcm(0) + T56(5,4);
JA5(5,5) = T56(5,5);


h5(0) = JA5(0,2);
h5(1) = JA5(1,2);
h5(2) = JA5(2,2);
h5(3) = JA5(3,2);
h5(4) = JA5(4,2);
h5(5) = JA5(5,2);

d(4) = 1.e-10 + h5(5);

T145(0,0) = -((h5(0)*h5(3))/d(4)) + JA5(0,0);
T145(0,1) = -((h5(0)*h5(4))/d(4)) + JA5(0,1);
T145(0,2) = -((h5(0)*h5(5))/d(4)) + JA5(0,2);
T145(0,3) = -(pow(h5(0),2)/d(4)) + JA5(0,3);
T145(0,4) = -((h5(0)*h5(1))/d(4)) + JA5(0,4);
T145(0,5) = -((h5(0)*h5(2))/d(4)) + JA5(0,5);

T145(1,0) = -((h5(1)*h5(3))/d(4)) + JA5(1,0);
T145(1,1) = -((h5(1)*h5(4))/d(4)) + JA5(1,1);
T145(1,2) = -((h5(1)*h5(5))/d(4)) + JA5(1,2);
T145(1,3) = -((h5(0)*h5(1))/d(4)) + JA5(1,3);
T145(1,4) = -(pow(h5(1),2)/d(4)) + JA5(1,4);
T145(1,5) = -((h5(1)*h5(2))/d(4)) + JA5(1,5);

T145(2,0) = -((h5(2)*h5(3))/d(4)) + JA5(2,0);
T145(2,1) = -((h5(2)*h5(4))/d(4)) + JA5(2,1);
T145(2,2) = -((h5(2)*h5(5))/d(4)) + JA5(2,2);
T145(2,3) = -((h5(0)*h5(2))/d(4)) + JA5(2,3);
T145(2,4) = -((h5(1)*h5(2))/d(4)) + JA5(2,4);
T145(2,5) = -(pow(h5(2),2)/d(4)) + JA5(2,5);

T145(3,0) = -(pow(h5(3),2)/d(4)) + JA5(3,0);
T145(3,1) = -((h5(3)*h5(4))/d(4)) + JA5(3,1);
T145(3,2) = -((h5(3)*h5(5))/d(4)) + JA5(3,2);
T145(3,3) = -((h5(0)*h5(3))/d(4)) + JA5(3,3);
T145(3,4) = -((h5(1)*h5(3))/d(4)) + JA5(3,4);
T145(3,5) = -((h5(2)*h5(3))/d(4)) + JA5(3,5);

T145(4,0) = -((h5(3)*h5(4))/d(4)) + JA5(4,0);
T145(4,1) = -(pow(h5(4),2)/d(4)) + JA5(4,1);
T145(4,2) = -((h5(4)*h5(5))/d(4)) + JA5(4,2);
T145(4,3) = -((h5(0)*h5(4))/d(4)) + JA5(4,3);
T145(4,4) = -((h5(1)*h5(4))/d(4)) + JA5(4,4);
T145(4,5) = -((h5(2)*h5(4))/d(4)) + JA5(4,5);

T145(5,0) = -((h5(3)*h5(5))/d(4)) + JA5(5,0);
T145(5,1) = -((h5(4)*h5(5))/d(4)) + JA5(5,1);
T145(5,2) = -(pow(h5(5),2)/d(4)) + JA5(5,2);
T145(5,3) = -((h5(0)*h5(5))/d(4)) + JA5(5,3);
T145(5,4) = -((h5(1)*h5(5))/d(4)) + JA5(5,4);
T145(5,5) = -((h5(2)*h5(5))/d(4)) + JA5(5,5);


T45(0,0) = T145(2,2) + YWR*S54(0,2)*T145(2,3) + YWR*S54(1,2)*T145(2,4);
T45(0,1) = S54(0,1)*T145(2,0) + S54(1,1)*T145(2,1) - ZWR*S54(0,2)*T145(2,3) - ZWR*S54(1,2)*T145(2,4);
T45(0,2) = S54(0,2)*T145(2,0) + S54(1,2)*T145(2,1) + ZWR*S54(0,1)*T145(2,3) + ZWR*S54(1,1)*T145(2,4) - YWR*T145(2,5);
T45(0,3) = T145(2,5);
T45(0,4) = S54(0,1)*T145(2,3) + S54(1,1)*T145(2,4);
T45(0,5) = S54(0,2)*T145(2,3) + S54(1,2)*T145(2,4);

T45(1,0) = Si45(1,0)*T145(0,2) + Si45(1,1)*T145(1,2) + YWR*S54(0,2)*(Si45(1,0)*T145(0,3) + Si45(1,1)*T145(1,3)) + YWR*S54(1,2)*(Si45(1,0)*T145(0,4) + Si45(1,1)*T145(1,4));
T45(1,1) = S54(0,1)*(Si45(1,0)*T145(0,0) + Si45(1,1)*T145(1,0)) + S54(1,1)*(Si45(1,0)*T145(0,1) + Si45(1,1)*T145(1,1)) - ZWR*S54(0,2)*(Si45(1,0)*T145(0,3) + Si45(1,1)*T145(1,3)) - ZWR*S54(1,2)*(Si45(1,0)*T145(0,4) + Si45(1,1)*T145(1,4));
T45(1,2) = S54(0,2)*(Si45(1,0)*T145(0,0) + Si45(1,1)*T145(1,0)) + S54(1,2)*(Si45(1,0)*T145(0,1) + Si45(1,1)*T145(1,1)) + ZWR*S54(0,1)*(Si45(1,0)*T145(0,3) + Si45(1,1)*T145(1,3)) + ZWR*S54(1,1)*(Si45(1,0)*T145(0,4) + Si45(1,1)*T145(1,4)) - YWR*(Si45(1,0)*T145(0,5) + Si45(1,1)*T145(1,5));
T45(1,3) = Si45(1,0)*T145(0,5) + Si45(1,1)*T145(1,5);
T45(1,4) = S54(0,1)*(Si45(1,0)*T145(0,3) + Si45(1,1)*T145(1,3)) + S54(1,1)*(Si45(1,0)*T145(0,4) + Si45(1,1)*T145(1,4));
T45(1,5) = S54(0,2)*(Si45(1,0)*T145(0,3) + Si45(1,1)*T145(1,3)) + S54(1,2)*(Si45(1,0)*T145(0,4) + Si45(1,1)*T145(1,4));

T45(2,0) = Si45(2,0)*T145(0,2) + Si45(2,1)*T145(1,2) + YWR*S54(0,2)*(Si45(2,0)*T145(0,3) + Si45(2,1)*T145(1,3)) + YWR*S54(1,2)*(Si45(2,0)*T145(0,4) + Si45(2,1)*T145(1,4));
T45(2,1) = S54(0,1)*(Si45(2,0)*T145(0,0) + Si45(2,1)*T145(1,0)) + S54(1,1)*(Si45(2,0)*T145(0,1) + Si45(2,1)*T145(1,1)) - ZWR*S54(0,2)*(Si45(2,0)*T145(0,3) + Si45(2,1)*T145(1,3)) - ZWR*S54(1,2)*(Si45(2,0)*T145(0,4) + Si45(2,1)*T145(1,4));
T45(2,2) = S54(0,2)*(Si45(2,0)*T145(0,0) + Si45(2,1)*T145(1,0)) + S54(1,2)*(Si45(2,0)*T145(0,1) + Si45(2,1)*T145(1,1)) + ZWR*S54(0,1)*(Si45(2,0)*T145(0,3) + Si45(2,1)*T145(1,3)) + ZWR*S54(1,1)*(Si45(2,0)*T145(0,4) + Si45(2,1)*T145(1,4)) - YWR*(Si45(2,0)*T145(0,5) + Si45(2,1)*T145(1,5));
T45(2,3) = Si45(2,0)*T145(0,5) + Si45(2,1)*T145(1,5);
T45(2,4) = S54(0,1)*(Si45(2,0)*T145(0,3) + Si45(2,1)*T145(1,3)) + S54(1,1)*(Si45(2,0)*T145(0,4) + Si45(2,1)*T145(1,4));
T45(2,5) = S54(0,2)*(Si45(2,0)*T145(0,3) + Si45(2,1)*T145(1,3)) + S54(1,2)*(Si45(2,0)*T145(0,4) + Si45(2,1)*T145(1,4));

T45(3,0) = YWR*Si45(2,0)*T145(0,2) + YWR*Si45(2,1)*T145(1,2) + T145(5,2) + YWR*S54(0,2)*(YWR*Si45(2,0)*T145(0,3) + YWR*Si45(2,1)*T145(1,3) + T145(5,3)) + YWR*S54(1,2)*(YWR*Si45(2,0)*T145(0,4) + YWR*Si45(2,1)*T145(1,4) + T145(5,4));
T45(3,1) = S54(0,1)*(YWR*Si45(2,0)*T145(0,0) + YWR*Si45(2,1)*T145(1,0) + T145(5,0)) + S54(1,1)*(YWR*Si45(2,0)*T145(0,1) + YWR*Si45(2,1)*T145(1,1) + T145(5,1)) - ZWR*S54(0,2)*(YWR*Si45(2,0)*T145(0,3) + YWR*Si45(2,1)*T145(1,3) + T145(5,3)) - ZWR*S54(1,2)*(YWR*Si45(2,0)*T145(0,4) + YWR*Si45(2,1)*T145(1,4) + T145(5,4));
T45(3,2) = S54(0,2)*(YWR*Si45(2,0)*T145(0,0) + YWR*Si45(2,1)*T145(1,0) + T145(5,0)) + S54(1,2)*(YWR*Si45(2,0)*T145(0,1) + YWR*Si45(2,1)*T145(1,1) + T145(5,1)) + ZWR*S54(0,1)*(YWR*Si45(2,0)*T145(0,3) + YWR*Si45(2,1)*T145(1,3) + T145(5,3)) + ZWR*S54(1,1)*(YWR*Si45(2,0)*T145(0,4) + YWR*Si45(2,1)*T145(1,4) + T145(5,4)) - YWR*(YWR*Si45(2,0)*T145(0,5) + YWR*Si45(2,1)*T145(1,5) + T145(5,5));
T45(3,3) = YWR*Si45(2,0)*T145(0,5) + YWR*Si45(2,1)*T145(1,5) + T145(5,5);
T45(3,4) = S54(0,1)*(YWR*Si45(2,0)*T145(0,3) + YWR*Si45(2,1)*T145(1,3) + T145(5,3)) + S54(1,1)*(YWR*Si45(2,0)*T145(0,4) + YWR*Si45(2,1)*T145(1,4) + T145(5,4));
T45(3,5) = S54(0,2)*(YWR*Si45(2,0)*T145(0,3) + YWR*Si45(2,1)*T145(1,3) + T145(5,3)) + S54(1,2)*(YWR*Si45(2,0)*T145(0,4) + YWR*Si45(2,1)*T145(1,4) + T145(5,4));

T45(4,0) = -(ZWR*Si45(2,0)*T145(0,2)) - ZWR*Si45(2,1)*T145(1,2) + Si45(1,0)*T145(3,2) + Si45(1,1)*T145(4,2) + YWR*S54(0,2)*(-(ZWR*Si45(2,0)*T145(0,3)) - ZWR*Si45(2,1)*T145(1,3) + Si45(1,0)*T145(3,3) + Si45(1,1)*T145(4,3)) + YWR*S54(1,2)*(-(ZWR*Si45(2,0)*T145(0,4)) - ZWR*Si45(2,1)*T145(1,4) + Si45(1,0)*T145(3,4) + Si45(1,1)*T145(4,4));
T45(4,1) = S54(0,1)*(-(ZWR*Si45(2,0)*T145(0,0)) - ZWR*Si45(2,1)*T145(1,0) + Si45(1,0)*T145(3,0) + Si45(1,1)*T145(4,0)) + S54(1,1)*(-(ZWR*Si45(2,0)*T145(0,1)) - ZWR*Si45(2,1)*T145(1,1) + Si45(1,0)*T145(3,1) + Si45(1,1)*T145(4,1)) - ZWR*S54(0,2)*(-(ZWR*Si45(2,0)*T145(0,3)) - ZWR*Si45(2,1)*T145(1,3) + Si45(1,0)*T145(3,3) + Si45(1,1)*T145(4,3)) - ZWR*S54(1,2)*(-(ZWR*Si45(2,0)*T145(0,4)) - ZWR*Si45(2,1)*T145(1,4) + Si45(1,0)*T145(3,4) + Si45(1,1)*T145(4,4));
T45(4,2) = S54(0,2)*(-(ZWR*Si45(2,0)*T145(0,0)) - ZWR*Si45(2,1)*T145(1,0) + Si45(1,0)*T145(3,0) + Si45(1,1)*T145(4,0)) + S54(1,2)*(-(ZWR*Si45(2,0)*T145(0,1)) - ZWR*Si45(2,1)*T145(1,1) + Si45(1,0)*T145(3,1) + Si45(1,1)*T145(4,1)) + ZWR*S54(0,1)*(-(ZWR*Si45(2,0)*T145(0,3)) - ZWR*Si45(2,1)*T145(1,3) + Si45(1,0)*T145(3,3) + Si45(1,1)*T145(4,3)) + ZWR*S54(1,1)*(-(ZWR*Si45(2,0)*T145(0,4)) - ZWR*Si45(2,1)*T145(1,4) + Si45(1,0)*T145(3,4) + Si45(1,1)*T145(4,4)) - YWR*(-(ZWR*Si45(2,0)*T145(0,5)) - ZWR*Si45(2,1)*T145(1,5) + Si45(1,0)*T145(3,5) + Si45(1,1)*T145(4,5));
T45(4,3) = -(ZWR*Si45(2,0)*T145(0,5)) - ZWR*Si45(2,1)*T145(1,5) + Si45(1,0)*T145(3,5) + Si45(1,1)*T145(4,5);
T45(4,4) = S54(0,1)*(-(ZWR*Si45(2,0)*T145(0,3)) - ZWR*Si45(2,1)*T145(1,3) + Si45(1,0)*T145(3,3) + Si45(1,1)*T145(4,3)) + S54(1,1)*(-(ZWR*Si45(2,0)*T145(0,4)) - ZWR*Si45(2,1)*T145(1,4) + Si45(1,0)*T145(3,4) + Si45(1,1)*T145(4,4));
T45(4,5) = S54(0,2)*(-(ZWR*Si45(2,0)*T145(0,3)) - ZWR*Si45(2,1)*T145(1,3) + Si45(1,0)*T145(3,3) + Si45(1,1)*T145(4,3)) + S54(1,2)*(-(ZWR*Si45(2,0)*T145(0,4)) - ZWR*Si45(2,1)*T145(1,4) + Si45(1,0)*T145(3,4) + Si45(1,1)*T145(4,4));

T45(5,0) = ZWR*Si45(1,0)*T145(0,2) + ZWR*Si45(1,1)*T145(1,2) - YWR*T145(2,2) + Si45(2,0)*T145(3,2) + Si45(2,1)*T145(4,2) + YWR*S54(0,2)*(ZWR*Si45(1,0)*T145(0,3) + ZWR*Si45(1,1)*T145(1,3) - YWR*T145(2,3) + Si45(2,0)*T145(3,3) + Si45(2,1)*T145(4,3)) + YWR*S54(1,2)*(ZWR*Si45(1,0)*T145(0,4) + ZWR*Si45(1,1)*T145(1,4) - YWR*T145(2,4) + Si45(2,0)*T145(3,4) + Si45(2,1)*T145(4,4));
T45(5,1) = S54(0,1)*(ZWR*Si45(1,0)*T145(0,0) + ZWR*Si45(1,1)*T145(1,0) - YWR*T145(2,0) + Si45(2,0)*T145(3,0) + Si45(2,1)*T145(4,0)) + S54(1,1)*(ZWR*Si45(1,0)*T145(0,1) + ZWR*Si45(1,1)*T145(1,1) - YWR*T145(2,1) + Si45(2,0)*T145(3,1) + Si45(2,1)*T145(4,1)) - ZWR*S54(0,2)*(ZWR*Si45(1,0)*T145(0,3) + ZWR*Si45(1,1)*T145(1,3) - YWR*T145(2,3) + Si45(2,0)*T145(3,3) + Si45(2,1)*T145(4,3)) - ZWR*S54(1,2)*(ZWR*Si45(1,0)*T145(0,4) + ZWR*Si45(1,1)*T145(1,4) - YWR*T145(2,4) + Si45(2,0)*T145(3,4) + Si45(2,1)*T145(4,4));
T45(5,2) = S54(0,2)*(ZWR*Si45(1,0)*T145(0,0) + ZWR*Si45(1,1)*T145(1,0) - YWR*T145(2,0) + Si45(2,0)*T145(3,0) + Si45(2,1)*T145(4,0)) + S54(1,2)*(ZWR*Si45(1,0)*T145(0,1) + ZWR*Si45(1,1)*T145(1,1) - YWR*T145(2,1) + Si45(2,0)*T145(3,1) + Si45(2,1)*T145(4,1)) + ZWR*S54(0,1)*(ZWR*Si45(1,0)*T145(0,3) + ZWR*Si45(1,1)*T145(1,3) - YWR*T145(2,3) + Si45(2,0)*T145(3,3) + Si45(2,1)*T145(4,3)) + ZWR*S54(1,1)*(ZWR*Si45(1,0)*T145(0,4) + ZWR*Si45(1,1)*T145(1,4) - YWR*T145(2,4) + Si45(2,0)*T145(3,4) + Si45(2,1)*T145(4,4)) - YWR*(ZWR*Si45(1,0)*T145(0,5) + ZWR*Si45(1,1)*T145(1,5) - YWR*T145(2,5) + Si45(2,0)*T145(3,5) + Si45(2,1)*T145(4,5));
T45(5,3) = ZWR*Si45(1,0)*T145(0,5) + ZWR*Si45(1,1)*T145(1,5) - YWR*T145(2,5) + Si45(2,0)*T145(3,5) + Si45(2,1)*T145(4,5);
T45(5,4) = S54(0,1)*(ZWR*Si45(1,0)*T145(0,3) + ZWR*Si45(1,1)*T145(1,3) - YWR*T145(2,3) + Si45(2,0)*T145(3,3) + Si45(2,1)*T145(4,3)) + S54(1,1)*(ZWR*Si45(1,0)*T145(0,4) + ZWR*Si45(1,1)*T145(1,4) - YWR*T145(2,4) + Si45(2,0)*T145(3,4) + Si45(2,1)*T145(4,4));
T45(5,5) = S54(0,2)*(ZWR*Si45(1,0)*T145(0,3) + ZWR*Si45(1,1)*T145(1,3) - YWR*T145(2,3) + Si45(2,0)*T145(3,3) + Si45(2,1)*T145(4,3)) + S54(1,2)*(ZWR*Si45(1,0)*T145(0,4) + ZWR*Si45(1,1)*T145(1,4) - YWR*T145(2,4) + Si45(2,0)*T145(3,4) + Si45(2,1)*T145(4,4));


//wam_ForDynArtfunc11

JA4(0,0) = T45(0,0);
JA4(0,1) = link_param[4].mcm(2) + T45(0,1);
JA4(0,2) = -link_param[4].mcm(1) + T45(0,2);
JA4(0,3) = link_param[4].mass+ T45(0,3);
JA4(0,4) = T45(0,4);
JA4(0,5) = T45(0,5);

JA4(1,0) = -link_param[4].mcm(2) + T45(1,0);
JA4(1,1) = T45(1,1);
JA4(1,2) = link_param[4].mcm(0) + T45(1,2);
JA4(1,3) = T45(1,3);
JA4(1,4) = link_param[4].mass+ T45(1,4);
JA4(1,5) = T45(1,5);

JA4(2,0) = link_param[4].mcm(1) + T45(2,0);
JA4(2,1) = -link_param[4].mcm(0) + T45(2,1);
JA4(2,2) = T45(2,2);
JA4(2,3) = T45(2,3);
JA4(2,4) = T45(2,4);
JA4(2,5) = link_param[4].mass+ T45(2,5);

JA4(3,0) = link_param[4].inertia(0,0) + T45(3,0);
JA4(3,1) = link_param[4].inertia(0,1) + T45(3,1);
JA4(3,2) = link_param[4].inertia(0,2) + T45(3,2);
JA4(3,3) = T45(3,3);
JA4(3,4) = -link_param[4].mcm(2) + T45(3,4);
JA4(3,5) = link_param[4].mcm(1) + T45(3,5);

JA4(4,0) = link_param[4].inertia(0,1) + T45(4,0);
JA4(4,1) = link_param[4].inertia(1,1) + T45(4,1);
JA4(4,2) = link_param[4].inertia(1,2) + T45(4,2);
JA4(4,3) = link_param[4].mcm(2) + T45(4,3);
JA4(4,4) = T45(4,4);
JA4(4,5) = -link_param[4].mcm(0) + T45(4,5);

JA4(5,0) = link_param[4].inertia(0,2) + T45(5,0);
JA4(5,1) = link_param[4].inertia(1,2) + T45(5,1);
JA4(5,2) = link_param[4].inertia(2,2) + T45(5,2);
JA4(5,3) = -link_param[4].mcm(1) + T45(5,3);
JA4(5,4) = link_param[4].mcm(0) + T45(5,4);
JA4(5,5) = T45(5,5);


h4(0) = JA4(0,2);
h4(1) = JA4(1,2);
h4(2) = JA4(2,2);
h4(3) = JA4(3,2);
h4(4) = JA4(4,2);
h4(5) = JA4(5,2);

d(3) = 1.e-10 + h4(5);

T134(0,0) = -((h4(0)*h4(3))/d(3)) + JA4(0,0);
T134(0,1) = -((h4(0)*h4(4))/d(3)) + JA4(0,1);
T134(0,2) = -((h4(0)*h4(5))/d(3)) + JA4(0,2);
T134(0,3) = -(pow(h4(0),2)/d(3)) + JA4(0,3);
T134(0,4) = -((h4(0)*h4(1))/d(3)) + JA4(0,4);
T134(0,5) = -((h4(0)*h4(2))/d(3)) + JA4(0,5);

T134(1,0) = -((h4(1)*h4(3))/d(3)) + JA4(1,0);
T134(1,1) = -((h4(1)*h4(4))/d(3)) + JA4(1,1);
T134(1,2) = -((h4(1)*h4(5))/d(3)) + JA4(1,2);
T134(1,3) = -((h4(0)*h4(1))/d(3)) + JA4(1,3);
T134(1,4) = -(pow(h4(1),2)/d(3)) + JA4(1,4);
T134(1,5) = -((h4(1)*h4(2))/d(3)) + JA4(1,5);

T134(2,0) = -((h4(2)*h4(3))/d(3)) + JA4(2,0);
T134(2,1) = -((h4(2)*h4(4))/d(3)) + JA4(2,1);
T134(2,2) = -((h4(2)*h4(5))/d(3)) + JA4(2,2);
T134(2,3) = -((h4(0)*h4(2))/d(3)) + JA4(2,3);
T134(2,4) = -((h4(1)*h4(2))/d(3)) + JA4(2,4);
T134(2,5) = -(pow(h4(2),2)/d(3)) + JA4(2,5);

T134(3,0) = -(pow(h4(3),2)/d(3)) + JA4(3,0);
T134(3,1) = -((h4(3)*h4(4))/d(3)) + JA4(3,1);
T134(3,2) = -((h4(3)*h4(5))/d(3)) + JA4(3,2);
T134(3,3) = -((h4(0)*h4(3))/d(3)) + JA4(3,3);
T134(3,4) = -((h4(1)*h4(3))/d(3)) + JA4(3,4);
T134(3,5) = -((h4(2)*h4(3))/d(3)) + JA4(3,5);

T134(4,0) = -((h4(3)*h4(4))/d(3)) + JA4(4,0);
T134(4,1) = -(pow(h4(4),2)/d(3)) + JA4(4,1);
T134(4,2) = -((h4(4)*h4(5))/d(3)) + JA4(4,2);
T134(4,3) = -((h4(0)*h4(4))/d(3)) + JA4(4,3);
T134(4,4) = -((h4(1)*h4(4))/d(3)) + JA4(4,4);
T134(4,5) = -((h4(2)*h4(4))/d(3)) + JA4(4,5);

T134(5,0) = -((h4(3)*h4(5))/d(3)) + JA4(5,0);
T134(5,1) = -((h4(4)*h4(5))/d(3)) + JA4(5,1);
T134(5,2) = -(pow(h4(5),2)/d(3)) + JA4(5,2);
T134(5,3) = -((h4(0)*h4(5))/d(3)) + JA4(5,3);
T134(5,4) = -((h4(1)*h4(5))/d(3)) + JA4(5,4);
T134(5,5) = -((h4(2)*h4(5))/d(3)) + JA4(5,5);


T34(0,0) = T134(2,2) - (-(ZEB*S43(0,1)) + YEB*S43(0,2))*T134(2,3) - (-(ZEB*S43(1,1)) + YEB*S43(1,2))*T134(2,4);
T34(0,1) = -(S43(0,1)*T134(2,0)) - S43(1,1)*T134(2,1) + ZEB*T134(2,5);
T34(0,2) = -(S43(0,2)*T134(2,0)) - S43(1,2)*T134(2,1) - YEB*T134(2,5);
T34(0,3) = T134(2,5);
T34(0,4) = -(S43(0,1)*T134(2,3)) - S43(1,1)*T134(2,4);
T34(0,5) = -(S43(0,2)*T134(2,3)) - S43(1,2)*T134(2,4);

T34(1,0) = -(Si34(1,0)*T134(0,2)) - Si34(1,1)*T134(1,2) + (-(ZEB*S43(0,1)) + YEB*S43(0,2))*(Si34(1,0)*T134(0,3) + Si34(1,1)*T134(1,3)) + (-(ZEB*S43(1,1)) + YEB*S43(1,2))*(Si34(1,0)*T134(0,4) + Si34(1,1)*T134(1,4));
T34(1,1) = S43(0,1)*(Si34(1,0)*T134(0,0) + Si34(1,1)*T134(1,0)) + S43(1,1)*(Si34(1,0)*T134(0,1) + Si34(1,1)*T134(1,1)) - ZEB*(Si34(1,0)*T134(0,5) + Si34(1,1)*T134(1,5));
T34(1,2) = S43(0,2)*(Si34(1,0)*T134(0,0) + Si34(1,1)*T134(1,0)) + S43(1,2)*(Si34(1,0)*T134(0,1) + Si34(1,1)*T134(1,1)) + YEB*(Si34(1,0)*T134(0,5) + Si34(1,1)*T134(1,5));
T34(1,3) = -(Si34(1,0)*T134(0,5)) - Si34(1,1)*T134(1,5);
T34(1,4) = S43(0,1)*(Si34(1,0)*T134(0,3) + Si34(1,1)*T134(1,3)) + S43(1,1)*(Si34(1,0)*T134(0,4) + Si34(1,1)*T134(1,4));
T34(1,5) = S43(0,2)*(Si34(1,0)*T134(0,3) + Si34(1,1)*T134(1,3)) + S43(1,2)*(Si34(1,0)*T134(0,4) + Si34(1,1)*T134(1,4));

T34(2,0) = -(Si34(2,0)*T134(0,2)) - Si34(2,1)*T134(1,2) + (-(ZEB*S43(0,1)) + YEB*S43(0,2))*(Si34(2,0)*T134(0,3) + Si34(2,1)*T134(1,3)) + (-(ZEB*S43(1,1)) + YEB*S43(1,2))*(Si34(2,0)*T134(0,4) + Si34(2,1)*T134(1,4));
T34(2,1) = S43(0,1)*(Si34(2,0)*T134(0,0) + Si34(2,1)*T134(1,0)) + S43(1,1)*(Si34(2,0)*T134(0,1) + Si34(2,1)*T134(1,1)) - ZEB*(Si34(2,0)*T134(0,5) + Si34(2,1)*T134(1,5));
T34(2,2) = S43(0,2)*(Si34(2,0)*T134(0,0) + Si34(2,1)*T134(1,0)) + S43(1,2)*(Si34(2,0)*T134(0,1) + Si34(2,1)*T134(1,1)) + YEB*(Si34(2,0)*T134(0,5) + Si34(2,1)*T134(1,5));
T34(2,3) = -(Si34(2,0)*T134(0,5)) - Si34(2,1)*T134(1,5);
T34(2,4) = S43(0,1)*(Si34(2,0)*T134(0,3) + Si34(2,1)*T134(1,3)) + S43(1,1)*(Si34(2,0)*T134(0,4) + Si34(2,1)*T134(1,4));
T34(2,5) = S43(0,2)*(Si34(2,0)*T134(0,3) + Si34(2,1)*T134(1,3)) + S43(1,2)*(Si34(2,0)*T134(0,4) + Si34(2,1)*T134(1,4));

T34(3,0) = -((-(ZEB*Si34(1,0)) + YEB*Si34(2,0))*T134(0,2)) - (-(ZEB*Si34(1,1)) + YEB*Si34(2,1))*T134(1,2) + T134(5,2) + (-(ZEB*S43(0,1)) + YEB*S43(0,2))*((-(ZEB*Si34(1,0)) + YEB*Si34(2,0))*T134(0,3) + (-(ZEB*Si34(1,1)) + YEB*Si34(2,1))*T134(1,3) - T134(5,3)) + (-(ZEB*S43(1,1)) + YEB*S43(1,2))*((-(ZEB*Si34(1,0)) + YEB*Si34(2,0))*T134(0,4) + (-(ZEB*Si34(1,1)) + YEB*Si34(2,1))*T134(1,4) - T134(5,4));
T34(3,1) = S43(0,1)*((-(ZEB*Si34(1,0)) + YEB*Si34(2,0))*T134(0,0) + (-(ZEB*Si34(1,1)) + YEB*Si34(2,1))*T134(1,0) - T134(5,0)) + S43(1,1)*((-(ZEB*Si34(1,0)) + YEB*Si34(2,0))*T134(0,1) + (-(ZEB*Si34(1,1)) + YEB*Si34(2,1))*T134(1,1) - T134(5,1)) - ZEB*((-(ZEB*Si34(1,0)) + YEB*Si34(2,0))*T134(0,5) + (-(ZEB*Si34(1,1)) + YEB*Si34(2,1))*T134(1,5) - T134(5,5));
T34(3,2) = S43(0,2)*((-(ZEB*Si34(1,0)) + YEB*Si34(2,0))*T134(0,0) + (-(ZEB*Si34(1,1)) + YEB*Si34(2,1))*T134(1,0) - T134(5,0)) + S43(1,2)*((-(ZEB*Si34(1,0)) + YEB*Si34(2,0))*T134(0,1) + (-(ZEB*Si34(1,1)) + YEB*Si34(2,1))*T134(1,1) - T134(5,1)) + YEB*((-(ZEB*Si34(1,0)) + YEB*Si34(2,0))*T134(0,5) + (-(ZEB*Si34(1,1)) + YEB*Si34(2,1))*T134(1,5) - T134(5,5));
T34(3,3) = -((-(ZEB*Si34(1,0)) + YEB*Si34(2,0))*T134(0,5)) - (-(ZEB*Si34(1,1)) + YEB*Si34(2,1))*T134(1,5) + T134(5,5);
T34(3,4) = S43(0,1)*((-(ZEB*Si34(1,0)) + YEB*Si34(2,0))*T134(0,3) + (-(ZEB*Si34(1,1)) + YEB*Si34(2,1))*T134(1,3) - T134(5,3)) + S43(1,1)*((-(ZEB*Si34(1,0)) + YEB*Si34(2,0))*T134(0,4) + (-(ZEB*Si34(1,1)) + YEB*Si34(2,1))*T134(1,4) - T134(5,4));
T34(3,5) = S43(0,2)*((-(ZEB*Si34(1,0)) + YEB*Si34(2,0))*T134(0,3) + (-(ZEB*Si34(1,1)) + YEB*Si34(2,1))*T134(1,3) - T134(5,3)) + S43(1,2)*((-(ZEB*Si34(1,0)) + YEB*Si34(2,0))*T134(0,4) + (-(ZEB*Si34(1,1)) + YEB*Si34(2,1))*T134(1,4) - T134(5,4));

T34(4,0) = ZEB*T134(2,2) - Si34(1,0)*T134(3,2) - Si34(1,1)*T134(4,2) + (-(ZEB*S43(0,1)) + YEB*S43(0,2))*(-(ZEB*T134(2,3)) + Si34(1,0)*T134(3,3) + Si34(1,1)*T134(4,3)) + (-(ZEB*S43(1,1)) + YEB*S43(1,2))*(-(ZEB*T134(2,4)) + Si34(1,0)*T134(3,4) + Si34(1,1)*T134(4,4));
T34(4,1) = S43(0,1)*(-(ZEB*T134(2,0)) + Si34(1,0)*T134(3,0) + Si34(1,1)*T134(4,0)) + S43(1,1)*(-(ZEB*T134(2,1)) + Si34(1,0)*T134(3,1) + Si34(1,1)*T134(4,1)) - ZEB*(-(ZEB*T134(2,5)) + Si34(1,0)*T134(3,5) + Si34(1,1)*T134(4,5));
T34(4,2) = S43(0,2)*(-(ZEB*T134(2,0)) + Si34(1,0)*T134(3,0) + Si34(1,1)*T134(4,0)) + S43(1,2)*(-(ZEB*T134(2,1)) + Si34(1,0)*T134(3,1) + Si34(1,1)*T134(4,1)) + YEB*(-(ZEB*T134(2,5)) + Si34(1,0)*T134(3,5) + Si34(1,1)*T134(4,5));
T34(4,3) = ZEB*T134(2,5) - Si34(1,0)*T134(3,5) - Si34(1,1)*T134(4,5);
T34(4,4) = S43(0,1)*(-(ZEB*T134(2,3)) + Si34(1,0)*T134(3,3) + Si34(1,1)*T134(4,3)) + S43(1,1)*(-(ZEB*T134(2,4)) + Si34(1,0)*T134(3,4) + Si34(1,1)*T134(4,4));
T34(4,5) = S43(0,2)*(-(ZEB*T134(2,3)) + Si34(1,0)*T134(3,3) + Si34(1,1)*T134(4,3)) + S43(1,2)*(-(ZEB*T134(2,4)) + Si34(1,0)*T134(3,4) + Si34(1,1)*T134(4,4));

T34(5,0) = -(YEB*T134(2,2)) - Si34(2,0)*T134(3,2) - Si34(2,1)*T134(4,2) + (-(ZEB*S43(0,1)) + YEB*S43(0,2))*(YEB*T134(2,3) + Si34(2,0)*T134(3,3) + Si34(2,1)*T134(4,3)) + (-(ZEB*S43(1,1)) + YEB*S43(1,2))*(YEB*T134(2,4) + Si34(2,0)*T134(3,4) + Si34(2,1)*T134(4,4));
T34(5,1) = S43(0,1)*(YEB*T134(2,0) + Si34(2,0)*T134(3,0) + Si34(2,1)*T134(4,0)) + S43(1,1)*(YEB*T134(2,1) + Si34(2,0)*T134(3,1) + Si34(2,1)*T134(4,1)) - ZEB*(YEB*T134(2,5) + Si34(2,0)*T134(3,5) + Si34(2,1)*T134(4,5));
T34(5,2) = S43(0,2)*(YEB*T134(2,0) + Si34(2,0)*T134(3,0) + Si34(2,1)*T134(4,0)) + S43(1,2)*(YEB*T134(2,1) + Si34(2,0)*T134(3,1) + Si34(2,1)*T134(4,1)) + YEB*(YEB*T134(2,5) + Si34(2,0)*T134(3,5) + Si34(2,1)*T134(4,5));
T34(5,3) = -(YEB*T134(2,5)) - Si34(2,0)*T134(3,5) - Si34(2,1)*T134(4,5);
T34(5,4) = S43(0,1)*(YEB*T134(2,3) + Si34(2,0)*T134(3,3) + Si34(2,1)*T134(4,3)) + S43(1,1)*(YEB*T134(2,4) + Si34(2,0)*T134(3,4) + Si34(2,1)*T134(4,4));
T34(5,5) = S43(0,2)*(YEB*T134(2,3) + Si34(2,0)*T134(3,3) + Si34(2,1)*T134(4,3)) + S43(1,2)*(YEB*T134(2,4) + Si34(2,0)*T134(3,4) + Si34(2,1)*T134(4,4));

//wam_ForDynArtfunc12

JA3(0,0) = T34(0,0);
JA3(0,1) = link_param[3].mcm(2) + T34(0,1);
JA3(0,2) = -link_param[3].mcm(1) + T34(0,2);
JA3(0,3) = link_param[3].mass+ T34(0,3);
JA3(0,4) = T34(0,4);
JA3(0,5) = T34(0,5);

JA3(1,0) = -link_param[3].mcm(2) + T34(1,0);
JA3(1,1) = T34(1,1);
JA3(1,2) = link_param[3].mcm(0) + T34(1,2);
JA3(1,3) = T34(1,3);
JA3(1,4) = link_param[3].mass+ T34(1,4);
JA3(1,5) = T34(1,5);

JA3(2,0) = link_param[3].mcm(1) + T34(2,0);
JA3(2,1) = -link_param[3].mcm(0) + T34(2,1);
JA3(2,2) = T34(2,2);
JA3(2,3) = T34(2,3);
JA3(2,4) = T34(2,4);
JA3(2,5) = link_param[3].mass+ T34(2,5);

JA3(3,0) = link_param[3].inertia(0,0) + T34(3,0);
JA3(3,1) = link_param[3].inertia(0,1) + T34(3,1);
JA3(3,2) = link_param[3].inertia(0,2) + T34(3,2);
JA3(3,3) = T34(3,3);
JA3(3,4) = -link_param[3].mcm(2) + T34(3,4);
JA3(3,5) = link_param[3].mcm(1) + T34(3,5);

JA3(4,0) = link_param[3].inertia(0,1) + T34(4,0);
JA3(4,1) = link_param[3].inertia(1,1) + T34(4,1);
JA3(4,2) = link_param[3].inertia(1,2) + T34(4,2);
JA3(4,3) = link_param[3].mcm(2) + T34(4,3);
JA3(4,4) = T34(4,4);
JA3(4,5) = -link_param[3].mcm(0) + T34(4,5);

JA3(5,0) = link_param[3].inertia(0,2) + T34(5,0);
JA3(5,1) = link_param[3].inertia(1,2) + T34(5,1);
JA3(5,2) = link_param[3].inertia(2,2) + T34(5,2);
JA3(5,3) = -link_param[3].mcm(1) + T34(5,3);
JA3(5,4) = link_param[3].mcm(0) + T34(5,4);
JA3(5,5) = T34(5,5);


h3(0) = JA3(0,2);
h3(1) = JA3(1,2);
h3(2) = JA3(2,2);
h3(3) = JA3(3,2);
h3(4) = JA3(4,2);
h3(5) = JA3(5,2);

d(2) = 1.e-10 + h3(5);

T123(0,0) = -((h3(0)*h3(3))/d(2)) + JA3(0,0);
T123(0,1) = -((h3(0)*h3(4))/d(2)) + JA3(0,1);
T123(0,2) = -((h3(0)*h3(5))/d(2)) + JA3(0,2);
T123(0,3) = -(pow(h3(0),2)/d(2)) + JA3(0,3);
T123(0,4) = -((h3(0)*h3(1))/d(2)) + JA3(0,4);
T123(0,5) = -((h3(0)*h3(2))/d(2)) + JA3(0,5);

T123(1,0) = -((h3(1)*h3(3))/d(2)) + JA3(1,0);
T123(1,1) = -((h3(1)*h3(4))/d(2)) + JA3(1,1);
T123(1,2) = -((h3(1)*h3(5))/d(2)) + JA3(1,2);
T123(1,3) = -((h3(0)*h3(1))/d(2)) + JA3(1,3);
T123(1,4) = -(pow(h3(1),2)/d(2)) + JA3(1,4);
T123(1,5) = -((h3(1)*h3(2))/d(2)) + JA3(1,5);

T123(2,0) = -((h3(2)*h3(3))/d(2)) + JA3(2,0);
T123(2,1) = -((h3(2)*h3(4))/d(2)) + JA3(2,1);
T123(2,2) = -((h3(2)*h3(5))/d(2)) + JA3(2,2);
T123(2,3) = -((h3(0)*h3(2))/d(2)) + JA3(2,3);
T123(2,4) = -((h3(1)*h3(2))/d(2)) + JA3(2,4);
T123(2,5) = -(pow(h3(2),2)/d(2)) + JA3(2,5);

T123(3,0) = -(pow(h3(3),2)/d(2)) + JA3(3,0);
T123(3,1) = -((h3(3)*h3(4))/d(2)) + JA3(3,1);
T123(3,2) = -((h3(3)*h3(5))/d(2)) + JA3(3,2);
T123(3,3) = -((h3(0)*h3(3))/d(2)) + JA3(3,3);
T123(3,4) = -((h3(1)*h3(3))/d(2)) + JA3(3,4);
T123(3,5) = -((h3(2)*h3(3))/d(2)) + JA3(3,5);

T123(4,0) = -((h3(3)*h3(4))/d(2)) + JA3(4,0);
T123(4,1) = -(pow(h3(4),2)/d(2)) + JA3(4,1);
T123(4,2) = -((h3(4)*h3(5))/d(2)) + JA3(4,2);
T123(4,3) = -((h3(0)*h3(4))/d(2)) + JA3(4,3);
T123(4,4) = -((h3(1)*h3(4))/d(2)) + JA3(4,4);
T123(4,5) = -((h3(2)*h3(4))/d(2)) + JA3(4,5);

T123(5,0) = -((h3(3)*h3(5))/d(2)) + JA3(5,0);
T123(5,1) = -((h3(4)*h3(5))/d(2)) + JA3(5,1);
T123(5,2) = -(pow(h3(5),2)/d(2)) + JA3(5,2);
T123(5,3) = -((h3(0)*h3(5))/d(2)) + JA3(5,3);
T123(5,4) = -((h3(1)*h3(5))/d(2)) + JA3(5,4);
T123(5,5) = -((h3(2)*h3(5))/d(2)) + JA3(5,5);


T23(0,0) = T123(2,2);
T23(0,1) = S32(0,1)*T123(2,0) + S32(1,1)*T123(2,1) - ZHR*S32(0,2)*T123(2,3) - ZHR*S32(1,2)*T123(2,4);
T23(0,2) = S32(0,2)*T123(2,0) + S32(1,2)*T123(2,1) + ZHR*S32(0,1)*T123(2,3) + ZHR*S32(1,1)*T123(2,4);
T23(0,3) = T123(2,5);
T23(0,4) = S32(0,1)*T123(2,3) + S32(1,1)*T123(2,4);
T23(0,5) = S32(0,2)*T123(2,3) + S32(1,2)*T123(2,4);

T23(1,0) = Si23(1,0)*T123(0,2) + Si23(1,1)*T123(1,2);
T23(1,1) = S32(0,1)*(Si23(1,0)*T123(0,0) + Si23(1,1)*T123(1,0)) + S32(1,1)*(Si23(1,0)*T123(0,1) + Si23(1,1)*T123(1,1)) - ZHR*S32(0,2)*(Si23(1,0)*T123(0,3) + Si23(1,1)*T123(1,3)) - ZHR*S32(1,2)*(Si23(1,0)*T123(0,4) + Si23(1,1)*T123(1,4));
T23(1,2) = S32(0,2)*(Si23(1,0)*T123(0,0) + Si23(1,1)*T123(1,0)) + S32(1,2)*(Si23(1,0)*T123(0,1) + Si23(1,1)*T123(1,1)) + ZHR*S32(0,1)*(Si23(1,0)*T123(0,3) + Si23(1,1)*T123(1,3)) + ZHR*S32(1,1)*(Si23(1,0)*T123(0,4) + Si23(1,1)*T123(1,4));
T23(1,3) = Si23(1,0)*T123(0,5) + Si23(1,1)*T123(1,5);
T23(1,4) = S32(0,1)*(Si23(1,0)*T123(0,3) + Si23(1,1)*T123(1,3)) + S32(1,1)*(Si23(1,0)*T123(0,4) + Si23(1,1)*T123(1,4));
T23(1,5) = S32(0,2)*(Si23(1,0)*T123(0,3) + Si23(1,1)*T123(1,3)) + S32(1,2)*(Si23(1,0)*T123(0,4) + Si23(1,1)*T123(1,4));

T23(2,0) = Si23(2,0)*T123(0,2) + Si23(2,1)*T123(1,2);
T23(2,1) = S32(0,1)*(Si23(2,0)*T123(0,0) + Si23(2,1)*T123(1,0)) + S32(1,1)*(Si23(2,0)*T123(0,1) + Si23(2,1)*T123(1,1)) - ZHR*S32(0,2)*(Si23(2,0)*T123(0,3) + Si23(2,1)*T123(1,3)) - ZHR*S32(1,2)*(Si23(2,0)*T123(0,4) + Si23(2,1)*T123(1,4));
T23(2,2) = S32(0,2)*(Si23(2,0)*T123(0,0) + Si23(2,1)*T123(1,0)) + S32(1,2)*(Si23(2,0)*T123(0,1) + Si23(2,1)*T123(1,1)) + ZHR*S32(0,1)*(Si23(2,0)*T123(0,3) + Si23(2,1)*T123(1,3)) + ZHR*S32(1,1)*(Si23(2,0)*T123(0,4) + Si23(2,1)*T123(1,4));
T23(2,3) = Si23(2,0)*T123(0,5) + Si23(2,1)*T123(1,5);
T23(2,4) = S32(0,1)*(Si23(2,0)*T123(0,3) + Si23(2,1)*T123(1,3)) + S32(1,1)*(Si23(2,0)*T123(0,4) + Si23(2,1)*T123(1,4));
T23(2,5) = S32(0,2)*(Si23(2,0)*T123(0,3) + Si23(2,1)*T123(1,3)) + S32(1,2)*(Si23(2,0)*T123(0,4) + Si23(2,1)*T123(1,4));

T23(3,0) = T123(5,2);
T23(3,1) = S32(0,1)*T123(5,0) + S32(1,1)*T123(5,1) - ZHR*S32(0,2)*T123(5,3) - ZHR*S32(1,2)*T123(5,4);
T23(3,2) = S32(0,2)*T123(5,0) + S32(1,2)*T123(5,1) + ZHR*S32(0,1)*T123(5,3) + ZHR*S32(1,1)*T123(5,4);
T23(3,3) = T123(5,5);
T23(3,4) = S32(0,1)*T123(5,3) + S32(1,1)*T123(5,4);
T23(3,5) = S32(0,2)*T123(5,3) + S32(1,2)*T123(5,4);

T23(4,0) = -(ZHR*Si23(2,0)*T123(0,2)) - ZHR*Si23(2,1)*T123(1,2) + Si23(1,0)*T123(3,2) + Si23(1,1)*T123(4,2);
T23(4,1) = S32(0,1)*(-(ZHR*Si23(2,0)*T123(0,0)) - ZHR*Si23(2,1)*T123(1,0) + Si23(1,0)*T123(3,0) + Si23(1,1)*T123(4,0)) + S32(1,1)*(-(ZHR*Si23(2,0)*T123(0,1)) - ZHR*Si23(2,1)*T123(1,1) + Si23(1,0)*T123(3,1) + Si23(1,1)*T123(4,1)) - ZHR*S32(0,2)*(-(ZHR*Si23(2,0)*T123(0,3)) - ZHR*Si23(2,1)*T123(1,3) + Si23(1,0)*T123(3,3) + Si23(1,1)*T123(4,3)) - ZHR*S32(1,2)*(-(ZHR*Si23(2,0)*T123(0,4)) - ZHR*Si23(2,1)*T123(1,4) + Si23(1,0)*T123(3,4) + Si23(1,1)*T123(4,4));
T23(4,2) = S32(0,2)*(-(ZHR*Si23(2,0)*T123(0,0)) - ZHR*Si23(2,1)*T123(1,0) + Si23(1,0)*T123(3,0) + Si23(1,1)*T123(4,0)) + S32(1,2)*(-(ZHR*Si23(2,0)*T123(0,1)) - ZHR*Si23(2,1)*T123(1,1) + Si23(1,0)*T123(3,1) + Si23(1,1)*T123(4,1)) + ZHR*S32(0,1)*(-(ZHR*Si23(2,0)*T123(0,3)) - ZHR*Si23(2,1)*T123(1,3) + Si23(1,0)*T123(3,3) + Si23(1,1)*T123(4,3)) + ZHR*S32(1,1)*(-(ZHR*Si23(2,0)*T123(0,4)) - ZHR*Si23(2,1)*T123(1,4) + Si23(1,0)*T123(3,4) + Si23(1,1)*T123(4,4));
T23(4,3) = -(ZHR*Si23(2,0)*T123(0,5)) - ZHR*Si23(2,1)*T123(1,5) + Si23(1,0)*T123(3,5) + Si23(1,1)*T123(4,5);
T23(4,4) = S32(0,1)*(-(ZHR*Si23(2,0)*T123(0,3)) - ZHR*Si23(2,1)*T123(1,3) + Si23(1,0)*T123(3,3) + Si23(1,1)*T123(4,3)) + S32(1,1)*(-(ZHR*Si23(2,0)*T123(0,4)) - ZHR*Si23(2,1)*T123(1,4) + Si23(1,0)*T123(3,4) + Si23(1,1)*T123(4,4));
T23(4,5) = S32(0,2)*(-(ZHR*Si23(2,0)*T123(0,3)) - ZHR*Si23(2,1)*T123(1,3) + Si23(1,0)*T123(3,3) + Si23(1,1)*T123(4,3)) + S32(1,2)*(-(ZHR*Si23(2,0)*T123(0,4)) - ZHR*Si23(2,1)*T123(1,4) + Si23(1,0)*T123(3,4) + Si23(1,1)*T123(4,4));

T23(5,0) = ZHR*Si23(1,0)*T123(0,2) + ZHR*Si23(1,1)*T123(1,2) + Si23(2,0)*T123(3,2) + Si23(2,1)*T123(4,2);
T23(5,1) = S32(0,1)*(ZHR*Si23(1,0)*T123(0,0) + ZHR*Si23(1,1)*T123(1,0) + Si23(2,0)*T123(3,0) + Si23(2,1)*T123(4,0)) + S32(1,1)*(ZHR*Si23(1,0)*T123(0,1) + ZHR*Si23(1,1)*T123(1,1) + Si23(2,0)*T123(3,1) + Si23(2,1)*T123(4,1)) - ZHR*S32(0,2)*(ZHR*Si23(1,0)*T123(0,3) + ZHR*Si23(1,1)*T123(1,3) + Si23(2,0)*T123(3,3) + Si23(2,1)*T123(4,3)) - ZHR*S32(1,2)*(ZHR*Si23(1,0)*T123(0,4) + ZHR*Si23(1,1)*T123(1,4) + Si23(2,0)*T123(3,4) + Si23(2,1)*T123(4,4));
T23(5,2) = S32(0,2)*(ZHR*Si23(1,0)*T123(0,0) + ZHR*Si23(1,1)*T123(1,0) + Si23(2,0)*T123(3,0) + Si23(2,1)*T123(4,0)) + S32(1,2)*(ZHR*Si23(1,0)*T123(0,1) + ZHR*Si23(1,1)*T123(1,1) + Si23(2,0)*T123(3,1) + Si23(2,1)*T123(4,1)) + ZHR*S32(0,1)*(ZHR*Si23(1,0)*T123(0,3) + ZHR*Si23(1,1)*T123(1,3) + Si23(2,0)*T123(3,3) + Si23(2,1)*T123(4,3)) + ZHR*S32(1,1)*(ZHR*Si23(1,0)*T123(0,4) + ZHR*Si23(1,1)*T123(1,4) + Si23(2,0)*T123(3,4) + Si23(2,1)*T123(4,4));
T23(5,3) = ZHR*Si23(1,0)*T123(0,5) + ZHR*Si23(1,1)*T123(1,5) + Si23(2,0)*T123(3,5) + Si23(2,1)*T123(4,5);
T23(5,4) = S32(0,1)*(ZHR*Si23(1,0)*T123(0,3) + ZHR*Si23(1,1)*T123(1,3) + Si23(2,0)*T123(3,3) + Si23(2,1)*T123(4,3)) + S32(1,1)*(ZHR*Si23(1,0)*T123(0,4) + ZHR*Si23(1,1)*T123(1,4) + Si23(2,0)*T123(3,4) + Si23(2,1)*T123(4,4));
T23(5,5) = S32(0,2)*(ZHR*Si23(1,0)*T123(0,3) + ZHR*Si23(1,1)*T123(1,3) + Si23(2,0)*T123(3,3) + Si23(2,1)*T123(4,3)) + S32(1,2)*(ZHR*Si23(1,0)*T123(0,4) + ZHR*Si23(1,1)*T123(1,4) + Si23(2,0)*T123(3,4) + Si23(2,1)*T123(4,4));

//wam_ForDynArtfunc13

JA2(0,0) = T23(0,0);
JA2(0,1) = link_param[2].mcm(2) + T23(0,1);
JA2(0,2) = -link_param[2].mcm(1) + T23(0,2);
JA2(0,3) = link_param[2].mass+ T23(0,3);
JA2(0,4) = T23(0,4);
JA2(0,5) = T23(0,5);

JA2(1,0) = -link_param[2].mcm(2) + T23(1,0);
JA2(1,1) = T23(1,1);
JA2(1,2) = link_param[2].mcm(0) + T23(1,2);
JA2(1,3) = T23(1,3);
JA2(1,4) = link_param[2].mass+ T23(1,4);
JA2(1,5) = T23(1,5);

JA2(2,0) = link_param[2].mcm(1) + T23(2,0);
JA2(2,1) = -link_param[2].mcm(0) + T23(2,1);
JA2(2,2) = T23(2,2);
JA2(2,3) = T23(2,3);
JA2(2,4) = T23(2,4);
JA2(2,5) = link_param[2].mass+ T23(2,5);

JA2(3,0) = link_param[2].inertia(0,0) + T23(3,0);
JA2(3,1) = link_param[2].inertia(0,1) + T23(3,1);
JA2(3,2) = link_param[2].inertia(0,2) + T23(3,2);
JA2(3,3) = T23(3,3);
JA2(3,4) = -link_param[2].mcm(2) + T23(3,4);
JA2(3,5) = link_param[2].mcm(1) + T23(3,5);

JA2(4,0) = link_param[2].inertia(0,1) + T23(4,0);
JA2(4,1) = link_param[2].inertia(1,1) + T23(4,1);
JA2(4,2) = link_param[2].inertia(1,2) + T23(4,2);
JA2(4,3) = link_param[2].mcm(2) + T23(4,3);
JA2(4,4) = T23(4,4);
JA2(4,5) = -link_param[2].mcm(0) + T23(4,5);

JA2(5,0) = link_param[2].inertia(0,2) + T23(5,0);
JA2(5,1) = link_param[2].inertia(1,2) + T23(5,1);
JA2(5,2) = link_param[2].inertia(2,2) + T23(5,2);
JA2(5,3) = -link_param[2].mcm(1) + T23(5,3);
JA2(5,4) = link_param[2].mcm(0) + T23(5,4);
JA2(5,5) = T23(5,5);


h2(0) = JA2(0,2);
h2(1) = JA2(1,2);
h2(2) = JA2(2,2);
h2(3) = JA2(3,2);
h2(4) = JA2(4,2);
h2(5) = JA2(5,2);

d(1) = 1.e-10 + h2(5);

T112(0,0) = -((h2(0)*h2(3))/d(1)) + JA2(0,0);
T112(0,1) = -((h2(0)*h2(4))/d(1)) + JA2(0,1);
T112(0,2) = -((h2(0)*h2(5))/d(1)) + JA2(0,2);
T112(0,3) = -(pow(h2(0),2)/d(1)) + JA2(0,3);
T112(0,4) = -((h2(0)*h2(1))/d(1)) + JA2(0,4);
T112(0,5) = -((h2(0)*h2(2))/d(1)) + JA2(0,5);

T112(1,0) = -((h2(1)*h2(3))/d(1)) + JA2(1,0);
T112(1,1) = -((h2(1)*h2(4))/d(1)) + JA2(1,1);
T112(1,2) = -((h2(1)*h2(5))/d(1)) + JA2(1,2);
T112(1,3) = -((h2(0)*h2(1))/d(1)) + JA2(1,3);
T112(1,4) = -(pow(h2(1),2)/d(1)) + JA2(1,4);
T112(1,5) = -((h2(1)*h2(2))/d(1)) + JA2(1,5);

T112(2,0) = -((h2(2)*h2(3))/d(1)) + JA2(2,0);
T112(2,1) = -((h2(2)*h2(4))/d(1)) + JA2(2,1);
T112(2,2) = -((h2(2)*h2(5))/d(1)) + JA2(2,2);
T112(2,3) = -((h2(0)*h2(2))/d(1)) + JA2(2,3);
T112(2,4) = -((h2(1)*h2(2))/d(1)) + JA2(2,4);
T112(2,5) = -(pow(h2(2),2)/d(1)) + JA2(2,5);

T112(3,0) = -(pow(h2(3),2)/d(1)) + JA2(3,0);
T112(3,1) = -((h2(3)*h2(4))/d(1)) + JA2(3,1);
T112(3,2) = -((h2(3)*h2(5))/d(1)) + JA2(3,2);
T112(3,3) = -((h2(0)*h2(3))/d(1)) + JA2(3,3);
T112(3,4) = -((h2(1)*h2(3))/d(1)) + JA2(3,4);
T112(3,5) = -((h2(2)*h2(3))/d(1)) + JA2(3,5);

T112(4,0) = -((h2(3)*h2(4))/d(1)) + JA2(4,0);
T112(4,1) = -(pow(h2(4),2)/d(1)) + JA2(4,1);
T112(4,2) = -((h2(4)*h2(5))/d(1)) + JA2(4,2);
T112(4,3) = -((h2(0)*h2(4))/d(1)) + JA2(4,3);
T112(4,4) = -((h2(1)*h2(4))/d(1)) + JA2(4,4);
T112(4,5) = -((h2(2)*h2(4))/d(1)) + JA2(4,5);

T112(5,0) = -((h2(3)*h2(5))/d(1)) + JA2(5,0);
T112(5,1) = -((h2(4)*h2(5))/d(1)) + JA2(5,1);
T112(5,2) = -(pow(h2(5),2)/d(1)) + JA2(5,2);
T112(5,3) = -((h2(0)*h2(5))/d(1)) + JA2(5,3);
T112(5,4) = -((h2(1)*h2(5))/d(1)) + JA2(5,4);
T112(5,5) = -((h2(2)*h2(5))/d(1)) + JA2(5,5);


T12(0,0) = T112(2,2);
T12(0,1) = -(S21(0,1)*T112(2,0)) - S21(1,1)*T112(2,1);
T12(0,2) = -(S21(0,2)*T112(2,0)) - S21(1,2)*T112(2,1);
T12(0,3) = T112(2,5);
T12(0,4) = -(S21(0,1)*T112(2,3)) - S21(1,1)*T112(2,4);
T12(0,5) = -(S21(0,2)*T112(2,3)) - S21(1,2)*T112(2,4);

T12(1,0) = -(Si12(1,0)*T112(0,2)) - Si12(1,1)*T112(1,2);
T12(1,1) = S21(0,1)*(Si12(1,0)*T112(0,0) + Si12(1,1)*T112(1,0)) + S21(1,1)*(Si12(1,0)*T112(0,1) + Si12(1,1)*T112(1,1));
T12(1,2) = S21(0,2)*(Si12(1,0)*T112(0,0) + Si12(1,1)*T112(1,0)) + S21(1,2)*(Si12(1,0)*T112(0,1) + Si12(1,1)*T112(1,1));
T12(1,3) = -(Si12(1,0)*T112(0,5)) - Si12(1,1)*T112(1,5);
T12(1,4) = S21(0,1)*(Si12(1,0)*T112(0,3) + Si12(1,1)*T112(1,3)) + S21(1,1)*(Si12(1,0)*T112(0,4) + Si12(1,1)*T112(1,4));
T12(1,5) = S21(0,2)*(Si12(1,0)*T112(0,3) + Si12(1,1)*T112(1,3)) + S21(1,2)*(Si12(1,0)*T112(0,4) + Si12(1,1)*T112(1,4));

T12(2,0) = -(Si12(2,0)*T112(0,2)) - Si12(2,1)*T112(1,2);
T12(2,1) = S21(0,1)*(Si12(2,0)*T112(0,0) + Si12(2,1)*T112(1,0)) + S21(1,1)*(Si12(2,0)*T112(0,1) + Si12(2,1)*T112(1,1));
T12(2,2) = S21(0,2)*(Si12(2,0)*T112(0,0) + Si12(2,1)*T112(1,0)) + S21(1,2)*(Si12(2,0)*T112(0,1) + Si12(2,1)*T112(1,1));
T12(2,3) = -(Si12(2,0)*T112(0,5)) - Si12(2,1)*T112(1,5);
T12(2,4) = S21(0,1)*(Si12(2,0)*T112(0,3) + Si12(2,1)*T112(1,3)) + S21(1,1)*(Si12(2,0)*T112(0,4) + Si12(2,1)*T112(1,4));
T12(2,5) = S21(0,2)*(Si12(2,0)*T112(0,3) + Si12(2,1)*T112(1,3)) + S21(1,2)*(Si12(2,0)*T112(0,4) + Si12(2,1)*T112(1,4));

T12(3,0) = T112(5,2);
T12(3,1) = -(S21(0,1)*T112(5,0)) - S21(1,1)*T112(5,1);
T12(3,2) = -(S21(0,2)*T112(5,0)) - S21(1,2)*T112(5,1);
T12(3,3) = T112(5,5);
T12(3,4) = -(S21(0,1)*T112(5,3)) - S21(1,1)*T112(5,4);
T12(3,5) = -(S21(0,2)*T112(5,3)) - S21(1,2)*T112(5,4);

T12(4,0) = -(Si12(1,0)*T112(3,2)) - Si12(1,1)*T112(4,2);
T12(4,1) = S21(0,1)*(Si12(1,0)*T112(3,0) + Si12(1,1)*T112(4,0)) + S21(1,1)*(Si12(1,0)*T112(3,1) + Si12(1,1)*T112(4,1));
T12(4,2) = S21(0,2)*(Si12(1,0)*T112(3,0) + Si12(1,1)*T112(4,0)) + S21(1,2)*(Si12(1,0)*T112(3,1) + Si12(1,1)*T112(4,1));
T12(4,3) = -(Si12(1,0)*T112(3,5)) - Si12(1,1)*T112(4,5);
T12(4,4) = S21(0,1)*(Si12(1,0)*T112(3,3) + Si12(1,1)*T112(4,3)) + S21(1,1)*(Si12(1,0)*T112(3,4) + Si12(1,1)*T112(4,4));
T12(4,5) = S21(0,2)*(Si12(1,0)*T112(3,3) + Si12(1,1)*T112(4,3)) + S21(1,2)*(Si12(1,0)*T112(3,4) + Si12(1,1)*T112(4,4));

T12(5,0) = -(Si12(2,0)*T112(3,2)) - Si12(2,1)*T112(4,2);
T12(5,1) = S21(0,1)*(Si12(2,0)*T112(3,0) + Si12(2,1)*T112(4,0)) + S21(1,1)*(Si12(2,0)*T112(3,1) + Si12(2,1)*T112(4,1));
T12(5,2) = S21(0,2)*(Si12(2,0)*T112(3,0) + Si12(2,1)*T112(4,0)) + S21(1,2)*(Si12(2,0)*T112(3,1) + Si12(2,1)*T112(4,1));
T12(5,3) = -(Si12(2,0)*T112(3,5)) - Si12(2,1)*T112(4,5);
T12(5,4) = S21(0,1)*(Si12(2,0)*T112(3,3) + Si12(2,1)*T112(4,3)) + S21(1,1)*(Si12(2,0)*T112(3,4) + Si12(2,1)*T112(4,4));
T12(5,5) = S21(0,2)*(Si12(2,0)*T112(3,3) + Si12(2,1)*T112(4,3)) + S21(1,2)*(Si12(2,0)*T112(3,4) + Si12(2,1)*T112(4,4));

//wam_ForDynArtfunc14

JA1(0,0) = T12(0,0);
JA1(0,1) = link_param[1].mcm(2) + T12(0,1);
JA1(0,2) = -link_param[1].mcm(1) + T12(0,2);
JA1(0,3) = link_param[1].mass+ T12(0,3);
JA1(0,4) = T12(0,4);
JA1(0,5) = T12(0,5);

JA1(1,0) = -link_param[1].mcm(2) + T12(1,0);
JA1(1,1) = T12(1,1);
JA1(1,2) = link_param[1].mcm(0) + T12(1,2);
JA1(1,3) = T12(1,3);
JA1(1,4) = link_param[1].mass+ T12(1,4);
JA1(1,5) = T12(1,5);

JA1(2,0) = link_param[1].mcm(1) + T12(2,0);
JA1(2,1) = -link_param[1].mcm(0) + T12(2,1);
JA1(2,2) = T12(2,2);
JA1(2,3) = T12(2,3);
JA1(2,4) = T12(2,4);
JA1(2,5) = link_param[1].mass+ T12(2,5);

JA1(3,0) = link_param[1].inertia(0,0) + T12(3,0);
JA1(3,1) = link_param[1].inertia(0,1) + T12(3,1);
JA1(3,2) = link_param[1].inertia(0,2) + T12(3,2);
JA1(3,3) = T12(3,3);
JA1(3,4) = -link_param[1].mcm(2) + T12(3,4);
JA1(3,5) = link_param[1].mcm(1) + T12(3,5);

JA1(4,0) = link_param[1].inertia(0,1) + T12(4,0);
JA1(4,1) = link_param[1].inertia(1,1) + T12(4,1);
JA1(4,2) = link_param[1].inertia(1,2) + T12(4,2);
JA1(4,3) = link_param[1].mcm(2) + T12(4,3);
JA1(4,4) = T12(4,4);
JA1(4,5) = -link_param[1].mcm(0) + T12(4,5);

JA1(5,0) = link_param[1].inertia(0,2) + T12(5,0);
JA1(5,1) = link_param[1].inertia(1,2) + T12(5,1);
JA1(5,2) = link_param[1].inertia(2,2) + T12(5,2);
JA1(5,3) = -link_param[1].mcm(1) + T12(5,3);
JA1(5,4) = link_param[1].mcm(0) + T12(5,4);
JA1(5,5) = T12(5,5);


h1(0) = JA1(0,2);
h1(1) = JA1(1,2);
h1(2) = JA1(2,2);
h1(3) = JA1(3,2);
h1(4) = JA1(4,2);
h1(5) = JA1(5,2);

d(0) = 1.e-10 + h1(5);

T101(0,0) = -((h1(0)*h1(3))/d(0)) + JA1(0,0);
T101(0,1) = -((h1(0)*h1(4))/d(0)) + JA1(0,1);
T101(0,2) = -((h1(0)*h1(5))/d(0)) + JA1(0,2);
T101(0,3) = -(pow(h1(0),2)/d(0)) + JA1(0,3);
T101(0,4) = -((h1(0)*h1(1))/d(0)) + JA1(0,4);
T101(0,5) = -((h1(0)*h1(2))/d(0)) + JA1(0,5);

T101(1,0) = -((h1(1)*h1(3))/d(0)) + JA1(1,0);
T101(1,1) = -((h1(1)*h1(4))/d(0)) + JA1(1,1);
T101(1,2) = -((h1(1)*h1(5))/d(0)) + JA1(1,2);
T101(1,3) = -((h1(0)*h1(1))/d(0)) + JA1(1,3);
T101(1,4) = -(pow(h1(1),2)/d(0)) + JA1(1,4);
T101(1,5) = -((h1(1)*h1(2))/d(0)) + JA1(1,5);

T101(2,0) = -((h1(2)*h1(3))/d(0)) + JA1(2,0);
T101(2,1) = -((h1(2)*h1(4))/d(0)) + JA1(2,1);
T101(2,2) = -((h1(2)*h1(5))/d(0)) + JA1(2,2);
T101(2,3) = -((h1(0)*h1(2))/d(0)) + JA1(2,3);
T101(2,4) = -((h1(1)*h1(2))/d(0)) + JA1(2,4);
T101(2,5) = -(pow(h1(2),2)/d(0)) + JA1(2,5);

T101(3,0) = -(pow(h1(3),2)/d(0)) + JA1(3,0);
T101(3,1) = -((h1(3)*h1(4))/d(0)) + JA1(3,1);
T101(3,2) = -((h1(3)*h1(5))/d(0)) + JA1(3,2);
T101(3,3) = -((h1(0)*h1(3))/d(0)) + JA1(3,3);
T101(3,4) = -((h1(1)*h1(3))/d(0)) + JA1(3,4);
T101(3,5) = -((h1(2)*h1(3))/d(0)) + JA1(3,5);

T101(4,0) = -((h1(3)*h1(4))/d(0)) + JA1(4,0);
T101(4,1) = -(pow(h1(4),2)/d(0)) + JA1(4,1);
T101(4,2) = -((h1(4)*h1(5))/d(0)) + JA1(4,2);
T101(4,3) = -((h1(0)*h1(4))/d(0)) + JA1(4,3);
T101(4,4) = -((h1(1)*h1(4))/d(0)) + JA1(4,4);
T101(4,5) = -((h1(2)*h1(4))/d(0)) + JA1(4,5);

T101(5,0) = -((h1(3)*h1(5))/d(0)) + JA1(5,0);
T101(5,1) = -((h1(4)*h1(5))/d(0)) + JA1(5,1);
T101(5,2) = -(pow(h1(5),2)/d(0)) + JA1(5,2);
T101(5,3) = -((h1(0)*h1(5))/d(0)) + JA1(5,3);
T101(5,4) = -((h1(1)*h1(5))/d(0)) + JA1(5,4);
T101(5,5) = -((h1(2)*h1(5))/d(0)) + JA1(5,5);


T01(0,0) = S10(0,0)*(Si01(0,0)*T101(0,0) + Si01(0,1)*T101(1,0)) + S10(1,0)*(Si01(0,0)*T101(0,1) + Si01(0,1)*T101(1,1)) - ZSFE*S10(0,1)*(Si01(0,0)*T101(0,3) + Si01(0,1)*T101(1,3)) - ZSFE*S10(1,1)*(Si01(0,0)*T101(0,4) + Si01(0,1)*T101(1,4));
T01(0,1) = S10(0,1)*(Si01(0,0)*T101(0,0) + Si01(0,1)*T101(1,0)) + S10(1,1)*(Si01(0,0)*T101(0,1) + Si01(0,1)*T101(1,1)) + ZSFE*S10(0,0)*(Si01(0,0)*T101(0,3) + Si01(0,1)*T101(1,3)) + ZSFE*S10(1,0)*(Si01(0,0)*T101(0,4) + Si01(0,1)*T101(1,4));
T01(0,2) = Si01(0,0)*T101(0,2) + Si01(0,1)*T101(1,2);
T01(0,3) = S10(0,0)*(Si01(0,0)*T101(0,3) + Si01(0,1)*T101(1,3)) + S10(1,0)*(Si01(0,0)*T101(0,4) + Si01(0,1)*T101(1,4));
T01(0,4) = S10(0,1)*(Si01(0,0)*T101(0,3) + Si01(0,1)*T101(1,3)) + S10(1,1)*(Si01(0,0)*T101(0,4) + Si01(0,1)*T101(1,4));
T01(0,5) = Si01(0,0)*T101(0,5) + Si01(0,1)*T101(1,5);

T01(1,0) = S10(0,0)*(Si01(1,0)*T101(0,0) + Si01(1,1)*T101(1,0)) + S10(1,0)*(Si01(1,0)*T101(0,1) + Si01(1,1)*T101(1,1)) - ZSFE*S10(0,1)*(Si01(1,0)*T101(0,3) + Si01(1,1)*T101(1,3)) - ZSFE*S10(1,1)*(Si01(1,0)*T101(0,4) + Si01(1,1)*T101(1,4));
T01(1,1) = S10(0,1)*(Si01(1,0)*T101(0,0) + Si01(1,1)*T101(1,0)) + S10(1,1)*(Si01(1,0)*T101(0,1) + Si01(1,1)*T101(1,1)) + ZSFE*S10(0,0)*(Si01(1,0)*T101(0,3) + Si01(1,1)*T101(1,3)) + ZSFE*S10(1,0)*(Si01(1,0)*T101(0,4) + Si01(1,1)*T101(1,4));
T01(1,2) = Si01(1,0)*T101(0,2) + Si01(1,1)*T101(1,2);
T01(1,3) = S10(0,0)*(Si01(1,0)*T101(0,3) + Si01(1,1)*T101(1,3)) + S10(1,0)*(Si01(1,0)*T101(0,4) + Si01(1,1)*T101(1,4));
T01(1,4) = S10(0,1)*(Si01(1,0)*T101(0,3) + Si01(1,1)*T101(1,3)) + S10(1,1)*(Si01(1,0)*T101(0,4) + Si01(1,1)*T101(1,4));
T01(1,5) = Si01(1,0)*T101(0,5) + Si01(1,1)*T101(1,5);

T01(2,0) = S10(0,0)*T101(2,0) + S10(1,0)*T101(2,1) - ZSFE*S10(0,1)*T101(2,3) - ZSFE*S10(1,1)*T101(2,4);
T01(2,1) = S10(0,1)*T101(2,0) + S10(1,1)*T101(2,1) + ZSFE*S10(0,0)*T101(2,3) + ZSFE*S10(1,0)*T101(2,4);
T01(2,2) = T101(2,2);
T01(2,3) = S10(0,0)*T101(2,3) + S10(1,0)*T101(2,4);
T01(2,4) = S10(0,1)*T101(2,3) + S10(1,1)*T101(2,4);
T01(2,5) = T101(2,5);

T01(3,0) = S10(0,0)*(-(ZSFE*Si01(1,0)*T101(0,0)) - ZSFE*Si01(1,1)*T101(1,0) + Si01(0,0)*T101(3,0) + Si01(0,1)*T101(4,0)) + S10(1,0)*(-(ZSFE*Si01(1,0)*T101(0,1)) - ZSFE*Si01(1,1)*T101(1,1) + Si01(0,0)*T101(3,1) + Si01(0,1)*T101(4,1)) - ZSFE*S10(0,1)*(-(ZSFE*Si01(1,0)*T101(0,3)) - ZSFE*Si01(1,1)*T101(1,3) + Si01(0,0)*T101(3,3) + Si01(0,1)*T101(4,3)) - ZSFE*S10(1,1)*(-(ZSFE*Si01(1,0)*T101(0,4)) - ZSFE*Si01(1,1)*T101(1,4) + Si01(0,0)*T101(3,4) + Si01(0,1)*T101(4,4));
T01(3,1) = S10(0,1)*(-(ZSFE*Si01(1,0)*T101(0,0)) - ZSFE*Si01(1,1)*T101(1,0) + Si01(0,0)*T101(3,0) + Si01(0,1)*T101(4,0)) + S10(1,1)*(-(ZSFE*Si01(1,0)*T101(0,1)) - ZSFE*Si01(1,1)*T101(1,1) + Si01(0,0)*T101(3,1) + Si01(0,1)*T101(4,1)) + ZSFE*S10(0,0)*(-(ZSFE*Si01(1,0)*T101(0,3)) - ZSFE*Si01(1,1)*T101(1,3) + Si01(0,0)*T101(3,3) + Si01(0,1)*T101(4,3)) + ZSFE*S10(1,0)*(-(ZSFE*Si01(1,0)*T101(0,4)) - ZSFE*Si01(1,1)*T101(1,4) + Si01(0,0)*T101(3,4) + Si01(0,1)*T101(4,4));
T01(3,2) = -(ZSFE*Si01(1,0)*T101(0,2)) - ZSFE*Si01(1,1)*T101(1,2) + Si01(0,0)*T101(3,2) + Si01(0,1)*T101(4,2);
T01(3,3) = S10(0,0)*(-(ZSFE*Si01(1,0)*T101(0,3)) - ZSFE*Si01(1,1)*T101(1,3) + Si01(0,0)*T101(3,3) + Si01(0,1)*T101(4,3)) + S10(1,0)*(-(ZSFE*Si01(1,0)*T101(0,4)) - ZSFE*Si01(1,1)*T101(1,4) + Si01(0,0)*T101(3,4) + Si01(0,1)*T101(4,4));
T01(3,4) = S10(0,1)*(-(ZSFE*Si01(1,0)*T101(0,3)) - ZSFE*Si01(1,1)*T101(1,3) + Si01(0,0)*T101(3,3) + Si01(0,1)*T101(4,3)) + S10(1,1)*(-(ZSFE*Si01(1,0)*T101(0,4)) - ZSFE*Si01(1,1)*T101(1,4) + Si01(0,0)*T101(3,4) + Si01(0,1)*T101(4,4));
T01(3,5) = -(ZSFE*Si01(1,0)*T101(0,5)) - ZSFE*Si01(1,1)*T101(1,5) + Si01(0,0)*T101(3,5) + Si01(0,1)*T101(4,5);

T01(4,0) = S10(0,0)*(ZSFE*Si01(0,0)*T101(0,0) + ZSFE*Si01(0,1)*T101(1,0) + Si01(1,0)*T101(3,0) + Si01(1,1)*T101(4,0)) + S10(1,0)*(ZSFE*Si01(0,0)*T101(0,1) + ZSFE*Si01(0,1)*T101(1,1) + Si01(1,0)*T101(3,1) + Si01(1,1)*T101(4,1)) - ZSFE*S10(0,1)*(ZSFE*Si01(0,0)*T101(0,3) + ZSFE*Si01(0,1)*T101(1,3) + Si01(1,0)*T101(3,3) + Si01(1,1)*T101(4,3)) - ZSFE*S10(1,1)*(ZSFE*Si01(0,0)*T101(0,4) + ZSFE*Si01(0,1)*T101(1,4) + Si01(1,0)*T101(3,4) + Si01(1,1)*T101(4,4));
T01(4,1) = S10(0,1)*(ZSFE*Si01(0,0)*T101(0,0) + ZSFE*Si01(0,1)*T101(1,0) + Si01(1,0)*T101(3,0) + Si01(1,1)*T101(4,0)) + S10(1,1)*(ZSFE*Si01(0,0)*T101(0,1) + ZSFE*Si01(0,1)*T101(1,1) + Si01(1,0)*T101(3,1) + Si01(1,1)*T101(4,1)) + ZSFE*S10(0,0)*(ZSFE*Si01(0,0)*T101(0,3) + ZSFE*Si01(0,1)*T101(1,3) + Si01(1,0)*T101(3,3) + Si01(1,1)*T101(4,3)) + ZSFE*S10(1,0)*(ZSFE*Si01(0,0)*T101(0,4) + ZSFE*Si01(0,1)*T101(1,4) + Si01(1,0)*T101(3,4) + Si01(1,1)*T101(4,4));
T01(4,2) = ZSFE*Si01(0,0)*T101(0,2) + ZSFE*Si01(0,1)*T101(1,2) + Si01(1,0)*T101(3,2) + Si01(1,1)*T101(4,2);
T01(4,3) = S10(0,0)*(ZSFE*Si01(0,0)*T101(0,3) + ZSFE*Si01(0,1)*T101(1,3) + Si01(1,0)*T101(3,3) + Si01(1,1)*T101(4,3)) + S10(1,0)*(ZSFE*Si01(0,0)*T101(0,4) + ZSFE*Si01(0,1)*T101(1,4) + Si01(1,0)*T101(3,4) + Si01(1,1)*T101(4,4));
T01(4,4) = S10(0,1)*(ZSFE*Si01(0,0)*T101(0,3) + ZSFE*Si01(0,1)*T101(1,3) + Si01(1,0)*T101(3,3) + Si01(1,1)*T101(4,3)) + S10(1,1)*(ZSFE*Si01(0,0)*T101(0,4) + ZSFE*Si01(0,1)*T101(1,4) + Si01(1,0)*T101(3,4) + Si01(1,1)*T101(4,4));
T01(4,5) = ZSFE*Si01(0,0)*T101(0,5) + ZSFE*Si01(0,1)*T101(1,5) + Si01(1,0)*T101(3,5) + Si01(1,1)*T101(4,5);

T01(5,0) = S10(0,0)*T101(5,0) + S10(1,0)*T101(5,1) - ZSFE*S10(0,1)*T101(5,3) - ZSFE*S10(1,1)*T101(5,4);
T01(5,1) = S10(0,1)*T101(5,0) + S10(1,1)*T101(5,1) + ZSFE*S10(0,0)*T101(5,3) + ZSFE*S10(1,0)*T101(5,4);
T01(5,2) = T101(5,2);
T01(5,3) = S10(0,0)*T101(5,3) + S10(1,0)*T101(5,4);
T01(5,4) = S10(0,1)*T101(5,3) + S10(1,1)*T101(5,4);
T01(5,5) = T101(5,5);

//wam_ForDynArtfunc15

JA0(0,0) = T01(0,0);
JA0(0,1) = link_param[0].mcm(2) + T01(0,1);
JA0(0,2) = -link_param[0].mcm(1) + T01(0,2);
JA0(0,3) = link_param[0].mass+ T01(0,3);
JA0(0,4) = T01(0,4);
JA0(0,5) = T01(0,5);

JA0(1,0) = -link_param[0].mcm(2) + T01(1,0);
JA0(1,1) = T01(1,1);
JA0(1,2) = link_param[0].mcm(0) + T01(1,2);
JA0(1,3) = T01(1,3);
JA0(1,4) = link_param[0].mass+ T01(1,4);
JA0(1,5) = T01(1,5);

JA0(2,0) = link_param[0].mcm(1) + T01(2,0);
JA0(2,1) = -link_param[0].mcm(0) + T01(2,1);
JA0(2,2) = T01(2,2);
JA0(2,3) = T01(2,3);
JA0(2,4) = T01(2,4);
JA0(2,5) = link_param[0].mass+ T01(2,5);

JA0(3,0) = link_param[0].inertia(0,0) + T01(3,0);
JA0(3,1) = link_param[0].inertia(0,1) + T01(3,1);
JA0(3,2) = link_param[0].inertia(0,2) + T01(3,2);
JA0(3,3) = T01(3,3);
JA0(3,4) = -link_param[0].mcm(2) + T01(3,4);
JA0(3,5) = link_param[0].mcm(1) + T01(3,5);

JA0(4,0) = link_param[0].inertia(0,1) + T01(4,0);
JA0(4,1) = link_param[0].inertia(1,1) + T01(4,1);
JA0(4,2) = link_param[0].inertia(1,2) + T01(4,2);
JA0(4,3) = link_param[0].mcm(2) + T01(4,3);
JA0(4,4) = T01(4,4);
JA0(4,5) = -link_param[0].mcm(0) + T01(4,5);

JA0(5,0) = link_param[0].inertia(0,2) + T01(5,0);
JA0(5,1) = link_param[0].inertia(1,2) + T01(5,1);
JA0(5,2) = link_param[0].inertia(2,2) + T01(5,2);
JA0(5,3) = -link_param[0].mcm(1) + T01(5,3);
JA0(5,4) = link_param[0].mcm(0) + T01(5,4);
JA0(5,5) = T01(5,5);

//wam_ForDynArtfunc16

// bias forces
p8(0) = pv8(0);
p8(1) = pv8(1);
p8(2) = pv8(2);
p8(3) = pv8(3);
p8(4) = pv8(4);
p8(5) = pv8(5);

pmm8(0) = p8(0);
pmm8(1) = p8(1);
pmm8(2) = p8(2);
pmm8(3) = p8(3);
pmm8(4) = p8(4);
pmm8(5) = p8(5);

pm8(0) = pmm8(0)*Si78(0,0) + pmm8(1)*Si78(0,1) + pmm8(2)*Si78(0,2);
pm8(1) = pmm8(0)*Si78(1,0) + pmm8(1)*Si78(1,1) + pmm8(2)*Si78(1,2);
pm8(2) = pmm8(0)*Si78(2,0) + pmm8(1)*Si78(2,1) + pmm8(2)*Si78(2,2);
pm8(3) = pmm8(3)*Si78(0,0) + pmm8(4)*Si78(0,1) + pmm8(5)*Si78(0,2) + pmm8(0)*(-(endeff_param.x(2)*Si78(1,0)) + endeff_param.x(1)*Si78(2,0)) + pmm8(1)*(-(endeff_param.x(2)*Si78(1,1)) + endeff_param.x(1)*Si78(2,1)) + pmm8(2)*(-(endeff_param.x(2)*Si78(1,2)) + endeff_param.x(1)*Si78(2,2));
pm8(4) = pmm8(3)*Si78(1,0) + pmm8(4)*Si78(1,1) + pmm8(5)*Si78(1,2) + pmm8(0)*(endeff_param.x(2)*Si78(0,0) - endeff_param.x(0)*Si78(2,0)) + pmm8(1)*(endeff_param.x(2)*Si78(0,1) - endeff_param.x(0)*Si78(2,1)) + pmm8(2)*(endeff_param.x(2)*Si78(0,2) - endeff_param.x(0)*Si78(2,2));
pm8(5) = pmm8(0)*(-(endeff_param.x(1)*Si78(0,0)) + endeff_param.x(0)*Si78(1,0)) + pmm8(1)*(-(endeff_param.x(1)*Si78(0,1)) + endeff_param.x(0)*Si78(1,1)) + pmm8(2)*(-(endeff_param.x(1)*Si78(0,2)) + endeff_param.x(0)*Si78(1,2)) + pmm8(3)*Si78(2,0) + pmm8(4)*Si78(2,1) + pmm8(5)*Si78(2,2);

p7(0) = pm8(0) + pv7(0);
p7(1) = pm8(1) + pv7(1);
p7(2) = pm8(2) + pv7(2);
p7(3) = pm8(3) + pv7(3);
p7(4) = pm8(4) + pv7(4);
p7(5) = pm8(5) + pv7(5);

u(6) = u(6) - c7(3)*h7(0) - c7(4)*h7(1) + 0.*h7(2) - c7(0)*h7(3) - c7(1)*h7(4) + 0.*h7(5) - p7(5);

pmm7(0) = p7(0) + (h7(0)*u(6))/d(6) + c7(0)*JA7(0,0) + c7(1)*JA7(0,1) + c7(3)*JA7(0,3) + c7(4)*JA7(0,4);
pmm7(1) = p7(1) + (h7(1)*u(6))/d(6) + c7(0)*JA7(1,0) + c7(1)*JA7(1,1) + c7(3)*JA7(1,3) + c7(4)*JA7(1,4);
pmm7(2) = p7(2) + (h7(2)*u(6))/d(6) + c7(0)*JA7(2,0) + c7(1)*JA7(2,1) + c7(3)*JA7(2,3) + c7(4)*JA7(2,4);
pmm7(3) = p7(3) + (h7(3)*u(6))/d(6) + c7(0)*JA7(3,0) + c7(1)*JA7(3,1) + c7(3)*JA7(3,3) + c7(4)*JA7(3,4);
pmm7(4) = p7(4) + (h7(4)*u(6))/d(6) + c7(0)*JA7(4,0) + c7(1)*JA7(4,1) + c7(3)*JA7(4,3) + c7(4)*JA7(4,4);
pmm7(5) = p7(5) + (h7(5)*u(6))/d(6) + c7(0)*JA7(5,0) + c7(1)*JA7(5,1) + c7(3)*JA7(5,3) + c7(4)*JA7(5,4);

pm7(0) = pmm7(2);
pm7(1) = pmm7(0)*Si67(1,0) + pmm7(1)*Si67(1,1);
pm7(2) = pmm7(0)*Si67(2,0) + pmm7(1)*Si67(2,1);
pm7(3) = pmm7(5);
pm7(4) = pmm7(3)*Si67(1,0) + pmm7(4)*Si67(1,1);
pm7(5) = pmm7(3)*Si67(2,0) + pmm7(4)*Si67(2,1);

p6(0) = pm7(0) + pv6(0);
p6(1) = pm7(1) + pv6(1);
p6(2) = pm7(2) + pv6(2);
p6(3) = pm7(3) + pv6(3);
p6(4) = pm7(4) + pv6(4);
p6(5) = pm7(5) + pv6(5);

u(5) = u(5) - c6(3)*h6(0) - c6(4)*h6(1) + 0.*h6(2) - c6(0)*h6(3) - c6(1)*h6(4) + 0.*h6(5) - p6(5);

pmm6(0) = p6(0) + (h6(0)*u(5))/d(5) + c6(0)*JA6(0,0) + c6(1)*JA6(0,1) + c6(3)*JA6(0,3) + c6(4)*JA6(0,4);
pmm6(1) = p6(1) + (h6(1)*u(5))/d(5) + c6(0)*JA6(1,0) + c6(1)*JA6(1,1) + c6(3)*JA6(1,3) + c6(4)*JA6(1,4);
pmm6(2) = p6(2) + (h6(2)*u(5))/d(5) + c6(0)*JA6(2,0) + c6(1)*JA6(2,1) + c6(3)*JA6(2,3) + c6(4)*JA6(2,4);
pmm6(3) = p6(3) + (h6(3)*u(5))/d(5) + c6(0)*JA6(3,0) + c6(1)*JA6(3,1) + c6(3)*JA6(3,3) + c6(4)*JA6(3,4);
pmm6(4) = p6(4) + (h6(4)*u(5))/d(5) + c6(0)*JA6(4,0) + c6(1)*JA6(4,1) + c6(3)*JA6(4,3) + c6(4)*JA6(4,4);
pmm6(5) = p6(5) + (h6(5)*u(5))/d(5) + c6(0)*JA6(5,0) + c6(1)*JA6(5,1) + c6(3)*JA6(5,3) + c6(4)*JA6(5,4);

pm6(0) = -pmm6(2);
pm6(1) = pmm6(0)*Si56(1,0) + pmm6(1)*Si56(1,1);
pm6(2) = pmm6(0)*Si56(2,0) + pmm6(1)*Si56(2,1);
pm6(3) = -pmm6(5) - ZWFE*pmm6(0)*Si56(1,0) - ZWFE*pmm6(1)*Si56(1,1);
pm6(4) = -(ZWFE*pmm6(2)) + pmm6(3)*Si56(1,0) + pmm6(4)*Si56(1,1);
pm6(5) = pmm6(3)*Si56(2,0) + pmm6(4)*Si56(2,1);

p5(0) = pm6(0) + pv5(0);
p5(1) = pm6(1) + pv5(1);
p5(2) = pm6(2) + pv5(2);
p5(3) = pm6(3) + pv5(3);
p5(4) = pm6(4) + pv5(4);
p5(5) = pm6(5) + pv5(5);

u(4) = u(4) - c5(3)*h5(0) - c5(4)*h5(1) + 0.*h5(2) - c5(0)*h5(3) - c5(1)*h5(4) + 0.*h5(5) - p5(5);

pmm5(0) = p5(0) + (h5(0)*u(4))/d(4) + c5(0)*JA5(0,0) + c5(1)*JA5(0,1) + c5(3)*JA5(0,3) + c5(4)*JA5(0,4);
pmm5(1) = p5(1) + (h5(1)*u(4))/d(4) + c5(0)*JA5(1,0) + c5(1)*JA5(1,1) + c5(3)*JA5(1,3) + c5(4)*JA5(1,4);
pmm5(2) = p5(2) + (h5(2)*u(4))/d(4) + c5(0)*JA5(2,0) + c5(1)*JA5(2,1) + c5(3)*JA5(2,3) + c5(4)*JA5(2,4);
pmm5(3) = p5(3) + (h5(3)*u(4))/d(4) + c5(0)*JA5(3,0) + c5(1)*JA5(3,1) + c5(3)*JA5(3,3) + c5(4)*JA5(3,4);
pmm5(4) = p5(4) + (h5(4)*u(4))/d(4) + c5(0)*JA5(4,0) + c5(1)*JA5(4,1) + c5(3)*JA5(4,3) + c5(4)*JA5(4,4);
pmm5(5) = p5(5) + (h5(5)*u(4))/d(4) + c5(0)*JA5(5,0) + c5(1)*JA5(5,1) + c5(3)*JA5(5,3) + c5(4)*JA5(5,4);

pm5(0) = pmm5(2);
pm5(1) = pmm5(0)*Si45(1,0) + pmm5(1)*Si45(1,1);
pm5(2) = pmm5(0)*Si45(2,0) + pmm5(1)*Si45(2,1);
pm5(3) = pmm5(5) + YWR*pmm5(0)*Si45(2,0) + YWR*pmm5(1)*Si45(2,1);
pm5(4) = pmm5(3)*Si45(1,0) + pmm5(4)*Si45(1,1) - ZWR*pmm5(0)*Si45(2,0) - ZWR*pmm5(1)*Si45(2,1);
pm5(5) = -(YWR*pmm5(2)) + ZWR*pmm5(0)*Si45(1,0) + ZWR*pmm5(1)*Si45(1,1) + pmm5(3)*Si45(2,0) + pmm5(4)*Si45(2,1);

p4(0) = pm5(0) + pv4(0);
p4(1) = pm5(1) + pv4(1);
p4(2) = pm5(2) + pv4(2);
p4(3) = pm5(3) + pv4(3);
p4(4) = pm5(4) + pv4(4);
p4(5) = pm5(5) + pv4(5);

u(3) = u(3) - c4(3)*h4(0) - c4(4)*h4(1) + 0.*h4(2) - c4(0)*h4(3) - c4(1)*h4(4) + 0.*h4(5) - p4(5);

pmm4(0) = p4(0) + (h4(0)*u(3))/d(3) + c4(0)*JA4(0,0) + c4(1)*JA4(0,1) + c4(3)*JA4(0,3) + c4(4)*JA4(0,4);
pmm4(1) = p4(1) + (h4(1)*u(3))/d(3) + c4(0)*JA4(1,0) + c4(1)*JA4(1,1) + c4(3)*JA4(1,3) + c4(4)*JA4(1,4);
pmm4(2) = p4(2) + (h4(2)*u(3))/d(3) + c4(0)*JA4(2,0) + c4(1)*JA4(2,1) + c4(3)*JA4(2,3) + c4(4)*JA4(2,4);
pmm4(3) = p4(3) + (h4(3)*u(3))/d(3) + c4(0)*JA4(3,0) + c4(1)*JA4(3,1) + c4(3)*JA4(3,3) + c4(4)*JA4(3,4);
pmm4(4) = p4(4) + (h4(4)*u(3))/d(3) + c4(0)*JA4(4,0) + c4(1)*JA4(4,1) + c4(3)*JA4(4,3) + c4(4)*JA4(4,4);
pmm4(5) = p4(5) + (h4(5)*u(3))/d(3) + c4(0)*JA4(5,0) + c4(1)*JA4(5,1) + c4(3)*JA4(5,3) + c4(4)*JA4(5,4);

pm4(0) = -pmm4(2);
pm4(1) = pmm4(0)*Si34(1,0) + pmm4(1)*Si34(1,1);
pm4(2) = pmm4(0)*Si34(2,0) + pmm4(1)*Si34(2,1);
pm4(3) = -pmm4(5) + pmm4(0)*(-(ZEB*Si34(1,0)) + YEB*Si34(2,0)) + pmm4(1)*(-(ZEB*Si34(1,1)) + YEB*Si34(2,1));
pm4(4) = -(ZEB*pmm4(2)) + pmm4(3)*Si34(1,0) + pmm4(4)*Si34(1,1);
pm4(5) = YEB*pmm4(2) + pmm4(3)*Si34(2,0) + pmm4(4)*Si34(2,1);

p3(0) = pm4(0) + pv3(0);
p3(1) = pm4(1) + pv3(1);
p3(2) = pm4(2) + pv3(2);
p3(3) = pm4(3) + pv3(3);
p3(4) = pm4(4) + pv3(4);
p3(5) = pm4(5) + pv3(5);

u(2) = u(2) - c3(3)*h3(0) - c3(4)*h3(1) + 0.*h3(2) - c3(0)*h3(3) - c3(1)*h3(4) + 0.*h3(5) - p3(5);

pmm3(0) = p3(0) + (h3(0)*u(2))/d(2) + c3(0)*JA3(0,0) + c3(1)*JA3(0,1) + c3(3)*JA3(0,3) + c3(4)*JA3(0,4);
pmm3(1) = p3(1) + (h3(1)*u(2))/d(2) + c3(0)*JA3(1,0) + c3(1)*JA3(1,1) + c3(3)*JA3(1,3) + c3(4)*JA3(1,4);
pmm3(2) = p3(2) + (h3(2)*u(2))/d(2) + c3(0)*JA3(2,0) + c3(1)*JA3(2,1) + c3(3)*JA3(2,3) + c3(4)*JA3(2,4);
pmm3(3) = p3(3) + (h3(3)*u(2))/d(2) + c3(0)*JA3(3,0) + c3(1)*JA3(3,1) + c3(3)*JA3(3,3) + c3(4)*JA3(3,4);
pmm3(4) = p3(4) + (h3(4)*u(2))/d(2) + c3(0)*JA3(4,0) + c3(1)*JA3(4,1) + c3(3)*JA3(4,3) + c3(4)*JA3(4,4);
pmm3(5) = p3(5) + (h3(5)*u(2))/d(2) + c3(0)*JA3(5,0) + c3(1)*JA3(5,1) + c3(3)*JA3(5,3) + c3(4)*JA3(5,4);

pm3(0) = pmm3(2);
pm3(1) = pmm3(0)*Si23(1,0) + pmm3(1)*Si23(1,1);
pm3(2) = pmm3(0)*Si23(2,0) + pmm3(1)*Si23(2,1);
pm3(3) = pmm3(5);
pm3(4) = pmm3(3)*Si23(1,0) + pmm3(4)*Si23(1,1) - ZHR*pmm3(0)*Si23(2,0) - ZHR*pmm3(1)*Si23(2,1);
pm3(5) = ZHR*pmm3(0)*Si23(1,0) + ZHR*pmm3(1)*Si23(1,1) + pmm3(3)*Si23(2,0) + pmm3(4)*Si23(2,1);

p2(0) = pm3(0) + pv2(0);
p2(1) = pm3(1) + pv2(1);
p2(2) = pm3(2) + pv2(2);
p2(3) = pm3(3) + pv2(3);
p2(4) = pm3(4) + pv2(4);
p2(5) = pm3(5) + pv2(5);

u(1) = u(1) - c2(3)*h2(0) - c2(4)*h2(1) + 0.*h2(2) - c2(0)*h2(3) - c2(1)*h2(4) + 0.*h2(5) - p2(5);

pmm2(0) = p2(0) + (h2(0)*u(1))/d(1) + c2(0)*JA2(0,0) + c2(1)*JA2(0,1) + c2(3)*JA2(0,3) + c2(4)*JA2(0,4);
pmm2(1) = p2(1) + (h2(1)*u(1))/d(1) + c2(0)*JA2(1,0) + c2(1)*JA2(1,1) + c2(3)*JA2(1,3) + c2(4)*JA2(1,4);
pmm2(2) = p2(2) + (h2(2)*u(1))/d(1) + c2(0)*JA2(2,0) + c2(1)*JA2(2,1) + c2(3)*JA2(2,3) + c2(4)*JA2(2,4);
pmm2(3) = p2(3) + (h2(3)*u(1))/d(1) + c2(0)*JA2(3,0) + c2(1)*JA2(3,1) + c2(3)*JA2(3,3) + c2(4)*JA2(3,4);
pmm2(4) = p2(4) + (h2(4)*u(1))/d(1) + c2(0)*JA2(4,0) + c2(1)*JA2(4,1) + c2(3)*JA2(4,3) + c2(4)*JA2(4,4);
pmm2(5) = p2(5) + (h2(5)*u(1))/d(1) + c2(0)*JA2(5,0) + c2(1)*JA2(5,1) + c2(3)*JA2(5,3) + c2(4)*JA2(5,4);

pm2(0) = -pmm2(2);
pm2(1) = pmm2(0)*Si12(1,0) + pmm2(1)*Si12(1,1);
pm2(2) = pmm2(0)*Si12(2,0) + pmm2(1)*Si12(2,1);
pm2(3) = -pmm2(5);
pm2(4) = pmm2(3)*Si12(1,0) + pmm2(4)*Si12(1,1);
pm2(5) = pmm2(3)*Si12(2,0) + pmm2(4)*Si12(2,1);

p1(0) = pm2(0) + pv1(0);
p1(1) = pm2(1) + pv1(1);
p1(2) = pm2(2) + pv1(2);
p1(3) = pm2(3) + pv1(3);
p1(4) = pm2(4) + pv1(4);
p1(5) = pm2(5) + pv1(5);

u(0) = u(0) - c1(3)*h1(0) - c1(4)*h1(1) + 0.*h1(2) - c1(0)*h1(3) - c1(1)*h1(4) + 0.*h1(5) - p1(5);

pmm1(0) = p1(0) + (h1(0)*u(0))/d(0) + c1(0)*JA1(0,0) + c1(1)*JA1(0,1) + c1(3)*JA1(0,3) + c1(4)*JA1(0,4);
pmm1(1) = p1(1) + (h1(1)*u(0))/d(0) + c1(0)*JA1(1,0) + c1(1)*JA1(1,1) + c1(3)*JA1(1,3) + c1(4)*JA1(1,4);
pmm1(2) = p1(2) + (h1(2)*u(0))/d(0) + c1(0)*JA1(2,0) + c1(1)*JA1(2,1) + c1(3)*JA1(2,3) + c1(4)*JA1(2,4);
pmm1(3) = p1(3) + (h1(3)*u(0))/d(0) + c1(0)*JA1(3,0) + c1(1)*JA1(3,1) + c1(3)*JA1(3,3) + c1(4)*JA1(3,4);
pmm1(4) = p1(4) + (h1(4)*u(0))/d(0) + c1(0)*JA1(4,0) + c1(1)*JA1(4,1) + c1(3)*JA1(4,3) + c1(4)*JA1(4,4);
pmm1(5) = p1(5) + (h1(5)*u(0))/d(0) + c1(0)*JA1(5,0) + c1(1)*JA1(5,1) + c1(3)*JA1(5,3) + c1(4)*JA1(5,4);

pm1(0) = pmm1(0)*Si01(0,0) + pmm1(1)*Si01(0,1);
pm1(1) = pmm1(0)*Si01(1,0) + pmm1(1)*Si01(1,1);
pm1(2) = pmm1(2);
pm1(3) = pmm1(3)*Si01(0,0) + pmm1(4)*Si01(0,1) - ZSFE*pmm1(0)*Si01(1,0) - ZSFE*pmm1(1)*Si01(1,1);
pm1(4) = ZSFE*pmm1(0)*Si01(0,0) + ZSFE*pmm1(1)*Si01(0,1) + pmm1(3)*Si01(1,0) + pmm1(4)*Si01(1,1);
pm1(5) = pmm1(5);

p0(0) = pm1(0) + pv0(0);
p0(1) = pm1(1) + pv0(1);
p0(2) = pm1(2) + pv0(2);
p0(3) = pm1(3) + pv0(3);
p0(4) = pm1(4) + pv0(4);
p0(5) = pm1(5) + pv0(5);

//// Compute acceleration vectors and joint accelerations
qdd(0) = u(0)/d(0);

a1(0) = c1(0);
a1(1) = c1(1);
a1(2) = qdd(0);
a1(3) = c1(3);
a1(4) = c1(4);

qdd(1) = (a1(3) * h2(2) + a1(0) * h2(5) + u(1) - a1(4) * (h2(0) * S21(0,1)
         + h2(1) * S21(1,1)) - a1(1) * (h2(3) * S21(0,1) + h2(4) * S21(1,1))
         - a1(2) * (h2(3) * S21(0,2) + h2(4) * S21(1,2)))/d(1);

a2(0) = c2(0) + a1(1) * S21(0,1) + a1(2) * S21(0,2);
a2(1) = c2(1) + a1(1) * S21(1,1) + a1(2) * S21(1,2);
a2(2) = qdd(1) - a1(0);
a2(3) = c2(3) + a1(4) * S21(0,1);
a2(4) = c2(4) + a1(4) * S21(1,1);
a2(5) = -a1(3);

qdd(2) = (-(a2(3) * h3(2)) - a2(0) * h3(5) + u(2) - a2(4) * (h3(0) * S32(0,1)
        + h3(1) * S32(1,1)) - a2(5) * (h3(0) * S32(0,2) + h3(1) * S32(1,2))
        - a2(1) * (h3(3) * S32(0,1) - ZHR * h3(0) * S32(0,2) + h3(4) * S32(1,1)
        - ZHR * h3(1) * S32(1,2)) - a2(2) * (ZHR * h3(0) * S32(0,1) +
        h3(3) * S32(0,2) + ZHR * h3(1) * S32(1,1) + h3(4) * S32(1,2)))/d(2);

a3(0) = c3(0) + a2(1) * S32(0,1) + a2(2) * S32(0,2);
a3(1) = c3(1) + a2(1) * S32(1,1) + a2(2) * S32(1,2);
a3(2) = qdd(2) + a2(0);
a3(3) = c3(3) + ZHR * a2(2) * S32(0,1) + a2(4) * S32(0,1) - ZHR * a2(1) * S32(0,2)
        + a2(5) * S32(0,2);
a3(4) = c3(4) + ZHR * a2(2) * S32(1,1) + a2(4) * S32(1,1) - ZHR * a2(1) * S32(1,2)
        + a2(5) * S32(1,2);
a3(5) = a2(3);

qdd(3) = (a3(3) * h4(2) + u(3) - a3(4) * (h4(0) * S43(0,1) + h4(1) * S43(1,1))
        - a3(1) * (-(ZEB * h4(2)) + h4(3) * S43(0,1) + h4(4) * S43(1,1)) - a3(5) *
        (h4(0) * S43(0,2) + h4(1) * S43(1,2)) - a3(2) * (YEB * h4(2) + h4(3) *
        S43(0,2) + h4(4) * S43(1,2)) - a3(0) * (-h4(5) + h4(0) * (-(ZEB * S43(0,1))
        + YEB * S43(0,2)) + h4(1) * (-(ZEB * S43(1,1)) + YEB * S43(1,2))))/d(3);

a4(0) = c4(0) + a3(1) * S43(0,1) + a3(2) * S43(0,2);
a4(1) = c4(1) + a3(1) * S43(1,1) + a3(2) * S43(1,2);
a4(2) = qdd(3) - a3(0);
a4(3) = c4(3) + a3(4) * S43(0,1) + a3(5) * S43(0,2) + a3(0) * (-(ZEB * S43(0,1))
        + YEB * S43(0,2));
a4(4) = c4(4) + a3(4) * S43(1,1) + a3(5) * S43(1,2) + a3(0) * (-(ZEB * S43(1,1))
        + YEB * S43(1,2));
a4(5) = -(ZEB * a3(1)) + YEB * a3(2) - a3(3);

qdd(4) = (-(a4(3) * h5(2)) + u(4) - a4(4) * (h5(0) * S54(0,1) + h5(1) * S54(1,1))
        - a4(5) * (h5(0) * S54(0,2) + h5(1) * S54(1,2)) - a4(0) * (h5(5) + YWR * h5(0)
        * S54(0,2) + YWR * h5(1) * S54(1,2)) - a4(1) * (h5(3) * S54(0,1) - ZWR *
        h5(0) * S54(0,2) + h5(4) * S54(1,1) - ZWR * h5(1) * S54(1,2)) - a4(2) *
        (-(YWR * h5(2)) + ZWR * h5(0) * S54(0,1) + h5(3) * S54(0,2) + ZWR *
        h5(1) * S54(1,1) + h5(4) * S54(1,2)))/d(4);

a5(0) = c5(0) + a4(1) * S54(0,1) + a4(2) * S54(0,2);
a5(1) = c5(1) + a4(1) * S54(1,1) + a4(2) * S54(1,2);
a5(2) = qdd(4) + a4(0);
a5(3) = c5(3) + ZWR * a4(2) * S54(0,1) + a4(4) * S54(0,1) + YWR * a4(0) * S54(0,2)
        - ZWR * a4(1) * S54(0,2) + a4(5) * S54(0,2);
a5(4) = c5(4) + ZWR * a4(2) * S54(1,1) + a4(4) * S54(1,1) + YWR * a4(0) * S54(1,2)
        - ZWR * a4(1) * S54(1,2) + a4(5) * S54(1,2);
a5(5) = -(YWR * a4(2)) + a4(3);

qdd(5) = (a5(3) * h6(2) + u(5) - a5(4) * (h6(0) * S65(0,1) + h6(1) * S65(1,1))
        - a5(0) * (-h6(5) - ZWFE * h6(0) * S65(0,1) - ZWFE * h6(1) * S65(1,1))
        - a5(1) * (-(ZWFE * h6(2)) + h6(3) * S65(0,1) + h6(4) * S65(1,1)) -
         a5(5) * (h6(0) * S65(0,2) + h6(1) * S65(1,2)) - a5(2) * (h6(3) * S65(0,2)
         + h6(4) * S65(1,2)))/d(5);

a6(0) = c6(0) + a5(1) * S65(0,1) + a5(2) * S65(0,2);
a6(1) = c6(1) + a5(1) * S65(1,1) + a5(2) * S65(1,2);
a6(2) = qdd(5) - a5(0);
a6(3) = c6(3) - ZWFE * a5(0) * S65(0,1) + a5(4) * S65(0,1) + a5(5) * S65(0,2);
a6(4) = c6(4) - ZWFE * a5(0) * S65(1,1) + a5(4) * S65(1,1) + a5(5) * S65(1,2);
a6(5) = -(ZWFE * a5(1)) - a5(3);

qdd(6) = (-(a6(3) * h7(2)) - a6(0) * h7(5) + u(6) - a6(4) * (h7(0) * S76(0,1) +
         h7(1) * S76(1,1)) - a6(1) * (h7(3) * S76(0,1) + h7(4) * S76(1,1)) - a6(5) *
         (h7(0) * S76(0,2) + h7(1) * S76(1,2)) - a6(2) * (h7(3) * S76(0,2) +
         h7(4) * S76(1,2)))/d(6);

a7(0) = c7(0) + a6(1) * S76(0,1) + a6(2) * S76(0,2);
a7(1) = c7(1) + a6(1) * S76(1,1) + a6(2) * S76(1,2);
a7(2) = qdd(6) + a6(0);
a7(3) = c7(3) + a6(4) * S76(0,1) + a6(5) * S76(0,2);
a7(4) = c7(4) + a6(4) * S76(1,1) + a6(5) * S76(1,2);
a7(5) = a6(3);


Q.qdd = qdd;
}


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    
    static bool firsttime = true;
    static link link_param1[NDOF+1];
    static link link_param2[NDOF+1];
    static link link_param3[NDOF+1];
    const std::string filename1 = "LinkParameters1.cfg";
    const std::string filename2 = "LinkParameters2.cfg";
    const std::string filename3 = "LinkParameters3.cfg";

    
    if (firsttime) {
        load_link_params(filename1,link_param1); 
        load_link_params(filename2,link_param2); 
        load_link_params(filename3,link_param3); 
        firsttime = false;
    }    
    
    // Check the number of input arguments.
    if (nrhs != 4) {
        mexErrMsgTxt("First input firstthe link parameter file");
        mexErrMsgTxt("Then input q, qd, u separately to calculate qdd!");        
    }

    // Check type of input.
    int i;
    for (i = 1; i < nrhs; i++) {
      if (mxGetClassID(prhs[i]) != mxDOUBLE_CLASS) {
          mexErrMsgTxt("The inputs must be of type double!");
      }

    }

    int m = 7;
    int n = 1;
    int linkval = mxGetScalar(prhs[0]); 
    vec7 q = armaGetPr(prhs[1]);
    vec7 qd = armaGetPr(prhs[2]);
    vec7 u = armaGetPr(prhs[3]);

    joint Q(q,qd,u);
    
    if (linkval == 1)
        barrett_wam_dynamics_art(link_param1,Q);
    else if (linkval == 2)
        barrett_wam_dynamics_art(link_param2,Q);
    else if (linkval == 3)
        barrett_wam_dynamics_art(link_param3,Q);
    else
        mexErrMsgTxt("Input link value must be between [1,3]");

    // Create the output argument plhs[0] to return matrix
    plhs[0] = armaCreateMxMatrix(m, n);

    // Return the matrix as plhs[0] in Matlab  
    armaSetPr(plhs[0], Q.qdd);
    return;
}