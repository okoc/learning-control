% README for MEXING the files in this directory

1. Make sure you run first

mex -setup C++

2. If we want to use GCC with MATLAB 2017a we have to specify an earlier
version with a flag, i.e. the command

mex -v GCC='/usr/bin/gcc-5' mex_funs/barrett_wam_dyn_art.cpp  -outdir mex_funs

compiles the barrett nominal dynamics and outputs in mex_funs/ folder.
MATLAB automatically optimizes code if -g option is not given.

3. Make sure ARMADILLO library is in LD_LIBRARY_PATH 
(this was not a problem so far).