/*
 * For unit testing with ARMADILLO random matrices
 * it is nice to compare with MATLAB code with some existing
 * test functions.
 *
 *
 */

#include <armadillo>
#include "armaMex.hpp"

#define NDOF 7

// definitions
const double ZSFE  =  0.346; //!< z height of SAA axis above ground
const double ZHR  =  0.505;  //!< length of upper arm until 4.5cm before elbow link
const double YEB  =  0.045;  //!< elbow y offset
const double ZEB  =  0.045;  //!< elbow z offset
const double YWR  = -0.045;  //!< elbow y offset (back to forewarm)
const double ZWR  =  0.045;  //!< elbow z offset (back to forearm)
const double ZWFE  =  0.255; //!< forearm length (minus 4.5cm)

const double g = 9.81; //TODO: why is this positive?

using namespace arma;

/**
 * @brief Desired/actual joint positions, velocities, accelerations and torques.
 *
 * Main Player function play() updates desired joint values
 * every 2ms for Barrett WAM.
 */
struct joint {
	vec7 q = zeros<vec>(NDOF); //!< desired/actual joint pos
	vec7 qd = zeros<vec>(NDOF); //!< desired/actual joint vels
	vec7 qdd = zeros<vec>(NDOF); //!< desired/actual joint accs
	vec7 u = zeros<vec>(NDOF); //!< calculated (desired) torques
    
    joint(vec7 & q_, vec7 & qd_, vec7 & qdd_) : q(q_), qd(qd_), qdd(qdd_) {};
};

/**
 * @brief Link parameters
 *
 * Normally 1 for the base and 7 for the joints: 8 link structures are needed.
 * We can load them from a file.
 */
struct link {
	double mass = 0.0; //!< mass of the link
	vec3 mcm = zeros<vec>(3); //!< mass times center of mass of link
	mat33 inertia = zeros<mat>(3,3); //!< inertia matrix corresponding to link
};

/**
 * @brief Base cartesian position and orientation (quaternion)
 */
struct base {
	vec3 x = zeros<vec>(3); //!< Cartesian position
	vec3 xd = zeros<vec>(3); //!< Cartesian velocity
	vec3 xdd = zeros<vec>(3); //!< Cartesian acceleration
	vec4 q = {0.0, 1.0, 0.0, 0.0}; //!< Orientation as quaternion
	vec3 ad = zeros<vec>(3); //!< Angular Velocity [alpha,beta,gamma]
	vec3 add = zeros<vec>(3); //!< Angular Accelerations
};

/**
 * @brief External forces and torques
 */
struct force_ex {
	vec3 base_force = zeros<vec>(3);
	vec3 base_torque = zeros<vec>(3);
	mat joint_force = zeros<mat>(7,3);
	mat joint_torque = zeros<mat>(7,3);
};

/**
 * @brief Endeffector parameters that are necessary (mass, position, ...)
 */
struct end_effector {
	double mass = 0.0; //!< mass
	vec3 mcm = zeros<vec>(3); //!< mass times center of mass
	vec3 x = {0.0, 0.0, 0.30}; //!< Cartesian position
	vec3 a = zeros<vec>(3); //!< Euler angles
};

void barrett_wam_inv_dynamics_ne(const link link_param[NDOF+1], joint & Q);
void load_link_params(const std::string & filename, link link_param[NDOF+1]);

/**
 * @brief Loads the link parameters from config file to global array of links structure.
 */
void load_link_params(const std::string & filename, link link_param[NDOF+1]) {

	using namespace std;
	string env = getenv("HOME");
	string fullname = env + "/learning-control/config/" + filename;
	mat link_mat;
	link_mat.load(fullname);
	//cout << link_mat;
	mat33 inertia;

	for (int i = 0; i < link_mat.n_rows; i++) {
		link_param[i].mass = link_mat(i,0);
		link_param[i].mcm(0) = link_mat(i,1);
		link_param[i].mcm(1) = link_mat(i,2);
		link_param[i].mcm(2) = link_mat(i,3);
		inertia(0,0) = link_mat(i,4); //element 11
		inertia(0,1) = link_mat(i,5); //12 and 21
		inertia(0,2) = link_mat(i,6);
		inertia(1,1) = link_mat(i,7);
		inertia(1,2) = link_mat(i,8);
		inertia(2,2) = link_mat(i,9);
		link_param[i].inertia = symmatu(inertia);
	}
}

/**
 * @brief Barrett WAM inverse dynamics for control
 *
 * Newton-Euler based Inverse Dynamics taken from SL:
 * shared/barrett/math/InvDynNE_declare.h
 * shared/barrett/math/InvDynNE_math.h
 * shared/barrett/math/InvDynNE_functions.h
 *
 * These are called from shared/barrett/src/SL_dynamics.c
 *
 * @param Q joint positions,velocities and acc. given, u updated
 */
void barrett_wam_inv_dynamics_ne(const link link_param[NDOF+1], joint & Q) {

static base base_param;
static force_ex force_ex_param;
static end_effector endeff_param;

vec7 q  =  Q.q;
vec7 qd  =  Q.qd;
vec7 qdd = Q.qdd;
vec7 u = zeros<vec>(7);

// warning! uex_des is what desired state structure in SL carries
// zeroed here
static vec7 uex_des;

static double  ss1th;
static double  cs1th;
static double  ss2th;
static double  cs2th;
static double  ss3th;
static double  cs3th;
static double  ss4th;
static double  cs4th;
static double  ss5th;
static double  cs5th;
static double  ss6th;
static double  cs6th;
static double  ss7th;
static double  cs7th;

static double  rseff1a1;
static double  rceff1a1;
static double  rseff1a2;
static double  rceff1a2;
static double  rseff1a3;
static double  rceff1a3;

static mat33 S00;
static mat33 S10;
static mat33 S21;
static mat33 S32;
static mat33 S43;
static mat33 S54;
static mat33 S65;
static mat33 S76;
static mat33 S87;

static mat33 Si00;
static mat33 Si01;
static mat33 Si12;
static mat33 Si23;
static mat33 Si34;
static mat33 Si45;
static mat33 Si56;
static mat33 Si67;
static mat33 Si78;

static mat33 SG10;
static mat33 SG20;
static mat33 SG30;
static mat33 SG40;
static mat33 SG50;
static mat33 SG60;
static mat33 SG70;
static mat33 SG80;

static vec6 v0;
static vec6 v1;
static vec6 v2;
static vec6 v3;
static vec6 v4;
static vec6 v5;
static vec6 v6;
static vec6 v7;
static vec6 v8;

static vec6 c1;
static vec6 c2;
static vec6 c3;
static vec6 c4;
static vec6 c5;
static vec6 c6;
static vec6 c7;

static vec6 pv0;
static vec6 pv1;
static vec6 pv2;
static vec6 pv3;
static vec6 pv4;
static vec6 pv5;
static vec6 pv6;
static vec6 pv7;
static vec6 pv8;

static mat66 JA0;
static mat66 JA1;
static mat66 JA2;
static mat66 JA3;
static mat66 JA4;
static mat66 JA5;
static mat66 JA6;
static mat66 JA7;
static mat66 JA8;

static vec6 h1;
static vec6 h2;
static vec6 h3;
static vec6 h4;
static vec6 h5;
static vec6 h6;
static vec6 h7;

static vec8 d;

static mat66 T101;
static mat66 T112;
static mat66 T123;
static mat66 T134;
static mat66 T145;
static mat66 T156;
static mat66 T167;
static mat66 T178;

static mat66 T01;
static mat66 T12;
static mat66 T23;
static mat66 T34;
static mat66 T45;
static mat66 T56;
static mat66 T67;
static mat66 T78;

static vec6 p0;
static vec6 p1;
static vec6 p2;
static vec6 p3;
static vec6 p4;
static vec6 p5;
static vec6 p6;
static vec6 p7;
static vec6 p8;

static vec6 pmm1;
static vec6 pmm2;
static vec6 pmm3;
static vec6 pmm4;
static vec6 pmm5;
static vec6 pmm6;
static vec6 pmm7;
static vec6 pmm8;

static vec6 pm1;
static vec6 pm2;
static vec6 pm3;
static vec6 pm4;
static vec6 pm5;
static vec6 pm6;
static vec6 pm7;
static vec6 pm8;

static vec6 a0;
static vec6 a1;
static vec6 a2;
static vec6 a3;
static vec6 a4;
static vec6 a5;
static vec6 a6;
static vec6 a7;
static vec6 a8;

static vec6 fnet0;
static vec6 fnet1;
static vec6 fnet2;
static vec6 fnet3;
static vec6 fnet4;
static vec6 fnet5;
static vec6 fnet6;
static vec6 fnet7;
static vec6 fnet8;

static vec6 fex0;
static vec6 fex1;
static vec6 fex2;
static vec6 fex3;
static vec6 fex4;
static vec6 fex5;
static vec6 fex6;
static vec6 fex7;

static vec6 fext0;
static vec6 fext1;
static vec6 fext2;
static vec6 fext3;
static vec6 fext4;
static vec6 fext5;
static vec6 fext6;
static vec6 fext7;

static vec6 f0;
static vec6 f1;
static vec6 f2;
static vec6 f3;
static vec6 f4;
static vec6 f5;
static vec6 f6;
static vec6 f7;
static vec6 f8;

// sine and cosine precomputation
ss1th   =   sin(q(0));
cs1th   =   cos(q(0));
ss2th   =   sin(q(1));
cs2th   =   cos(q(1));
ss3th   =   sin(q(2));
cs3th   =   cos(q(2));
ss4th   =   sin(q(3));
cs4th   =   cos(q(3));
ss5th   =   sin(q(4));
cs5th   =   cos(q(4));
ss6th   =   sin(q(5));
cs6th   =   cos(q(5));
ss7th   =   sin(q(6));
cs7th   =   cos(q(6));

// endeffector orientations

rseff1a1   =   sin(endeff_param.a(0));
rceff1a1   =   cos(endeff_param.a(0));

rseff1a2   =   sin(endeff_param.a(1));
rceff1a2   =   cos(endeff_param.a(1));

rseff1a3   =   sin(endeff_param.a(2));
rceff1a3   =   cos(endeff_param.a(2));

//// Includes the functions one by one

//barrett_InvDynNEfunc1(void)

// rotation matrices
S00(0,0) = -1 + 2*pow(base_param.q(0),2) + 2*pow(base_param.q(1),2);
S00(0,1) = 2*(base_param.q(1)*base_param.q(2) + base_param.q(0)*base_param.q(3));
S00(0,2) = 2*(-(base_param.q(0)*base_param.q(2)) + base_param.q(1)*base_param.q(3));

S00(1,0) = 2*(base_param.q(1)*base_param.q(2) - base_param.q(0)*base_param.q(3));
S00(1,1) = -1 + 2*pow(base_param.q(0),2) + 2*pow(base_param.q(2),2);
S00(1,2) = 2*(base_param.q(0)*base_param.q(1) + base_param.q(2)*base_param.q(3));

S00(2,0) = 2*(base_param.q(0)*base_param.q(2) + base_param.q(1)*base_param.q(3));
S00(2,1) = 2*(-(base_param.q(0)*base_param.q(1)) + base_param.q(2)*base_param.q(3));
S00(2,2) = -1 + 2*pow(base_param.q(0),2) + 2*pow(base_param.q(3),2);


S10(0,0) = cs1th;
S10(0,1) = ss1th;

S10(1,0) = -ss1th;
S10(1,1) = cs1th;


S21(0,1) = ss2th;
S21(0,2) = cs2th;

S21(1,1) = cs2th;
S21(1,2) = -ss2th;


S32(0,1) = ss3th;
S32(0,2) = -cs3th;

S32(1,1) = cs3th;
S32(1,2) = ss3th;


S43(0,1) = ss4th;
S43(0,2) = cs4th;

S43(1,1) = cs4th;
S43(1,2) = -ss4th;


S54(0,1) = ss5th;
S54(0,2) = -cs5th;

S54(1,1) = cs5th;
S54(1,2) = ss5th;


S65(0,1) = ss6th;
S65(0,2) = cs6th;

S65(1,1) = cs6th;
S65(1,2) = -ss6th;


S76(0,1) = ss7th;
S76(0,2) = -cs7th;

S76(1,1) = cs7th;
S76(1,2) = ss7th;


S87(0,0) = rceff1a2*rceff1a3;
S87(0,1) = rceff1a3*rseff1a1*rseff1a2 + rceff1a1*rseff1a3;
S87(0,2) = -(rceff1a1*rceff1a3*rseff1a2) + rseff1a1*rseff1a3;

S87(1,0) = -(rceff1a2*rseff1a3);
S87(1,1) = rceff1a1*rceff1a3 - rseff1a1*rseff1a2*rseff1a3;
S87(1,2) = rceff1a3*rseff1a1 + rceff1a1*rseff1a2*rseff1a3;

S87(2,0) = rseff1a2;
S87(2,1) = -(rceff1a2*rseff1a1);
S87(2,2) = rceff1a1*rceff1a2;

//barrett_InvDynNEfunc2(void)

// inverse rotation matrices
Si00(0,0) = -1 + 2*pow(base_param.q(0),2) + 2*pow(base_param.q(1),2);
Si00(0,1) = 2*(base_param.q(1)*base_param.q(2) - base_param.q(0)*base_param.q(3));
Si00(0,2) = 2*(base_param.q(0)*base_param.q(2) + base_param.q(1)*base_param.q(3));

Si00(1,0) = 2*(base_param.q(1)*base_param.q(2) + base_param.q(0)*base_param.q(3));
Si00(1,1) = -1 + 2*pow(base_param.q(0),2) + 2*pow(base_param.q(2),2);
Si00(1,2) = 2*(-(base_param.q(0)*base_param.q(1)) + base_param.q(2)*base_param.q(3));

Si00(2,0) = 2*(-(base_param.q(0)*base_param.q(2)) + base_param.q(1)*base_param.q(3));
Si00(2,1) = 2*(base_param.q(0)*base_param.q(1) + base_param.q(2)*base_param.q(3));
Si00(2,2) = -1 + 2*pow(base_param.q(0),2) + 2*pow(base_param.q(3),2);


Si01(0,0) = cs1th;
Si01(0,1) = -ss1th;

Si01(1,0) = ss1th;
Si01(1,1) = cs1th;


Si12(1,0) = ss2th;
Si12(1,1) = cs2th;

Si12(2,0) = cs2th;
Si12(2,1) = -ss2th;


Si23(1,0) = ss3th;
Si23(1,1) = cs3th;

Si23(2,0) = -cs3th;
Si23(2,1) = ss3th;


Si34(1,0) = ss4th;
Si34(1,1) = cs4th;

Si34(2,0) = cs4th;
Si34(2,1) = -ss4th;


Si45(1,0) = ss5th;
Si45(1,1) = cs5th;

Si45(2,0) = -cs5th;
Si45(2,1) = ss5th;


Si56(1,0) = ss6th;
Si56(1,1) = cs6th;

Si56(2,0) = cs6th;
Si56(2,1) = -ss6th;


Si67(1,0) = ss7th;
Si67(1,1) = cs7th;

Si67(2,0) = -cs7th;
Si67(2,1) = ss7th;


Si78(0,0) = rceff1a2*rceff1a3;
Si78(0,1) = -(rceff1a2*rseff1a3);
Si78(0,2) = rseff1a2;

Si78(1,0) = rceff1a3*rseff1a1*rseff1a2 + rceff1a1*rseff1a3;
Si78(1,1) = rceff1a1*rceff1a3 - rseff1a1*rseff1a2*rseff1a3;
Si78(1,2) = -(rceff1a2*rseff1a1);

Si78(2,0) = -(rceff1a1*rceff1a3*rseff1a2) + rseff1a1*rseff1a3;
Si78(2,1) = rceff1a3*rseff1a1 + rceff1a1*rseff1a2*rseff1a3;
Si78(2,2) = rceff1a1*rceff1a2;


//barrett_InvDynNEfunc3(void)

// rotation matrices from global to link coordinates
SG10(0,0) = S00(0,0)*S10(0,0) + S00(1,0)*S10(0,1);
SG10(0,1) = S00(0,1)*S10(0,0) + S00(1,1)*S10(0,1);
SG10(0,2) = S00(0,2)*S10(0,0) + S00(1,2)*S10(0,1);

SG10(1,0) = S00(0,0)*S10(1,0) + S00(1,0)*S10(1,1);
SG10(1,1) = S00(0,1)*S10(1,0) + S00(1,1)*S10(1,1);
SG10(1,2) = S00(0,2)*S10(1,0) + S00(1,2)*S10(1,1);

SG10(2,0) = S00(2,0);
SG10(2,1) = S00(2,1);
SG10(2,2) = S00(2,2);


SG20(0,0) = S21(0,1)*SG10(1,0) + S21(0,2)*SG10(2,0);
SG20(0,1) = S21(0,1)*SG10(1,1) + S21(0,2)*SG10(2,1);
SG20(0,2) = S21(0,1)*SG10(1,2) + S21(0,2)*SG10(2,2);

SG20(1,0) = S21(1,1)*SG10(1,0) + S21(1,2)*SG10(2,0);
SG20(1,1) = S21(1,1)*SG10(1,1) + S21(1,2)*SG10(2,1);
SG20(1,2) = S21(1,1)*SG10(1,2) + S21(1,2)*SG10(2,2);

SG20(2,0) = -SG10(0,0);
SG20(2,1) = -SG10(0,1);
SG20(2,2) = -SG10(0,2);


SG30(0,0) = S32(0,1)*SG20(1,0) + S32(0,2)*SG20(2,0);
SG30(0,1) = S32(0,1)*SG20(1,1) + S32(0,2)*SG20(2,1);
SG30(0,2) = S32(0,1)*SG20(1,2) + S32(0,2)*SG20(2,2);

SG30(1,0) = S32(1,1)*SG20(1,0) + S32(1,2)*SG20(2,0);
SG30(1,1) = S32(1,1)*SG20(1,1) + S32(1,2)*SG20(2,1);
SG30(1,2) = S32(1,1)*SG20(1,2) + S32(1,2)*SG20(2,2);

SG30(2,0) = SG20(0,0);
SG30(2,1) = SG20(0,1);
SG30(2,2) = SG20(0,2);


SG40(0,0) = S43(0,1)*SG30(1,0) + S43(0,2)*SG30(2,0);
SG40(0,1) = S43(0,1)*SG30(1,1) + S43(0,2)*SG30(2,1);
SG40(0,2) = S43(0,1)*SG30(1,2) + S43(0,2)*SG30(2,2);

SG40(1,0) = S43(1,1)*SG30(1,0) + S43(1,2)*SG30(2,0);
SG40(1,1) = S43(1,1)*SG30(1,1) + S43(1,2)*SG30(2,1);
SG40(1,2) = S43(1,1)*SG30(1,2) + S43(1,2)*SG30(2,2);

SG40(2,0) = -SG30(0,0);
SG40(2,1) = -SG30(0,1);
SG40(2,2) = -SG30(0,2);


SG50(0,0) = S54(0,1)*SG40(1,0) + S54(0,2)*SG40(2,0);
SG50(0,1) = S54(0,1)*SG40(1,1) + S54(0,2)*SG40(2,1);
SG50(0,2) = S54(0,1)*SG40(1,2) + S54(0,2)*SG40(2,2);

SG50(1,0) = S54(1,1)*SG40(1,0) + S54(1,2)*SG40(2,0);
SG50(1,1) = S54(1,1)*SG40(1,1) + S54(1,2)*SG40(2,1);
SG50(1,2) = S54(1,1)*SG40(1,2) + S54(1,2)*SG40(2,2);

SG50(2,0) = SG40(0,0);
SG50(2,1) = SG40(0,1);
SG50(2,2) = SG40(0,2);


SG60(0,0) = S65(0,1)*SG50(1,0) + S65(0,2)*SG50(2,0);
SG60(0,1) = S65(0,1)*SG50(1,1) + S65(0,2)*SG50(2,1);
SG60(0,2) = S65(0,1)*SG50(1,2) + S65(0,2)*SG50(2,2);

SG60(1,0) = S65(1,1)*SG50(1,0) + S65(1,2)*SG50(2,0);
SG60(1,1) = S65(1,1)*SG50(1,1) + S65(1,2)*SG50(2,1);
SG60(1,2) = S65(1,1)*SG50(1,2) + S65(1,2)*SG50(2,2);

SG60(2,0) = -SG50(0,0);
SG60(2,1) = -SG50(0,1);
SG60(2,2) = -SG50(0,2);


SG70(0,0) = S76(0,1)*SG60(1,0) + S76(0,2)*SG60(2,0);
SG70(0,1) = S76(0,1)*SG60(1,1) + S76(0,2)*SG60(2,1);
SG70(0,2) = S76(0,1)*SG60(1,2) + S76(0,2)*SG60(2,2);

SG70(1,0) = S76(1,1)*SG60(1,0) + S76(1,2)*SG60(2,0);
SG70(1,1) = S76(1,1)*SG60(1,1) + S76(1,2)*SG60(2,1);
SG70(1,2) = S76(1,1)*SG60(1,2) + S76(1,2)*SG60(2,2);

SG70(2,0) = SG60(0,0);
SG70(2,1) = SG60(0,1);
SG70(2,2) = SG60(0,2);


SG80(0,0) = S87(0,0)*SG70(0,0) + S87(0,1)*SG70(1,0) + S87(0,2)*SG70(2,0);
SG80(0,1) = S87(0,0)*SG70(0,1) + S87(0,1)*SG70(1,1) + S87(0,2)*SG70(2,1);
SG80(0,2) = S87(0,0)*SG70(0,2) + S87(0,1)*SG70(1,2) + S87(0,2)*SG70(2,2);

SG80(1,0) = S87(1,0)*SG70(0,0) + S87(1,1)*SG70(1,0) + S87(1,2)*SG70(2,0);
SG80(1,1) = S87(1,0)*SG70(0,1) + S87(1,1)*SG70(1,1) + S87(1,2)*SG70(2,1);
SG80(1,2) = S87(1,0)*SG70(0,2) + S87(1,1)*SG70(1,2) + S87(1,2)*SG70(2,2);

SG80(2,0) = S87(2,0)*SG70(0,0) + S87(2,1)*SG70(1,0) + S87(2,2)*SG70(2,0);
SG80(2,1) = S87(2,0)*SG70(0,1) + S87(2,1)*SG70(1,1) + S87(2,2)*SG70(2,1);
SG80(2,2) = S87(2,0)*SG70(0,2) + S87(2,1)*SG70(1,2) + S87(2,2)*SG70(2,2);

//barrett_InvDynNEfunc4(void)

// velocity vectors
v0(0) = base_param.ad(0)*S00(0,0) + base_param.ad(1)*S00(0,1) + base_param.ad(2)*S00(0,2);
v0(1) = base_param.ad(0)*S00(1,0) + base_param.ad(1)*S00(1,1) + base_param.ad(2)*S00(1,2);
v0(2) = base_param.ad(0)*S00(2,0) + base_param.ad(1)*S00(2,1) + base_param.ad(2)*S00(2,2);
v0(3) = base_param.xd(0)*S00(0,0) + base_param.xd(1)*S00(0,1) + base_param.xd(2)*S00(0,2);
v0(4) = base_param.xd(0)*S00(1,0) + base_param.xd(1)*S00(1,1) + base_param.xd(2)*S00(1,2);
v0(5) = base_param.xd(0)*S00(2,0) + base_param.xd(1)*S00(2,1) + base_param.xd(2)*S00(2,2);

v1(0) = v0(0)*S10(0,0) + v0(1)*S10(0,1);
v1(1) = v0(0)*S10(1,0) + v0(1)*S10(1,1);
v1(2) = qd(0) + v0(2);
v1(3) = ZSFE*v0(1)*S10(0,0) + v0(3)*S10(0,0) - ZSFE*v0(0)*S10(0,1) + v0(4)*S10(0,1);
v1(4) = ZSFE*v0(1)*S10(1,0) + v0(3)*S10(1,0) - ZSFE*v0(0)*S10(1,1) + v0(4)*S10(1,1);
v1(5) = v0(5);

v2(0) = v1(1)*S21(0,1) + v1(2)*S21(0,2);
v2(1) = v1(1)*S21(1,1) + v1(2)*S21(1,2);
v2(2) = qd(1) - v1(0);
v2(3) = v1(4)*S21(0,1) + v1(5)*S21(0,2);
v2(4) = v1(4)*S21(1,1) + v1(5)*S21(1,2);
v2(5) = -v1(3);

v3(0) = v2(1)*S32(0,1) + v2(2)*S32(0,2);
v3(1) = v2(1)*S32(1,1) + v2(2)*S32(1,2);
v3(2) = qd(2) + v2(0);
v3(3) = ZHR*v2(2)*S32(0,1) + v2(4)*S32(0,1) - ZHR*v2(1)*S32(0,2) + v2(5)*S32(0,2);
v3(4) = ZHR*v2(2)*S32(1,1) + v2(4)*S32(1,1) - ZHR*v2(1)*S32(1,2) + v2(5)*S32(1,2);
v3(5) = v2(3);

v4(0) = v3(1)*S43(0,1) + v3(2)*S43(0,2);
v4(1) = v3(1)*S43(1,1) + v3(2)*S43(1,2);
v4(2) = qd(3) - v3(0);
v4(3) = v3(4)*S43(0,1) + v3(5)*S43(0,2) + v3(0)*(-(ZEB*S43(0,1)) + YEB*S43(0,2));
v4(4) = v3(4)*S43(1,1) + v3(5)*S43(1,2) + v3(0)*(-(ZEB*S43(1,1)) + YEB*S43(1,2));
v4(5) = -(ZEB*v3(1)) + YEB*v3(2) - v3(3);

v5(0) = v4(1)*S54(0,1) + v4(2)*S54(0,2);
v5(1) = v4(1)*S54(1,1) + v4(2)*S54(1,2);
v5(2) = qd(4) + v4(0);
v5(3) = ZWR*v4(2)*S54(0,1) + v4(4)*S54(0,1) + YWR*v4(0)*S54(0,2) - ZWR*v4(1)*S54(0,2) + v4(5)*S54(0,2);
v5(4) = ZWR*v4(2)*S54(1,1) + v4(4)*S54(1,1) + YWR*v4(0)*S54(1,2) - ZWR*v4(1)*S54(1,2) + v4(5)*S54(1,2);
v5(5) = -(YWR*v4(2)) + v4(3);

v6(0) = v5(1)*S65(0,1) + v5(2)*S65(0,2);
v6(1) = v5(1)*S65(1,1) + v5(2)*S65(1,2);
v6(2) = qd(5) - v5(0);
v6(3) = -(ZWFE*v5(0)*S65(0,1)) + v5(4)*S65(0,1) + v5(5)*S65(0,2);
v6(4) = -(ZWFE*v5(0)*S65(1,1)) + v5(4)*S65(1,1) + v5(5)*S65(1,2);
v6(5) = -(ZWFE*v5(1)) - v5(3);

v7(0) = v6(1)*S76(0,1) + v6(2)*S76(0,2);
v7(1) = v6(1)*S76(1,1) + v6(2)*S76(1,2);
v7(2) = qd(6) + v6(0);
v7(3) = v6(4)*S76(0,1) + v6(5)*S76(0,2);
v7(4) = v6(4)*S76(1,1) + v6(5)*S76(1,2);
v7(5) = v6(3);

v8(0) = v7(0)*S87(0,0) + v7(1)*S87(0,1) + v7(2)*S87(0,2);
v8(1) = v7(0)*S87(1,0) + v7(1)*S87(1,1) + v7(2)*S87(1,2);
v8(2) = v7(0)*S87(2,0) + v7(1)*S87(2,1) + v7(2)*S87(2,2);
v8(3) = v7(3)*S87(0,0) + v7(4)*S87(0,1) + v7(2)*(-(endeff_param.x(1)*S87(0,0)) + endeff_param.x(0)*S87(0,1)) + v7(5)*S87(0,2) + v7(1)*(endeff_param.x(2)*S87(0,0) - endeff_param.x(0)*S87(0,2)) + v7(0)*(-(endeff_param.x(2)*S87(0,1)) + endeff_param.x(1)*S87(0,2));
v8(4) = v7(3)*S87(1,0) + v7(4)*S87(1,1) + v7(2)*(-(endeff_param.x(1)*S87(1,0)) + endeff_param.x(0)*S87(1,1)) + v7(5)*S87(1,2) + v7(1)*(endeff_param.x(2)*S87(1,0) - endeff_param.x(0)*S87(1,2)) + v7(0)*(-(endeff_param.x(2)*S87(1,1)) + endeff_param.x(1)*S87(1,2));
v8(5) = v7(3)*S87(2,0) + v7(4)*S87(2,1) + v7(2)*(-(endeff_param.x(1)*S87(2,0)) + endeff_param.x(0)*S87(2,1)) + v7(5)*S87(2,2) + v7(1)*(endeff_param.x(2)*S87(2,0) - endeff_param.x(0)*S87(2,2)) + v7(0)*(-(endeff_param.x(2)*S87(2,1)) + endeff_param.x(1)*S87(2,2));

//barrett_InvDynNEfunc5(void)

// acceleration vectors
a0(0) = base_param.add(0)*S00(0,0) + base_param.add(1)*S00(0,1) + base_param.add(2)*S00(0,2);
a0(1) = base_param.add(0)*S00(1,0) + base_param.add(1)*S00(1,1) + base_param.add(2)*S00(1,2);
a0(2) = base_param.add(0)*S00(2,0) + base_param.add(1)*S00(2,1) + base_param.add(2)*S00(2,2);
a0(3) = base_param.xdd(0)*S00(0,0) + base_param.xdd(1)*S00(0,1) + (g + base_param.xdd(2))*S00(0,2);
a0(4) = base_param.xdd(0)*S00(1,0) + base_param.xdd(1)*S00(1,1) + (g + base_param.xdd(2))*S00(1,2);
a0(5) = base_param.xdd(0)*S00(2,0) + base_param.xdd(1)*S00(2,1) + (g + base_param.xdd(2))*S00(2,2);

a1(0) = qd(0)*v1(1) + a0(0)*S10(0,0) + a0(1)*S10(0,1);
a1(1) = -(qd(0)*v1(0)) + a0(0)*S10(1,0) + a0(1)*S10(1,1);
a1(2) = qdd(0) + a0(2);
a1(3) = qd(0)*v1(4) + ZSFE*a0(1)*S10(0,0) + a0(3)*S10(0,0) - ZSFE*a0(0)*S10(0,1) + a0(4)*S10(0,1);
a1(4) = -(qd(0)*v1(3)) + ZSFE*a0(1)*S10(1,0) + a0(3)*S10(1,0) - ZSFE*a0(0)*S10(1,1) + a0(4)*S10(1,1);
a1(5) = a0(5);

a2(0) = qd(1)*v2(1) + a1(1)*S21(0,1) + a1(2)*S21(0,2);
a2(1) = -(qd(1)*v2(0)) + a1(1)*S21(1,1) + a1(2)*S21(1,2);
a2(2) = qdd(1) - a1(0);
a2(3) = qd(1)*v2(4) + a1(4)*S21(0,1) + a1(5)*S21(0,2);
a2(4) = -(qd(1)*v2(3)) + a1(4)*S21(1,1) + a1(5)*S21(1,2);
a2(5) = -a1(3);

a3(0) = qd(2)*v3(1) + a2(1)*S32(0,1) + a2(2)*S32(0,2);
a3(1) = -(qd(2)*v3(0)) + a2(1)*S32(1,1) + a2(2)*S32(1,2);
a3(2) = qdd(2) + a2(0);
a3(3) = qd(2)*v3(4) + ZHR*a2(2)*S32(0,1) + a2(4)*S32(0,1) - ZHR*a2(1)*S32(0,2) + a2(5)*S32(0,2);
a3(4) = -(qd(2)*v3(3)) + ZHR*a2(2)*S32(1,1) + a2(4)*S32(1,1) - ZHR*a2(1)*S32(1,2) + a2(5)*S32(1,2);
a3(5) = a2(3);

a4(0) = qd(3)*v4(1) + a3(1)*S43(0,1) + a3(2)*S43(0,2);
a4(1) = -(qd(3)*v4(0)) + a3(1)*S43(1,1) + a3(2)*S43(1,2);
a4(2) = qdd(3) - a3(0);
a4(3) = qd(3)*v4(4) + a3(4)*S43(0,1) + a3(5)*S43(0,2) + a3(0)*(-(ZEB*S43(0,1)) + YEB*S43(0,2));
a4(4) = -(qd(3)*v4(3)) + a3(4)*S43(1,1) + a3(5)*S43(1,2) + a3(0)*(-(ZEB*S43(1,1)) + YEB*S43(1,2));
a4(5) = -(ZEB*a3(1)) + YEB*a3(2) - a3(3);

a5(0) = qd(4)*v5(1) + a4(1)*S54(0,1) + a4(2)*S54(0,2);
a5(1) = -(qd(4)*v5(0)) + a4(1)*S54(1,1) + a4(2)*S54(1,2);
a5(2) = qdd(4) + a4(0);
a5(3) = qd(4)*v5(4) + ZWR*a4(2)*S54(0,1) + a4(4)*S54(0,1) + YWR*a4(0)*S54(0,2) - ZWR*a4(1)*S54(0,2) + a4(5)*S54(0,2);
a5(4) = -(qd(4)*v5(3)) + ZWR*a4(2)*S54(1,1) + a4(4)*S54(1,1) + YWR*a4(0)*S54(1,2) - ZWR*a4(1)*S54(1,2) + a4(5)*S54(1,2);
a5(5) = -(YWR*a4(2)) + a4(3);

a6(0) = qd(5)*v6(1) + a5(1)*S65(0,1) + a5(2)*S65(0,2);
a6(1) = -(qd(5)*v6(0)) + a5(1)*S65(1,1) + a5(2)*S65(1,2);
a6(2) = qdd(5) - a5(0);
a6(3) = qd(5)*v6(4) - ZWFE*a5(0)*S65(0,1) + a5(4)*S65(0,1) + a5(5)*S65(0,2);
a6(4) = -(qd(5)*v6(3)) - ZWFE*a5(0)*S65(1,1) + a5(4)*S65(1,1) + a5(5)*S65(1,2);
a6(5) = -(ZWFE*a5(1)) - a5(3);

a7(0) = qd(6)*v7(1) + a6(1)*S76(0,1) + a6(2)*S76(0,2);
a7(1) = -(qd(6)*v7(0)) + a6(1)*S76(1,1) + a6(2)*S76(1,2);
a7(2) = qdd(6) + a6(0);
a7(3) = qd(6)*v7(4) + a6(4)*S76(0,1) + a6(5)*S76(0,2);
a7(4) = -(qd(6)*v7(3)) + a6(4)*S76(1,1) + a6(5)*S76(1,2);
a7(5) = a6(3);

a8(0) = a7(0)*S87(0,0) + a7(1)*S87(0,1) + a7(2)*S87(0,2);
a8(1) = a7(0)*S87(1,0) + a7(1)*S87(1,1) + a7(2)*S87(1,2);
a8(2) = a7(0)*S87(2,0) + a7(1)*S87(2,1) + a7(2)*S87(2,2);
a8(3) = a7(3)*S87(0,0) + a7(4)*S87(0,1) + a7(2)*(-(endeff_param.x(1)*S87(0,0)) + endeff_param.x(0)*S87(0,1)) + a7(5)*S87(0,2) + a7(1)*(endeff_param.x(2)*S87(0,0) - endeff_param.x(0)*S87(0,2)) + a7(0)*(-(endeff_param.x(2)*S87(0,1)) + endeff_param.x(1)*S87(0,2));
a8(4) = a7(3)*S87(1,0) + a7(4)*S87(1,1) + a7(2)*(-(endeff_param.x(1)*S87(1,0)) + endeff_param.x(0)*S87(1,1)) + a7(5)*S87(1,2) + a7(1)*(endeff_param.x(2)*S87(1,0) - endeff_param.x(0)*S87(1,2)) + a7(0)*(-(endeff_param.x(2)*S87(1,1)) + endeff_param.x(1)*S87(1,2));
a8(5) = a7(3)*S87(2,0) + a7(4)*S87(2,1) + a7(2)*(-(endeff_param.x(1)*S87(2,0)) + endeff_param.x(0)*S87(2,1)) + a7(5)*S87(2,2) + a7(1)*(endeff_param.x(2)*S87(2,0) - endeff_param.x(0)*S87(2,2)) + a7(0)*(-(endeff_param.x(2)*S87(2,1)) + endeff_param.x(1)*S87(2,2));

//barrett_InvDynNEfunc6(void)

// net forces and external forces for each joint
fnet0(0) = link_param[0].mass*a0(3) - a0(2)*link_param[0].mcm(1) + a0(1)*link_param[0].mcm(2) - link_param[0].mcm(0)*pow(v0(1),2) - link_param[0].mcm(0)*pow(v0(2),2) + v0(0)*(link_param[0].mcm(1)*v0(1) + link_param[0].mcm(2)*v0(2)) - link_param[0].mass*v0(2)*v0(4) + link_param[0].mass*v0(1)*v0(5);
fnet0(1) = link_param[0].mass*a0(4) + a0(2)*link_param[0].mcm(0) - a0(0)*link_param[0].mcm(2) - link_param[0].mcm(1)*pow(v0(0),2) - link_param[0].mcm(1)*pow(v0(2),2) + v0(1)*(link_param[0].mcm(0)*v0(0) + link_param[0].mcm(2)*v0(2)) + link_param[0].mass*v0(2)*v0(3) - link_param[0].mass*v0(0)*v0(5);
fnet0(2) = link_param[0].mass*a0(5) - a0(1)*link_param[0].mcm(0) + a0(0)*link_param[0].mcm(1) - link_param[0].mcm(2)*pow(v0(0),2) - link_param[0].mcm(2)*pow(v0(1),2) + (link_param[0].mcm(0)*v0(0) + link_param[0].mcm(1)*v0(1))*v0(2) - link_param[0].mass*v0(1)*v0(3) + link_param[0].mass*v0(0)*v0(4);
fnet0(3) = a0(5)*link_param[0].mcm(1) - a0(4)*link_param[0].mcm(2) + (-(link_param[0].mcm(1)*v0(1)) - link_param[0].mcm(2)*v0(2))*v0(3) + (link_param[0].mcm(0)*v0(2) + link_param[0].mass*v0(4))*v0(5) + v0(4)*(link_param[0].mcm(0)*v0(1) - link_param[0].mass*v0(5)) + a0(0)*link_param[0].inertia(0,0) + a0(1)*link_param[0].inertia(0,1) + a0(2)*link_param[0].inertia(0,2) + v0(0)*(link_param[0].mcm(1)*v0(4) + link_param[0].mcm(2)*v0(5) - v0(2)*link_param[0].inertia(0,1) + v0(1)*link_param[0].inertia(0,2)) + v0(1)*(-(link_param[0].mcm(0)*v0(4)) - v0(2)*link_param[0].inertia(1,1) + v0(1)*link_param[0].inertia(1,2)) + v0(2)*(-(link_param[0].mcm(0)*v0(5)) - v0(2)*link_param[0].inertia(1,2) + v0(1)*link_param[0].inertia(2,2));
fnet0(4) = -(a0(5)*link_param[0].mcm(0)) + a0(3)*link_param[0].mcm(2) + (-(link_param[0].mcm(0)*v0(0)) - link_param[0].mcm(2)*v0(2))*v0(4) + (link_param[0].mcm(1)*v0(2) - link_param[0].mass*v0(3))*v0(5) + v0(3)*(link_param[0].mcm(1)*v0(0) + link_param[0].mass*v0(5)) + a0(0)*link_param[0].inertia(0,1) + v0(0)*(-(link_param[0].mcm(1)*v0(3)) + v0(2)*link_param[0].inertia(0,0) - v0(0)*link_param[0].inertia(0,2)) + a0(1)*link_param[0].inertia(1,1) + a0(2)*link_param[0].inertia(1,2) + v0(1)*(link_param[0].mcm(0)*v0(3) + link_param[0].mcm(2)*v0(5) + v0(2)*link_param[0].inertia(0,1) - v0(0)*link_param[0].inertia(1,2)) + v0(2)*(-(link_param[0].mcm(1)*v0(5)) + v0(2)*link_param[0].inertia(0,2) - v0(0)*link_param[0].inertia(2,2));
fnet0(5) = a0(4)*link_param[0].mcm(0) - a0(3)*link_param[0].mcm(1) + (link_param[0].mcm(2)*v0(1) + link_param[0].mass*v0(3))*v0(4) + v0(3)*(link_param[0].mcm(2)*v0(0) - link_param[0].mass*v0(4)) + (-(link_param[0].mcm(0)*v0(0)) - link_param[0].mcm(1)*v0(1))*v0(5) + v0(0)*(-(link_param[0].mcm(2)*v0(3)) - v0(1)*link_param[0].inertia(0,0) + v0(0)*link_param[0].inertia(0,1)) + a0(0)*link_param[0].inertia(0,2) + v0(1)*(-(link_param[0].mcm(2)*v0(4)) - v0(1)*link_param[0].inertia(0,1) + v0(0)*link_param[0].inertia(1,1)) + a0(1)*link_param[0].inertia(1,2) + v0(2)*(link_param[0].mcm(0)*v0(3) + link_param[0].mcm(1)*v0(4) - v0(1)*link_param[0].inertia(0,2) + v0(0)*link_param[0].inertia(1,2)) + a0(2)*link_param[0].inertia(2,2);

fnet1(0) = link_param[1].mass*a1(3) - a1(2)*link_param[1].mcm(1) + a1(1)*link_param[1].mcm(2) - link_param[1].mcm(0)*pow(v1(1),2) - link_param[1].mcm(0)*pow(v1(2),2) + v1(0)*(link_param[1].mcm(1)*v1(1) + link_param[1].mcm(2)*v1(2)) - link_param[1].mass*v1(2)*v1(4) + link_param[1].mass*v1(1)*v1(5);
fnet1(1) = link_param[1].mass*a1(4) + a1(2)*link_param[1].mcm(0) - a1(0)*link_param[1].mcm(2) - link_param[1].mcm(1)*pow(v1(0),2) - link_param[1].mcm(1)*pow(v1(2),2) + v1(1)*(link_param[1].mcm(0)*v1(0) + link_param[1].mcm(2)*v1(2)) + link_param[1].mass*v1(2)*v1(3) - link_param[1].mass*v1(0)*v1(5);
fnet1(2) = link_param[1].mass*a1(5) - a1(1)*link_param[1].mcm(0) + a1(0)*link_param[1].mcm(1) - link_param[1].mcm(2)*pow(v1(0),2) - link_param[1].mcm(2)*pow(v1(1),2) + (link_param[1].mcm(0)*v1(0) + link_param[1].mcm(1)*v1(1))*v1(2) - link_param[1].mass*v1(1)*v1(3) + link_param[1].mass*v1(0)*v1(4);
fnet1(3) = a1(5)*link_param[1].mcm(1) - a1(4)*link_param[1].mcm(2) + (-(link_param[1].mcm(1)*v1(1)) - link_param[1].mcm(2)*v1(2))*v1(3) + (link_param[1].mcm(0)*v1(2) + link_param[1].mass*v1(4))*v1(5) + v1(4)*(link_param[1].mcm(0)*v1(1) - link_param[1].mass*v1(5)) + a1(0)*link_param[1].inertia(0,0) + a1(1)*link_param[1].inertia(0,1) + a1(2)*link_param[1].inertia(0,2) + v1(0)*(link_param[1].mcm(1)*v1(4) + link_param[1].mcm(2)*v1(5) - v1(2)*link_param[1].inertia(0,1) + v1(1)*link_param[1].inertia(0,2)) + v1(1)*(-(link_param[1].mcm(0)*v1(4)) - v1(2)*link_param[1].inertia(1,1) + v1(1)*link_param[1].inertia(1,2)) + v1(2)*(-(link_param[1].mcm(0)*v1(5)) - v1(2)*link_param[1].inertia(1,2) + v1(1)*link_param[1].inertia(2,2));
fnet1(4) = -(a1(5)*link_param[1].mcm(0)) + a1(3)*link_param[1].mcm(2) + (-(link_param[1].mcm(0)*v1(0)) - link_param[1].mcm(2)*v1(2))*v1(4) + (link_param[1].mcm(1)*v1(2) - link_param[1].mass*v1(3))*v1(5) + v1(3)*(link_param[1].mcm(1)*v1(0) + link_param[1].mass*v1(5)) + a1(0)*link_param[1].inertia(0,1) + v1(0)*(-(link_param[1].mcm(1)*v1(3)) + v1(2)*link_param[1].inertia(0,0) - v1(0)*link_param[1].inertia(0,2)) + a1(1)*link_param[1].inertia(1,1) + a1(2)*link_param[1].inertia(1,2) + v1(1)*(link_param[1].mcm(0)*v1(3) + link_param[1].mcm(2)*v1(5) + v1(2)*link_param[1].inertia(0,1) - v1(0)*link_param[1].inertia(1,2)) + v1(2)*(-(link_param[1].mcm(1)*v1(5)) + v1(2)*link_param[1].inertia(0,2) - v1(0)*link_param[1].inertia(2,2));
fnet1(5) = a1(4)*link_param[1].mcm(0) - a1(3)*link_param[1].mcm(1) + (link_param[1].mcm(2)*v1(1) + link_param[1].mass*v1(3))*v1(4) + v1(3)*(link_param[1].mcm(2)*v1(0) - link_param[1].mass*v1(4)) + (-(link_param[1].mcm(0)*v1(0)) - link_param[1].mcm(1)*v1(1))*v1(5) + v1(0)*(-(link_param[1].mcm(2)*v1(3)) - v1(1)*link_param[1].inertia(0,0) + v1(0)*link_param[1].inertia(0,1)) + a1(0)*link_param[1].inertia(0,2) + v1(1)*(-(link_param[1].mcm(2)*v1(4)) - v1(1)*link_param[1].inertia(0,1) + v1(0)*link_param[1].inertia(1,1)) + a1(1)*link_param[1].inertia(1,2) + v1(2)*(link_param[1].mcm(0)*v1(3) + link_param[1].mcm(1)*v1(4) - v1(1)*link_param[1].inertia(0,2) + v1(0)*link_param[1].inertia(1,2)) + a1(2)*link_param[1].inertia(2,2);

fnet2(0) = link_param[2].mass*a2(3) - a2(2)*link_param[2].mcm(1) + a2(1)*link_param[2].mcm(2) - link_param[2].mcm(0)*pow(v2(1),2) - link_param[2].mcm(0)*pow(v2(2),2) + v2(0)*(link_param[2].mcm(1)*v2(1) + link_param[2].mcm(2)*v2(2)) - link_param[2].mass*v2(2)*v2(4) + link_param[2].mass*v2(1)*v2(5);
fnet2(1) = link_param[2].mass*a2(4) + a2(2)*link_param[2].mcm(0) - a2(0)*link_param[2].mcm(2) - link_param[2].mcm(1)*pow(v2(0),2) - link_param[2].mcm(1)*pow(v2(2),2) + v2(1)*(link_param[2].mcm(0)*v2(0) + link_param[2].mcm(2)*v2(2)) + link_param[2].mass*v2(2)*v2(3) - link_param[2].mass*v2(0)*v2(5);
fnet2(2) = link_param[2].mass*a2(5) - a2(1)*link_param[2].mcm(0) + a2(0)*link_param[2].mcm(1) - link_param[2].mcm(2)*pow(v2(0),2) - link_param[2].mcm(2)*pow(v2(1),2) + (link_param[2].mcm(0)*v2(0) + link_param[2].mcm(1)*v2(1))*v2(2) - link_param[2].mass*v2(1)*v2(3) + link_param[2].mass*v2(0)*v2(4);
fnet2(3) = a2(5)*link_param[2].mcm(1) - a2(4)*link_param[2].mcm(2) + (-(link_param[2].mcm(1)*v2(1)) - link_param[2].mcm(2)*v2(2))*v2(3) + (link_param[2].mcm(0)*v2(2) + link_param[2].mass*v2(4))*v2(5) + v2(4)*(link_param[2].mcm(0)*v2(1) - link_param[2].mass*v2(5)) + a2(0)*link_param[2].inertia(0,0) + a2(1)*link_param[2].inertia(0,1) + a2(2)*link_param[2].inertia(0,2) + v2(0)*(link_param[2].mcm(1)*v2(4) + link_param[2].mcm(2)*v2(5) - v2(2)*link_param[2].inertia(0,1) + v2(1)*link_param[2].inertia(0,2)) + v2(1)*(-(link_param[2].mcm(0)*v2(4)) - v2(2)*link_param[2].inertia(1,1) + v2(1)*link_param[2].inertia(1,2)) + v2(2)*(-(link_param[2].mcm(0)*v2(5)) - v2(2)*link_param[2].inertia(1,2) + v2(1)*link_param[2].inertia(2,2));
fnet2(4) = -(a2(5)*link_param[2].mcm(0)) + a2(3)*link_param[2].mcm(2) + (-(link_param[2].mcm(0)*v2(0)) - link_param[2].mcm(2)*v2(2))*v2(4) + (link_param[2].mcm(1)*v2(2) - link_param[2].mass*v2(3))*v2(5) + v2(3)*(link_param[2].mcm(1)*v2(0) + link_param[2].mass*v2(5)) + a2(0)*link_param[2].inertia(0,1) + v2(0)*(-(link_param[2].mcm(1)*v2(3)) + v2(2)*link_param[2].inertia(0,0) - v2(0)*link_param[2].inertia(0,2)) + a2(1)*link_param[2].inertia(1,1) + a2(2)*link_param[2].inertia(1,2) + v2(1)*(link_param[2].mcm(0)*v2(3) + link_param[2].mcm(2)*v2(5) + v2(2)*link_param[2].inertia(0,1) - v2(0)*link_param[2].inertia(1,2)) + v2(2)*(-(link_param[2].mcm(1)*v2(5)) + v2(2)*link_param[2].inertia(0,2) - v2(0)*link_param[2].inertia(2,2));
fnet2(5) = a2(4)*link_param[2].mcm(0) - a2(3)*link_param[2].mcm(1) + (link_param[2].mcm(2)*v2(1) + link_param[2].mass*v2(3))*v2(4) + v2(3)*(link_param[2].mcm(2)*v2(0) - link_param[2].mass*v2(4)) + (-(link_param[2].mcm(0)*v2(0)) - link_param[2].mcm(1)*v2(1))*v2(5) + v2(0)*(-(link_param[2].mcm(2)*v2(3)) - v2(1)*link_param[2].inertia(0,0) + v2(0)*link_param[2].inertia(0,1)) + a2(0)*link_param[2].inertia(0,2) + v2(1)*(-(link_param[2].mcm(2)*v2(4)) - v2(1)*link_param[2].inertia(0,1) + v2(0)*link_param[2].inertia(1,1)) + a2(1)*link_param[2].inertia(1,2) + v2(2)*(link_param[2].mcm(0)*v2(3) + link_param[2].mcm(1)*v2(4) - v2(1)*link_param[2].inertia(0,2) + v2(0)*link_param[2].inertia(1,2)) + a2(2)*link_param[2].inertia(2,2);

fnet3(0) = link_param[3].mass*a3(3) - a3(2)*link_param[3].mcm(1) + a3(1)*link_param[3].mcm(2) - link_param[3].mcm(0)*pow(v3(1),2) - link_param[3].mcm(0)*pow(v3(2),2) + v3(0)*(link_param[3].mcm(1)*v3(1) + link_param[3].mcm(2)*v3(2)) - link_param[3].mass*v3(2)*v3(4) + link_param[3].mass*v3(1)*v3(5);
fnet3(1) = link_param[3].mass*a3(4) + a3(2)*link_param[3].mcm(0) - a3(0)*link_param[3].mcm(2) - link_param[3].mcm(1)*pow(v3(0),2) - link_param[3].mcm(1)*pow(v3(2),2) + v3(1)*(link_param[3].mcm(0)*v3(0) + link_param[3].mcm(2)*v3(2)) + link_param[3].mass*v3(2)*v3(3) - link_param[3].mass*v3(0)*v3(5);
fnet3(2) = link_param[3].mass*a3(5) - a3(1)*link_param[3].mcm(0) + a3(0)*link_param[3].mcm(1) - link_param[3].mcm(2)*pow(v3(0),2) - link_param[3].mcm(2)*pow(v3(1),2) + (link_param[3].mcm(0)*v3(0) + link_param[3].mcm(1)*v3(1))*v3(2) - link_param[3].mass*v3(1)*v3(3) + link_param[3].mass*v3(0)*v3(4);
fnet3(3) = a3(5)*link_param[3].mcm(1) - a3(4)*link_param[3].mcm(2) + (-(link_param[3].mcm(1)*v3(1)) - link_param[3].mcm(2)*v3(2))*v3(3) + (link_param[3].mcm(0)*v3(2) + link_param[3].mass*v3(4))*v3(5) + v3(4)*(link_param[3].mcm(0)*v3(1) - link_param[3].mass*v3(5)) + a3(0)*link_param[3].inertia(0,0) + a3(1)*link_param[3].inertia(0,1) + a3(2)*link_param[3].inertia(0,2) + v3(0)*(link_param[3].mcm(1)*v3(4) + link_param[3].mcm(2)*v3(5) - v3(2)*link_param[3].inertia(0,1) + v3(1)*link_param[3].inertia(0,2)) + v3(1)*(-(link_param[3].mcm(0)*v3(4)) - v3(2)*link_param[3].inertia(1,1) + v3(1)*link_param[3].inertia(1,2)) + v3(2)*(-(link_param[3].mcm(0)*v3(5)) - v3(2)*link_param[3].inertia(1,2) + v3(1)*link_param[3].inertia(2,2));
fnet3(4) = -(a3(5)*link_param[3].mcm(0)) + a3(3)*link_param[3].mcm(2) + (-(link_param[3].mcm(0)*v3(0)) - link_param[3].mcm(2)*v3(2))*v3(4) + (link_param[3].mcm(1)*v3(2) - link_param[3].mass*v3(3))*v3(5) + v3(3)*(link_param[3].mcm(1)*v3(0) + link_param[3].mass*v3(5)) + a3(0)*link_param[3].inertia(0,1) + v3(0)*(-(link_param[3].mcm(1)*v3(3)) + v3(2)*link_param[3].inertia(0,0) - v3(0)*link_param[3].inertia(0,2)) + a3(1)*link_param[3].inertia(1,1) + a3(2)*link_param[3].inertia(1,2) + v3(1)*(link_param[3].mcm(0)*v3(3) + link_param[3].mcm(2)*v3(5) + v3(2)*link_param[3].inertia(0,1) - v3(0)*link_param[3].inertia(1,2)) + v3(2)*(-(link_param[3].mcm(1)*v3(5)) + v3(2)*link_param[3].inertia(0,2) - v3(0)*link_param[3].inertia(2,2));
fnet3(5) = a3(4)*link_param[3].mcm(0) - a3(3)*link_param[3].mcm(1) + (link_param[3].mcm(2)*v3(1) + link_param[3].mass*v3(3))*v3(4) + v3(3)*(link_param[3].mcm(2)*v3(0) - link_param[3].mass*v3(4)) + (-(link_param[3].mcm(0)*v3(0)) - link_param[3].mcm(1)*v3(1))*v3(5) + v3(0)*(-(link_param[3].mcm(2)*v3(3)) - v3(1)*link_param[3].inertia(0,0) + v3(0)*link_param[3].inertia(0,1)) + a3(0)*link_param[3].inertia(0,2) + v3(1)*(-(link_param[3].mcm(2)*v3(4)) - v3(1)*link_param[3].inertia(0,1) + v3(0)*link_param[3].inertia(1,1)) + a3(1)*link_param[3].inertia(1,2) + v3(2)*(link_param[3].mcm(0)*v3(3) + link_param[3].mcm(1)*v3(4) - v3(1)*link_param[3].inertia(0,2) + v3(0)*link_param[3].inertia(1,2)) + a3(2)*link_param[3].inertia(2,2);

fnet4(0) = link_param[4].mass*a4(3) - a4(2)*link_param[4].mcm(1) + a4(1)*link_param[4].mcm(2) - link_param[4].mcm(0)*pow(v4(1),2) - link_param[4].mcm(0)*pow(v4(2),2) + v4(0)*(link_param[4].mcm(1)*v4(1) + link_param[4].mcm(2)*v4(2)) - link_param[4].mass*v4(2)*v4(4) + link_param[4].mass*v4(1)*v4(5);
fnet4(1) = link_param[4].mass*a4(4) + a4(2)*link_param[4].mcm(0) - a4(0)*link_param[4].mcm(2) - link_param[4].mcm(1)*pow(v4(0),2) - link_param[4].mcm(1)*pow(v4(2),2) + v4(1)*(link_param[4].mcm(0)*v4(0) + link_param[4].mcm(2)*v4(2)) + link_param[4].mass*v4(2)*v4(3) - link_param[4].mass*v4(0)*v4(5);
fnet4(2) = link_param[4].mass*a4(5) - a4(1)*link_param[4].mcm(0) + a4(0)*link_param[4].mcm(1) - link_param[4].mcm(2)*pow(v4(0),2) - link_param[4].mcm(2)*pow(v4(1),2) + (link_param[4].mcm(0)*v4(0) + link_param[4].mcm(1)*v4(1))*v4(2) - link_param[4].mass*v4(1)*v4(3) + link_param[4].mass*v4(0)*v4(4);
fnet4(3) = a4(5)*link_param[4].mcm(1) - a4(4)*link_param[4].mcm(2) + (-(link_param[4].mcm(1)*v4(1)) - link_param[4].mcm(2)*v4(2))*v4(3) + (link_param[4].mcm(0)*v4(2) + link_param[4].mass*v4(4))*v4(5) + v4(4)*(link_param[4].mcm(0)*v4(1) - link_param[4].mass*v4(5)) + a4(0)*link_param[4].inertia(0,0) + a4(1)*link_param[4].inertia(0,1) + a4(2)*link_param[4].inertia(0,2) + v4(0)*(link_param[4].mcm(1)*v4(4) + link_param[4].mcm(2)*v4(5) - v4(2)*link_param[4].inertia(0,1) + v4(1)*link_param[4].inertia(0,2)) + v4(1)*(-(link_param[4].mcm(0)*v4(4)) - v4(2)*link_param[4].inertia(1,1) + v4(1)*link_param[4].inertia(1,2)) + v4(2)*(-(link_param[4].mcm(0)*v4(5)) - v4(2)*link_param[4].inertia(1,2) + v4(1)*link_param[4].inertia(2,2));
fnet4(4) = -(a4(5)*link_param[4].mcm(0)) + a4(3)*link_param[4].mcm(2) + (-(link_param[4].mcm(0)*v4(0)) - link_param[4].mcm(2)*v4(2))*v4(4) + (link_param[4].mcm(1)*v4(2) - link_param[4].mass*v4(3))*v4(5) + v4(3)*(link_param[4].mcm(1)*v4(0) + link_param[4].mass*v4(5)) + a4(0)*link_param[4].inertia(0,1) + v4(0)*(-(link_param[4].mcm(1)*v4(3)) + v4(2)*link_param[4].inertia(0,0) - v4(0)*link_param[4].inertia(0,2)) + a4(1)*link_param[4].inertia(1,1) + a4(2)*link_param[4].inertia(1,2) + v4(1)*(link_param[4].mcm(0)*v4(3) + link_param[4].mcm(2)*v4(5) + v4(2)*link_param[4].inertia(0,1) - v4(0)*link_param[4].inertia(1,2)) + v4(2)*(-(link_param[4].mcm(1)*v4(5)) + v4(2)*link_param[4].inertia(0,2) - v4(0)*link_param[4].inertia(2,2));
fnet4(5) = a4(4)*link_param[4].mcm(0) - a4(3)*link_param[4].mcm(1) + (link_param[4].mcm(2)*v4(1) + link_param[4].mass*v4(3))*v4(4) + v4(3)*(link_param[4].mcm(2)*v4(0) - link_param[4].mass*v4(4)) + (-(link_param[4].mcm(0)*v4(0)) - link_param[4].mcm(1)*v4(1))*v4(5) + v4(0)*(-(link_param[4].mcm(2)*v4(3)) - v4(1)*link_param[4].inertia(0,0) + v4(0)*link_param[4].inertia(0,1)) + a4(0)*link_param[4].inertia(0,2) + v4(1)*(-(link_param[4].mcm(2)*v4(4)) - v4(1)*link_param[4].inertia(0,1) + v4(0)*link_param[4].inertia(1,1)) + a4(1)*link_param[4].inertia(1,2) + v4(2)*(link_param[4].mcm(0)*v4(3) + link_param[4].mcm(1)*v4(4) - v4(1)*link_param[4].inertia(0,2) + v4(0)*link_param[4].inertia(1,2)) + a4(2)*link_param[4].inertia(2,2);

fnet5(0) = link_param[5].mass*a5(3) - a5(2)*link_param[5].mcm(1) + a5(1)*link_param[5].mcm(2) - link_param[5].mcm(0)*pow(v5(1),2) - link_param[5].mcm(0)*pow(v5(2),2) + v5(0)*(link_param[5].mcm(1)*v5(1) + link_param[5].mcm(2)*v5(2)) - link_param[5].mass*v5(2)*v5(4) + link_param[5].mass*v5(1)*v5(5);
fnet5(1) = link_param[5].mass*a5(4) + a5(2)*link_param[5].mcm(0) - a5(0)*link_param[5].mcm(2) - link_param[5].mcm(1)*pow(v5(0),2) - link_param[5].mcm(1)*pow(v5(2),2) + v5(1)*(link_param[5].mcm(0)*v5(0) + link_param[5].mcm(2)*v5(2)) + link_param[5].mass*v5(2)*v5(3) - link_param[5].mass*v5(0)*v5(5);
fnet5(2) = link_param[5].mass*a5(5) - a5(1)*link_param[5].mcm(0) + a5(0)*link_param[5].mcm(1) - link_param[5].mcm(2)*pow(v5(0),2) - link_param[5].mcm(2)*pow(v5(1),2) + (link_param[5].mcm(0)*v5(0) + link_param[5].mcm(1)*v5(1))*v5(2) - link_param[5].mass*v5(1)*v5(3) + link_param[5].mass*v5(0)*v5(4);
fnet5(3) = a5(5)*link_param[5].mcm(1) - a5(4)*link_param[5].mcm(2) + (-(link_param[5].mcm(1)*v5(1)) - link_param[5].mcm(2)*v5(2))*v5(3) + (link_param[5].mcm(0)*v5(2) + link_param[5].mass*v5(4))*v5(5) + v5(4)*(link_param[5].mcm(0)*v5(1) - link_param[5].mass*v5(5)) + a5(0)*link_param[5].inertia(0,0) + a5(1)*link_param[5].inertia(0,1) + a5(2)*link_param[5].inertia(0,2) + v5(0)*(link_param[5].mcm(1)*v5(4) + link_param[5].mcm(2)*v5(5) - v5(2)*link_param[5].inertia(0,1) + v5(1)*link_param[5].inertia(0,2)) + v5(1)*(-(link_param[5].mcm(0)*v5(4)) - v5(2)*link_param[5].inertia(1,1) + v5(1)*link_param[5].inertia(1,2)) + v5(2)*(-(link_param[5].mcm(0)*v5(5)) - v5(2)*link_param[5].inertia(1,2) + v5(1)*link_param[5].inertia(2,2));
fnet5(4) = -(a5(5)*link_param[5].mcm(0)) + a5(3)*link_param[5].mcm(2) + (-(link_param[5].mcm(0)*v5(0)) - link_param[5].mcm(2)*v5(2))*v5(4) + (link_param[5].mcm(1)*v5(2) - link_param[5].mass*v5(3))*v5(5) + v5(3)*(link_param[5].mcm(1)*v5(0) + link_param[5].mass*v5(5)) + a5(0)*link_param[5].inertia(0,1) + v5(0)*(-(link_param[5].mcm(1)*v5(3)) + v5(2)*link_param[5].inertia(0,0) - v5(0)*link_param[5].inertia(0,2)) + a5(1)*link_param[5].inertia(1,1) + a5(2)*link_param[5].inertia(1,2) + v5(1)*(link_param[5].mcm(0)*v5(3) + link_param[5].mcm(2)*v5(5) + v5(2)*link_param[5].inertia(0,1) - v5(0)*link_param[5].inertia(1,2)) + v5(2)*(-(link_param[5].mcm(1)*v5(5)) + v5(2)*link_param[5].inertia(0,2) - v5(0)*link_param[5].inertia(2,2));
fnet5(5) = a5(4)*link_param[5].mcm(0) - a5(3)*link_param[5].mcm(1) + (link_param[5].mcm(2)*v5(1) + link_param[5].mass*v5(3))*v5(4) + v5(3)*(link_param[5].mcm(2)*v5(0) - link_param[5].mass*v5(4)) + (-(link_param[5].mcm(0)*v5(0)) - link_param[5].mcm(1)*v5(1))*v5(5) + v5(0)*(-(link_param[5].mcm(2)*v5(3)) - v5(1)*link_param[5].inertia(0,0) + v5(0)*link_param[5].inertia(0,1)) + a5(0)*link_param[5].inertia(0,2) + v5(1)*(-(link_param[5].mcm(2)*v5(4)) - v5(1)*link_param[5].inertia(0,1) + v5(0)*link_param[5].inertia(1,1)) + a5(1)*link_param[5].inertia(1,2) + v5(2)*(link_param[5].mcm(0)*v5(3) + link_param[5].mcm(1)*v5(4) - v5(1)*link_param[5].inertia(0,2) + v5(0)*link_param[5].inertia(1,2)) + a5(2)*link_param[5].inertia(2,2);

fnet6(0) = link_param[6].mass*a6(3) - a6(2)*link_param[6].mcm(1) + a6(1)*link_param[6].mcm(2) - link_param[6].mcm(0)*pow(v6(1),2) - link_param[6].mcm(0)*pow(v6(2),2) + v6(0)*(link_param[6].mcm(1)*v6(1) + link_param[6].mcm(2)*v6(2)) - link_param[6].mass*v6(2)*v6(4) + link_param[6].mass*v6(1)*v6(5);
fnet6(1) = link_param[6].mass*a6(4) + a6(2)*link_param[6].mcm(0) - a6(0)*link_param[6].mcm(2) - link_param[6].mcm(1)*pow(v6(0),2) - link_param[6].mcm(1)*pow(v6(2),2) + v6(1)*(link_param[6].mcm(0)*v6(0) + link_param[6].mcm(2)*v6(2)) + link_param[6].mass*v6(2)*v6(3) - link_param[6].mass*v6(0)*v6(5);
fnet6(2) = link_param[6].mass*a6(5) - a6(1)*link_param[6].mcm(0) + a6(0)*link_param[6].mcm(1) - link_param[6].mcm(2)*pow(v6(0),2) - link_param[6].mcm(2)*pow(v6(1),2) + (link_param[6].mcm(0)*v6(0) + link_param[6].mcm(1)*v6(1))*v6(2) - link_param[6].mass*v6(1)*v6(3) + link_param[6].mass*v6(0)*v6(4);
fnet6(3) = a6(5)*link_param[6].mcm(1) - a6(4)*link_param[6].mcm(2) + (-(link_param[6].mcm(1)*v6(1)) - link_param[6].mcm(2)*v6(2))*v6(3) + (link_param[6].mcm(0)*v6(2) + link_param[6].mass*v6(4))*v6(5) + v6(4)*(link_param[6].mcm(0)*v6(1) - link_param[6].mass*v6(5)) + a6(0)*link_param[6].inertia(0,0) + a6(1)*link_param[6].inertia(0,1) + a6(2)*link_param[6].inertia(0,2) + v6(0)*(link_param[6].mcm(1)*v6(4) + link_param[6].mcm(2)*v6(5) - v6(2)*link_param[6].inertia(0,1) + v6(1)*link_param[6].inertia(0,2)) + v6(1)*(-(link_param[6].mcm(0)*v6(4)) - v6(2)*link_param[6].inertia(1,1) + v6(1)*link_param[6].inertia(1,2)) + v6(2)*(-(link_param[6].mcm(0)*v6(5)) - v6(2)*link_param[6].inertia(1,2) + v6(1)*link_param[6].inertia(2,2));
fnet6(4) = -(a6(5)*link_param[6].mcm(0)) + a6(3)*link_param[6].mcm(2) + (-(link_param[6].mcm(0)*v6(0)) - link_param[6].mcm(2)*v6(2))*v6(4) + (link_param[6].mcm(1)*v6(2) - link_param[6].mass*v6(3))*v6(5) + v6(3)*(link_param[6].mcm(1)*v6(0) + link_param[6].mass*v6(5)) + a6(0)*link_param[6].inertia(0,1) + v6(0)*(-(link_param[6].mcm(1)*v6(3)) + v6(2)*link_param[6].inertia(0,0) - v6(0)*link_param[6].inertia(0,2)) + a6(1)*link_param[6].inertia(1,1) + a6(2)*link_param[6].inertia(1,2) + v6(1)*(link_param[6].mcm(0)*v6(3) + link_param[6].mcm(2)*v6(5) + v6(2)*link_param[6].inertia(0,1) - v6(0)*link_param[6].inertia(1,2)) + v6(2)*(-(link_param[6].mcm(1)*v6(5)) + v6(2)*link_param[6].inertia(0,2) - v6(0)*link_param[6].inertia(2,2));
fnet6(5) = a6(4)*link_param[6].mcm(0) - a6(3)*link_param[6].mcm(1) + (link_param[6].mcm(2)*v6(1) + link_param[6].mass*v6(3))*v6(4) + v6(3)*(link_param[6].mcm(2)*v6(0) - link_param[6].mass*v6(4)) + (-(link_param[6].mcm(0)*v6(0)) - link_param[6].mcm(1)*v6(1))*v6(5) + v6(0)*(-(link_param[6].mcm(2)*v6(3)) - v6(1)*link_param[6].inertia(0,0) + v6(0)*link_param[6].inertia(0,1)) + a6(0)*link_param[6].inertia(0,2) + v6(1)*(-(link_param[6].mcm(2)*v6(4)) - v6(1)*link_param[6].inertia(0,1) + v6(0)*link_param[6].inertia(1,1)) + a6(1)*link_param[6].inertia(1,2) + v6(2)*(link_param[6].mcm(0)*v6(3) + link_param[6].mcm(1)*v6(4) - v6(1)*link_param[6].inertia(0,2) + v6(0)*link_param[6].inertia(1,2)) + a6(2)*link_param[6].inertia(2,2);

fnet7(0) = link_param[7].mass*a7(3) - a7(2)*link_param[7].mcm(1) + a7(1)*link_param[7].mcm(2) - link_param[7].mcm(0)*pow(v7(1),2) - link_param[7].mcm(0)*pow(v7(2),2) + v7(0)*(link_param[7].mcm(1)*v7(1) + link_param[7].mcm(2)*v7(2)) - link_param[7].mass*v7(2)*v7(4) + link_param[7].mass*v7(1)*v7(5);
fnet7(1) = link_param[7].mass*a7(4) + a7(2)*link_param[7].mcm(0) - a7(0)*link_param[7].mcm(2) - link_param[7].mcm(1)*pow(v7(0),2) - link_param[7].mcm(1)*pow(v7(2),2) + v7(1)*(link_param[7].mcm(0)*v7(0) + link_param[7].mcm(2)*v7(2)) + link_param[7].mass*v7(2)*v7(3) - link_param[7].mass*v7(0)*v7(5);
fnet7(2) = link_param[7].mass*a7(5) - a7(1)*link_param[7].mcm(0) + a7(0)*link_param[7].mcm(1) - link_param[7].mcm(2)*pow(v7(0),2) - link_param[7].mcm(2)*pow(v7(1),2) + (link_param[7].mcm(0)*v7(0) + link_param[7].mcm(1)*v7(1))*v7(2) - link_param[7].mass*v7(1)*v7(3) + link_param[7].mass*v7(0)*v7(4);
fnet7(3) = a7(5)*link_param[7].mcm(1) - a7(4)*link_param[7].mcm(2) + (-(link_param[7].mcm(1)*v7(1)) - link_param[7].mcm(2)*v7(2))*v7(3) + (link_param[7].mcm(0)*v7(2) + link_param[7].mass*v7(4))*v7(5) + v7(4)*(link_param[7].mcm(0)*v7(1) - link_param[7].mass*v7(5)) + a7(0)*link_param[7].inertia(0,0) + a7(1)*link_param[7].inertia(0,1) + a7(2)*link_param[7].inertia(0,2) + v7(0)*(link_param[7].mcm(1)*v7(4) + link_param[7].mcm(2)*v7(5) - v7(2)*link_param[7].inertia(0,1) + v7(1)*link_param[7].inertia(0,2)) + v7(1)*(-(link_param[7].mcm(0)*v7(4)) - v7(2)*link_param[7].inertia(1,1) + v7(1)*link_param[7].inertia(1,2)) + v7(2)*(-(link_param[7].mcm(0)*v7(5)) - v7(2)*link_param[7].inertia(1,2) + v7(1)*link_param[7].inertia(2,2));
fnet7(4) = -(a7(5)*link_param[7].mcm(0)) + a7(3)*link_param[7].mcm(2) + (-(link_param[7].mcm(0)*v7(0)) - link_param[7].mcm(2)*v7(2))*v7(4) + (link_param[7].mcm(1)*v7(2) - link_param[7].mass*v7(3))*v7(5) + v7(3)*(link_param[7].mcm(1)*v7(0) + link_param[7].mass*v7(5)) + a7(0)*link_param[7].inertia(0,1) + v7(0)*(-(link_param[7].mcm(1)*v7(3)) + v7(2)*link_param[7].inertia(0,0) - v7(0)*link_param[7].inertia(0,2)) + a7(1)*link_param[7].inertia(1,1) + a7(2)*link_param[7].inertia(1,2) + v7(1)*(link_param[7].mcm(0)*v7(3) + link_param[7].mcm(2)*v7(5) + v7(2)*link_param[7].inertia(0,1) - v7(0)*link_param[7].inertia(1,2)) + v7(2)*(-(link_param[7].mcm(1)*v7(5)) + v7(2)*link_param[7].inertia(0,2) - v7(0)*link_param[7].inertia(2,2));
fnet7(5) = a7(4)*link_param[7].mcm(0) - a7(3)*link_param[7].mcm(1) + (link_param[7].mcm(2)*v7(1) + link_param[7].mass*v7(3))*v7(4) + v7(3)*(link_param[7].mcm(2)*v7(0) - link_param[7].mass*v7(4)) + (-(link_param[7].mcm(0)*v7(0)) - link_param[7].mcm(1)*v7(1))*v7(5) + v7(0)*(-(link_param[7].mcm(2)*v7(3)) - v7(1)*link_param[7].inertia(0,0) + v7(0)*link_param[7].inertia(0,1)) + a7(0)*link_param[7].inertia(0,2) + v7(1)*(-(link_param[7].mcm(2)*v7(4)) - v7(1)*link_param[7].inertia(0,1) + v7(0)*link_param[7].inertia(1,1)) + a7(1)*link_param[7].inertia(1,2) + v7(2)*(link_param[7].mcm(0)*v7(3) + link_param[7].mcm(1)*v7(4) - v7(1)*link_param[7].inertia(0,2) + v7(0)*link_param[7].inertia(1,2)) + a7(2)*link_param[7].inertia(2,2);

fnet8(0) = endeff_param.mass*a8(3) - a8(2)*endeff_param.mcm(1) + a8(1)*endeff_param.mcm(2) - endeff_param.mcm(0)*pow(v8(1),2) - endeff_param.mcm(0)*pow(v8(2),2) + v8(0)*(endeff_param.mcm(1)*v8(1) + endeff_param.mcm(2)*v8(2)) - endeff_param.mass*v8(2)*v8(4) + endeff_param.mass*v8(1)*v8(5);
fnet8(1) = endeff_param.mass*a8(4) + a8(2)*endeff_param.mcm(0) - a8(0)*endeff_param.mcm(2) - endeff_param.mcm(1)*pow(v8(0),2) - endeff_param.mcm(1)*pow(v8(2),2) + v8(1)*(endeff_param.mcm(0)*v8(0) + endeff_param.mcm(2)*v8(2)) + endeff_param.mass*v8(2)*v8(3) - endeff_param.mass*v8(0)*v8(5);
fnet8(2) = endeff_param.mass*a8(5) - a8(1)*endeff_param.mcm(0) + a8(0)*endeff_param.mcm(1) - endeff_param.mcm(2)*pow(v8(0),2) - endeff_param.mcm(2)*pow(v8(1),2) + (endeff_param.mcm(0)*v8(0) + endeff_param.mcm(1)*v8(1))*v8(2) - endeff_param.mass*v8(1)*v8(3) + endeff_param.mass*v8(0)*v8(4);
fnet8(3) = a8(5)*endeff_param.mcm(1) - a8(4)*endeff_param.mcm(2) + (-(endeff_param.mcm(1)*v8(1)) - endeff_param.mcm(2)*v8(2))*v8(3) - endeff_param.mcm(0)*v8(1)*v8(4) - endeff_param.mcm(0)*v8(2)*v8(5) + (endeff_param.mcm(0)*v8(2) + endeff_param.mass*v8(4))*v8(5) + v8(4)*(endeff_param.mcm(0)*v8(1) - endeff_param.mass*v8(5)) + v8(0)*(endeff_param.mcm(1)*v8(4) + endeff_param.mcm(2)*v8(5));
fnet8(4) = -(a8(5)*endeff_param.mcm(0)) + a8(3)*endeff_param.mcm(2) - endeff_param.mcm(1)*v8(0)*v8(3) + (-(endeff_param.mcm(0)*v8(0)) - endeff_param.mcm(2)*v8(2))*v8(4) - endeff_param.mcm(1)*v8(2)*v8(5) + (endeff_param.mcm(1)*v8(2) - endeff_param.mass*v8(3))*v8(5) + v8(3)*(endeff_param.mcm(1)*v8(0) + endeff_param.mass*v8(5)) + v8(1)*(endeff_param.mcm(0)*v8(3) + endeff_param.mcm(2)*v8(5));
fnet8(5) = a8(4)*endeff_param.mcm(0) - a8(3)*endeff_param.mcm(1) - endeff_param.mcm(2)*v8(0)*v8(3) - endeff_param.mcm(2)*v8(1)*v8(4) + (endeff_param.mcm(2)*v8(1) + endeff_param.mass*v8(3))*v8(4) + v8(3)*(endeff_param.mcm(2)*v8(0) - endeff_param.mass*v8(4)) + v8(2)*(endeff_param.mcm(0)*v8(3) + endeff_param.mcm(1)*v8(4)) + (-(endeff_param.mcm(0)*v8(0)) - endeff_param.mcm(1)*v8(1))*v8(5);


fex0(0) = -(force_ex_param.base_force(0)*S00(0,0)) - force_ex_param.base_force(1)*S00(0,1) - force_ex_param.base_force(2)*S00(0,2);
fex0(1) = -(force_ex_param.base_force(0)*S00(1,0)) - force_ex_param.base_force(1)*S00(1,1) - force_ex_param.base_force(2)*S00(1,2);
fex0(2) = -(force_ex_param.base_force(0)*S00(2,0)) - force_ex_param.base_force(1)*S00(2,1) - force_ex_param.base_force(2)*S00(2,2);
fex0(3) = -(force_ex_param.base_torque(0)*S00(0,0)) - force_ex_param.base_torque(1)*S00(0,1) - force_ex_param.base_torque(2)*S00(0,2);
fex0(4) = -(force_ex_param.base_torque(0)*S00(1,0)) - force_ex_param.base_torque(1)*S00(1,1) - force_ex_param.base_torque(2)*S00(1,2);
fex0(5) = -(force_ex_param.base_torque(0)*S00(2,0)) - force_ex_param.base_torque(1)*S00(2,1) - force_ex_param.base_torque(2)*S00(2,2);

fex1(0) = -(force_ex_param.joint_force(0,0)*SG10(0,0)) - force_ex_param.joint_force(0,1)*SG10(0,1) - force_ex_param.joint_force(0,2)*SG10(0,2);
fex1(1) = -(force_ex_param.joint_force(0,0)*SG10(1,0)) - force_ex_param.joint_force(0,1)*SG10(1,1) - force_ex_param.joint_force(0,2)*SG10(1,2);
fex1(2) = -(force_ex_param.joint_force(0,0)*SG10(2,0)) - force_ex_param.joint_force(0,1)*SG10(2,1) - force_ex_param.joint_force(0,2)*SG10(2,2);
fex1(3) = -(force_ex_param.joint_torque(0,0)*SG10(0,0)) - force_ex_param.joint_torque(0,1)*SG10(0,1) - force_ex_param.joint_torque(0,2)*SG10(0,2);
fex1(4) = -(force_ex_param.joint_torque(0,0)*SG10(1,0)) - force_ex_param.joint_torque(0,1)*SG10(1,1) - force_ex_param.joint_torque(0,2)*SG10(1,2);
fex1(5) = -(force_ex_param.joint_torque(0,0)*SG10(2,0)) - force_ex_param.joint_torque(0,1)*SG10(2,1) - force_ex_param.joint_torque(0,2)*SG10(2,2);

fex2(0) = -(force_ex_param.joint_force(1,0)*SG20(0,0)) - force_ex_param.joint_force(1,1)*SG20(0,1) - force_ex_param.joint_force(1,2)*SG20(0,2);
fex2(1) = -(force_ex_param.joint_force(1,0)*SG20(1,0)) - force_ex_param.joint_force(1,1)*SG20(1,1) - force_ex_param.joint_force(1,2)*SG20(1,2);
fex2(2) = -(force_ex_param.joint_force(1,0)*SG20(2,0)) - force_ex_param.joint_force(1,1)*SG20(2,1) - force_ex_param.joint_force(1,2)*SG20(2,2);
fex2(3) = -(force_ex_param.joint_torque(1,0)*SG20(0,0)) - force_ex_param.joint_torque(1,1)*SG20(0,1) - force_ex_param.joint_torque(1,2)*SG20(0,2);
fex2(4) = -(force_ex_param.joint_torque(1,0)*SG20(1,0)) - force_ex_param.joint_torque(1,1)*SG20(1,1) - force_ex_param.joint_torque(1,2)*SG20(1,2);
fex2(5) = -(force_ex_param.joint_torque(1,0)*SG20(2,0)) - force_ex_param.joint_torque(1,1)*SG20(2,1) - force_ex_param.joint_torque(1,2)*SG20(2,2);

fex3(0) = -(force_ex_param.joint_force(2,0)*SG30(0,0)) - force_ex_param.joint_force(2,1)*SG30(0,1) - force_ex_param.joint_force(2,2)*SG30(0,2);
fex3(1) = -(force_ex_param.joint_force(2,0)*SG30(1,0)) - force_ex_param.joint_force(2,1)*SG30(1,1) - force_ex_param.joint_force(2,2)*SG30(1,2);
fex3(2) = -(force_ex_param.joint_force(2,0)*SG30(2,0)) - force_ex_param.joint_force(2,1)*SG30(2,1) - force_ex_param.joint_force(2,2)*SG30(2,2);
fex3(3) = -(force_ex_param.joint_torque(2,0)*SG30(0,0)) - force_ex_param.joint_torque(2,1)*SG30(0,1) - force_ex_param.joint_torque(2,2)*SG30(0,2);
fex3(4) = -(force_ex_param.joint_torque(2,0)*SG30(1,0)) - force_ex_param.joint_torque(2,1)*SG30(1,1) - force_ex_param.joint_torque(2,2)*SG30(1,2);
fex3(5) = -(force_ex_param.joint_torque(2,0)*SG30(2,0)) - force_ex_param.joint_torque(2,1)*SG30(2,1) - force_ex_param.joint_torque(2,2)*SG30(2,2);

fex4(0) = -(force_ex_param.joint_force(3,0)*SG40(0,0)) - force_ex_param.joint_force(3,1)*SG40(0,1) - force_ex_param.joint_force(3,2)*SG40(0,2);
fex4(1) = -(force_ex_param.joint_force(3,0)*SG40(1,0)) - force_ex_param.joint_force(3,1)*SG40(1,1) - force_ex_param.joint_force(3,2)*SG40(1,2);
fex4(2) = -(force_ex_param.joint_force(3,0)*SG40(2,0)) - force_ex_param.joint_force(3,1)*SG40(2,1) - force_ex_param.joint_force(3,2)*SG40(2,2);
fex4(3) = -(force_ex_param.joint_torque(3,0)*SG40(0,0)) - force_ex_param.joint_torque(3,1)*SG40(0,1) - force_ex_param.joint_torque(3,2)*SG40(0,2);
fex4(4) = -(force_ex_param.joint_torque(3,0)*SG40(1,0)) - force_ex_param.joint_torque(3,1)*SG40(1,1) - force_ex_param.joint_torque(3,2)*SG40(1,2);
fex4(5) = -(force_ex_param.joint_torque(3,0)*SG40(2,0)) - force_ex_param.joint_torque(3,1)*SG40(2,1) - force_ex_param.joint_torque(3,2)*SG40(2,2);

fex5(0) = -(force_ex_param.joint_force(4,0)*SG50(0,0)) - force_ex_param.joint_force(4,1)*SG50(0,1) - force_ex_param.joint_force(4,2)*SG50(0,2);
fex5(1) = -(force_ex_param.joint_force(4,0)*SG50(1,0)) - force_ex_param.joint_force(4,1)*SG50(1,1) - force_ex_param.joint_force(4,2)*SG50(1,2);
fex5(2) = -(force_ex_param.joint_force(4,0)*SG50(2,0)) - force_ex_param.joint_force(4,1)*SG50(2,1) - force_ex_param.joint_force(4,2)*SG50(2,2);
fex5(3) = -(force_ex_param.joint_torque(4,0)*SG50(0,0)) - force_ex_param.joint_torque(4,1)*SG50(0,1) - force_ex_param.joint_torque(4,2)*SG50(0,2);
fex5(4) = -(force_ex_param.joint_torque(4,0)*SG50(1,0)) - force_ex_param.joint_torque(4,1)*SG50(1,1) - force_ex_param.joint_torque(4,2)*SG50(1,2);
fex5(5) = -(force_ex_param.joint_torque(4,0)*SG50(2,0)) - force_ex_param.joint_torque(4,1)*SG50(2,1) - force_ex_param.joint_torque(4,2)*SG50(2,2);

fex6(0) = -(force_ex_param.joint_force(5,0)*SG60(0,0)) - force_ex_param.joint_force(5,1)*SG60(0,1) - force_ex_param.joint_force(5,2)*SG60(0,2);
fex6(1) = -(force_ex_param.joint_force(5,0)*SG60(1,0)) - force_ex_param.joint_force(5,1)*SG60(1,1) - force_ex_param.joint_force(5,2)*SG60(1,2);
fex6(2) = -(force_ex_param.joint_force(5,0)*SG60(2,0)) - force_ex_param.joint_force(5,1)*SG60(2,1) - force_ex_param.joint_force(5,2)*SG60(2,2);
fex6(3) = -(force_ex_param.joint_torque(5,0)*SG60(0,0)) - force_ex_param.joint_torque(5,1)*SG60(0,1) - force_ex_param.joint_torque(5,2)*SG60(0,2);
fex6(4) = -(force_ex_param.joint_torque(5,0)*SG60(1,0)) - force_ex_param.joint_torque(5,1)*SG60(1,1) - force_ex_param.joint_torque(5,2)*SG60(1,2);
fex6(5) = -(force_ex_param.joint_torque(5,0)*SG60(2,0)) - force_ex_param.joint_torque(5,1)*SG60(2,1) - force_ex_param.joint_torque(5,2)*SG60(2,2);

fex7(0) = -(force_ex_param.joint_force(6,0)*SG70(0,0)) - force_ex_param.joint_force(6,1)*SG70(0,1) - force_ex_param.joint_force(6,2)*SG70(0,2);
fex7(1) = -(force_ex_param.joint_force(6,0)*SG70(1,0)) - force_ex_param.joint_force(6,1)*SG70(1,1) - force_ex_param.joint_force(6,2)*SG70(1,2);
fex7(2) = -(force_ex_param.joint_force(6,0)*SG70(2,0)) - force_ex_param.joint_force(6,1)*SG70(2,1) - force_ex_param.joint_force(6,2)*SG70(2,2);
fex7(3) = -(force_ex_param.joint_torque(6,0)*SG70(0,0)) - force_ex_param.joint_torque(6,1)*SG70(0,1) - force_ex_param.joint_torque(6,2)*SG70(0,2);
fex7(4) = -(force_ex_param.joint_torque(6,0)*SG70(1,0)) - force_ex_param.joint_torque(6,1)*SG70(1,1) - force_ex_param.joint_torque(6,2)*SG70(1,2);
fex7(5) = -(force_ex_param.joint_torque(6,0)*SG70(2,0)) - force_ex_param.joint_torque(6,1)*SG70(2,1) - force_ex_param.joint_torque(6,2)*SG70(2,2);

//barrett_InvDynNEfunc7(void)

// total forces and external forces for each joint
f8(0) = fnet8(0);
f8(1) = fnet8(1);
f8(2) = fnet8(2);
f8(3) = fnet8(3);
f8(4) = fnet8(4);
f8(5) = fnet8(5);

f7(0) = fnet7(0) + f8(0)*Si78(0,0) + f8(1)*Si78(0,1) + f8(2)*Si78(0,2);
f7(1) = fnet7(1) + f8(0)*Si78(1,0) + f8(1)*Si78(1,1) + f8(2)*Si78(1,2);
f7(2) = fnet7(2) + f8(0)*Si78(2,0) + f8(1)*Si78(2,1) + f8(2)*Si78(2,2);
f7(3) = fnet7(3) + f8(3)*Si78(0,0) + f8(4)*Si78(0,1) + f8(5)*Si78(0,2) + f8(0)*(-(endeff_param.x(2)*Si78(1,0)) + endeff_param.x(1)*Si78(2,0)) + f8(1)*(-(endeff_param.x(2)*Si78(1,1)) + endeff_param.x(1)*Si78(2,1)) + f8(2)*(-(endeff_param.x(2)*Si78(1,2)) + endeff_param.x(1)*Si78(2,2));
f7(4) = fnet7(4) + f8(3)*Si78(1,0) + f8(4)*Si78(1,1) + f8(5)*Si78(1,2) + f8(0)*(endeff_param.x(2)*Si78(0,0) - endeff_param.x(0)*Si78(2,0)) + f8(1)*(endeff_param.x(2)*Si78(0,1) - endeff_param.x(0)*Si78(2,1)) + f8(2)*(endeff_param.x(2)*Si78(0,2) - endeff_param.x(0)*Si78(2,2));
f7(5) = fnet7(5) + f8(0)*(-(endeff_param.x(1)*Si78(0,0)) + endeff_param.x(0)*Si78(1,0)) + f8(1)*(-(endeff_param.x(1)*Si78(0,1)) + endeff_param.x(0)*Si78(1,1)) + f8(2)*(-(endeff_param.x(1)*Si78(0,2)) + endeff_param.x(0)*Si78(1,2)) + f8(3)*Si78(2,0) + f8(4)*Si78(2,1) + f8(5)*Si78(2,2);

f6(0) = f7(2) + fnet6(0);
f6(1) = fnet6(1) + f7(0)*Si67(1,0) + f7(1)*Si67(1,1);
f6(2) = fnet6(2) + f7(0)*Si67(2,0) + f7(1)*Si67(2,1);
f6(3) = f7(5) + fnet6(3);
f6(4) = fnet6(4) + f7(3)*Si67(1,0) + f7(4)*Si67(1,1);
f6(5) = fnet6(5) + f7(3)*Si67(2,0) + f7(4)*Si67(2,1);

f5(0) = -f6(2) + fnet5(0);
f5(1) = fnet5(1) + f6(0)*Si56(1,0) + f6(1)*Si56(1,1);
f5(2) = fnet5(2) + f6(0)*Si56(2,0) + f6(1)*Si56(2,1);
f5(3) = -f6(5) + fnet5(3) - ZWFE*f6(0)*Si56(1,0) - ZWFE*f6(1)*Si56(1,1);
f5(4) = -(ZWFE*f6(2)) + fnet5(4) + f6(3)*Si56(1,0) + f6(4)*Si56(1,1);
f5(5) = fnet5(5) + f6(3)*Si56(2,0) + f6(4)*Si56(2,1);

f4(0) = f5(2) + fnet4(0);
f4(1) = fnet4(1) + f5(0)*Si45(1,0) + f5(1)*Si45(1,1);
f4(2) = fnet4(2) + f5(0)*Si45(2,0) + f5(1)*Si45(2,1);
f4(3) = f5(5) + fnet4(3) + YWR*f5(0)*Si45(2,0) + YWR*f5(1)*Si45(2,1);
f4(4) = fnet4(4) + f5(3)*Si45(1,0) + f5(4)*Si45(1,1) - ZWR*f5(0)*Si45(2,0) - ZWR*f5(1)*Si45(2,1);
f4(5) = -(YWR*f5(2)) + fnet4(5) + ZWR*f5(0)*Si45(1,0) + ZWR*f5(1)*Si45(1,1) + f5(3)*Si45(2,0) + f5(4)*Si45(2,1);

f3(0) = -f4(2) + fnet3(0);
f3(1) = fnet3(1) + f4(0)*Si34(1,0) + f4(1)*Si34(1,1);
f3(2) = fnet3(2) + f4(0)*Si34(2,0) + f4(1)*Si34(2,1);
f3(3) = -f4(5) + fnet3(3) + f4(0)*(-(ZEB*Si34(1,0)) + YEB*Si34(2,0)) + f4(1)*(-(ZEB*Si34(1,1)) + YEB*Si34(2,1));
f3(4) = -(ZEB*f4(2)) + fnet3(4) + f4(3)*Si34(1,0) + f4(4)*Si34(1,1);
f3(5) = YEB*f4(2) + fnet3(5) + f4(3)*Si34(2,0) + f4(4)*Si34(2,1);

f2(0) = f3(2) + fnet2(0);
f2(1) = fnet2(1) + f3(0)*Si23(1,0) + f3(1)*Si23(1,1);
f2(2) = fnet2(2) + f3(0)*Si23(2,0) + f3(1)*Si23(2,1);
f2(3) = f3(5) + fnet2(3);
f2(4) = fnet2(4) + f3(3)*Si23(1,0) + f3(4)*Si23(1,1) - ZHR*f3(0)*Si23(2,0) - ZHR*f3(1)*Si23(2,1);
f2(5) = fnet2(5) + ZHR*f3(0)*Si23(1,0) + ZHR*f3(1)*Si23(1,1) + f3(3)*Si23(2,0) + f3(4)*Si23(2,1);

f1(0) = -f2(2) + fnet1(0);
f1(1) = fnet1(1) + f2(0)*Si12(1,0) + f2(1)*Si12(1,1);
f1(2) = fnet1(2) + f2(0)*Si12(2,0) + f2(1)*Si12(2,1);
f1(3) = -f2(5) + fnet1(3);
f1(4) = fnet1(4) + f2(3)*Si12(1,0) + f2(4)*Si12(1,1);
f1(5) = fnet1(5) + f2(3)*Si12(2,0) + f2(4)*Si12(2,1);

f0(0) = fnet0(0) + f1(0)*Si01(0,0) + f1(1)*Si01(0,1);
f0(1) = fnet0(1) + f1(0)*Si01(1,0) + f1(1)*Si01(1,1);
f0(2) = f1(2) + fnet0(2);
f0(3) = fnet0(3) + f1(3)*Si01(0,0) + f1(4)*Si01(0,1) - ZSFE*f1(0)*Si01(1,0) - ZSFE*f1(1)*Si01(1,1);
f0(4) = fnet0(4) + ZSFE*f1(0)*Si01(0,0) + ZSFE*f1(1)*Si01(0,1) + f1(3)*Si01(1,0) + f1(4)*Si01(1,1);
f0(5) = f1(5) + fnet0(5);


fext7(0) = fex7(0);
fext7(1) = fex7(1);
fext7(2) = fex7(2);
fext7(3) = fex7(3);
fext7(4) = fex7(4);
fext7(5) = fex7(5);

fext6(0) = fex6(0) + fext7(2);
fext6(1) = fex6(1) + fext7(0)*Si67(1,0) + fext7(1)*Si67(1,1);
fext6(2) = fex6(2) + fext7(0)*Si67(2,0) + fext7(1)*Si67(2,1);
fext6(3) = fex6(3) + fext7(5);
fext6(4) = fex6(4) + fext7(3)*Si67(1,0) + fext7(4)*Si67(1,1);
fext6(5) = fex6(5) + fext7(3)*Si67(2,0) + fext7(4)*Si67(2,1);

fext5(0) = fex5(0) - fext6(2);
fext5(1) = fex5(1) + fext6(0)*Si56(1,0) + fext6(1)*Si56(1,1);
fext5(2) = fex5(2) + fext6(0)*Si56(2,0) + fext6(1)*Si56(2,1);
fext5(3) = fex5(3) - fext6(5) - ZWFE*fext6(0)*Si56(1,0) - ZWFE*fext6(1)*Si56(1,1);
fext5(4) = fex5(4) - ZWFE*fext6(2) + fext6(3)*Si56(1,0) + fext6(4)*Si56(1,1);
fext5(5) = fex5(5) + fext6(3)*Si56(2,0) + fext6(4)*Si56(2,1);

fext4(0) = fex4(0) + fext5(2);
fext4(1) = fex4(1) + fext5(0)*Si45(1,0) + fext5(1)*Si45(1,1);
fext4(2) = fex4(2) + fext5(0)*Si45(2,0) + fext5(1)*Si45(2,1);
fext4(3) = fex4(3) + fext5(5) + YWR*fext5(0)*Si45(2,0) + YWR*fext5(1)*Si45(2,1);
fext4(4) = fex4(4) + fext5(3)*Si45(1,0) + fext5(4)*Si45(1,1) - ZWR*fext5(0)*Si45(2,0) - ZWR*fext5(1)*Si45(2,1);
fext4(5) = fex4(5) - YWR*fext5(2) + ZWR*fext5(0)*Si45(1,0) + ZWR*fext5(1)*Si45(1,1) + fext5(3)*Si45(2,0) + fext5(4)*Si45(2,1);

fext3(0) = fex3(0) - fext4(2);
fext3(1) = fex3(1) + fext4(0)*Si34(1,0) + fext4(1)*Si34(1,1);
fext3(2) = fex3(2) + fext4(0)*Si34(2,0) + fext4(1)*Si34(2,1);
fext3(3) = fex3(3) - fext4(5) + fext4(0)*(-(ZEB*Si34(1,0)) + YEB*Si34(2,0)) + fext4(1)*(-(ZEB*Si34(1,1)) + YEB*Si34(2,1));
fext3(4) = fex3(4) - ZEB*fext4(2) + fext4(3)*Si34(1,0) + fext4(4)*Si34(1,1);
fext3(5) = fex3(5) + YEB*fext4(2) + fext4(3)*Si34(2,0) + fext4(4)*Si34(2,1);

fext2(0) = fex2(0) + fext3(2);
fext2(1) = fex2(1) + fext3(0)*Si23(1,0) + fext3(1)*Si23(1,1);
fext2(2) = fex2(2) + fext3(0)*Si23(2,0) + fext3(1)*Si23(2,1);
fext2(3) = fex2(3) + fext3(5);
fext2(4) = fex2(4) + fext3(3)*Si23(1,0) + fext3(4)*Si23(1,1) - ZHR*fext3(0)*Si23(2,0) - ZHR*fext3(1)*Si23(2,1);
fext2(5) = fex2(5) + ZHR*fext3(0)*Si23(1,0) + ZHR*fext3(1)*Si23(1,1) + fext3(3)*Si23(2,0) + fext3(4)*Si23(2,1);

fext1(0) = fex1(0) - fext2(2);
fext1(1) = fex1(1) + fext2(0)*Si12(1,0) + fext2(1)*Si12(1,1);
fext1(2) = fex1(2) + fext2(0)*Si12(2,0) + fext2(1)*Si12(2,1);
fext1(3) = fex1(3) - fext2(5);
fext1(4) = fex1(4) + fext2(3)*Si12(1,0) + fext2(4)*Si12(1,1);
fext1(5) = fex1(5) + fext2(3)*Si12(2,0) + fext2(4)*Si12(2,1);

fext0(0) = fex0(0) + fext1(0)*Si01(0,0) + fext1(1)*Si01(0,1);
fext0(1) = fex0(1) + fext1(0)*Si01(1,0) + fext1(1)*Si01(1,1);
fext0(2) = fex0(2) + fext1(2);
fext0(3) = fex0(3) + fext1(3)*Si01(0,0) + fext1(4)*Si01(0,1) - ZSFE*fext1(0)*Si01(1,0) - ZSFE*fext1(1)*Si01(1,1);
fext0(4) = fex0(4) + ZSFE*fext1(0)*Si01(0,0) + ZSFE*fext1(1)*Si01(0,1) + fext1(3)*Si01(1,0) + fext1(4)*Si01(1,1);
fext0(5) = fex0(5) + fext1(5);


//// Acceleration vectors, joint torques

// force/torque of base in world coordinates
/*fbase(0) = f0(0)*Si00(0,0) + f0(1)*Si00(0,1) + f0(2)*Si00(0,2);
fbase(1) = f0(0)*Si00(1,0) + f0(1)*Si00(1,1) + f0(2)*Si00(1,2);
fbase(2) = f0(0)*Si00(2,0) + f0(1)*Si00(2,1) + f0(2)*Si00(2,2);
fbase(3) = f0(3)*Si00(0,0) + f0(4)*Si00(0,1) + f0(5)*Si00(0,2);
fbase(4) = f0(3)*Si00(1,0) + f0(4)*Si00(1,1) + f0(5)*Si00(1,2);
fbase(5) = f0(3)*Si00(2,0) + f0(4)*Si00(2,1) + f0(5)*Si00(2,2);*/

// inverse dynamics torques
u(0) = -uex_des(0) + f1(5) + fext1(5);
u(1) = -uex_des(1) + f2(5) + fext2(5);
u(2) = -uex_des(2) + f3(5) + fext3(5);
u(3) = -uex_des(3) + f4(5) + fext4(5);
u(4) = -uex_des(4) + f5(5) + fext5(5);
u(5) = -uex_des(5) + f6(5) + fext6(5);
u(6) = -uex_des(6) + f7(5) + fext7(5);

// torques due to external forces
/*qext(0) = -uex_des(0) + fext1(5);
qext(1) = -uex_des(1) + fext2(5);
qext(2) = -uex_des(2) + fext3(5);
qext(3) = -uex_des(3) + fext4(5);
qext(4) = -uex_des(4) + fext5(5);
qext(5) = -uex_des(5) + fext6(5);
qext(6) = -uex_des(6) + fext7(5);*/

Q.u = u;

}


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    
    static bool firsttime = true;
    static link link_param1[NDOF+1];
    static link link_param2[NDOF+1];
    static link link_param3[NDOF+1];
    const std::string filename1 = "LinkParameters1.cfg";
    const std::string filename2 = "LinkParameters2.cfg";
    const std::string filename3 = "LinkParameters3.cfg";

    
    if (firsttime) {
        load_link_params(filename1,link_param1); 
        load_link_params(filename2,link_param2); 
        load_link_params(filename3,link_param3); 
        firsttime = false;
    }    
    
    // Check the number of input arguments.
    if (nrhs != 4) {
        mexErrMsgTxt("First input firstthe link parameter file");
        mexErrMsgTxt("Then input q, qd, u separately to calculate qdd!");        
    }

    // Check type of input.
    int i;
    for (i = 1; i < nrhs; i++) {
      if (mxGetClassID(prhs[i]) != mxDOUBLE_CLASS) {
          mexErrMsgTxt("The inputs must be of type double!");
      }

    }

    int m = 7;
    int n = 1;
    int linkval = mxGetScalar(prhs[0]); 
    vec7 q = armaGetPr(prhs[1]);
    vec7 qd = armaGetPr(prhs[2]);
    vec7 qdd = armaGetPr(prhs[3]);

    joint Q(q,qd,qdd);
    
    if (linkval == 1)
        barrett_wam_inv_dynamics_ne(link_param1,Q);
    else if (linkval == 2)
        barrett_wam_inv_dynamics_ne(link_param2,Q);
    else if (linkval == 3)
        barrett_wam_inv_dynamics_ne(link_param3,Q);
    else
        mexErrMsgTxt("Input link value must be between [1,3]");

    // Create the output argument plhs[0] to return matrix
    plhs[0] = armaCreateMxMatrix(m, n);

    // Return the matrix as plhs[0] in Matlab  
    armaSetPr(plhs[0], Q.u);
    return;
}