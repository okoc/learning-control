% Function that performs trust region like weighting
% 
% Algorithm 4.1 taken from Nocedal's book on Numerical Optimization
% q_max : overall bound on the weights

function M = trust_region_weighting(M,A,B,K,uff,x0,x_current,x_last,ref)

xs = [x0,x_last];
e_current = x_current -ref;
e_last = x_last - ref;
q_max = 100;
q_min = 0.01;
N = size(e_current,2);
for i = 1:N-1
    % compute the actual reduction
    step_cost_cur = e_current(:,i)'*e_current(:,i);
    step_cost_last = e_last(:,i)'*e_last(:,i);
    % compute the predicted reduction
    e_pred_next = (A(:,:,i) + B(:,:,i)*K(:,:,i))*(xs(:,i)-ref(:,i)) + B(:,:,i)*uff(:,i);
    pred_err = e_pred_next' * e_pred_next;
    pred_red = step_cost_last - pred_err;
    rho = (step_cost_last - step_cost_cur) / pred_red;

    % Version ONE for covariances or penalty matrices Rs
    %{
    if rho < 1/4 && max(diag(M(:,:,i))) < q_max/4
        M(:,:,i) = M(:,:,i)*4;
    else
        if rho > 3/4 && min(diag(M(:,:,i))) > 2*q_min
            M(:,:,i) = M(:,:,i)/2;
        end
    end
    %}
    
    % Version TWO for weights Q
    %%{
    if rho < 1/4 && min(diag(M(:,:,i))) > 4*q_min
        M(:,:,i) = M(:,:,i)/2;
    else
        if rho > 3/4 && max(diag(M(:,:,i))) < q_max/2
            M(:,:,i) = M(:,:,i)*2;
        end
    end
    %}

end