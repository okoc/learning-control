% Sample nonlinear dynamics
% Condition first on nominal controls
% Then widen GP by conditioning on nearby random controls

function [gp] = sample_nonlin_dyn(num_traj,m,dt,t,x0,gp)

N = length(t);
n = length(x0);
l_v = 0.1; % smoothness of trajectories

% create M random trajectories
% feed them all to the GP (i.e. condition)
x = zeros(n,N+1);
x(:,1) = x0;

for l = 1:num_traj

    u = sample_traj(m,t,l_v,randn(n,1))';
    
    for i = 1:N

        data = [x(:,i);u(:,i)];
        for j = 1:n
            [mu,s2] = gp(j).predict(data);
            f = mu + sqrt(s2) * randn;
            gp(j) = gp(j).update(data,f);
            x(j,i+1) = x(j,i) + dt*f;
        end
    end

end
