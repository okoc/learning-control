% Lift the linearized dynamics around a reference trajectory
% H is the response to initial condition - also called observability mat.
function [F,Fc,H] = lift_dyn(Ad,Bd,varargin)

N = size(Ad,3);
assert(size(Ad,3) == size(Bd,3));
assert(size(Ad,2) == size(Ad,1));
assert(size(Ad,2) == size(Bd,1));
n = size(Bd,1);
m = size(Bd,2);
F = zeros(N*n,N*m);
H = zeros(N*n,N*n);
for i = 1:N
    vec_u = (i-1)*m + 1:i*m;
    vec_h = (i-1)*n + 1:i*n;
    L = eye(n);
    M = Bd(:,:,i);
    for j = i:N-1
        vec_x = (j-1)*n + 1:j*n;
        F(vec_x,vec_u) = M;
        H(vec_x,vec_h) = L;
        M = Ad(:,:,j+1) * M;
        L = Ad(:,:,j+1) * L;
    end
    vec_x = (N-1)*n + 1:N*n;
    F(vec_x,vec_u) = M;
    H(vec_x,vec_h) = L;
end


% if C is also given
if nargin == 3
    C = varargin{1};
    %p = size(C,1);
    Cs = repmat(C, 1, N);
    Cc = mat2cell(Cs, size(C,1), repmat(size(C,2),1,N));
    Cfull = blkdiag(Cc{:});
    Fc = Cfull * F;
else
    Fc = [];
end
% 
% % if feedback is also given
% if nargin == 4
%     K = varargin{2};
%     for i = 1:N
%         vec_u = (i-1)*m + 1:i*m;
%         L = eye(n);
%         M = Bd(:,:,i);
%         for j = i:N-1
%             vec_x = (j-1)*n + 1:j*n;
%             F_fb(vec_x,vec_u) = M;
%             H_fb(vec_x,vec_u) = L;
%             Mat = (Ad(:,:,j+1) - Bd(:,:,j+1) * K(:,:,j+1)); 
%             M = Mat * M;
%             L = Mat * L;
%         end
%         vec_x = (N-1)*n + 1:N*n;
%         F_fb(vec_x,vec_u) = M;
%         H_fb(vec_x,vec_u) = L;
%     end
% else
%     F_fb = []; 
%     H_fb = [];
% end