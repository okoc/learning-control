% Class implementing a basic Kalman filter
% assuming an LTI system

classdef KF < handle
    
    properties
          
        % Kalman filter covariance of the state
        P
        % state to be estimated
        x
        % A matrix of linear system
        A
        % B matrix transforming inputs to state
        B
        % Observation matrix
        C
        % process noise covariance
        Q
        % observation noise covariance
        R
        % flag for LTI systems
        LTI
        % index for LTV system
        t
    end
    
    methods
        
        %% Initialize variances
        % Note that Q, R are not time varying
        function obj = KF(Q,R,A,B,C,h,discretized)
            
            obj.t = 0;
            % initialize covariances
            obj.Q = Q; % process noise covariance
            obj.R = R; % obs. noise covariance
            obj.LTI = (length(size(A)) == 2);
            
            % get the model matrices
            if ~discretized && obj.LTI
                warning('LQR only discretizes LTI systems!');
                % get Ad, Bd matrices
                obj.discretizeMatrices(A,B,h);
            else
                % discrete matrices provided
                obj.A = A;
                obj.B = B;
            end
            obj.C = C;
            
            dimx = size(A,1);
            obj.P = eye(dimx);
            obj.x = zeros(dimx,1);
        end
        
        %% Get LTI discrete matrices for prediction
        function discretizeMatrices(obj,A,B,h)
            
            dimu = size(B,2);
            dimx = size(B,1);
            % trick to get discrete time versions
            Mat = [A, B; zeros(dimu, dimx + dimu)];
            MD = expm(h * Mat);
            obj.A = MD(1:dimx,1:dimx);
            obj.B = MD(1:dimx,dimx+1:end);
        end
        
        %% Check for observability in LTI systems
        % otherwise give an error
        function assertObservability(obj)
            
            if (obj.LTI)
                dimx = size(obj.B,1);
                dimy = size(obj.C,1);
                % construct controllability Kalman matrix
                K = zeros(dimx*dimy,dimx);
                for i = 0:dimx-1
                    K((dimu*i+1):dimu*(i+1),:) = (obj.A^i)*obj.C;
                end
                assert(rank(K) == dimx, 'System is not observable!');
            end
        end
        
        %% Initialize state
        function initState(obj,x0,P0)
            obj.t = 0;
            obj.x = x0;
            obj.P = P0;
        end
        
        %% predict state and covariance
        
        function predict(obj,u)
            if (obj.LTI)
                obj.predict_lti(u);
            else
                obj.t = obj.t + 1;
                obj.predict_ltv(u);
            end
        end
        
        function predict_lti(obj,u)
            
            obj.x = obj.A * obj.x + obj.B * u;
            obj.P = obj.A * obj.P * obj.A' + obj.Q;
        end
        
        function predict_ltv(obj,u)
            
            At = obj.A(:,:,obj.t);
            Bt = obj.B(:,:,obj.t);
            obj.x = At * obj.x + Bt * u;
            obj.P = At * obj.P * At' + obj.Q;
        end        
        
        %% update Kalman filter after observation
        
        function update(obj,y)
            
            % Innovation sequence
            Inno = y(:) - obj.C*obj.x;
            % Innovation (output/residual) covariance
            Theta = obj.C * obj.P * obj.C' + obj.R;
            % Optimal Kalman gain
            K = obj.P * obj.C' * inv(Theta); %#ok
            % update state/variance
            obj.P = obj.P - K * obj.C * obj.P;
            obj.x = obj.x + K * Inno;
        end
        
        %% Kalman smoother
        % Rauch–Tung–Striebel (RTS) smoother
        % class must be initialized and initState must be run before
        % TODO: only works for LTV!
        function [X,V] = smooth(obj,Y,U)            
            
            dimx = length(obj.x);
            N = size(Y,2);
            X = zeros(length(obj.x),N);
            X_pred = zeros(length(obj.x),N-1);
            V = zeros(dimx,dimx,N);
            V_pred = zeros(dimx,dimx,N-1);
            num_iter = 1;
            
            for iter = 1:num_iter
                % forward pass
                for i = 1:N-1
                    obj.update(Y(:,i));
                    X(:,i) = obj.x;
                    V(:,:,i) = obj.P;
                    obj.predict(U(:,i));
                    X_pred(:,i) = obj.x;
                    V_pred(:,:,i) = obj.P;
                end
                obj.update(Y(:,N));
                X(:,N) = obj.x;
                V(:,:,N) = obj.P;
                % backward pass
                for i = N-1:-1:1
                    % Rauch recursion 
                    H = V(:,:,i) * obj.A(:,:,i)' * pinv(V_pred(:,:,i));
                    X(:,i) = X(:,i) + H * (X(:,i+1) - X_pred(:,i));
                    V(:,:,i) = V(:,:,i) + H * (V(:,:,i+1) - V_pred(:,:,i)) * H';
                    %M = V(:,:,i) * obj.A(:,:,i)';
                    %X(:,i) = X(:,i) + M * (V_pred(:,:,i)\(X(:,i+1) - X_pred(:,i)));
                    %Ht = V_pred(:,:,i) \ (obj.A(:,:,i) * V(:,:,i));
                    %V(:,:,i) = V(:,:,i) + M * (V_pred(:,:,i)\(V(:,:,i+1) - V_pred(:,:,i))) * Ht;
                end
                obj.initState(X(:,1),V(:,:,1));
            end
            
        end
        
    end
    
end