% Sample LTV dynamic system matrices in phase space
% drawn from multivariate normal distributions
% with squared exponential kernel based covariance matrix
%
function [As,Bs] = sample_ltv_dyn(n,m,t,uncorr)

dim_total = n*n + n*m;
N = length(t);

if uncorr
    As = randn(n,n,N); % Nominal system
    Bs = randn(n,m,N);
    return;
end

% correlation structure
l = rand(1,dim_total);
l_var = 0.01 * rand(1,dim_total);
s = rand(1,dim_total);
s_var = 0.01 * rand(1,dim_total);
sn = 0.1 * rand(1,dim_total); %noise
sn_var = 0.01 * rand(1,dim_total);

% sample each dimension of dynamics model matrices
hp.type = 'squared exponential ard';
dyns = zeros(N,dim_total);
for i = 1:dim_total
    hp.l = l(i) + sqrt(l_var(i)) * randn;
    hp.scale = s(i) + sqrt(s_var(i)) * randn; 
    hp.noise.var = sn(i) + sqrt(sn_var(i)) * randn;
    gp(i) = GP(hp,[],[],false,[],[]);
    [mu,Sigma] = gp(i).predict_mesh(t);
    [U,S] = eig(Sigma);
    dyns(:,i) = mu(:) + U * sqrt(max(0,real(S))) * randn(N,1);
end

As = zeros(n,n,N);
Bs = zeros(n,m,N);
% arrange dyns into A,B matrices
for i = 1:n
    for j = 1:n
        As(i,j,:) = dyns(:,(i-1)*n + j);
    end
    for j = 1:m
        Bs(i,j,:) = dyns(:,n*n + (i-1)*m + j);
    end
end