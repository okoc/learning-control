% ILC for Barrett WAM

clc; clear; close all; clear functions; rng(1);
recursive = true;
cautious = true;
adaptive = true;
ic_variations = true;
dyn.use_cpp = true;
dyn.nom = 2;
dyn.act = 1;
[wam,PD,Q0] = init_wam(dyn); % set to false to use .m files for dynamics
n = 14; m = 7; ndof = 7;
dt = 0.002;
prec_d = 1e6; % discrete precision matrix weight
num_exp = 1;
num_iter = 10;
window_size = 10; % only keep last iterations data, forget the rest
gamma = 0.01; % covariance of IC variations
Qinit = Q0;
experiment = cell(1,num_exp);
Q = eye(n);
R = 0.05 * eye(m);
C = wam.SIM.C;

for j = 1:num_exp

    % Generate inputs for a desired trajectory
    %rng(j);
    disp(['Experiment ', int2str(j)]);
    % generate a ref and inv dynamics commands here
    [t,ref,p] = lookup_rand_strike(Q0(1:7),1,1);
    u_idyn = gen_invdyn_inputs(wam,t,ref);
    uff = u_idyn;
    ref = ref(1:n,:);
    ref_init = ref;
    N = size(ref,2);
    lambdas = 0.8*ones(1,N); % forgetting factor for adaptation
    sigmas_n = ones(1,N); % noise covariance for each step
    % initialize model
    [Ad,Bd,Ac,Bc] = wam.linearize(ref,uff);
    % load initial LQR
    [K,P] = gen_lqr_fb(wam,Q,R,Ad,Bd);
    %Qs = zeros(n,n,N);
    %Qs(:,:,end) = Q;
    Qs = repmat(Q,1,1,N);
    Rs = repmat(R,1,1,N);

    %K = repmat(PD,1,1,length(t));
    %LQR = load('../config/LQR4.txt');
    %K = repmat(LQR,1,1,length(t));

    % Evolve system dynamics and animate the robot arm

    X = zeros(n,N,num_iter+1);
    U = zeros(m,N,num_iter+1);
    % observe with feedback
    [X(:,:,1),ufb] = evolve(wam,uff,K,ref,Q0);
    % add performance 
    U(:,:,1) = uff + ufb;

    % for adaptation using LBR
    if adaptive || cautious
        Sigmas_err = zeros(n,n,N);
        Sigmas_d = zeros(n*(n+m),n*(n+m),N);
        Gammas_d = zeros(n*(n+m),n*(n+m),N);
        Sigmas_cts = zeros(ndof*(n+m),ndof*(n+m),N);
        Gammas_cts = zeros(ndof*(n+m),ndof*(n+m),N);
        prec = dt*dt*prec_d;
        for i = 1:N            
            Sigmas_d(:,:,i) = (1/prec_d) * eye(n*(n+m));
            Sigmas_cts(:,:,i) = (1/prec) * eye(ndof*(n+m));
            Gammas_d(:,:,i) = prec_d * eye(n*(n+m));
            Gammas_cts(:,:,i) = prec * eye(ndof*(n+m));
        end
    end
    %R = 1e-2 * eye(m); % change R for learning
    
    % linearize around act. traj
    %[Ad,Bd] = wam.linearize(X(:,:,1),U(:,:,1));
    
    for k = 1:N
        e = X(:,k,1) - ref_init(:,k);
        Sigmas_err(:,:,k) = e*e';
    end
    
    for i = 1:num_iter
        % get next inputs
        %[Ad,Bd] = wam.linearize(X(:,:,i),uff);
        fprintf('Iteration %d\n', i);
        if recursive
            if cautious
                [u_ilc,K] = recursive_cautious_ilc(Ad, Bd, C, Qs, Rs, Sigmas_d, X(:,:,i)-ref, ufb);
            else
                u_ilc = recursive_ilc(Ad, Bd, C, P, Qs, Rs, X(:,:,i)-ref);
            end
            uff = uff + u_ilc;
        else
            batch_update.down_sample = 10;
            batch_update.cur_iter = true;
            batch_update.model = true;
            batch_update.linearize_around_ref = false; % lin. around reference not last traj.
            batch_update.linearize_each_iter = false;
            batch_update.goal_ilc = false;
            [uff,u_ilc2] = ilc_direct_wam(wam,X(:,:,i),ufb,ref,uff,batch_update);
        end
       
        % evolve system with feedback
        if ic_variations
            Qinit(1:7) = Qinit(1:7) + sqrt(gamma) * randn(7,1);
            % adapt ref
            %%{
            qf = p(1:7);
            qfdot = p(8:14);
            T = p(15);            
            q0dot = zeros(7,1);
            ref = calc_hit(dt,Qinit(1:7),q0dot,qf,qfdot,T);
            uff = uff - u_idyn;
            u_idyn = gen_invdyn_inputs(wam,t,ref);
            ref = ref(1:n,:);
            uff = uff + u_idyn;
            %}
        end
        [X(:,:,i+1),ufb] = evolve(wam,uff,K,ref,Qinit);
        U(:,:,i+1) = uff + ufb;
        
        % LBR to adapt linearized model
        if adaptive
            du = U(:,:,i+1) - U(:,:,i);
            de = X(:,:,i+1) - X(:,:,i);
            %[Ad,Bd,Sigmas_d,Gammas_d] = est_recursive_ltv(Ad,Bd,du,de,1.0,lambda,Sigmas_d,Gammas_d);
            %%{
            [Ac,Bc,Sigmas_cts,Gammas_cts] = est_robot_cts_ltv(Ac,Bc,du,de,dt,...
                                                              sigmas_n./(dt*dt),lambdas,Sigmas_cts,Gammas_cts);
            if i > window_size
                du = U(:,:,i+1-window_size,j) - U(:,:,i-window_size,j);
                de = X(:,:,i+1-window_size,j) - X(:,:,i-window_size,j);
                [Ac,Bc,Sigmas_cts,Gammas_cts] = est_robot_cts_ltv(Ac,Bc,du,de,dt,...
                                                              -sigmas_n./(dt*dt),lambdas,Sigmas_cts,Gammas_cts);
            end
            for k = 1:N
                Sigmas_d(:,:,k) = zeros(n*(n+m));
                Bd(:,:,k) = [zeros(ndof);Bc(:,:,k)];
                Ad(:,:,k) = [zeros(ndof), eye(ndof) ;Ac(:,:,k)];
            end
            [Ad,Bd] = discretize_dyn(Ad,Bd,dt);
            idx = 1:2*ndof*(2*ndof+m);
            idx = reshape(idx,ndof,2*(2*ndof+m));
            idx_cts = idx(:,2:2:end);
            idx_cts = idx_cts(:);
            for k = 1:N
                Sigmas_d(idx_cts,idx_cts,k) = dt*dt*Sigmas_cts(:,:,k);
            end
            %}
            %[Ad,Bd,Sigmas] = broyden_est_mats(Ad,Bd,du,de,Sigmas);
            %{
            for k = 1:N
                %e = X(:,k,i+1) - ref_init(:,k);
                Sigmas_err(:,:,k) = (i*Sigmas_err(:,:,k) + de(:,k)*de(:,k)')/(i+1);
                lambdas(k) = 10*max(eig((Sigmas_err(:,:,k))));
            end
            %}
        end
        
    end
    experiment{j}.u = U;
    experiment{j}.x = X;
    experiment{j}.r = ref;
end

% Plot the controls and animate the robot arm
wam.plot_inputs(U);
wam.plot_outputs(X,ref,1);
wam.show_experiment(experiment);