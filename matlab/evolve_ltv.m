% Function that evolves an LTV system dynamics

% LQR-calculated K has to be in error-feedback form!
function [y,u,ufb] = evolve_ltv(Ad,Bd,K,C,uff,ref,s2,x0)
    
    noise = false;
    if trace(s2) ~= 0
        std_dev = chol(s2);
        noise = true;
    end
    N = length(ref);
    n = length(x0);
    x = zeros(n,N+1);
    ufb = zeros(size(uff,1),N);
    u = zeros(size(uff,1),N);
    x(:,1) = x0;
    y(:,1) = C * x(:,1);
    for i = 1:N
        ufb(:,i) = K(:,:,i) * x(:,i);
        %ufb(:,i) = K(:,:,i) * (x(:,i)-s(:,i));
        u(:,i) = uff(:,i) + ufb(:,i);
        x(:,i+1) = Ad(:,:,i) * x(:,i) + Bd(:,:,i) * u(:,i);
        y(:,i+1) = C * x(:,i+1);
        if noise
            y(:,i+1) = y(:,i+1) + std_dev * randn(n,1);
        end
    end
    y = y(:,2:end);
end