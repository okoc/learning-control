% Test Broydens method on top of (cautious) LQR

clc; clear; close all; rng(5);
% track trajectory using lqr
% update models (A,B matrices) using Broydens method
% update models with LBR
% compare with LBR + ILC
% compare with not-updated LQR

k = 50; % number of experiments
n = 2; % dim_x
m = 2; % dim_u
p = 2; % dim_y
C = eye(p,n);
T = 1.0; % final time
N = 50;
dt = T/N; % discretization
t = dt*(0:N-1);
x0 = zeros(n,1);
s = sample_traj(p,t,0.2,x0)'; 
% for comparison with direct inversion, make sure it starts from x0
Q = eye(p);
Qf = Q; %10 * Q;
q = 6; % regularizer negative exponent
R = (10^(-q))*eye(m);
uncorr = false;
[Ac_act,Bc_act] = sample_ltv_dyn(n,m,t,uncorr);
[A_act,B_act] = discretize_dyn(Ac_act,Bc_act,dt);
A_nom = zeros(n,n,N);
B_nom = zeros(n,m,N);
Sigmas = zeros(n*(n+m),n*(n+m),N);
prec = 1e-4;
for i = 1:N
    Sigmas(:,:,i) = prec * eye(n*(n+m));
end
Sigmas_bro = Sigmas;
Sigmas_lbr = Sigmas;

for j = 1:N
    Sigma = Sigmas(:,:,j);
    pert = chol(Sigma)*randn(n*(n+m),1);
    A_pert = reshape(pert(1:n*n),n,n);
    B_pert = reshape(pert(n*n+1:end),n,m);
    A_nom(:,:,j) = A_act(:,:,j) + A_pert;
    B_nom(:,:,j) = B_act(:,:,j) + B_pert;
end

err_ilc = zeros(3,k+1); % recursive ilc + broyden/LBR
uff_ilc = zeros(m,N,k+1,3);
x_ilc = zeros(n,N,k+1,3);
ufull_ilc = zeros(m,N,k+1,3);

lqr = LQR(Q,R,Qf,A_nom,B_nom,C,N,dt,1);
[K,P] = lqr.computeFinHorizonLTV();
uff = recursive_ilc(A_nom,B_nom,C,P,Q,R,-s);
[x,u,~] = evolve_ltv(A_act,B_act,K,C,uff,s,0,x0);
A_bro = A_nom;
B_bro = B_nom;
A_lbr = A_nom;
B_lbr = B_nom;
x_ilc(:,:,1,:) = repmat(x,1,1,3);
uff_ilc(:,:,1,:) = repmat(uff,1,1,3);
ufull_ilc(:,:,1,1:3) = repmat(u,1,1,3);
err_ilc(1:3,1) = norm(x-s);

for i = 1:k

    % track with nominal model based recursive ILC
    e = x_ilc(:,:,i,1) - s;
    uff = recursive_ilc(A_nom,B_nom,C,P,Q,R,e);
    uff_ilc(:,:,i+1,1) = uff_ilc(:,:,i,1) + uff;
    %[K,uff] = lqr.computeFinHorizonTracking2(s,false);
    [x,~,~] = evolve_ltv(A_act,B_act,K,C,uff_ilc(:,:,i+1,1),s,0,x0);
    x_ilc(:,:,i+1,1) = x;
    err_ilc(1,i+1) = norm(x-s);
    
    % track with Broyden-updated model based recursive ILC 
    e = x_ilc(:,:,i,2) - s;
    [uff,K1] = recursive_cautious_ilc(A_bro,B_bro,C,Q,R,Sigmas_bro,e);
    uff_ilc(:,:,i+1,2) = uff_ilc(:,:,i,2) + uff;
    [x,u,~] = evolve_ltv(A_act,B_act,K1,C,uff_ilc(:,:,i+1,2),s,0,x0);
    ufull_ilc(:,:,i+1,2) = u;
    err_ilc(2,i+1) = norm(x-s);
    x_ilc(:,:,i+1,2) = x;
    du = ufull_ilc(:,:,i+1,2) - ufull_ilc(:,:,i,2);
    de = x_ilc(:,:,i+1,2) - x_ilc(:,:,i,2);
    [A_bro,B_bro,Sigmas_bro] = broyden_est_mats(A_bro,B_bro,du,de,Sigmas_bro);
    
    % track with LBR-updated model based recursive ILC
    e = x_ilc(:,:,i,3) - s;
    %uff = recursive_ilc(A_lbr,B_lbr,C,P,Q,R,e);
    [uff,K2] = recursive_cautious_ilc(A_lbr,B_lbr,C,Q,R,Sigmas_lbr,e);
    uff_ilc(:,:,i+1,3) = uff_ilc(:,:,i,3) + uff;
    %[x,u,~] = evolve_ltv(A_act,B_act,K,C,uff_ilc(:,:,i+1,3),s,0,x0);
    [x,u,~] = evolve_ltv(A_act,B_act,K2,C,uff_ilc(:,:,i+1,3),s,0,x0);
    ufull_ilc(:,:,i+1,3) = u;
    err_ilc(3,i+1) = norm(x-s);
    x_ilc(:,:,i+1,3) = x;
    du = ufull_ilc(:,:,i+1,3) - ufull_ilc(:,:,i,3);
    de = x_ilc(:,:,i+1,3) - x_ilc(:,:,i,3);
    s2 = 0.005 * err_ilc(3,i); % noise covariance in LBR model
    [A_lbr,B_lbr,Sigmas_lbr] = est_recursive_ltv(A_lbr,B_lbr,du,de,s2,Sigmas_lbr);
end

figure;
plot(1:k+1,err_ilc(2:3,:)');
legend('broyden ilc','lbr ilc');