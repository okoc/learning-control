% Checking for convergence
% Perturbing nominal matrix by a constant alpha times the minimum
% singular value of nominal matrix
%
% bounds are very conservative
% to observe under what conditions non-monotonicity / non-conv.
% arises, we need to check the properties of the traj.

clc; clear; close all; rng(4);
recursive = false;
cautious = false;
adaptive = false;
apply_fb = false;
current_iter = false; % added only if batch ilc is used
noise = false;
ic_variations = false;
check_mc = false;
alpha = 1000;
lambda = 1e-6; % damping parameter for pseudo-inverse
n = 2; % dim_x
m = 2; % dim_u
p = 2; % dim_y
T = 1.0; % final time
N = 50; % number of time steps
dt = T/N; % discretization
t = dt*(1:N);
prec = 1e6; % precision of LTV model matrices
sigma = 1e-3 * eye(p); % covariance of noise
gamma = 10; % covariance of initial condition variations
uncorr = false; % correlated nominal and actual dynamics
% observe errors
k = 41; % number of trials
l = 1; % number of experiments
X = zeros(p,N,k,l); % performance along trajectory for each trial
Uff = zeros(m,N,k,l); % ff inputs along trajectory for each trial
Utot = zeros(m,N,k,l); % total inputs
C = eye(p,n);
Q = C'*eye(p)*C;
R = lambda*eye(m);
x0 = zeros(n,1); % starting state
ref = zeros(n,N,l);

for j = 1:l % diff. experiments for convergence stats

    [A_nom,B_nom] = sample_ltv_dyn(n,m,t,uncorr);

    % Discretize and lift nominal dynamics
    [Ad_nom,Bd_nom] = discretize_dyn(A_nom,B_nom,dt);    
    [F_nom,Fc_nom] = lift_dyn(Ad_nom,Bd_nom,C);
    L = (Fc_nom'*Fc_nom + lambda*eye(m*N))\(Fc_nom');
    
    % Generate perturbation matrix and the actual dynamics
    thresh = alpha*min(svd(F_nom));
    [Ad_act,Bd_act,F_act,Fc_act] = perturb_ltv_dyn(Ad_nom,Bd_nom,C,thresh,uncorr,dt);
    
    % Reference trajectory is a draw from a Gaussian Process
    ref(:,:,j) = sample_traj(p,t,0.2,x0)';
    traj = ref(:,:,j);
    
    Qf = Q;
    lqr = LQR(Q,R,Qf,Ad_nom,Bd_nom,C,N,dt,1);
    [K,P] = lqr.computeFinHorizonLTV();
    if ~apply_fb
        K = zeros(m,n,N);
    end
    
    Proj = check_learn_stab(F_act,F_nom,L,K,traj,false);
    
    % precision of initial precision matrices
    Sigmas = zeros(n*(n+m),n*(n+m),N);
    for i = 1:N
        Sigmas(:,:,i) = (1/prec) * eye(n*(n+m));
    end
    
    if ~noise
        sigma = 0.0;
    end
    
    [X(:,:,1,j),Utot(:,:,1,j),ufb] = evolve_ltv(Ad_act,Bd_act,K,C,Uff(:,:,1,j),traj,sigma,x0);
    u_ci = zeros(m,N); % current iteration errors to feedback to feedforward

    for i = 1:k-1
        e = X(:,:,i,j) - traj; % we assume no noise for now
        
        % ESTIMATE COVARIANCE OF DISTURBANCE
        %[S,mu] = estimate_covar(X(:,:,1:i,j),F_nom,U(:,:,1:i,j),traj);
        %W = diag(diag(pinv(S))); % weights for ILC update

        % DIRECT/RECURSIVE ILC
        if recursive
            if cautious
                [u,K] = recursive_cautious_ilc(Ad_nom,Bd_nom,C,Q,R,Sigmas,e,ufb);
            else
                u = recursive_ilc(Ad_nom, Bd_nom, C, P, Q, R, e);
            end
            Uff(:,:,i+1,j) = Uff(:,:,i,j) + u;
        else
            %L = (Fc_nom'*W*Fc_nom + lambda*eye(m*N))\(Fc_nom'*W);
            uff_direct = -reshape(L*e(:),m,N);
            Uff(:,:,i+1,j) = Uff(:,:,i,j) + uff_direct;
            if current_iter
                for l = 1:N
                    u_ci(:,l) = K(:,:,l)*e(:,l);
                end
                Uff(:,:,i+1,j) = Uff(:,:,i+1,j) + u_ci;
            end
        end
        
        if check_mc
            if adaptive
                Proj = check_learn_stab(F_act,F_nom,L,K,traj,false);
            end
            if norm(Proj*e(:)) >= norm(e(:))
                fprintf('Iter %d: error will increase!\n', i);
            end
        end

        % CALCULATE ERROR
        if ic_variations
            x_init = x0 + sqrt(gamma) * randn(n,1);
        else
            x_init = x0;
        end
        
        [X(:,:,i+1,j),Utot(:,:,i+1,j),ufb] = evolve_ltv(Ad_act,Bd_act,K,C,Uff(:,:,i+1,j),traj,sigma,x_init);

        if adaptive
            du = Utot(:,:,i+1,j) - Utot(:,:,i,j);
            de = X(:,:,i+1,j) - X(:,:,i,j);
            %de = filtfilt2(de,0.40);
            %{
            plot(de');
            hold on;
            de = filtfilt2(de,0.40);
            plot(de');
            %}
            s2 = 0.005 * norm(de);
            %s2 = 0.01;
            [Ad_nom,Bd_nom,Sigmas] = est_recursive_ltv(Ad_nom,Bd_nom,du,de,s2,Sigmas);
            [F_nom,Fc_nom] = lift_dyn(Ad_nom,Bd_nom,C);
            L = (Fc_nom'*Fc_nom + lambda*eye(m*N))\(Fc_nom');
            if apply_fb && ~cautious
                lqr = LQR(Q,R,Qf,Ad_nom,Bd_nom,C,N,dt,1);
                [K,P] = lqr.computeFinHorizonLTV();
            end
        end
        
    end

end

% it will only plot first experiment trajectories
[hx,hu] = plot_performance(X,Uff,ref,0);