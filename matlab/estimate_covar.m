%% Estimate covariance of disturbance

function [S,mu] = estimate_covar(X,F,U,traj)

n = size(X,1);
m = size(U,1);
k = size(X,3);
N = size(X,2);
D = zeros(N*n,k);
S = eye(N*n);
mu = zeros(N*n,1);
for i = 1:k
    e = X(:,:,i) - traj;
    u = U(:,:,i);
    D(:,i) = e(:) - F*u(:);
    mu = mu + D(:,i);
end
mu = mu/k;

if k > 1
    for i = 1:k
        S = S + (D(:,i)-mu)*(D(:,i)-mu)';
    end
    S = S/(k-1);
end