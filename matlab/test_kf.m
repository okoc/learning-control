%% Test Kalman Filter on an LTV system

clc; clear; close all; rng(1);

%% Initialize LTV system dynamics
n = 2; % dim_x
m = 2; % dim_u
p = 1; % dim_ys
T = 1.0; % final time
N = 200; % num of traj. points
dt = T/N; % discretization
x0 = zeros(n,1);
t = dt * (1:N);
ref = sample_traj(p,t,0.2,x0); % sample references from Gaussian Process
[As,Bs] = sample_ltv_dyn(n,m,t,false); % sample LTV dynamics matrices from GP
[Ad,Bd] = discretize_dyn(As,Bs,dt); 
C = eye(p,n);

%% Evolve system with random inputs

eps = 0.01;
% process noise var
Q = zeros(n);
% observation noise var
R = eps*eye(p);
% sample random inputs
u = sample_traj(m,t,0.2,zeros(n,1))';
%simulate system
x = zeros(n,N+1);
x(:,1) = x0;
y = zeros(p,N+1);
y(:,1) = C*x(:,1) + chol(R)*randn(p,1);
for i = 1:N
    x(:,i+1) = Ad(:,:,i)*x(:,i) + Bd(:,:,i)*u(:,i);
    y(:,i+1) = C*x(:,i+1) + chol(R)*randn(p,1);
end

%% Apply Kalman Filter and compare with Filtfilt

% initialize KF
y_kalman = zeros(p,N+1);
filter = KF(Q,R,Ad,Bd,C,dt,true);
filter.initState(x0,100);

for i = 1:N
    filter.update(y(:,i));
    y_kalman(:,i) = C * filter.x;
    filter.predict(u(:,i));
end
filter.update(y(:,N+1));
y_kalman(:,N+1) = C * filter.x;

w_cut = 45/180;
y_filtfilt = filtfilt2(y,w_cut);

tic;
filter.initState(x0,100);
x_smooth = filter.smooth(y,u);
y_smooth = C*x_smooth;
fprintf('Kalman smoothing took %f sec.\n', toc);

RMS(1) = norm(y_kalman - C*x); 
RMS(2) = norm(y_filtfilt - C*x);
RMS(3) = norm(y_smooth - C*x);

%% Apply batch filtering = Kalman smoothing

% construct P batch
tic;
Rs = repmat(R, 1, N);
Rc = mat2cell(Rs, size(R,1), repmat(size(R,2),1,N));
Rfull = blkdiag(Rc{:});
ys = y(:,2:end);
Cs = repmat(C, 1, N);
Cc = mat2cell(Cs, size(C,1), repmat(size(C,2),1,N));
Cfull = blkdiag(Cc{:});
H = reshape(permute(Ad,[1 3 2]),N*n,n);
[F,Fc] = lift_dyn(Ad,Bd,C);
M = Cfull * H;
%x0hat = M'*((M*M' + Rfull)\(ys(:)-Fc*u(:)));
x0hat = pinv(M, 0.05) * (ys(:) - Fc*u(:));
yhats = M * x0hat + Fc*u(:);
% M = H * pinv(Cfull * H, 0.05);
% add = F*u(:) - M * Fc * u(:);
% xhats = M * ys(:) + add;
% yhats = Cfull * xhats;

fprintf('Batch inversion took %f sec.\n', toc);
y_batch_filt = reshape(yhats,p,N);
RMS(4) = norm(y_batch_filt - C*x(:,2:end));

disp(RMS);

figure;
plot([0,t], C*x, 'r');
hold on;
plot([0,t], y_kalman, 'b--');
plot([0,t], y_filtfilt, 'k*');
plot([0,t], y_smooth, 'g--');
plot(t, y_batch_filt);
legend('signal','kalman filter','filtfilt','kalman smoother','batch filt');