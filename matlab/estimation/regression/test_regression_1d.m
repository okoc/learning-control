% Comparing regression algorithms on 1D - 10D dimensions
% restricting data in cube [-1,1]

clc; clear; close all; rng(3);

xmax = 1;
xmin = -1;
N_train = 20; % number of training samples
N_test = 40; 
sigma_noise = 0.01;
x_train = xmin + (xmax-xmin) * rand(N_train,1);
x_test = xmin + (xmax-xmin) * rand(N_test,1);
x_test = sort(x_test);
%fnc = @(x) x.^2 + 3.*x + log(x.^4);
fnc = @(x) sin(2*x) + 3*x;
y_train = fnc(x_train) + sigma_noise * randn(N_train,1);
f_test = fnc(x_test);

%% least_squares
x_ls_train = [x_train,ones(N_train,1)];
x_ls_test = [x_test,ones(N_test,1)];
w_ls = x_ls_train\y_train;
y_test_ls = x_ls_test*w_ls;
% rms error on the test set
rms_ls = norm(f_test - y_test_ls)/sqrt(N_test)

%% local weighted regression
E = 4; % number of local models
c = linspace(xmin,xmax,E);
s2 = 0.05*ones(1,E);
w_lwr = zeros(2,E);
f_weight = @(x,c,s2) 1./(sqrt(2*pi*s2)) .* exp(-(1/(2*s2)).*(x-c).^2);
for i = 1:E
    W = f_weight(x_train,c(i),s2(i)); 
    w_lwr(:,i) =  (x_ls_train'*diag(W)*x_ls_train)\(x_ls_train'*diag(W)*y_train);
end

y_test_lwr = zeros(N_test,1);
weight_test = zeros(E,1);
for j = 1:N_test
    for i = 1:E
        weight_test(i) = f_weight(x_ls_test(j),c(i),s2(i));
    end
    weight_test = weight_test/sum(weight_test);
    y_test_lwr(j) = [x_test(j),1] * w_lwr * weight_test;
end
rms_lwr = norm(f_test - y_test_lwr)/sqrt(N_test)

%% LOESS

% using the same kernel f_weight
% fix the number of points
y_test_loess = zeros(N_test,1);
N_model = 4;
for j = 1:N_test
    % find N_model neighbors
    [B,I] = sort((x_test(j) - x_train).^2);
    x_keep = x_train(I(1:N_model));
    y_keep = y_train(I(1:N_model));
    x_keep_bar = [x_keep, ones(N_model,1)];
    % do weighted regression
    W = f_weight(x_keep,x_test(j),s2(1));
    weight_loess = (x_keep_bar'*diag(W)*x_keep_bar)\(x_keep_bar'*diag(W)*y_keep);
    y_test_loess(j) = [x_test(j),1] * weight_loess;
end 
rms_loess = norm(f_test - y_test_loess)/sqrt(N_test)

%% GP REGRESSION

hp.type = 'squared exponential iso';
hp.l = 0.8;
hp.scale = 5;
hp.noise.var = 0.01;
der = true;
gp = GP(hp,x_train',y_train,der,[],[]);
[gp_mean, gp_var] = gp.predict_mesh(x_test');
rms_gp = norm(gp_mean - f_test)/sqrt(N_test)

%% Plot the prediction on test set

scatter(x_train,y_train,'r');
hold on;
plot(x_test,f_test,'r');
%plot(x_test,y_test_ls,'b');
plot(x_test,y_test_loess,'b');
plot(x_test,y_test_lwr,'k');
plot(x_test,gp_mean,'--');
legend('training','function','LOESS','LWR','GP');