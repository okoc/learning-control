%% Test regression on Barrett WAM

clc; clear; close all; rng(1);
% Create sample q,qd,qdd,u pairs on a set of trajectories
dyn.use_cpp = true;
dyn.nom = 2;
dyn.act = 3;
[wam,PD,Q0] = init_wam(dyn); % false to use .m files for dynamics
cut_traj = 10; % cut 10 times
dim = 7;
N_traj = 10;
N_traj_test = 2;
N_traj_train = N_traj - N_traj_test;

Q = [];
Qd = [];
U = [];
Qdd = [];
for i = 1:N_traj_train
    [t,ref] = lookup_rand_strike(Q0(1:7),cut_traj,1);
    N = size(ref,2);
    u = gen_invdyn_inputs(wam,t,ref)';
    q = ref(1:dim,1:N)';
    qd = ref(dim+1:2*dim,1:N)';
    qdd = ref(2*dim+1:end,1:N)';
    Q = [Q;q];
    Qd = [Qd;qd];
    Qdd = [Qdd;qdd];
    U = [U;u];
end

X_train = [Q,Qd,U];
N_train = size(X_train,1);
y_train = Qdd;

Q_test = [];
Qd_test = [];
U_test = [];
Qdd_test = [];
for i = 1:N_traj_test
    [t,ref] = lookup_rand_strike(Q0(1:7),cut_traj,1);
    N = size(ref,2);
    u = gen_invdyn_inputs(wam,t,ref)';
    q = ref(1:dim,1:N)';
    qd = ref(dim+1:2*dim,1:N)';
    qdd = ref(2*dim+1:end,1:N)';
    Q_test = [Q_test;q];
    Qd_test = [Qd_test;qd];
    Qdd_test = [Qdd_test;qdd];
    U_test = [U_test;u];
end
X_test = [Q_test,Qd_test,U_test];
N_test = size(X_test,1);
y_test = Qdd_test;

%% LOESS

N_model = 25;
s2 = 6*ones(1,N_model);
f_weight = @(x,c,s2) 1/(sqrt(2*pi*s2)) * exp(-(1/(2*s2))*(x-c)*(x-c)');

% using the same kernel f_weight
% fix the number of points
y_test_loess = zeros(N_test,dim);
for j = 1:N_test
    % find N_model neighbors
    M = repmat(X_test(j,:),N_train,1) - X_train;
    [B,I] = sort(sum(M.^2,2));
    X_keep = X_train(I(1:N_model),:);
    y_keep = y_train(I(1:N_model),:);
    X_keep_bar = [X_keep, ones(N_model,1)];
    % do weighted regression
    for i = 1:N_model
        W(i) = f_weight(X_keep(i,:),X_test(i,:),s2(i));
    end
    weight_loess = (X_keep_bar'*diag(W)*X_keep_bar)\(X_keep_bar'*diag(W)*y_keep);
    y_test_loess(j,:) = [X_test(j,:),1] * weight_loess;
end 
rms_loess = norm(y_test_loess - y_test, 'fro')/sqrt(N_test*dim)

%% GP regression

hp.type = 'squared exponential ard';
hp.l = ones(1,3*dim);
hp.scale = 1;
hp.noise.var = 0.01;
der = true;
gp_mean = zeros(size(y_test));
for i = 1:dim
    gp(i) = GP(hp,X_train',y_train(:,i),true,[],[]);
    [gp_mean(:,i), gp_var] = gp(i).predict_mesh(X_test');
end
rms_gp = norm(gp_mean - y_test, 'fro')/sqrt(N_test*dim)

%% Plot performance

figure;
for i = 1:dim
    subplot(dim,1,i);
    plot(1:N_test,y_test(:,i),'--r',1:N_test,gp_mean(:,i),'b');
end