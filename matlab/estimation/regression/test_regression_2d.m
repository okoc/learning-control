%% Compare regression algorithms in 2D

clc; clear; close all; rng(1);

N_dim = 2;
N_train = 100;
N_test = 40;
sigma_noise = 0.01;
xmin = -2; %-5
xmax = 2; %5
x_train = xmin + (xmax-xmin)*rand(N_train,N_dim);
x_test = xmin + (xmax-xmin)*rand(N_test,N_dim);
fnc = @(x) rosen(x);
%fnc = @(x) braninmodif(x);
y_train = fnc(x_train) + sigma_noise * randn(N_train,1);
f_test = fnc(x_test);

%% GP regression

hp.type = 'squared exponential ard';
hp.l = [1.6, 2.7]; % for rosenbrock - 2d
%hp.l = [4.0, 4.0]; % for branin
%hp.l = [4, 3, 6] % rosenbrock - 3d
hp.scale = 1000;
hp.noise.var = 0.01;
der = true;
gp = GP(hp,x_train',y_train,true,[],[]);
[gp_mean, gp_var] = gp.predict_mesh(x_test');
rms_gp = norm(gp_mean - f_test)/sqrt(N_test)

%% LOESS

N_model = 5;
s2 = 4*ones(1,N_model);
f_weight = @(x,c,s2) 1/(sqrt(2*pi*s2)) * exp(-(1/(2*s2))*(x-c)*(x-c)');

% using the same kernel f_weight
% fix the number of points
y_test_loess = zeros(N_test,1);
for j = 1:N_test
    % find N_model neighbors
    M = repmat(x_test(j,:),N_train,1) - x_train;
    [B,I] = sort(sum(M.^2,2));
    x_keep = x_train(I(1:N_model),:);
    y_keep = y_train(I(1:N_model));
    x_keep_bar = [x_keep, ones(N_model,1)];
    % do weighted regression
    for i = 1:N_model
        W(i) = f_weight(x_keep(i,:),x_test(i,:),s2(i));
    end
    weight_loess = (x_keep_bar'*diag(W)*x_keep_bar)\(x_keep_bar'*diag(W)*y_keep);
    y_test_loess(j) = [x_test(j,:),1] * weight_loess;
end 
rms_loess = norm(f_test - y_test_loess)/sqrt(N_test)

%% LWR
E = 3; % number of local models per dimension
x_train_bar = [x_train, ones(N_train,1)];
x_test_bar = [x_test, ones(N_test,1)];
v = linspace(xmin,xmax,E);
[x1,x2] = meshgrid(v,v);
c = [x1(:),x2(:)];
s2 = 0.2*ones(1,E^N_dim);
w_lwr = zeros(N_dim+1,E^N_dim);
f_weight = @(x,c,s2) 1/(sqrt(2*pi*s2))* exp(-(1/(2*s2))*(x-c)*(x-c)');
for i = 1:E^N_dim
    for j = 1:N_train
        W(j) = f_weight(x_train(j,:),c(i,:),s2(i)); 
    end
    w_lwr(:,i) =  (x_train_bar'*diag(W)*x_train_bar)\(x_train_bar'*diag(W)*y_train);
end

y_test_lwr = zeros(N_test,1);
weight_test = zeros(E^N_dim,1);
for j = 1:N_test
    for i = 1:E^N_dim
        weight_test(i) = f_weight(x_test(j,:),c(i,:),s2(i));
    end
    weight_test = weight_test/sum(weight_test);
    y_test_lwr(j) = x_test_bar(j,:) * w_lwr * weight_test;
end
rms_lwr = norm(f_test - y_test_lwr)/sqrt(N_test)