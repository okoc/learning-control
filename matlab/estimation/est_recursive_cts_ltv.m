%% Estimate A,B matrices and disturbances recursively
% Fast estimation with Linear Bayes Regression
%
% s2 is the variance of the noise - set to 1 if prior is properly scaled
% lambda is the forgetting factor - between 0 and 1

function [A,B,Sigmas,Gammas] = est_recursive_cts_ltv(A,B,du,de,dt,s2,lambda,Sigmas,Gammas)

N = size(A,3);
n = size(A,1);
m = size(B,2);
du = reshape(du,m,N);
de = reshape(de,n,N);
% variation in initial conditions is assumed to be zero!
de_last = zeros(n,1); 

for i = 1:N
    X = [de_last; du(:,i)];
    M = kron(X',eye(n));
    if i < N
        y = (de(:,i+1) - de_last)/(2*dt);
    else
        y = (de(:,i) - de(:,i-1))/dt;
    end
    mu = [A(:,:,i), B(:,:,i)];
    
    % new code
    gamma_new = (1/s2)*(M'*M) + lambda*Gammas(:,:,i);
    Sigmas(:,:,i) = inv(gamma_new);
    mu = Sigmas(:,:,i)*(lambda*Gammas(:,:,i)*mu(:) + (1/s2)*M'*y);
    Gammas(:,:,i) = gamma_new;
    
    A(:,:,i) = reshape(mu(1:n*n),n,n);
    B(:,:,i) = reshape(mu(n*n+1:end),n,m);
    
    de_last = de(:,i);
end