%% Estimate the covariance of discretized matrices

clc; clear; close all; rng(5);

%% Construct A,B and Ad,Bd matrices
n = 3;
m = 2;
ncoef = n*(2*n+m);
N = 10000;
dt = 0.002;
V = randn(ncoef);
V = (V + V')/2.0;
mean_cts = rand(ncoef,1);
Sigma_cts = V + ncoef*eye(ncoef);
Sigma_cts = Sigma_cts/ncoef;

A = zeros(2*n,2*n,N);
B = zeros(2*n,m,N);
for i = 1:N
    v = mean_cts + chol(Sigma_cts)' * randn(ncoef,1);
    A(n+1:end,:,i) = reshape(v(1:2*n*n),n,2*n);
    B(n+1:end,:,i) = reshape(v(2*n*n+1:end),n,m);
    %Ad(:,:,i) = eye(n) + dt*A(:,:,i);
    %Bd(:,:,i) = dt*B(:,:,i);
end
[Ad,Bd] = discretize_dyn(A,B,dt);

%% Estimate covariance of discrete matrices

mean_d = sum([Ad,Bd],3)/N;
var_d = zeros(2*ncoef,2*ncoef);

for i = 1:N
    v = [Ad(:,:,i),Bd(:,:,i)];
    diff = v(:) - mean_d(:);
    var_d = var_d + diff*diff';
end

var_d = var_d/(N-1);
Sigma_cts_est = var_d / (dt*dt);
idx = 1:2*n*(2*n+m);
idx = reshape(idx,n,2*(2*n+m));
idx_cts = idx(:,2:2:end);
idx_cts = idx_cts(:);
Sigma_cts_est = Sigma_cts_est(idx_cts,idx_cts);
diff_cov = Sigma_cts(:) - Sigma_cts_est(:);
norm(diff_cov)