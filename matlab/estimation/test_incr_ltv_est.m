%% Recursively estimate discrete LTV system matrices with 
% a) Linear Bayesian Regression (LBR)
% b) Broyden's method
% Batch versions (e.g. Structured Broyden's method) are also available
% for comparison
%
% assumption is that the trials are exactly the same
% for linear time varying system F with repeating disturbance d
% and random inputs
%
% Observations:
% putting random noise on disturbances affects Broyden quite a lot
% LBR less so,
% Filtfilt performs much more robustly than Kalman smoothing
% repeated Kalman smoothing does not help (or is unstable?)

clc; clear; close all; rng(1);
m = 2;
n = 2;
N = 20;
alpha = 10000;
eps = 1e-4; % noise covariance
num_model_elem = n*m*N*(N+1)/2;
dt = 1.0/N; % discretization
t = dt*(1:N);
uncorr = false;
C = eye(n);
Kmax = 50;
K = min(Kmax,N*m + N); % effective number of trials to estimate well = N*m
% add N if we are also estimating disturbances
% Discretize and lift nominal dynamics
[A_nom,B_nom] = sample_ltv_dyn(n,m,t,uncorr);
[Ad_nom,Bd_nom] = discretize_dyn(A_nom,B_nom,dt);
[F,Fc,H] = lift_dyn(Ad_nom,Bd_nom,C);
% Generate perturbation matrix and the actual dynamics
thresh = alpha*min(svd(F));
[Ad_act,Bd_act,Z,~] = perturb_ltv_dyn(Ad_nom,Bd_nom,C,thresh,uncorr,dt);

%% Make K experiments

%U = rand(N*m,K); 
%d = rand(N*n,1); % disturbance
U = zeros(N*m,K);
for i = 1:K
    inputs = sample_traj(m,t,0.2,randn(m,1))';
    U(:,i) = inputs(:);
end
dist = sample_traj(n,t,0.2,randn(n,1))';
d = dist(:);
D = repmat(d,1,K);
Dn = repmat(d,1,K) + sqrt(eps) * randn(N*n,K);
X = Z*U + D;
Xn = Z*U + Dn; % errors

% Filtering the signals
%{
w = 6; % cutoff frequency
w_perc = w/floor(N/2);
% apply filtfilt or Kalman smoother
for i = 1:K
    x = reshape(X(:,i),n,N);
    x = filtfilt2(x,w_perc);
    Xn(:,i) = x(:);
end
%}

for i = 1:K-1
    dEn(:,i) = Xn(:,i+1) - Xn(:,i);
    dU(:,i) = U(:,i+1) - U(:,i);
end

% Smoothen with a (nominal) Kalman Filter
%{
Q = eps * eye(n);
R = eps * eye(n);
for i = 1:K-1
    dEn(:,i) = Xn(:,i+1) - Xn(:,i);
    dU(:,i) = U(:,i+1) - U(:,i);
    filter = KF(Q,R,Ad_nom,Bd_nom,C,0.0,true);
    filter.initState(dEn(1:n,i),eps); 
    de = reshape(dEn(:,i),n,N);
    du = reshape(dU(:,i),m,N);
    de_smooth = filter.smooth(de,du);
    dEn(:,i) = de_smooth(:);
    %{
    de_o = X(:,i+1) - X(:,i);
    de_o = reshape(de_o,n,N);
    de_filtfilt = filtfilt2(de_o,w_perc);
    plot(1:N, de,'-r', 1:N, de_smooth, '--b', 1:N, de_filtfilt, '^g', 1:N, de_o, '*k');
    %}
end
%}

%% Test Broyden's method
% minimizes the frobenius norm of the updates

Sigmas_bro = zeros(n*(n+m),n*(n+m),N);
prec = 0.2;
for i = 1:N
    Sigmas_bro(:,:,i) = (1/prec) * eye(n*(n+m));
end

Q = eps * eye(n);
R = eps * eye(n);
Fb = F;
Ab = Ad_nom;
Bb = Bd_nom;
err_norm_broyden(1) = norm(Z-Fb,'fro');
for i = 1:K-1
    %{
    dEn(:,i) = Xn(:,i+1) - Xn(:,i);
    dU(:,i) = U(:,i+1) - U(:,i);
    filter = KF(Q,R,Ab,Bb,C,0.0,true);
    filter.initState(dEn(1:n,i),eps); 
    de = reshape(dEn(:,i),n,N);
    du = reshape(dU(:,i),m,N);
    de_smooth = filter.smooth(de,du);
    dEn(:,i) = de_smooth(:);
    %}
    [Ab,Bb,Sigmas_bro] = broyden_est_mats(Ab,Bb,dU(:,i),dEn(:,i),Sigmas_bro);
    Fb = lift_dyn(Ab,Bb);
    %Fb = broyden_incr_ltv(Fb,dU(:,i),dEn(:,i),N);
    err_norm_broyden(i+1) = norm(Z-Fb,'fro');
end


%% Full Linear Bayes Regression
% proportional to number of samples that was invested in 
% acquiring Fest (e.g. during system identification)

Sigmas_lbr = zeros(n*(n+m),n*(n+m),N);
Prec_lbr = Sigmas_lbr;
prec = 0.4;
for i = 1:N
    Sigmas_lbr(:,:,i) = (1/prec) * eye(n*(n+m));
    Prec_lbr(:,:,i) = prec * eye(n*(n+m));
end

A_lbr = Ad_nom;
B_lbr = Bd_nom;
% for each trial
F_lbr = F; % prior model
err_norm_lbr(1) = norm(Z-F_lbr,'fro');
s2 = 1;
lambda = 0.01;
for i = 1:K-1
    %{
    dEn(:,i) = Xn(:,i+1) - Xn(:,i);
    dU(:,i) = U(:,i+1) - U(:,i);
    filter = KF(Q,R,As,Bs,C,0.0,true);
    filter.initState(dEn(1:n,i),eps); 
    de = reshape(dEn(:,i),n,N);
    du = reshape(dU(:,i),m,N);
    de_smooth = filter.smooth(de,du);
    dEn(:,i) = de_smooth(:);
    %}
    
    [A_lbr,B_lbr,Sigmas_lbr,Prec_lbr] = est_recursive_ltv(A_lbr,B_lbr,dU(:,i),dEn(:,i),...
                                            s2,lambda,Sigmas_lbr,Prec_lbr);
    F_lbr = lift_dyn(A_lbr,B_lbr);
    err_norm_lbr(i+1) = norm(Z-F_lbr,'fro');
    %diff_mats = ([A_lbr,B_lbr]- [Ad_act,Bd_act]);
    %err_norm_lbr(i+1) = sqrt(sum(sum(sum(diff_mats .* diff_mats))));
    %[F_lbr,Gamma,hp] = est_incr_ltv(F_lbr,Gamma,hp,dU(:,i),dEn(:,i),N);
    %err_norm_lbr(i+1) = norm(Z-F_lbr,'fro');
    %errInvNorm(i) = norm(pinv(F)-pinv(Fest));
end

% check with batch estimation
%{
F_batch = F;
err_norm_batch(1) = norm(Z-F_batch,'fro');
for i = 1:K-1
    F_batch = est_batch_ltv(dU(:,1:i),dEn(:,1:i),[n,m]);
    err_norm_batch(i+1) = norm(Z-F_batch);
end
%}

plot(1:K,err_norm_lbr,1:K,err_norm_broyden);
legend('lbr','broyden');