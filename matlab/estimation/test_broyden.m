%% Test Broyden's method on a linear problem

clc; clear; close all; rng(1);
m = 2;
n = 2;
N = 10;
dt = 1.0/N; % discretization
t = dt*(1:N);
%Z = randn(N*n,N*m);
%r = randn(N*n,1);
[A,B] = sample_ltv_dyn(n,m,t,false);
[Ad,Bd] = discretize_dyn(A,B,dt);
[Z,~,~] = lift_dyn(Ad,Bd,eye(n));
ref = sample_traj(n,t,0.2,zeros(n,1))'; 
r = ref(:);

%% Broyden's method

assert(n == m, 'for Broydens method we need a square system');
K = 100;
U(:,1) = randn(N*n,1);
E(:,1) = Z * U(:,1) - r;
F = eye(N*n); 
%F = Z + 0.1 * randn(N*n,N*n); 
beta = 1.0; % caution parameter
err_mat_norm = zeros(1,K);
err_control_norm = norm(E(:,1));

for i = 1:K
    % Quasi-Newton descent
    %%{
    %U(:,i+1) = randn(N*n,1);
    U(:,i+1) = U(:,i) - beta * (F \ E(:,i));
    % observe errors
    E(:,i+1) = Z * U(:,i+1) - r;
    %}
    % update model matrix
    de = E(:,i+1) - E(:,i);
    du = U(:,i+1) - U(:,i);
    % Broyden update / Generalized Secant Method
    % Good Broyden
    z = du;
    % Bad Broyden
    %z = F'*de;
    % Generalized Secant Method
    %{
    z = z / sqrt(du'*du);
    for j = 1:i-1
        x = U(:,j+1) - U(:,j);
        z = z - ((z'*x)/(x'*x))*x;
    end
    z = z/sqrt(z'*z);
    %}
    DF = ((de - F*du)*z')/(z'*du);
    F = F + DF;
    %condest(F);
    err_mat_norm(i) = norm(Z - F, 'fro');
    err_control_norm(i) = norm(E(:,i+1));
end

%err_mat_norm
%err_control_norm
plot(1:K,err_mat_norm,1:K,err_control_norm);
legend('matrix norm diff', 'traj deviation norm');

%% Conjugate gradient 
% A needs to be symmetric positive definite
%{
b = r;
A = Z + Z';
K = 10;
x = zeros(N*n,1);
r = A*x - b;
p = -r;
res_norm = zeros(K,1);
for i = 1:K
    a = (r'*r)/(p'*A*p);
    x = x + a*p;
    s = r'*r;
    r = r + a*A*p;
    b = (r'*r) / s;
    p = -r + b * p;
    res_norm(i) = norm(r);
end
%}
%plot(1:K,res_norm);