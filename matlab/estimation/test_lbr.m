%% Testing (Multivariate) Linear Bayes Regression

clc; clear; close all; rng(2);
% number of parameters
n = 20;
% number of data points each round (K rounds)
N = 20;

% prior precision matrix
Gamma = 100.0 * eye(n);
beta_nom = randn(n,1);
beta = beta_nom + sqrt(Gamma)\randn(n,1);
var_nom = rand(1);
var = var_nom + rand(1);
mu = beta_nom;
% prior inv gamma distribution values
a = 2; 
b = var_nom;

% draw some values
% make K experiments
K = 1000;
err_mean = zeros(K,1);
err_var = zeros(K,1);
log_det_inv_gamma = zeros(K,1);
Xs = [];
ys = [];
for i = 1:K
    X = rand(N,n);
    eps = sqrt(var) * randn(N,1);
    y = X * beta + eps;
    % calculate posterior
    hp.a = a; hp.b = b;
    s2 = var;
    [mu,Gamma,hp] = LBR(mu,Gamma,X,y,s2,1.0,hp);
    a = hp.a; b = hp.b;
    % calculate error
    err_mean(i) = norm(beta - mu,2);
    err_var(i) = norm((b/(a-1)) - var,2);
    % how big is the new mean estimate variance
    log_det_inv_gamma(i) = log(1/det(Gamma));
    % save for batch processing later
    Xs = [Xs; X];
    ys = [ys; y];
end

% batch regression for comparison
mu_batch = Xs \ ys;
err_batch = norm(mu_batch - beta,2)

plot(err_mean);
title('Error norm for beta estimate over iterations');

%% Test forgetting with LBR

% Imagine a ball with a model moving
% But try to update the state with LBR
% Forgetting should reduce the rms error at the end

clc; clear; close all; rng(2);

n = 2;
p = 1;
N = 100;
eps = 0.01;
A = rand(n) + eye(n)/2;
s2 = 0.01;
x_ball_init = randn(n,1);
x_ball = x_ball_init;
ys_ball = zeros(p,N);
C = [1, 0];
for i = 1:N
    x_ball = A*x_ball;
    ys_ball(:,i) = C*x_ball + eps * randn(p,1);
end

mu = x_ball_init;
Gamma = 100 * eye(n);
for i = 1:N
    [mu,Gamma] = LBR(mu,Gamma,C,ys_ball(:,i),s2,1.0,[]);
end
err_lbr = norm(mu - x_ball,2)

%% now compare with sliding window
mu = x_ball_init;
window_size = 6;
Gamma = 100 * eye(n);
for i = 1:N
    [mu,Gamma,hp] = LBR(mu,Gamma,C,ys_ball(:,i),s2,1.0,[]);
    if i > window_size
        % forget the first data point
        [mu,Gamma,hp] = LBR(mu,Gamma,C,ys_ball(:,i-window_size),-s2,1.0,[]);
    end
end
err_lbr_window = norm(mu - x_ball,2)

%% no compare with forgetting
mu = x_ball_init;
Gamma = 100 * eye(n);
lambda = 0.6; % forgetting factor
for i = 1:N
    [mu,Gamma] = LBR(mu,Gamma,C,ys_ball(:,i),s2,lambda,[]);
end
err_lbr_forget = norm(mu - x_ball,2)
    