%% Estimate robot dynamics as qdd = A[q;qd] + Bu
% Fast estimation with Linear Bayes Regression
% A is 7 x 14 matrix 
% B is 7 x 7 matrix
%
% s2 is the variance of the noise - set to 1 if prior is properly scaled
% lambda is the forgetting factor - between 0 and 1

function [A,B,Sigmas,Gammas] = est_robot_cts_ltv(A,B,du,de,dt,s2,lambdas,Sigmas,Gammas)

N = size(A,3);
n1 = size(A,1);
n2 = size(A,2);
assert(n1 == n2/2, 'A must be 7 x 14');
m = size(B,2);
assert(m == n1);
du = reshape(du,m,N);
de = reshape(de,n2,N);
% variation in initial conditions is assumed to be zero!
de_last = zeros(n2,1); 

for i = 1:N
    X = [de_last; du(:,i)];
    M = kron(X',eye(n1));
    if i < N
        y = (de(n1+1:end,i+1) - de_last(n1+1:end))/(2*dt); %qdd
    else
        y = (de(n1+1:end,i) - de(n1+1:end,i-1))/dt;
    end
    mu = [A(:,:,i), B(:,:,i)];
    
    % new code
    gamma_new = (1/s2(i))*(M'*M) + lambdas(i)*Gammas(:,:,i);
    Sigmas(:,:,i) = inv(gamma_new);
    mu = Sigmas(:,:,i)*(lambdas(i)*Gammas(:,:,i)*mu(:) + (1/s2(i))*M'*y);
    Gammas(:,:,i) = gamma_new;
    
    A(:,:,i) = reshape(mu(1:n1*n2),n1,n2);
    B(:,:,i) = reshape(mu(n1*n2+1:end),n1,n1);
    
    de_last = de(:,i);
end