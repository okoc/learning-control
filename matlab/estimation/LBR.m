% Linear Bayesian Regression
% updates mean and precision matrix instead of covariance
%
% If there are no hyperparameters then initial variance 
% is not probabilistic
%
% s2 is the variance of the noise - set to 1 if prior is properly scaled
% lambda is the forgetting factor - between 0 and 1
%
function [mu,Gamma,hp] = LBR(mu,Gamma,X,y,s2,lambda,hp)

    n = length(mu);
    % hyperparameters of the precision matrix prior
    if ~isempty(hp)
        hp.a = hp.a + n/2;
        hp.b = hp.b + 0.5 * (y'*y + mu'*Gamma*mu);
    end
    % Pseudoinverse seems to be more stable when there is no noise       
    %mu = mu + (X'*X + s2 * lambda * Gamma)\(X'*(y - X*mu));
    mu = mu + pinv(X'*X + s2 * lambda * Gamma,0.01)*(X'*(y - X*mu));    
    Gamma = (X'*X/s2 + lambda * Gamma);
    
    if ~isempty(hp)
        hp.b = hp.b - 0.5 * (mu'*Gamma*mu);
    end
    
end