% Broydens method for estimating A,B matrices
%
% Error has to be filtered
%
function [A,B,Sigmas] = broyden_est_mats(A,B,du,de,Sigmas)

N = size(A,3);
n = size(A,1);
m = size(B,2);
du = reshape(du,m,N);
de = reshape(de,n,N);
% variation in initial conditions is assumed to be zero!
de_last = zeros(n,1);
for i = 1:N
    
    Sigma = Sigmas(:,:,i);
    s = [de_last; du(:,i)];    
    % Broyden's good method
    z = s;
    % Broyden's bad method
    %z = [A(:,:,i), B(:,:,i)]'*de(:,i);
    Pz = eye(n*(n+m)) - kron((z*s')/(z'*s),eye(n));
    Sigmas(:,:,i) = Pz*Sigma*Pz;
    D = (de(:,i) - (A(:,:,i)*de_last + B(:,:,i)*du(:,i)))*z';
    D = D/(z'*s);
    
    % estimate Ai, Bi matrices
    A(:,:,i) = A(:,:,i) + D(:,1:n);
    B(:,:,i) = B(:,:,i) + D(:,n+1:end);
    de_last = de(:,i);
end


