%% Show that as forgetting factor goes to 0, we can get
% Broyden's method out of LBR

clc; clear; close all; rng(1);
m = 2;
n = 2;
N = 20;
alpha = 10000;
eps = 1e-4; % noise covariance
num_model_elem = n*m*N*(N+1)/2;
dt = 1.0/N; % discretization
t = dt*(1:N);
uncorr = false;
C = eye(n);
Kmax = 50;
K = min(Kmax,N*m + N); % effective number of trials to estimate well = N*m
% add N if we are also estimating disturbances
% Discretize and lift nominal dynamics
[A_nom,B_nom] = sample_ltv_dyn(n,m,t,uncorr);
[Ad_nom,Bd_nom] = discretize_dyn(A_nom,B_nom,dt);
[F,Fc,H] = lift_dyn(Ad_nom,Bd_nom,C);
% Generate perturbation matrix and the actual dynamics
thresh = alpha*min(svd(F));
[Ad_act,Bd_act,Z,~] = perturb_ltv_dyn(Ad_nom,Bd_nom,C,thresh,uncorr,dt);

%% Make K experiments

U = zeros(N*m,K);
for i = 1:K
    inputs = sample_traj(m,t,0.2,randn(m,1))';
    U(:,i) = inputs(:);
end
dist = sample_traj(n,t,0.2,randn(n,1))';
d = dist(:);
D = repmat(d,1,K);
Dn = repmat(d,1,K) + sqrt(eps) * randn(N*n,K);
X = Z*U + D;
Xn = Z*U + Dn; % errors

for i = 1:K-1
    dEn(:,i) = Xn(:,i+1) - Xn(:,i);
    dU(:,i) = U(:,i+1) - U(:,i);
end

%% Test with different forgetting factors
Sigmas_bro = zeros(n*(n+m),n*(n+m),N);
Sigmas_lbr = zeros(n*(n+m),n*(n+m),N);
Prec_lbr = Sigmas_lbr;
prec = 0.4;
for i = 1:N
    Sigmas_bro(:,:,i) = (1/prec) * eye(n*(n+m));
    Sigmas_lbr(:,:,i) = (1/prec) * eye(n*(n+m));
    Prec_lbr(:,:,i) = prec * eye(n*(n+m));
end
Sigmas_lbr0 = Sigmas_lbr;
Prec_lbr0 = Prec_lbr;

% for each trial
F_lbr = F; % prior model
F_bro = F;
A_bro = Ad_nom;
B_bro = Bd_nom;
A_lbr = Ad_nom;
B_lbr = Bd_nom;
err_norm_lbr(1) = norm(Z-F_lbr,'fro');
err_norm_broyden(1) = norm(Z-F_bro,'fro');
s2 = 1;
lambda = 1;
for i = 1:K-1    
    [A_bro,B_bro,Sigmas_bro] = broyden_est_mats(A_bro,B_bro,dU(:,i),dEn(:,i),Sigmas_bro);
    [A_lbr,B_lbr,Sigmas_lbr,Prec_lbr] = est_recursive_ltv(A_lbr,B_lbr,dU(:,i),dEn(:,i),...
                                            s2,lambda,Sigmas_lbr,Prec_lbr);
    F_lbr = lift_dyn(A_lbr,B_lbr);
    F_bro = lift_dyn(A_bro,B_bro);
    err_norm_broyden(i+1) = norm(Z-F_bro,'fro');
    err_norm_lbr(i+1) = norm(Z-F_lbr,'fro');
end

plot(1:K,err_norm_lbr,1:K,err_norm_broyden);
hold on;

lambdas = [0.8, 0.4, 0.01];
for j = 1:length(lambdas)
    F_lbr = F; % prior model
    A_lbr = Ad_nom;
    B_lbr = Bd_nom;
    Sigmas_lbr = Sigmas_lbr0;
    Prec_lbr = Prec_lbr0;
    err_norm_lbr_forget(1) = norm(Z-F_lbr,'fro');    
    for i = 1:K-1    
        [A_lbr,B_lbr,Sigmas_lbr,Prec_lbr] = est_recursive_ltv(A_lbr,B_lbr,dU(:,i),dEn(:,i),...
                                            s2,lambdas(j),Sigmas_lbr,Prec_lbr);
        F_lbr = lift_dyn(A_lbr,B_lbr);
        err_norm_lbr_forget(i+1) = norm(Z-F_lbr,'fro');
    end
    plot(1:K,err_norm_lbr_forget);
end
legend('lbr','broyden','\lambda = 0.8', '\lambda = 0.4', '\lambda = 0.01');
hold off;