%% Apply Broyden's method
% to estimate LTV block system matrices

% inputs u and e are the inputs and errors (outputs)
% where
%
% u is mN x 1,
% e is nN x 1,
%
% N = horizon size
% m = number of inputs
% n = number of outputs
% 
% Estimating the matrices in vectorized form
%
function F_est = broyden_incr_ltv(F_est,u,e,N)

    m = length(u)/N;
    n = length(e)/N;
    [Est,D,~] = strip(F_est,m,n,N);
    % estimate F
    M = kron(u',eye(n*N))*D;
    
    L = pinv(M);
    % this is the 'structured' sBroyden update
    P = eye(size(M,2)) - L*M;
    Est = P*Est + L*e;
    
    EstAddZero = D*Est;
    F_est = reshape(EstAddZero',n*N,m*N);

end

% Strip the previously estimated model matrix
function [Est,D,E] = strip(F_est,m,n,N)

    Vec = F_est(:);
    [D,E] = gen_duplicate_mat(N,n,m);
    Est = E*Vec;
    
end