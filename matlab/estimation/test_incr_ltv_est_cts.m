%% Recursively estimate continuous LTV system matrices with 
% a) Linear Bayesian Regression (LBR)
% b) Broyden's method
%
% See the discrete version of this script for more info and other
% comparisons.

clc; clear; close all; rng(2);
m = 2;
n = 2;
N = 50;
eps = 1e-6; % noise covariance
num_model_elem = n*m*N*(N+1)/2;
dt = 1.0/N; % discretization
t = dt*(1:N);
uncorr = false;
C = eye(n);
Kmax = 50;
K = min(Kmax,N*m + N); % effective number of trials to estimate well = N*m
% add N if we are also estimating disturbances
% Discretize and lift nominal dynamics
[A_nom,B_nom] = sample_ltv_dyn(n,m,t,uncorr);
[Ad_nom,Bd_nom] = discretize_dyn(A_nom,B_nom,dt);
% Generate perturbation matrix and the actual dynamics
thresh = 1.0;
[A_pert,B_pert] = sample_ltv_dyn(n,m,t,uncorr);
A_act = A_nom + thresh * A_pert;
B_act = B_nom + thresh * B_pert;
[Ad_act,Bd_act] = discretize_dyn(A_act,B_act,dt);
Z = lift_dyn(Ad_act,Bd_act);

%% Make K experiments

U = zeros(N*m,K);
for i = 1:K
    inputs = sample_traj(m,t,0.2,randn(m,1))'; 
    U(:,i) = inputs(:);
end
dist = sample_traj(n,t,0.2,randn(n,1))';
d = dist(:);
D = repmat(d,1,K);
Dn = repmat(d,1,K) + sqrt(eps) * randn(N*n,K);
X = Z*U + D;
Xn = Z*U + Dn; % errors


%% Filtering the signals

w = 10; % cutoff frequency
w_perc = w/floor(N/2);
% apply filtfilt or Kalman smoother
for i = 1:K
    x = reshape(X(:,i),n,N);
    x = filtfilt2(x,w_perc);
    Xn(:,i) = x(:);
end
% 
for i = 1:K-1
    dEn(:,i) = Xn(:,i+1) - Xn(:,i);
    dU(:,i) = U(:,i+1) - U(:,i);
end

%% Linear Bayes Regression (cts and discrete)

Sigmas_lbr_cts = zeros(n*(n+m),n*(n+m),N);
Prec_lbr_cts = Sigmas_lbr_cts;
Sigmas_lbr_d = zeros(n*(n+m),n*(n+m),N);
Prec_lbr_d = Sigmas_lbr_d;
prec_d = 0.1;
prec_cts = prec_d * (dt*dt);
for i = 1:N
    Sigmas_lbr_cts(:,:,i) = (1/prec_cts) * eye(n*(n+m));
    Sigmas_lbr_d(:,:,i) = (1/prec_d) * eye(n*(n+m));
    Prec_lbr_cts(:,:,i) = prec_cts * eye(n*(n+m));
    Prec_lbr_d(:,:,i) = prec_d * eye(n*(n+m));
end

As = Ad_nom;
Bs = Bd_nom;
Ac = A_nom;
Bc = B_nom;
diff_mats = ([As,Bs]- [Ad_act,Bd_act]);
err_norm_lbr_d(1) = sqrt(sum(sum(sum(diff_mats .* diff_mats))));
[Ac_d,Bc_d] = discretize_dyn(Ac,Bc,dt);
diff_mats = ([Ac_d,Bc_d] - [Ad_act,Bd_act]);
err_norm_lbr_cts(1) = sqrt(sum(sum(sum(diff_mats .* diff_mats))));

% for each trial
for i = 1:K-1
    s2 = 1.0;
    % estimate continuous matrices
    [Ac,Bc,Sigmas_lbr_cts,Prec_lbr_cts] = est_recursive_cts_ltv(Ac,Bc,dU(:,i),dEn(:,i),dt,s2/(dt*dt),Sigmas_lbr_cts,Prec_lbr_cts);
    % estimate discrete matrices
    [As,Bs,Sigmas_lbr_d,Prec_lbr_d] = est_recursive_ltv(As,Bs,dU(:,i),dEn(:,i),s2,Sigmas_lbr_d,Prec_lbr_d);
    diff_mats = ([As,Bs]- [Ad_act,Bd_act]);
    err_norm_lbr_d(i+1) = sqrt(sum(sum(sum(diff_mats .* diff_mats))));
    [Ac_d,Bc_d] = discretize_dyn(Ac,Bc,dt);
    diff_mats = ([Ac_d,Bc_d] - [Ad_act,Bd_act]);
    err_norm_lbr_cts(i+1) = sqrt(sum(sum(sum(diff_mats .* diff_mats))));
end

%% Compare the two methods
plot(1:K,err_norm_lbr_d,1:K,err_norm_lbr_cts);
legend('lbr-discrete','lbr-cts');