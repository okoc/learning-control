%% Recursive least squares

clc; clear; close all;

n = 5;
N = 10;
X = rand(N,n);
beta = rand(n,1);
eps = randn(N,1);
y = X * beta + eps;

% estimate beta first
beta_est = X \ y

% get a new data point
x_add = rand(1,n);
y_add = x_add * beta + randn(1);
X_new = [X; x_add];
beta_est_batch = X_new \ [y;y_add]

% apply recursive least squares
%{
A = inv(X'*X);
B = x_add'*x_add;
alpha = 1/(1 + x_add*A*x_add');
C = eye(n) - alpha*A*B;
delta = y_add * C * A * x_add(:);
beta_est_rls = C * beta_est + delta;
%}

%% Test with Kalman filter / update forwards

R = 1.0;
P = inv(X'*X);
C = x_add;
S = (C*P*C' + R);
K = P * C' / S;
beta_est_kf = beta_est + K*(y_add - C*beta_est)
assert(norm(beta_est_batch - beta_est_kf) < 1e-3);

%% Update backwards with KF (downgrading/forgetting)

x_back = X(1,:);
y_back = y(1);
C = x_back;
S = (C*P*C' - R);
K = P * C' / S;
beta_est_kf_back = beta_est + K*(y_back - C*beta_est);
P = P - K*S*K'; % variance grows

S = (C*P*C' + R);
K = P * C' / S;
beta_est_kf = beta_est_kf_back + K*(y_back - C*beta_est_kf_back)
assert(norm(beta_est_kf - beta_est) < 1e-3);