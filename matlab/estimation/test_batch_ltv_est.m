%% Estimate LTV system matrices with least squares

% assumption is that the trials are exactly the same
% for linear time varying system F
% and random inputs

clc; clear; close all; rng(2);
m = 2;
n = 2;
N = 5;
K = N*m; % effective number of trials to estimate well = N*m
B = rand(n,m,N);
A = rand(n,n,N);
Z = lift_dyn(A,B);
U = rand(N*m,K);
E = Z*U;

tic
F = est_batch_ltv(U,E,[n,m]);
toc

err_norm = norm(Z-F)
%err_inv_norm = norm(pinv(F)-pinv(Fest))

tic 
% another method that does not have any for loops 
%but doesnt consider block structure
[D,~] = gen_duplicate_mat(N,n,m);
M = kron(U',eye(n*N))*D;
est = pinv(M)*E(:);
est_add_zero = D*est;
F2 = reshape(est_add_zero,n*N,m*N);
toc

err_norm = norm(Z-F2)