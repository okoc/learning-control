%% Estimate A,B matrices and disturbances recursively
% Fast estimation with Linear Bayes Regression
%
% s2 is the variance of the noise - set to 1 if prior is properly scaled
% lambda is the forgetting factor - between 0 and 1

function [A,B,Sigmas,Prec] = est_recursive_ltv(A,B,du,de,s2,lambda,Sigmas,Prec)

assert(lambda >= 0);
N = size(A,3);
n = size(A,1);
m = size(B,2);
du = reshape(du,m,N);
de = reshape(de,n,N);
% variation in initial conditions is assumed to be zero!
de_last = zeros(n,1); 

for i = 1:N
    X = [de_last; du(:,i)];
    M = kron(X',eye(n));
    y = de(:,i);
    mu = [A(:,:,i), B(:,:,i)];
    %Sigmas(:,:,i) = Sigmas(:,:,i) + 0.001*norm(du(:,i))*eye(n*(n+m));
    
    % new code
    %{
    gamma_new = (1/s2)*(M'*M) + lambda*Prec(:,:,i);
    Sigmas(:,:,i) = inv(gamma_new);
    mu = Sigmas(:,:,i)*(lambda*Prec(:,:,i)*mu(:) + (1/s2)*M'*de(:,i));
    Prec(:,:,i) = gamma_new;
    %}
    
    % old code
    %%{
    gamma = Prec(:,:,i); %inv(Sigmas(:,:,i));
    [mu,gamma_new] = LBR(mu(:),gamma,M,y,s2,lambda,[]);
    Prec(:,:,i) = gamma_new;
    %Sigmas(:,:,i) = inv(gamma_new);
    %}
    
    A(:,:,i) = reshape(mu(1:n*n),n,n);
    B(:,:,i) = reshape(mu(n*n+1:end),n,m);
    
    de_last = de(:,i);
end