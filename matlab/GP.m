% Class implementing a Gaussian Process regression model

classdef GP
    
    properties          
        % Tolerance when taking inverses
        tol
        % Hyperparameters
        hp
        % Training data
        x,y
        % Covariance matrix between (training) data points
        cov
        % Kernel function (handle)
        kernel
        % Derivative of kernel 
        der_kern
        % x1,x2 second derivative of k(x1,x2)
        der2_kern
        % Calculate derivatives
        flag_der
        % Parametric function as the prior mean and also its derivative
        f_mean
        f_mean_der
        
    end
    
    methods (Access = public)
        
        %% Initialize data points, kernel structure and covariance matrix
        % f_mean is a parametric mean function
        function obj = GP(hp,xs,ys,der,f_prior,f_der_prior)
            
            obj.flag_der = der;
            obj.x = xs;
            obj.y = ys;
            obj.hp = hp;
            obj.tol = 0.01;
            obj = obj.set_kernel(hp);
            obj = obj.build_cov(xs);
            if ~isempty(f_prior)
                obj.f_mean = f_prior;
                obj.f_mean_der = f_der_prior;
            else
                n = size(xs,2);
                obj.f_mean = @(x) 0.0;
                obj.f_mean_der = @(x) zeros(n,1);
            end
            
        end
        
        % Estimate hyperparameters using maximum likelihood
        % TODO: is this working?
        function obj = fit_hp(obj,hp0)
             
            try
                addpath(genpath('../gpml-matlab-v3.1-2010-09-27'));
            catch
                error('GPML toolbox not found!');
            end
            
            covar = @covSEard;
            lik = @likGauss;
            inf = @infExact;
            mean = [];
            cov_hp_len = size(obj.x,1) + 1; % scale hp added
            hyp.mean = [];
            hyp.cov = [log(hp0.l);log(sqrt(hp0.scale))];
            hyp.lik = log(sqrt(hp0.noise.var));            
            Ncg = 1000; % 100 line search steps
            hyp = minimize(hyp, @gp, Ncg, ...
                              inf, mean, covar, lik, obj.x', obj.y);
                          
            % update hp field after hp estimation
            obj.hp.l = exp(hyp.cov(1:end-1));
            obj.hp.scale = exp(hyp.cov(end))^2;
            obj.hp.noise.var = exp(hyp.lik)^2;
            obj.set_kernel(obj.hp);
        end
        
        
        %% predict/update mean and variance at test point(s)
        function [mu,s2,mu_der,s2_der] = predict(obj,xstar)
            
            % calculate kernel vector k and store
            [vec,vec_der] = obj.cov_test_and_data(xstar);
            % create mu function 
            %mu = vec' * pinv(obj.cov, obj.tol) * obj.y;
            mu = obj.f_mean(xstar) + vec' * (obj.cov \ obj.y);
            % initial covar is 1
            kxx = obj.kernel(xstar,xstar);
            % subtract information gain
            %s2 = max(0,kxx - vec' * pinv(obj.cov, obj.tol) * vec);
            s2 = max(0,kxx - vec' * (obj.cov \ vec));
            
            if obj.flag_der
                %mu_der = vec_der' * pinv(obj.cov, obj.tol) * obj.y;
                mu_der = obj.f_mean_der(xstar);
                if ~isempty(obj.cov)
                    mu_der = mu_der + vec_der' * (obj.cov \ obj.y);
                end
                kxx_der = obj.der_kern(xstar,xstar);
                %s2_der = kxx_der - 2*vec_der' * pinv(obj.cov, obj.tol) * vec;
                s2_der = kxx_der - 2*vec_der' * (obj.cov \ vec);
            else
                mu_der = [];
                s2_der = [];
            end
            
            if isempty(mu)
                mu = 0;
            end
        end
        
        function [mu,s2] = predict_mesh(obj,mesh)
           
            % calculate kernel matrix K at mesh test points
            cov_mesh = obj.cov_mesh(mesh);
            m = size(mesh,2);
            n = length(obj.y);
            if n == 0
                mu = obj.f_mean(mesh);
                s2 = cov_mesh;
            else
                mat = zeros(n,m);
                for i = 1:m
                    vec = obj.cov_test_and_data(mesh(:,i));
                    mat(:,i) = vec;
                end
                mu = obj.f_mean(mesh) + mat' * (obj.cov \ obj.y);
                s2 = cov_mesh - mat' * (obj.cov \ mat);
            end
            s2 = (s2 + s2')/2; % to enforce symmetric matrix
        end
        
        % Mean function as a handle
        function mu = mean(obj,x)
            % calculate kernel vector k and store
            vec = obj.cov_test_and_data(x);
            % create mu function 
            mu = obj.f_mean(x) + vec' * (obj.cov \ obj.y);
        end
        
        % Variance as a handle
        function s2 = var(obj,x)
            % calculate kernel vector k and store
            vec = obj.cov_test_and_data(x);
            % initial covar is 1
            kxx = obj.kernel(x,x);
            % subtract information gain
            s2 = kxx - vec' * (obj.cov \ vec);
        end
        
        % Mean derivate as a handle
        % Mean derivative = derivative of mean
        function mu_der = mean_der(obj,x)
            % calculate kernel vector k and store
            [~,vec_der] = obj.cov_test_and_data(x);
            % create mu function 
            %mu_der = vec_der' * (pinv(obj.cov, obj.tol) * obj.y);
            mu_der = vec_der' * (obj.cov \ obj.y);
            if ~isempty(obj.f_mean_der(x))
                mu_der = mu_der + obj.f_mean_der(x);
            end
        end
        
        % Derivative of variance as a handle
        function s2_der = der_var(obj,x)
            % calculate kernel vector k and store
            [vec,vec_der] = obj.cov_test_and_data(x);
            kxx_der = obj.der_kern(x,x);
            s2_der = kxx_der - 2*vec_der' * (obj.cov \ vec);
        end
        
        % Variance of derivative as a handle
        % Variance of derivative =! derivative of variance!
        function der_s2 = var_der(obj,x)
            K = obj.der2_kern(x);
            [~,vec_der] = obj.cov_test_and_data(x);
            der_s2 = K - vec_der' * (obj.cov \ vec_der);
            
        end
        
        % update mean and variance
        function obj = update(obj,xnew,ynew)
            
            obj = obj.update_cov(xnew);
            obj.x = [obj.x, xnew];
            obj.y = [obj.y; ynew];
        end        
    end
    
    methods (Access = private)
        
        %% Kernel related methods
        
        % Kernel called by the ker_matrix, ker_matrix_iter and ker_vector
        % functions.
        % x1 - Vector 1
        % x2 - Vector 2 
        %
        % Sets up a Gaussian or linear kernel function depending on 
        % the type field and hyperparameters
        %
        % IMPORTANT: does not consider noise as part of kernel
        %
        function obj = set_kernel(obj,hp)

            L = hp.l;
            type = hp.type;
            s = hp.scale;
            switch type
                case 'squared exponential iso'
                    if length(L) == 1
                        obj.kernel = @(x1,x2) s * exp(-(norm(x1(:)-x2(:),2)^2)/(2*(L^2)));
                        if obj.flag_der
                            obj.der_kern = @(x1,x2) (1/L^2)*(x1(:)-x2(:))*obj.kernel(x1,x2);
                            obj.der2_kern = @(x1,x2) ...
                            (1/L^2)*(eye(size(x1)) - (1/L^2)*(x1(:)-x2(:) * (x1(:)-x2(:))'))*obj.kernel(x1,x2);
                        end
                    else
                        error('Lengthscale parameter should be scalar!');
                    end
                case 'linear iso'
                    if isempty(L)
                        obj.kernel = @(x1,x2) s * x1(:)'*x2(:);
                        if obj.flag_der
                            obj.der_kern = @(x1,x2) s * x1(:);
                            obj.der2_kern = @(x1,x2) s;
                        end
                    else
                        error('Lengthscale parameter should be empty!');
                    end
                case 'squared exponential ard'
                    InvGamma = diag(1./(L.^2));
                    obj.kernel = @(x1,x2) s * exp(-0.5*((x1(:)-x2(:))')*InvGamma*(x1(:)-x2(:)));
                    if obj.flag_der
                        obj.der_kern = @(x1,x2) InvGamma*(x1(:)-x2(:))*obj.kernel(x1,x2);
                        obj.der2_kern = @(x1,x2) ...
                            InvGamma*(eye(size(x1)) - (x1(:)-x2(:) * (x1(:)-x2(:))')*InvGamma)*obj.kernel(x1,x2);
                    end
                case 'linear ard'
                    InvGamma = diag(1./(L.^2));
                    obj.kernel = @(x1,x2) s*x1(:)'*InvGamma*x2(:);
                    if obj.flag_der
                        obj.der_kern = @(x1,x2) s*InvGamma*x1(:);
                        obj.der2_kern = @(x1,x2) s*InvGamma;
                    end
                otherwise
                    error('Unrecognized kernel type');
            end
        
        end        
        
        % Constructs the kernel matrix all at once.
        % Only used to generate test functions.
        % INPUTS: 
        % x - training data points
        % kernel - covariance function
        % OUTPUTS
        % out - matrix of size nxn, where n is the length of us
        function obj = build_cov(obj,x)

            len = size(x,2);
            Kmat = zeros(len,len);
            for i = 1:len
                for j = i:len
                    Kmat(i,j) = obj.kernel(x(:,i), x(:,j));
                end
            end
            obj.cov = Kmat + Kmat' - diag(diag(Kmat));
            obj.cov = obj.cov + obj.hp.noise.var * eye(len);
        end
        
        % Construct the kernel matrix iteratively
        % INPUTS: 
        % t - last time stage (up to horizon)
        % us - control law points 
        % fun - function handle for the kernel
        % mat - previous kernel matrix
        % OUTPUT
        % Kmat - updated kernel matrix at the previous points and current test pt.
        function obj = update_cov(obj,xstar)

            % updates iteratively
            k = obj.cov_test_and_data(xstar);
            Kmat = obj.cov;
            % since kernel matrix is symmetric 
            % simply update the last row with 
            % the last column
            Kmat(:,end+1) = k;
            kxx = obj.kernel(xstar,xstar) + obj.hp.noise.var;
            Kmat(end+1,:) = [k;kxx]';
            obj.cov = Kmat;
        end
        
        % construct the kernel vector - covariances of the test pt and previous pts
        % INPUTS: 
        % xstar - test point
        % x     - data points 
        % OUTPUT
        % cov - kernel column vector describing the covariances between
        %       the previous points and current test pt.
        %
        function [cov,cov_der] = cov_test_and_data(obj,xstar)

            n = size(obj.x,2);
            cov = zeros(n,1);
            for i = 1:n
                cov(i) = obj.kernel(obj.x(:,i),xstar);
            end
            if obj.flag_der
                dim = length(xstar);
                cov_der = zeros(n,dim);
                for i = 1:n
                    cov_der(i,:) = obj.der_kern(obj.x(:,i),xstar);
                end
            else
                cov_der = [];
            end
                
        
        end
        
        % Construct the kernel matrix between the 
        % mesh of test points
        %
        function cov = cov_mesh(obj,xs)
           
            len = size(xs,2);
            cov = zeros(len);
            for i = 1:len
                for j = i:len
                    cov(i,j) = obj.kernel(xs(:,i), xs(:,j));
                end
            end
            cov = cov + cov' - diag(diag(cov));
        end
        
        
    end

end