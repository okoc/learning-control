%% Test Gaussian Process (GP) regression

clc; clear; close all; rng(1);

%% Simple regression

%hp.type = 'linear iso'; 
hp.type = 'squared exponential ard';
%hp.l = []; 
hp.l = 0.25;
hp.scale = 1;
hp.noise.var = 0.001;

n = 20;
mu = 0;
x = randn(n,1);
l = hp.l;
s = hp.scale;
%kern = @(x1,x2) s * x1(:)'*x2(:);
kern = @(x1,x2) s * exp(-(norm(x1(:)-x2(:),2)^2)/(2*(l^2)));
Sigma = zeros(n);
for i = 1:n
    for j = i+1:n
        Sigma(i,j) = kern(x(i),x(j));
    end
end
Sigma = Sigma + Sigma' + s * eye(n);
% Sigma = ker_matrix(x,kern);
y = mu + chol(Sigma + hp.noise.var*eye(n)) * randn(n,1);
f_mean = @(x) x';
f_der_mean = @(x) 1;
%gp = GP(hp,x',y,false,[],[]);
gp = GP(hp,x',y,false,f_mean,f_der_mean);

meshsize = 100;
z = linspace(min(x) - 0.5, max(x) + 0.5, meshsize);
[m,s2] = gp.predict_mesh(z);
s2 = diag(s2);
% m = zeros(meshsize,1);
% s2 = zeros(meshsize,1);
% for i = 1:length(z)
%     [mean, var] = gp.predict(z(i));
%     m(i) = mean;
%     s2(i) = var;
% end

figure(1);
subplot(2,1,1);
title('GP Regression');
%set(gca, 'FontSize', 24);
f = [m+2*sqrt(s2); flip(m-2*sqrt(s2))];
fill([z';flip(z')],f,'b','FaceAlpha',0.3);
hold on; 
plot(z, m, 'LineWidth', 2, 'Color', 'r'); 
plot(x, y, '*', 'Color', 'k', 'MarkerSize', 8);
axis tight;
%axis([-1.9 1.9 -0.9 3.9])
%grid on
xlabel('input x');
ylabel('output y');
hold off;

%% Test derivatives
der = true;
gp = GP(hp,x',y,der,[],[]);
D_exact = zeros(meshsize,1);
s2_der = zeros(meshsize,1);
for i = 1:length(z)
    [mean, var, mean_der, der_var] = gp.predict(z(i));
    D_exact(i) = mean_der;
    s2_der(i) = der_var;
end
subplot(2,1,2);
%title('GP derivatives');
plot(z,D_exact,z,s2_der, 'LineWidth', 2);
m_diff = diff(m)./diff(z)'; m_diff(end+1) = m_diff(end);
hold on;
s2_diff = diff(s2)./diff(z)'; s2_diff(end+1) = s2_diff(end);
plot(z,m_diff,z,s2_diff, 'LineWidth', 2);
legend('mean der','var der','mean diff', 'var diff');
axis tight;
hold off;

%% Test in 2D
%{
n = 50;
mu = 0;
x = randn(2,n);
l = 0.3;
s = hp.scale;
%kern = @(x1,x2) s * x1(:)'*x2(:);
kern = @(x1,x2) s * exp(-(norm(x1(:)-x2(:),2)^2)/(2*(l^2)));
Sigma = zeros(n);
for i = 1:n
    for j = i+1:n
        Sigma(i,j) = kern(x(:,i),x(:,j));
    end
end
Sigma = Sigma + Sigma' + s * eye(n);

y = mu + chol(Sigma + hp.noise.var*eye(n)) * randn(n,1);
gp = GP(hp,x,y,true,[],[]);

meshsize = 50;
z1 = linspace(min(x(1,:)) - 0.2, max(x(1,:)) + 0.2, meshsize);
z2 = linspace(min(x(2,:)) - 0.2, max(x(2,:)) + 0.2, meshsize);
[X1,X2] = meshgrid(z1,z2);
X = [X1(:),X2(:)]';
[m,s2] = gp.predict_mesh(X);
s2 = diag(s2);
M = reshape(m,meshsize,meshsize);
S2 = reshape(s2,meshsize,meshsize);
figure;
mesh(X1,X2,M);
%}

%% Test derivatives 2D
%{
[D1M,D2M] = gradient(M);
D1M = D1M/(z1(2)-z1(1));
D2M = D2M/(z2(2)-z2(1));
[D1S2,D2S2] = gradient(S2);
D1S2 = D1S2/(z1(2)-z1(1));
D2S2 = D2S2/(z2(2)-z2(1));
D_num = [D1M(:),D2M(:)]';

% get analytical mean derivatives
D_exact = zeros(2,length(X));

for i = 1:length(X)
    [mean, var, mean_der, var_der] = gp.predict(X(:,i));
    D_exact(:,i) = mean_der;
end
% plot to compare
figure;
subplot(2,1,1);
plot(1:length(X),D_num(1,:),1:length(X),D_exact(1,:));
legend('num','exact');
subplot(2,1,2);
plot(1:length(X),D_num(2,:),1:length(X),D_exact(2,:));
legend('num','exact');
%}