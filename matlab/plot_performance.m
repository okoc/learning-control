% Plot performance of ILC

function [hx,hu] = plot_performance(X,U,refs,fin_cost_draw)

num_exp = size(X,4);
num_exp_u = size(U,4);
num_exp_r = size(refs,3);
iter_x = size(X,3);
iter_u = size(U,3);
N_x = size(X,2);
N_u = size(U,2);
n = size(X,1);
tr = 1;
Q_goal = blkdiag(zeros((N_x-tr)*n),eye(tr*n)); 
assert(num_exp == num_exp_r, 'Number of experiments must match!');
assert(num_exp == num_exp_u, 'Number of experiments must match!');
assert(N_u >= (N_x - 1), 'Number of steps must match!');
assert(iter_x == iter_u, 'Number of iterations must match!');
assert(size(refs,2) == N_x, 'Number of steps must match!');
assert(size(refs,1) == n, 'Dimensions must match!');
num_iter = iter_x;
m = size(U,1);
t_x = 1:N_x;
t_u = 1:N_u;
err_norm = zeros(num_iter,num_exp);
final_cost = zeros(num_iter,num_exp);
labels_x = cell(1,4);
labels_u = cell(1,3);

for i = 1:num_iter
    for j = 1:num_exp
        e = X(:,:,i,j) - refs(:,:,j);
        err_norm(i,j) = norm(e,'fro');
        final_cost(i,j) = norm(Q_goal*e(:));
    end
    labels_u{i} = ['trial ', int2str(i)];
    labels_x{i+1} = ['trial ', int2str(i)];
end
labels_x{1} = 'desired traj';

figure;
for i = 1:n
    subplot(n,2,2*i-1);
    ylabel(['x_',int2str(i)]);
    xlabel('t');
    hold on;
    plot(t_x,refs(i,:,1),'r--','LineWidth',2);
    for j = 2:num_iter
        hx(i,j) = plot(t_x,X(i,:,j,1));
    end
    legend(labels_x{1:4},'Location','northwest');
    hold off;
end

for i = 1:m
    subplot(n,2,2*i);
    ylabel(['u_',int2str(i)]);
    xlabel('t');
    hold on;
    for j = 2:num_iter
        hu(i,j) = plot(t_u,U(i,:,j,1));
    end
    %legend(labels_u,'Location','northwest');
    hold off;
end

figure;
if fin_cost_draw
    subplot(2,1,1);
end
if num_exp == 1
    plot(1:num_iter,err_norm(:,1));
else
    sigmas = std(err_norm,1,2);
    errorbar(mean(err_norm,2),sigmas);
end
ylabel('Error norm');
xlabel('Iterations');
title('Error norm vs trials');

if fin_cost_draw
    subplot(2,1,2);
    if num_exp == 1
        plot(1:num_iter,final_cost(:,1));
    else
        sigmas = std(final_cost,1,2);
        errorbar(mean(final_cost,2),sigmas);
    end
    ylabel('Final cost norm');
    xlabel('Iterations');
    title('Final cost norm vs trials');
end

end       
