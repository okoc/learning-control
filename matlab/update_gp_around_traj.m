% Update GP with last trajectory (states and inputs)
% If derive flag is turned ON
% Create (batch) lifted dynamics matrix around last traj
% by taking derivatives of the MEAN

function [gp,F,Ad_nom,Bd_nom] = update_gp_around_traj(gp,dt,x,u,derive)

n = size(x,1);
m = size(u,1);
N = size(x,2)-1;
for j = 1:n
    for i = 1:N
        f = (x(j,i+1) - x(j,i))/dt;
        gp(j) = gp(j).update([x(:,i);u(:,i)],f);
    end
end

if derive
    % calculate derivatives
    As = zeros(n,n,N);
    Bs = zeros(n,m,N);
    for i = 1:N
        data = [x(:,i);u(:,i)];
        for j = 1:n
            v = gp(j).mean_der(data)';
            As(j,:,i) = v(1:n);
            Bs(j,:,i) = v(n+1:end);
        end
    end
    
    % Discretize and lift nominal dynamics
    [Ad_nom,Bd_nom] = discretize_dyn(As,Bs,dt);
    F = lift_dyn(Ad_nom,Bd_nom);
end
    