% Incremental pseudo-inverse of the errors by 
% running the disturbance rejection LQR on the errors

function uff = recursive_ilc(Ad, Bd, C, P, Q, R, e)

%tic;
m = size(Bd,2);
n = size(Ad,1);
p = size(C,1);
N = size(Ad,3);
e = reshape(e,p,N);
if ismatrix(Q)
    Q = repmat(Q,1,1,N);
end

if ismatrix(R)
    R = repmat(R,1,1,N);
end

% calculate optimal K and uff
uff = zeros(m,N);
nu = zeros(n,N+1);
for i = N:-1:1
    K(:,:,i) = -(R(:,:,i) + Bd(:,:,i)'*P(:,:,i+1)*Bd(:,:,i))\(Bd(:,:,i)'*P(:,:,i+1)*Ad(:,:,i));
    nu(:,i) = (Ad(:,:,i) + Bd(:,:,i)*K(:,:,i))'*nu(:,i+1) + Q(:,:,i)*e(:,i);
    uff(:,i) = -(R(:,:,i) + Bd(:,:,i)'*P(:,:,i+1)*Bd(:,:,i))\(Bd(:,:,i)'*nu(:,i));
end

%fprintf('LQR based inversion took %f sec.\n', toc);