%% Testing system identification of link parameters for WAM

clc; clear; close all; rng(3);

% Track with nominal inverse dynamics
% Form regressor by autodiff and update with LBR
% Track with inverse dynamics using updated parameters

num_iter = 5;
dyn.use_cpp = false;
dyn.nom = 2;
dyn.act = 3;
[wam,PD,Q0] = init_wam(dyn);
num_param = 10;
nonzero_idx =  logical([zeros(1,9), 1, 0, ones(1,70-11)]');
idx_identify = sum(nonzero_idx);
n = 7;
lambda = 1; % forgetting factor
theta_act = zeros(num_param,n);
theta = zeros(num_param,n);
for i = 1:n
    theta(1,i) = wam.PAR.links(i).m;
    theta(2:4,i) = wam.PAR.links(i).mcm(:)';
    theta(5:end,i) = wam.PAR.links(i).inertia([1 4 7 5 6 9]);
    
    theta_act(1,i) = wam.PAR_ACT.links(i).m;
    theta_act(2:4,i) = wam.PAR_ACT.links(i).mcm(:)';
    theta_act(5:end,i) = wam.PAR_ACT.links(i).inertia([1 4 7 5 6 9]);
end

prec = 0.1;
%Gamma = prec * eye(num_param*n,num_param*n); % precision matrix
Gamma = prec * eye(idx_identify);

% Generate inputs for a desired trajectory
% generate a ref and inv dynamics commands here
%PD = PD/10;
cut = 1;
[t,ref] = lookup_rand_strike(Q0(1:7),cut,1);
uff = gen_invdyn_inputs(wam,t,ref);
N = size(ref,2);
% compute initial LQR
%%{
% initialize model
[Ad,Bd,Ac,Bc] = wam.linearize(ref(1:2*n,:),uff);
Q = eye(2*n);
R = 0.05 * eye(n);
[K,P] = gen_lqr_fb(wam,Q,R,Ad,Bd);
%}
%K = PD;
X = zeros(2*n,N,num_iter);
U = zeros(n,N,num_iter);
Thetas = zeros(num_param,n,num_iter+1);
Thetas(:,:,1) = theta;
theta = theta(:);
[X(:,:,1),ufb] = evolve(wam,uff,K,ref(1:2*n,:),Q0);
U(:,:,1) = uff + ufb;

% BATCH SYSTEM IDENTIFICATION HERE
% [M,y] = form_regressor(X_regr,U_regr,theta,wam.PAR);
% %[M,y] = form_regressor(X(:,:,1),uff+ufb,theta,wam.PAR);
% %theta_est = M\y;
% theta_est = zeros(num_param*n,1);
% nonzero_idx = max(abs(M)) ~= 0;
% theta_est(nonzero_idx) = pinv(M(:,nonzero_idx),0.01)*y;
% theta_est = reshape(theta_est,num_param,n);

% RECURSIVE SYS ID.
for i = 1:num_iter
    
    % look at error
    err = X(:,:,i)-ref(1:2*n,:);
    fprintf('Norm of error: %f\n', norm(err,'fro'));
    
    % Form regressor by autodiff 
    [M,y] = form_regressor(X(:,:,i),U(:,:,i),theta,wam.PAR);
    %nonzero_idx = max(abs(M)) ~= 0;
    diff = theta(:) - theta_act(:);
    fprintf('Norm of parameter diff: %f\n',norm(diff(nonzero_idx)));
    M = M(:,nonzero_idx);

    
    % update parameters theta with LBR
    gamma_new = (M'*M) + lambda*Gamma;
    theta(nonzero_idx) = gamma_new\(lambda*Gamma*theta(nonzero_idx) + M'*y);
    Gamma = gamma_new;
    theta = reshape(theta,num_param,n);
    Thetas(:,:,i+1) = theta;
    
    % update wam parameters
    for j = 1:n
        wam.PAR.links(j).m = theta(1,j);
        wam.PAR.links(j).mcm = theta(2:4,j)';
        wam.PAR.links(j).inertia([1 4 7 5 6 9]) = theta(5:end,j)';
    end
    
    % recompute inverse dynamics
    uff = gen_invdyn_inputs(wam,t,ref);
    
    % OPTIONAL - update feedback, DOESNT WORK!
    %[Ad,Bd,Ac,Bc] = wam.linearize(ref(1:2*n,:),uff+ufb);
    %[K,P] = gen_lqr_fb(wam,Q,R,Ad,Bd);
    
    [X(:,:,i+1),ufb] = evolve(wam,uff,K,ref(1:2*n,:),Q0);
    U(:,:,i+1) = uff + ufb;
    
end
experiment{1}.u = U;
experiment{1}.x = X;
experiment{1}.r = ref(1:2*n,:);

wam.plot_inputs(U);
wam.plot_outputs(X,ref(1:2*n,:),1);
wam.show_experiment(experiment);