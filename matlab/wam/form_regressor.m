%% Form regressor matrix for system id. using autodiff
% used for inverse dynamics based sys. id.
%
% actual value of theta doesn't matter since inv.dyn
% is linear
%
function [Mat,y] = form_regressor(X,U,theta,par)

dt = 0.002;
n = 7;
num_param = 10;
assert(size(X,1) == 2*n, 'X must have 2*n=14 rows!');
N = size(X,2);
Mat = zeros((N-2)*n,num_param*n);
y = zeros((N-2)*n,1);
% Q = zeros(n,N-2);
% Qd = zeros(n,N-2);
% Qdd = zeros(n,N-2);

for i = 2:N-1
    q = X(1:n,i);
    qd = X(n+1:end,i);
    qdd = (X(n+1:end,i+1) - X(n+1:end,i-1))/(2*dt);
%     Q(:,i-1) = q;
%     Qd(:,i-1) = qd;
%     Qdd(:,i-1) = qdd;
    idx = (i-2)*n+1:(i-1)*n;
    y(idx) = U(:,i);
    fun = @(theta) barrettWamInvDynamicsNEAuto(q,qd,qdd,theta,par); 
    df = full(AutoDiffJacobianAutoDiff(fun,theta(:)));
    Mat(idx,:) = df;
end

% figure;
% t = dt*(1:N-2);
% for i = 1:7
%     subplot(7,3,3*i-2);
%     plot(t,Q(i,:),'-');
%     if i == 7
%         xlabel('Time (s)');
%     end
%     ylabel(['$q_', num2str(i), '$'], 'Interpreter', 'latex');
%     subplot(7,3,3*i-1);
%     plot(t,Qd(i,:),'-');
%     ylabel(['$\dot{q}_', num2str(i), '$'], 'Interpreter', 'latex');
%     subplot(7,3,3*i);
%     plot(t,Qdd(i,:),'-');
%     ylabel(['$\ddot{q}_', num2str(i), '$'], 'Interpreter', 'latex');
% end

