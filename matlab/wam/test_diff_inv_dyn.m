%% Differentiate inverse dynamics w.r.t. link parameters
%
% Num. diff and auto diff both satisfy linearity
% w.r.t. parameters theta!

clc; clear; close all; rng(1);

n = 7;
link_num = 2;
num_param = 10;
h = 1e-6;
q = ones(n,1);
qd = ones(n,1);
qdd = ones(n,1);
par = load_wam_links(link_num);
theta = zeros(num_param,n);

for i = 1:n
    theta(1,i) = par.links(i).m;
    theta(2:4,i) = par.links(i).mcm(:)';
    theta(5:end,i) = par.links(i).inertia([1 4 7 5 6 9]);
end

fun = @(theta) barrettWamInvDynamicsNEAuto(q,qd,qdd,theta,par); 

df_num = zeros(n,n*num_param);
for i = 1:n
    for j = 1:num_param
        % perturb link forwards
        theta(j,i) = theta(j,i) + h;
        % perturb plus
        u_plus = fun(theta(:));
        % perturb link backwards
        theta(j,i) = theta(j,i) - 2*h;
        u_minus = fun(theta(:));
        idx = (i-1)*num_param + j;
        df_num(:,idx) = (u_plus - u_minus)/(2*h);
        % perturb link back to normal
        theta(j,i) = theta(j,i) + h;
    end
end

%% Test autodiff

df_auto = full(AutoDiffJacobianAutoDiff(fun,theta(:)));