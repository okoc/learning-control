% generate nominal inputs for ILC 
% using inverse dynamics with nominal parameters
function uff = gen_invdyn_inputs(robot,t,ref)

N = length(t);
dim = size(ref,1)/3;
dimu = dim;

q = ref(1:dim,1:N);
qd = ref(dim+1:2*dim,1:N);
qdd = ref(2*dim+1:end,1:N);

% get the desired inputs
uff = zeros(dimu,N);
for i = 1:N
    uff(:,i) = robot.invDynamics(q(:,i),qd(:,i),qdd(:,i));
end