% Lookup a random striking polynomial

function [t,ref,params] = lookup_rand_strike(q0,cut,relax)

% load the lookup table
month = 'March'; % could be March, April or May
lookup_table = ['~/table-tennis/lookup_' month '_2016.txt'];
lookup = load(lookup_table);
n = length(lookup);
n = n/21; % number of entries in the lookup
lookup = reshape(lookup,n,21); % 15 for params and 6 for ball state
rand_idx = randi(n); 
params = lookup(rand_idx,7:end);
qf = params(1:7);
qfdot = params(8:14);
T = params(15);
dt = 0.002;
q0dot = zeros(7,1);
ref = calc_hit(dt,q0,q0dot,qf,qfdot,T);
[t,ref] = relax_traj(ref,relax,cut);
%Ndecimate = 10*floor(size(ref,2)/10);
%ref = ref(:,1:Ndecimate);
%t = t(1:Ndecimate);

end

function [t,ref] = relax_traj(ref,relax,cut)

dt = 0.002;
if (relax <= 1) % speed up
    speedup = round(1/relax);
    N = size(ref,2);
    ref = ref(:,1:speedup:N);
else 
    relax = round(relax);
    N = size(ref,2);
    t = 1:N;
    trelax = linspace(1/relax,N,relax*N);
    ref = interp1(t,ref',trelax,'linear','extrap')';
end

Ncut = round(length(ref)/cut);
ref = ref(:,1:Ncut);
t = dt * (1:length(ref));
end