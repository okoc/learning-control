%% Test covariance of forward dynamics derivative w.r.t q,qd,u

clc; clear; close all; rng(2);

% create the links
% create a variance matrix for the links
% use high-fidelity monte carlo to approx. the posterior
% compare performance with linearization (EKF-like)

n = 7; %2
link_num = 1;
num_param_per_dof = 10; %6
% load_rr_links;
par = load_wam_links(link_num);
mu = zeros(num_param_per_dof,n); % mean parameter vector
for i = 1:n
    mu(1,i) = par.links(i).m;
    mu(2:4,i) = par.links(i).mcm(:)';
    mu(5:end,i) = par.links(i).inertia([1 4 7 5 6 9]);
end
mu = mu(:);

% create random pos. definite sym. matrix
ncoef = n * num_param_per_dof;
V = randn(ncoef);
V = (V + V')/2.0;
Sigma = V + ncoef*eye(ncoef);
Sigma = 0.0001 * Sigma/ncoef; % variance of parameters

q = randn(n,1);
qd = randn(n,1);
u = randn(n,1);
fun = @(theta) barrettWamDynamicsArtAuto(q,qd,u,theta,par); 
%fun = @(theta) rr_dynamics(q,qd,u,theta,par);

%% Monte Carlo sampling
N_high = 10000;
thetas = repmat(mu,1,N_high) + chol(Sigma)*randn(ncoef,N_high);
fs = zeros(n,N_high);

for i = 1:N_high
    fs(:,i) = fun(thetas(:,i));
end

% compute mean
f_mean = sum(fs,2)/N_high;

% compute var
f_var_mc = zeros(n);
for i = 1:N_high
    f_var_mc = f_var_mc + (fs(:,i) - f_mean)*(fs(:,i) - f_mean)';
end
f_var_mc = f_var_mc/N_high

%% Compare with linearization

df_auto = full(AutoDiffJacobianAutoDiff(fun,mu));
f_var_lin = df_auto * Sigma * df_auto'