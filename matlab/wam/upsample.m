function mat_up = upsample(mat,indices,max_len)

    rate = indices(2) - indices(1);
    if rate == 1
        mat_up = mat;
        return;
    end
    tup = 1:max_len;    
    mat_up = interp1(indices,mat',tup,'linear','extrap')';
end