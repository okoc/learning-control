%% generating feedback with LQR to stabilize the robot
% around a given trajectory
function [K,P] = gen_lqr_fb(robot,Q,R,Ad,Bd)

    % calculate the optimal feedback law
    h = robot.SIM.h;
    N = size(Ad,3);
    t = h * (1:N);
    Cout = robot.SIM.C;
    % get linear time variant matrices around trajectory
    lqr = LQR(Q,R,Q,Ad,Bd,Cout,N,h,true);
    [K,P] = lqr.computeFinHorizonLTV();
end