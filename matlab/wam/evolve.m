% Function that evolves nonlinear robot dynamics
function [y,ufb] = evolve(robot,uff,K,ref,x0)
    fun = @(t,x,u) robot.actual(t,x,u);
    h = robot.SIM.h;
    N = size(uff,2);
    t = h * (1:N);
    x = zeros(length(x0),N+1);
    ufb = zeros(size(uff,1),N);
    x(:,1) = x0;
    y(:,1) = robot.SIM.C * x(:,1);
    for i = 1:N
        if ~isempty(K)
            if size(K,3) > 1
                ufb(:,i) = K(:,:,i) * (x(:,i)-ref(:,i));
            else
                ufb(:,i) = K * (x(:,i) - ref(:,i));
            end
        end
        x(:,i+1) = step(h,t(i),x(:,i),uff(:,i)+ufb(:,i),fun);
        y(:,i+1) = robot.SIM.C * x(:,i+1);
        % no constraint checking
    end
    y = y(:,2:end);
end

% one step simulation along the trajectory with Runge-Kutta (RK4)
function next = step(h,t,prev,u,fun)

    % get trajectory of states
    % using classical Runge-Kutta method (RK4)  
    k1 = h * fun(t,prev,u);
    x_k1 = prev + k1/2;
    k2 = h * fun(t,x_k1,u);
    x_k2 = prev + k2/2;
    k3 = h * fun(t,x_k2,u);
    x_k3 = prev + k3;
    k4 = h * fun(t,x_k3,u);
    next = prev + (k1 + 2*k2 + 2*k3 + k4)/6;

end