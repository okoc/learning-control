% Function that applies the ILC update after downsampling

function [uff_new,u_ilc] = ilc_direct_wam(wam,x,ufb,ref,uff,update)

Nref = size(ref,2);
down = update.down_sample;
idx_down = 1:down:Nref;
ref_down = ref(:,idx_down);
uff_down = uff(:,idx_down);
persistent firsttime;
persistent F;

if isempty(firsttime) || update.linearize_each_iter
    if update.linearize_around_ref
        [Ad,Bd] = wam.linearize(ref_down,uff_down);
    else
        x_down = x(:,idx_down);
        [Ad,Bd] = wam.linearize(x_down,uff_down);        
    end
    F = lift_dyn(Ad,Bd);
    singvals = svd(F);
    fprintf('Max singular value: %f\n', max(singvals));
    fprintf('Min singular value: %f\n', min(singvals));
    firsttime = false;
end

% get next inputs
e = x(:,idx_down) - ref(:,idx_down);


% current iteration ILC
if update.cur_iter
    uff_down = uff_down + ufb(:,idx_down);
end
% model based ILC update
if update.model
    if update.goal_ilc
        tr = 1;
        n = 14;
        Ncut = length(idx_down);
        Q = blkdiag(zeros((Ncut-tr)*n),eye(tr*n));
        L = pinv(sqrt(Q)*F,0.05)*sqrt(Q);
        u_ilc = -L*e(:);
    else
        u_ilc = -pinv(F,0.05)*e(:);
    end
    %u_ilc = -F \ e(:);
    %u_ilc = -(F'*F + 0.01*eye(size(F,2)))\(F'*e(:));
    u_ilc = reshape(u_ilc,7,length(idx_down));
    uff_down = uff_down + u_ilc;
end

uff_new = upsample(uff_down,idx_down,Nref);
u_ilc = uff_new - uff;

end