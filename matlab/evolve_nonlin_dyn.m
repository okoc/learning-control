% Sample nonlinear dynamics
% Condition first on nominal controls
% Then widen GP by conditioning on nearby random controls

function [x,ufb] = evolve_nonlin_dyn(gp,dt,x0,u,K)

n = length(x0);
m = size(u,1);
N = size(u,2);
x = zeros(n,N+1);
x(:,1) = x0;
ufb = zeros(m,N);

for i = 1:N

    if ~isempty(K)
        ufb(:,i) = K(:,:,i) * x(:,i);
    end
        
    ufull = u(:,i) + ufb(:,i);
    data = [x(:,i);ufull];
    for j = 1:n
        [f,~] = gp(j).predict(data);
        x(j,i+1) = x(j,i) + dt*f;
    end
end
x = x(:,2:end);

