% Applying 'TWO-SIDED-SECANT' to do numerical differentiation
% cpp calls the mexed dynamics functions
function [dfdx,dfdu] = num_diff(fun,Q,u,PAR,cpp)

NDOF = length(Q)/2;
NU = length(u);
% Take 2n differences with h small
h = 1e-3;
QhPlus = repmat(Q,1,2*NDOF) + h * eye(2*NDOF);
QhMinus = repmat(Q,1,2*NDOF) - h * eye(2*NDOF);
QdhPlus = zeros(2*NDOF);
QdhMinus = zeros(2*NDOF);
for i = 1:2*NDOF
    if cpp
        QdhPlus(:,i) = [QhPlus(NDOF+1:end,i);fun(QhPlus(1:NDOF,i),QhPlus(NDOF+1:end,i),u)];
        QdhMinus(:,i) = [QhMinus(NDOF+1:end,i);fun(QhMinus(1:NDOF,i),QhMinus(NDOF+1:end,i),u)];
    else
        QdhPlus(:,i) = fun(QhPlus(:,i),u,PAR);
        QdhMinus(:,i) = fun(QhMinus(:,i),u,PAR);
    end
end
dfdx = (QdhPlus - QdhMinus) / (2*h);
%dfdx = [zeros(NDOF), eye(NDOF); der(NDOF+1:2*NDOF,:)];
%dfdu = Mbig \ Bx;
uhPlus = repmat(u,1,NU) + h * eye(NU);
uhMinus = repmat(u,1,NU) - h * eye(NU);
QdhPlus = zeros(2*NDOF,NU);
QdhMinus = zeros(2*NDOF,NU);
for i = 1:NU
    if cpp
        QdhPlus(:,i) = [QhPlus(NDOF+1:end,i);fun(Q(1:NDOF),Q(NDOF+1:end),uhPlus(:,i))];
        QdhMinus(:,i) = [QhMinus(NDOF+1:end,i);fun(Q(1:NDOF),Q(NDOF+1:end),uhMinus(:,i))];
    else
        QdhPlus(:,i) = fun(Q,uhPlus(:,i),PAR);
        QdhMinus(:,i) = fun(Q,uhMinus(:,i),PAR);
    end
end
dfdu = (QdhPlus - QdhMinus) / (2*h);
    