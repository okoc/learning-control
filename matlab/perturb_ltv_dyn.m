% Create a block lower-diagonal perturbation matrix 
% that has 2-norm (max singular value) less than a given positive alpha

function [Ad_act,Bd_act,F_act,Fc_act] = perturb_ltv_dyn(Ad_nom,Bd_nom,C,alpha,uncorr,dt)

    [F_nom,~] = lift_dyn(Ad_nom,Bd_nom,C);
    n = size(Ad_nom,1);
    m = size(Bd_nom,2);
    N = size(F_nom,1)/n;
    t = 1:N;
                                                    
    assert(alpha >= 0, 'alpha should be nonnegative!');
    if (alpha == 0)
        Ad_act = Ad_nom;
        Bd_act = Bd_nom;
        F_act = F_nom;
        Fc_act = F_nom;
        return;
    end

    % generate the (correlated) perturbations
    [A_pert,B_pert] = sample_ltv_dyn(n,m,t,uncorr);
    [Ad_pert,Bd_pert] = discretize_dyn(A_pert,B_pert,dt);
    
    %{
    [F_act,Fc_act] = lift_dyn(Ad_nom + Ad_pert, Bd_nom + Bd_pert,varargin{:});
    E = F_act - F_nom;
    
    while norm(E) > alpha
        Ad_pert = Ad_pert/2;
        Bd_pert = Bd_pert/2;
        [F_act,Fc_act] = lift_dyn(Ad_nom + Ad_pert, Bd_nom + Bd_pert,varargin{:});
        E = F_act - F_nom;
    end
    %}
    f = @(x) calc_error_norm(Ad_nom,Bd_nom,F_nom,C,x);
    xzero = zeros(n,n+m,N); 
    xmax = [Ad_pert,Bd_pert];
    tol = 0.001;
    xbest = bisection(xzero,xmax,f,alpha,tol);
    Ad_pert = xbest(:,1:n,:);
    Bd_pert = xbest(:,n+1:end,:);
    
    Ad_act = Ad_nom + Ad_pert;
    Bd_act = Bd_nom + Bd_pert;
    [F_act,Fc_act] = lift_dyn(Ad_act, Bd_act, C);
end

function val = calc_error_norm(Anom,Bnom,Fnom,C,x)

n = size(Anom,2);
Apert = x(:,1:n,:);
Bpert = x(:,n+1:end,:);
[F_act,~] = lift_dyn(Anom + Apert, Bnom + Bpert,C);
E = F_act - Fnom;
val = norm(E);

end