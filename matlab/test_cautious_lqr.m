%% Testing Cautious LQR 

clc; clear; close all; rng(2);

% for each perturbed model
% control with perturbed model
% control with cautious nominal model
% show that cautious model has less error overall

l = 50; % number of experiments
n = 2; % dim_x
m = 2; % dim_u
p = 2; % dim_y
C = eye(p,n);
T = 1.0; % final time
N = 20;
dt = T/N; % discretization
t = dt*(0:N-1);
x0 = zeros(n,1);
s = sample_traj(p,t,0.2,x0)'; 
% for comparison with direct inversion, make sure it starts from x0
Q = eye(p);
Qf = Q; %10 * Q;
q = 6; % regularizer negative exponent
R = (10^(-q))*eye(m);
uncorr = false;
[Ac_act,Bc_act] = sample_ltv_dyn(n,m,t,uncorr);
[A_act,B_act] = discretize_dyn(Ac_act,Bc_act,dt);
A_nom = zeros(n,n,N);
B_nom = zeros(n,m,N);
Sigmas = zeros(n*(n+m),n*(n+m),N);
prec = 1e-4;
for i = 1:N
    Sigmas(:,:,i) = prec * eye(n*(n+m));
end

%thresh = 1;
%uncorr = true;
%[Ad_nom,Bd_nom] = perturb_ltv_dyn(Ad_act,Bd_act,C,thresh,uncorr,dt);

err_nom = zeros(1,l);
err_cautious = zeros(1,l);
x_avg_nom = zeros(n,N);
x_avg_cautious = zeros(n,N);

for i = 1:l
    
    % Perturb actual model 
    %{ 
    [A_pert,B_pert] = sample_ltv_dyn(n,m,t,false);
    %[A_pert,B_pert] = discretize_dyn(A_pert,B_pert,dt);
    A_nom = A_act + sqrt(1/prec) * A_pert;
    B_nom = B_act + sqrt(1/prec) * B_pert;
    %}
    %%{
    for j = 1:N
        Sigma = Sigmas(:,:,j);
        pert = chol(Sigma)*randn(n*(n+m),1);
        A_pert = reshape(pert(1:n*n),n,n);
        B_pert = reshape(pert(n*n+1:end),n,m);
        A_nom(:,:,j) = A_act(:,:,j) + A_pert;
        B_nom(:,:,j) = B_act(:,:,j) + B_pert;
    end
    %}
    
    % Control with nominal model
    lqr = LQR(Q,R,Qf,A_nom,B_nom,C,N,dt,1);
    [K,P] = lqr.computeFinHorizonLTV();
    uff = recursive_ilc(A_nom,B_nom,C,P,Q,R,-s);
    %[K,uff] = lqr.computeFinHorizonTracking2(s,false);
    [x,~,~] = evolve_ltv(A_act,B_act,K,C,uff,s,0,x0);
    x_avg_nom = x_avg_nom + x;
    err_nom(i) = norm(x-s);

    % Control with cautious nominal model
    [uff,K] = recursive_cautious_ilc(A_nom, B_nom, C, Q, R, Sigmas, -s);
    [x,~,~] = evolve_ltv(A_act,B_act,K,C,uff,s,0,x0);
    x_avg_cautious = x_avg_cautious + x;
    err_cautious(i) = norm(x-s);
end

figure;
plot(1:l,err_nom',1:l,err_cautious');
legend('cert. equiv.','cautious');
figure;
plot(t,x_avg_cautious/l,'.-',t,x_avg_nom/l,'-',t,s,'--');
legend('cautious avg traj1', ...
       'cautious avg traj2', ...
       'cert. equiv. avg traj1', ...
       'cert. equiv. avg traj2', ...
       'ref1', ...
       'ref2');
% figure;
% subplot(2,1,1);
% plot(t,x,'--',t,s,'-');
% subplot(2,1,2);
% plot(t,u);