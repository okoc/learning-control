%% Test script for LQR

clc; clear; close all; rng(3);

%% Tracking random trajectories with random dynamics
n = 2; % dim_x
m = 2; % dim_u
p = 2; % dim_y
T = 1.0; % final time
N = 20;
dt = T/N; % discretization
t = dt*(0:N-1);
uncorr = false; % correlated nominal and actual dynamics
[A,B] = sample_ltv_dyn(n,m,t,uncorr);
[Ad,Bd] = discretize_dyn(A,B,dt);
C = eye(p,n);
x0 = zeros(n,1);
s = sample_traj(p,t,0.2,x0)'; 
% for comparison with direct inversion, make sure it starts from x0
Q = eye(p);
Qf = Q; %10 * Q;
q = 3; % regularizer negative exponent
R = (10^(-q))*eye(m);

lqr = LQR(Q,R,Qf,Ad,Bd,C,N,dt,1);
Kbar = lqr.computeFinHorizonTracking(s);

%simulate system
x = zeros(n,N);
x(:,1) = x0;
u = zeros(m,N-1);
for i = 1:N
    u(:,i) = Kbar(:,:,i)*[x(:,i)-s(:,i);1];
    x(:,i+1) = Ad(:,:,i)*x(:,i) + Bd(:,:,i)*u(:,i);
end
x = x(:,1:end-1);

figure;
subplot(2,1,1);
plot(t,x,'--',t,s,'-');
subplot(2,1,2);
plot(t,u);

%% Compare direct inversion with LQR based tracking
% LQR is with state/error-feedback + feedforward term
%{
lqr = LQR(Q,R,Qf,Ad,Bd,C,N,dt,1);
[K,uff] = lqr.computeFinHorizonTracking2(s,0);

%simulate system
x = zeros(n,N);
x(:,1) = x0;
u = zeros(m,N);
for i = 1:N
    %u(:,i) = K(:,:,i)*(x(:,i)-s(:,i)) + uff(:,i);
    u(:,i) = K(:,:,i)*x(:,i) + uff(:,i);
    x(:,i+1) = Ad(:,:,i)*x(:,i) + Bd(:,:,i)*u(:,i);
end
x = x(:,2:end);

figure;
subplot(2,1,1);
plot(t,x,'--',t,s,'-');
title('LQR for trajectory tracking');
subplot(2,1,2);
plot(t,u);

% compare with direct inversion
[F,Fc] = lift_dyn(Ad,Bd,C);
%L = pinv(Fc,0);
L = (F'*F + (10^(-q))*eye(m*N))\(F');
u_direct = L*s(:);
x2 = F*u_direct;
x2 = reshape(x2,n,N);
u_direct = reshape(u_direct,m,N);

figure;
subplot(2,1,1);
plot(t,x2,'--',t,s,'-');
title('Tracking with direct inversion');
subplot(2,1,2);
plot(t,u_direct);

figure;
plot(t,u'-u_direct');
title('Difference between LQR and direct inversion');
%}

%% LQR for disturbance rejection
% % sample a disturbance
%{
p = 2; 
R = 1e-8*eye(m);
d = 0.5 * sample_traj(p,t,0.2,x0)'; 
lqr = LQR(Q,R,10*Q,Ad,Bd,C,N,dt,1);
[K,uff] = lqr.computeTrackingWithDist(s,d);

%simulate system
x = zeros(n,N);
x(:,1) = x0;
u = zeros(n,N-1);
for i = 1:N-1
    u(:,i) = K(:,:,i)*(x(:,i)-s(:,i)) + uff(:,i);
    %u(:,i) = K(:,:,i)*x(:,i) + uff(:,i);
    x(:,i+1) = Ad(:,:,i)*x(:,i) + Bd(:,:,i)*u(:,i) + d(:,i+1);
end

figure;
subplot(2,1,1);
plot(t,x,'--',t,s,'-');
subplot(2,1,2);
plot(t(1:end-1),u);
%}
