%% Load RR links

% constants
g = 9.81;
% joint parameters
m1 = 1; %mass of first link, kg
m2 = 0.5; %mass of second link, kg
l1 = 0.50; %length of first link, m
l2 = 0.40; %length of second link, m
l_c1 = 0.25; %distance of first link's center of gravity to prev. joint, m
l_c2 = 0.20; %dist. of second link's c.oZ.g. to prev. joint, m
I1 = (1/12)*m1*l1^2; %assume thin rod moment of inertia around c.o.g.
I2 = (1/12)*m2*l2^2; %kg m^2
% motor parameters
J_a1 = 0.100; % actuator inertia of link 1
J_g1 = 0.050; % gear inertia of link1
J_m1 = J_a1 + J_g1;
J_a2 = 0.080; % actuator inertia of link 2
J_g2 = 0.040; % gear inertia of link2
J_m2 = J_a2 + J_g2;
% gear ratio typically ranges from 20 to 200 or more - comment from book
r_1 = 20;
r_2 = 20;
 
% pass it as a parameter structure
par.const.g = g;
par.link1.mass = m1;
par.link2.mass = m2;
par.link1.length = l1;
par.link2.length = l2;
par.link1.centre.dist = l_c1;
par.link2.centre.dist = l_c2;
par.link1.inertia = I1;
par.link2.inertia = I2;
par.link1.motor.inertia = J_m1;
par.link2.motor.inertia = J_m2;
par.link1.motor.gear_ratio = r_1;
par.link2.motor.gear_ratio = r_2;

mu(1:6) = [m1;l1;l_c1;I1;J_m1;r_1];
mu(7:12) = [m2;l2;l_c1;I2;J_m2;r_2];
mu = mu(:);