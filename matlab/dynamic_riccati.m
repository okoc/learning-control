% Dynamic Riccati equation
% Can be iterated till it converges to the infinite-horizon matrix Pinf

function Ppre = dynamic_riccati(Pnext,Q,R,A,B)

% M = A'*Pnext*A;
% Phi = R + B'*Pnext*B;
% Psi = B'*Pnext*A;
% K = -Phi\Psi;
% Ppre = Q + M + Psi'*K; 
% Ppre = (Ppre + Ppre')/2;
Ppre = Q + A'*Pnext*A - A'*Pnext*B*((R + B'*Pnext*B)\((B'*Pnext)*A));

end