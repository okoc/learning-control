% Testing cautious and adaptive learning control on random nonlin systems

% Sample random desired trajectories and initial controls
% Condition a GP on the initial cond and controls
% Linearize the dynamics using GP mean derivatives
% Apply ILC

clc; clear; close all; rng(5);
recursive = true;
cautious = true;
adaptive = true;
n = 2; % dim_x
m = 2; % dim_u
p = 2; % dim_y
T = 1.0; % final time
N = 20;
dt = T/N; % discretization
t = dt*(1:N);
x0 = zeros(n,1);
num_exp = 10;
num_iter = 20;
lambda = 1e-6;
Q = eye(n);
C = eye(p,n);
Qf = Q;
R = lambda*eye(m);
X = zeros(n,N,num_iter+1,num_exp); % error along trajectory for each trial
U = zeros(m,N,num_iter+1,num_exp); % inputs along trajectory for each trial
Ufull = zeros(m,N,num_iter+1,num_exp); % total inputs (fb + ff) 
ref = zeros(n,N,num_exp);
u_ci = zeros(m,N); % current iteration errors to feedback to feedforward
K_pd = -20 * repmat(eye(n,n),1,1,N);

% GP correlation structure
l = 0.3 * ones(1,n);
l_var = 1e-6 * rand(1,n);
s = 0.1 * ones(1,n);
s_var = 0 * rand(1,n);
sn = 1e-6 * ones(1,n); %noise
sn_var = 0.0 * rand(1,n);
hp.type = 'squared exponential ard';

for j = 1:num_exp % average results over different traj. and models
    
    disp(['Experiment ', int2str(j)]);

    % Sample nonlinear dynamics

    % sample each dimension of dynamics model matrices
    for i = 1:n
        %hp.l = [];
        hp.l = l(i) + sqrt(l_var(i)) * randn;
        hp.scale = s(i) + sqrt(s_var(i)) * randn;
        hp.noise.var = sn(i) + sqrt(sn_var(i)) * randn;
        a = rand(n,1);
        b = rand(m,1);
        f = @(x) a'*x(1:n) + b'*x(n+1:end);
        df = @(x) [a;b];
        gp_act(i) = GP(hp,[],[],true,f,df); % calc derivatives of GP
        gp_nom(i) = GP(hp,[],[],true,f,df);
    end    
    num_traj = 10;
    ref(:,:,j) = sample_traj(m,t,0.5,zeros(n,1))';
    [gp_act] = sample_nonlin_dyn(num_traj,m,dt,t,x0,gp_act);
    u = sample_traj(m,t,0.5,randn(n,1))';
    [x,ufb] = evolve_nonlin_dyn(gp_act,dt,x0,u,K_pd);
    [gp_nom,F,Ad,Bd] = update_gp_around_traj(gp_nom,dt,[x0,x],u+ufb,true);
    
    % PREPARE ILC 
    if recursive
        if ~cautious
            lqr = LQR(Q,R,Qf,Ad,Bd,C,N,dt,1);
            [K,P] = lqr.computeFinHorizonLTV();
        end
    else
        lqr = LQR(Q,R,Qf,Ad,Bd,C,N,dt,1);
        [K,P] = lqr.computeFinHorizonLTV();
        %L = pinv(F,tol);
        L = (F'*F + lambda*eye(m*N)) \ (F');
    end
    % for adaptation using LBR
    if adaptive || cautious
        Sigmas = zeros(n*(n+m),n*(n+m),N);
        prec = 1e2;
        for i = 1:N
            Sigmas(:,:,i) = (1/prec) * eye(n*(n+m));
        end
    end
    
    % APPLY ILC
    U(:,:,1,j) = u;
    Ufull(:,:,1,j) = u + ufb;
    X(:,:,1,j) = x;

    for i = 1:num_iter
        e = x - ref(:,:,j);        

        if recursive 
            if cautious
                [uff,K] = recursive_cautious_ilc(Ad, Bd, C, Q, R, Sigmas, e);
            else
                uff = recursive_ilc(Ad, Bd, C, P, Q, R, e);
            end
            U(:,:,i+1,j) = U(:,:,i,j) + uff;
        else
            % DIRECT ILC happening here
            uff_direct = -reshape(L*e(:),m,N);
            U(:,:,i+1,j) = U(:,:,i,j) + uff_direct;
            % Adding current iteration correction for direct ILC
            %{ 
            for idx = 1:N
                u_ci(:,idx) = K(:,:,idx)*e(:,idx);
            end
            U(:,:,i+1,j) = U(:,:,i+1,j) + u_ci;
            %}
        end
        [x,ufb] = evolve_nonlin_dyn(gp_act,dt,x0,U(:,:,i+1,j),K);
        
        %[gp_nom,F,Ad_nom,Bd_nom] = update_gp_around_traj(gp_nom,dt,[x0,x],U(:,:,i+1,j)+ufb,true);
        %[gp_nom] = update_gp_around_traj(gp_nom,dt,[x0,x],U(:,:,i+1,j)+ufb,false);
        
        X(:,:,i+1,j) = x;
        Ufull(:,:,i+1,j) = U(:,:,i+1,j) + ufb;
        disp(['Iteration ', int2str(i)]);
        
        % LBR to adapt linearized model
        if adaptive
            s2 = 0.001; % noise covariance in LBR model
            du = Ufull(:,:,i+1,j) - Ufull(:,:,i,j);
            de = X(:,:,i+1,j) - X(:,:,i,j);
            [Ad,Bd,Sigmas] = est_recursive_ltv(Ad,Bd,du,de,s2,Sigmas);
            %[Ad,Bd,Sigmas] = broyden_est_mats(Ad,Bd,du,de,Sigmas);
        end
    end
end

plot_performance(X,U,ref,0);