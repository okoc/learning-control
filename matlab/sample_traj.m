% Sample a trajectory in phase space
% drawn from a multivariate normal distribution
% with squared exponential kernel based covariance matrix
%
% we condition on x0, the initial state at 0

function traj = sample_traj(n,t,l,x0)

hp.type = 'squared exponential ard';
hp.l = l;
hp.scale = 1;
hp.noise.var = 0.00;
s = hp.scale;
N = length(t);

for i = 1:n
    gp(i) = GP(hp,0,x0(i),false,[],[]);
    [mu,Sigma] = gp(i).predict_mesh(t);
    [U,S] = eig(Sigma);
    traj(:,i) = mu(:) + U * sqrt(max(0,real(S))) * randn(N,1);
end