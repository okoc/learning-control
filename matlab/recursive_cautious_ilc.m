% Recursive cautious ILC
% that computes cautious LQR for disturbance rejection

function [uff,K] = recursive_cautious_ilc(Amean, Bmean, C, Q, R, Sigmas, e, ufb)

    m = size(Bmean,2);
    n = size(Amean,1);
    p = size(C,1);
    N = size(Amean,3);
    e = reshape(e,p,N);
    if ismatrix(Q)
        Q = repmat(Q,1,1,N);
    end
    
    if ismatrix(R)
        R = repmat(R,1,1,N);
    end
    
    P = zeros(n,n,N+1);
    P(:,:,end) = Q(:,:,end);

    % calculate optimal K and uff
    K = zeros(m,n,N);
    uff = zeros(m,N);
    nu = zeros(n,N+1);
    
    for i = N:-1:1
       % introducing three new matrices for the cautious case
       [Phi,Psi,M] = estimate_cautious_covar(Sigmas(:,:,i),P(:,:,i+1),R(:,:,i),...
                                             Amean(:,:,i),Bmean(:,:,i));
       K(:,:,i) = -Phi\Psi;
       P(:,:,i) = Q(:,:,i) + M + Psi'*K(:,:,i);
       P(:,:,i) = (P(:,:,i) + P(:,:,i)')/2; % this is important for num. stability!
       nu(:,i) = (Amean(:,:,i) + Bmean(:,:,i)*K(:,:,i))'*nu(:,i+1) + Q(:,:,i)*e(:,i);
       uff(:,i) = -Phi\((Bmean(:,:,i)'*nu(:,i)));
       % this part is the addition of feedback adjustmen whenever K's are
       % updated!
       %uff(:,i) = uff(:,i) + (ufb(:,i) -  K(:,:,i) * e(:,i));
    end

end

% Estimate the matrices used in the cautious LQR/ILC
function [Phi,Psi,M] = estimate_cautious_covar(Sigma,P,R,Am,Bm)

    n = size(Am,1);
    m = size(Bm,2);
    
    Phi = Bm'*P*Bm + R;
    for i = 1:m
        for j = 1:m
            for k = 1:n
                for l = 1:n
                    Phi(i,j) = Phi(i,j) + P(k,l) * Sigma(n^2 + (i-1)*n + k, n^2 + (j-1)*n + l);
                end
            end
        end
    end    

    Psi = Bm'*P*Am;
    
    for i = 1:m
        for j = 1:n
            for k = 1:n
                for l = 1:n
                    Psi(i,j) = Psi(i,j) + P(k,l) * Sigma(n^2 + (i-1)*n + k, (j-1)*n + l);
                end
            end
        end
    end    
    
    M = Am'*P*Am;
    
    for i = 1:n
        for j = 1:n
            for k = 1:n
                for l = 1:n
                    M(i,j) = M(i,j) + P(k,l) * Sigma((i-1)*n + k, (j-1)*n + l);
                end
            end
        end
    end        
end

% THIS SCRIPT KINDA SHOWS THAT THERE IS NO INCENTIVE TO USE TRACE FORM
%{
n = 40;
M = zeros(n,n);
Sigma = rand(n*n,n*n);
P = rand(n,n);
P = P + P';
Mold = M;
tic
for i = 1:n
    for j = 1:n
        M(i,j) = M(i,j) + trace(P * Sigma((i-1)*n + (1:n), (j-1)*n + (1:n)));
    end
end
toc
Mdiff = M - Mold;
M = Mold;
tic;
for i = 1:n
    for j = 1:n
        for k = 1:n
            for l = 1:n
                M(i,j) = M(i,j) + P(k,l) * Sigma((i-1)*n + k, (j-1)*n + l);
            end
        end
    end
end 
toc
Mdiff2 = M - Mold;
Mdiff - Mdiff2
%}