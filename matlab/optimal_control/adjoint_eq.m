%% diff. eq for reference tracking minimum principle with time varying A, B matrices

function xbar_dot = adjoint_eq(t,xbar,params)

% xbar contains both x and lambda
n = length(xbar);
x = xbar(1:n/2);
lambda = xbar(n/2+1:end);

As = params.As;
Bs = params.Bs;
Q = params.Q;
R = params.R;
ts = params.ts;
ref = params.ref; % reference trajectory
N = size(As,3);
dt = ts(end) / N;

A = As(:,:,ceil(t/dt));
B = Bs(:,:,ceil(t/dt));
% if rem(t,dt) > 0
%     A = A + rem(t,dt) * As(:,:,floor(t/dt)+1);
%     B = B + rem(t,dt) * Bs(:,:,floor(t/dt)+1);
% end
r = interp1(ts,ref,t)';
x_dot = A*x - B*(R \ (B' * lambda));
lambda_dot = Q*(r-x) - A'*lambda;
xbar_dot = [x_dot(:); lambda_dot(:)];