%% diff. eq for reference tracking minimum principle
% for control affine nonlinear systems!!

function xbar_dot = pmp_nonlin_ref(t,xbar,params)

% xbar contains both x and lambda
n = length(xbar);
x = xbar(1:n/2);
lambda = xbar(n/2+1:end);
f = params.f;
fx = params.fx;
fu = params.fu;
Q = params.Q;
R = params.R;
ts = params.ts;
ref = params.ref; % reference trajectory
r = interp1(ts,ref,t)';
u = -(R \ (fu(x) * lambda));
x_dot = f(x,u);
lambda_dot = Q*(r-x) - fx(x,u)*lambda;
xbar_dot = [x_dot(:); lambda_dot(:)];