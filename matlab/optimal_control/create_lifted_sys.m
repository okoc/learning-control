%% Function that lifts a nonlinear system around a given reference

function [e,F] = create_lifted_sys(dyn,ref,x,u,dt)

n = size(x,1);
m = size(u,1);
N = size(x,2);
As = zeros(n,n,N);
Bs = zeros(n,m,N);
for i = 1:N
    %As(:,:,i) = dyn_x(x(:,i),u(:,i));
    %Bs(:,:,i) = dyn_u(x(:,i),u(:,i));
    fun_x = @(x) dyn(x,u(:,i));
    As(:,:,i) = full(AutoDiffJacobian(fun_x,x(:,i)));
    fun_u = @(u) dyn(x(:,i),u);
    Bs(:,:,i) = full(AutoDiffJacobian(fun_u,u(:,i)));
end
[Ad,Bd] = discretize_dyn(As,Bs,dt);
F = lift_dyn(Ad,Bd);
e = x - ref;