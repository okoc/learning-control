%% Testing direct vs. indirect optimal control 
% on a control-affine nonlinear dynamical system

clc; clear; close all; rng(3);

nx = 4; % dim_x
ny = 2; % dim_y
m = 2; % dim_u
T = 1.0; % final time
N = 50; % num of traj. points
dt = T/N; % discretization
x0 = zeros(nx,1);
t = dt * (1:N);
v = sample_traj(m,t,0.2,zeros(m,1))'; % sample references from Gaussian Process
u0 = v + sample_traj(m,t,0.2,zeros(m,1))';

%% Create dynamics model

% A = randn(n,n);
% B = randn(n,m);
% dyn = @(x,u) A*x*x + B*x*u; %1d system
% dyn_x = @(x,u) 2*A*x + B*x; 
% dyn_u = @(x,u) B*x; %u-derivative

load_rr_links;
dyn = @(x,u) [x(3:4);rr_dynamics(x(1:2),x(3:4),u,mu,par)];
dynt = @(t,x,u) dyn(x,u);
ref = evolve_dyn(dynt,v,[],[],x0,dt);

%% Initialize nominal LTV models
As = zeros(nx,nx,N);
Bs = zeros(nx,m,N);
for i = 1:N
    fun_x = @(x) dyn(x,v(:,i));
    As(:,:,i) = full(AutoDiffJacobian(fun_x,ref(:,i)));
    fun_u = @(u) dyn(ref(:,i),u);
    Bs(:,:,i) = full(AutoDiffJacobian(fun_u,v(:,i)));
end
[Ad,Bd] = discretize_dyn(As,Bs,dt);
thresh = 1;
C = eye(nx);
[Ad_nom,Bd_nom,Z,~] = perturb_ltv_dyn(Ad,Bd,C,thresh,false,dt);

%% Create feedback
Q = eye(nx);
R = 1e-6*eye(m);
Qf = Q;
lqr = LQR(Q,R,Qf,Ad_nom,Bd_nom,C,N,dt,1);
[K,P] = lqr.computeFinHorizonLTV();

%% Solve u's instead with BVP (minimum principle)

%{
tic;
solinit = bvpinit(linspace(dt,T,N),zeros(1,2*n));
params.f = dyn;
params.fx = dyn_x;
params.fu = dyn_u;
params.Q = eye(n);
params.R = 0.0001*eye(m);
params.ref = ref;
params.ts = t;
odefun = @(t,x) pmp_nonlin_ref(t,x,params);
bcfun = @(xa,xb) [xa(1:n)-ref(1,:)'; xb(n+1:end);];
sol = bvp4c(odefun,bcfun,solinit);
sol_at_t = deval(sol,t);
x_incr = sol_at_t(1:n,:);
lambda = sol_at_t(n+1:end,:); % TODO: should be evaluated from 0 to T-dt
%u_incr = -params.R\(B'*lambda);
fprintf('BVP-MP took %f sec.\n', toc);
figure(2);
plot(t,x_incr,'--',t,ref,'-');
%}

%% Solve with direct optimization (Newton's method)

%{
num_iter = 10;
U = zeros(m,N,num_iter+1);
X = zeros(n,N,num_iter+1);
U(:,:,1) = randn(m,N);
dyn_func = @(t,x,u) dyn(x,u);
X(:,:,1) = evolve_dyn(dyn_func,U(:,:,1),[],ref,x0,dt);

for i = 1:num_iter
    [e,F] = create_lifted_sys(dyn,dyn_x,dyn_u,ref,X(:,:,i),U(:,:,i),dt);
    du = pinv(F,0.01)*e(:);
    U(:,:,i+1) = U(:,:,i) - reshape(du,m,N);
    X(:,:,i+1) = evolve_dyn(dyn_func,U(:,:,i+1),[],ref,x0,dt);
end

plot_performance(X,U,ref,0);
%}

%% Solve with Broyden's method

num_iter = 40;
U = u0; %zeros(m,N,num_iter+1);
X = zeros(nx,N,num_iter+1);
U(:,:,1) = u0; %zeros(m,N);
X(:,:,1) = evolve_dyn(dynt,U(:,:,1),K,ref,x0,dt);
A_est = Ad_nom; %repmat(eye(nx,nx),1,1,N);
B_est = Bd_nom; %repmat(eye(nx,m),1,1,N);
F = lift_dyn(A_est,B_est);
Sigmas = zeros(nx*(nx+m),nx*(nx+m),N);
Prec = Sigmas;
prec = 1e6;
for i = 1:N
    Sigmas(:,:,i) = (1/prec) * eye(nx*(nx+m));
    Prec(:,:,i) = prec * eye(nx*(nx+m));
end
ufb = zeros(m,N);
s2 = 1.0;
lambda = 1.0; % forgetting factor
e_last = X(:,:,1) - ref;
eps = 0.1;

for i = 1:num_iter
    e = X(:,:,i) - ref;
    %{
    for j = 1:N
        if norm(e(:,j)) > norm(e_last(:,j))
            lambdas(j) = 0.5*lambdas(j);
        else if norm(e(:,j)) < (1-eps)*norm(e_last(:,j))
            lambdas(j) = 1.1*lambdas(j);
            end
        end
    end
    %}
    
    if norm(e) > norm(e_last)
        lambda = lambda/2;
    else if norm(e) < 0.8*norm(e_last)
            lambda = 1.2*lambda;
        end
    end
    
    %u_diff = -pinv(F,0.01)*e(:);
    [u_diff,K] = recursive_cautious_ilc(A_est,B_est,C,Q,R,Sigmas,e,ufb);
    U(:,:,i+1) = U(:,:,i) + reshape(u_diff,m,N);
    [X(:,:,i+1),ufb] = evolve_dyn(dynt,U(:,:,i+1),K,ref,x0,dt);
    dx = X(:,:,i+1) - X(:,:,i);
    du = U(:,:,i+1) - U(:,:,i);
    %[A_est,B_est,Sigmas] = broyden_est_mats(A_est,B_est,du(:),dx(:),Sigmas);
    [A_est,B_est,Sigmas,Prec] = est_recursive_ltv(A_est,B_est,du(:),dx(:),...
                                            s2,lambda,Sigmas,Prec);
    e_last = e;    
    %F = lift_dyn(A_bro,B_bro);
end

plot_performance(X,U,ref,0);
