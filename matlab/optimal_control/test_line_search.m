%% Testing line search

clc; clear; close all;

% dim of problem
n = 10;
% generate a random sym. p.d. matrix
S = randn(n);
S = (S + S')/2;
[V,D] = eig(S);
%Sigma = diag(rand(n,1));
Sigma = diag(randi(250,n,1)); % now line search corrects!
A = V*Sigma*V';
b = randn(n,1);
% construct toy problem
f = @(x) 0.5*(x'*A*x) - b'*x;
df = @(x) A*x - b;

% define basin
x_min = -5;
x_max = 5;
% termination criterion
tol = 1e-3;
% pick a random starting point
x0 = (x_max-x_min)*rand(n,1) - x_max;
x_last = x0; 
x_next = x0 + 2*tol;
iter = 0;
while norm(x_last - x_next) > tol
    % gradient descent
    fnc = @(x) deal(f(x),df(x));
    %dir = -df(x_next);
    dir = -A\df(x_next);
    alpha = linesearch(fnc,dir,x_next,0.5,10^-4);
    %alpha = 1;
    x_last = x_next;
    x_next = x_next + alpha * dir;
    iter = iter + 1;
end
fprintf('Total of %d iters\n', iter);
x_next