%% Comparing direct inversion vs. optimal control appproaches
% on an LTV problem

clc; clear; close all; rng(1);

% Comparing 
% 1. batch lifted-matrix inversion
% 2. Minimum Principle (MP) solution with BVP
% 3. MP solution with Gradient Descent
% 4. LQR/LQG solution
% Also showing that Truncated pinv = Q-ILC

n = 2; % dim_x
m = 2; % dim_u
p = 2; % dim_ys
T = 1.0; % final time
N = 50; % num of traj. points
dt = T/N; % discretization
x0 = zeros(n,1);
t = dt * (1:N);
ref = sample_traj(p,t,0.2,x0); % sample references from Gaussian Process
[As,Bs] = sample_ltv_dyn(n,m,t,false); % sample LTV dynamics matrices from GP
C = eye(p,n);

%% Invert the reference to find best controls and evolve system from x0=0

tol = 0.01;
rr = ref';
r_lift = rr(:);
% Discretize and lift nominal dynamics 
tic;
[Ad,Bd] = discretize_dyn(As,Bs,dt); 
fprintf('Discretizing continuous matrices took %f sec.\n', toc);
tic;
F = lift_dyn(Ad,Bd,C); % seems this operation is costly
fprintf('Forming input-output matrix took %f sec.\n', toc);
tic
lambda = 0.0001;
%u_lift = (F'*F + lambda*eye(size(F,2)))\(F'*r_lift);
%u_lift = F \ r_lift;
u_lift = pinv(F,tol) * r_lift;
fprintf('Plant inversion took %f sec.\n', toc);
y_lift = F * u_lift;
u_direct = reshape(u_lift,m,N);
y_direct = reshape(y_lift,p,N);
figure;
subplot(2,1,1);
plot(t,y_direct,'--',t,ref','-');
subplot(2,1,2);
plot(t,u_direct);
title('Direct plant inversion');

%% Test SVD and find Q-ILC equivalent of truncated pinv
%{
% p = n*N;
% [U,S,V] = svd(F);
% s_econ = diag(S(1:p,1:p));
% s_cut = s_econ(s_econ > tol);
% p_cut = length(s_cut);
% a = U'*r_lift;
% u_svd = V(:,1:p_cut) * (a(1:p_cut)./s_cut);
% u_svd2 = pinv(F,tol)*r_lift;
% 
% % plot what untrackable trajectories look like
% r_untracked = U(:,p_cut+1:end);
% R_untracked = zeros(n,N,p-p_cut);
% for i = 1:(p-p_cut)
%     R_untracked(:,:,i) = reshape(r_untracked(:,i),n,N);
% end
% plot(t,R_untracked(:,:,randi(p-p_cut)));
% 
% % find Q-ILC equivalent of truncated pinv
% % projection down to 'easily-tracked-components' of right-singular vectors of F
% Q = V(:,1:p_cut)*V(:,1:p_cut)';
% u_svd3 = Q * pinv(F,0.0) * r_lift;
%}

%% Solve u's instead with BVP (minimum principle)
%{
tic;
init_soln = @(t_mesh) [interp1(t,x_direct',t_mesh),zeros(1,n)];
%solinit = bvpinit(linspace(dt,T,N),init_soln);
solinit = bvpinit(linspace(dt,T,N),zeros(1,2*n));
params.As = As;
params.Bs = Bs;
params.Q = eye(n);
params.R = 0.001*eye(m);
params.ref = ref;
params.ts = t;
odefun = @(t,x) adjoint_eq(t,x,params);
bcfun = @(xa,xb) [xa(1:n)-rr(:,1); xb(n+1:end);];
sol = bvp4c(odefun,bcfun,solinit);
sol_at_t = deval(sol,t);
x_indirect = sol_at_t(1:n,:);
lambda = sol_at_t(n+1:end,:);
lambda = [lambda(:,1), lambda];
for i = 1:length(t)
    u_indirect(:,i) = -params.R \ (Bs(:,:,i)'*lambda(:,i));
end
%u_incr = -params.R\(B'*lambda);
fprintf('MP based inversion took %f sec.\n', toc);
figure(2);
plot(t,x_indirect,'--',t,ref,'-');
title('Solving min. principle with BVP');

% compare u_incr vs. u_lift
figure(3);
for i = 1:m
    subplot(m,1,i);
    plot(t,u_indirect(i,:),t,u_direct(i,:));
    legend('indirect','direct');
end
title('MP based controls vs. Direct inversion');
%}

%% Find the feedback solution using the Separation principle

Q = eye(p);
R = 0.0001*eye(m);
s = ref';
filter = KF(zeros(n),1e-6*eye(p),Ad,Bd,C,dt,true);
filter.initState(x0,100);
lqr = LQR(Q,R,Q,Ad,Bd,C,N,dt,1);
tic;
Kbar = lqr.computeFinHorizonTracking(s);
fprintf('LQR computation took %f sec.\n', toc);

%simulate system
x = zeros(n,N+1);
x(:,1) = x0;
y = zeros(p,N);
u_lqr = zeros(m,N-1);
for i = 1:N
    u_lqr(:,i) = Kbar(:,:,i)*[filter.x-(pinv(C)*s(:,i));1];
    % apply filter
    filter.predict(u_lqr(:,i));
    % apply control
    % evolve system
    x(:,i+1) = Ad(:,:,i)*x(:,i) + Bd(:,:,i)*u_lqr(:,i);
    % observe
    y(:,i) = C*x(:,i+1);% + chol(R) * randn(p,1);
    filter.update(y(:,i));
end

figure;
subplot(2,1,1);
plot(t,y,'--',t,s,'-');
subplot(2,1,2);
plot(t,u_lqr);
title('LQR feedback solution');

%% Solve with Gradient Descent (using adjoint eq from MP)
par.A = Ad;
par.B = Bd;
par.Q = eye(p);
par.R = 0.0001*eye(m);
tic;
u_init = zeros(m,N);
[x,u_grad] = descent_opt_control(n,N,2000,u_init,x0,ref,par,false);
fprintf('Grad descent took %f sec.\n', toc);
figure(4);
plot(t,x(:,2:end),'--',t,ref,'-');
