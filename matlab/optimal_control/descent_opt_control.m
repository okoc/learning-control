%% Indirect Descent (Gradient Descent / Newton's method)
% for Optimal Control

function [x,u] = descent_opt_control(n,N,max_num_iter,u_init,x0,ref,params,verbose)

A = params.A;
B = params.B;
Q = params.Q;
R = params.R;
r = ref';
u = u_init;
lambda = zeros(n,N+1);
lambda(:,end) = 0;
alpha = zeros(1,N);
m = size(u,1);
dHdu = Inf * ones(m,N);
eps = 0.001;
i = 1;
% QQ = []; RR = [];
% for l = 1:N
%     QQ = blkdiag(QQ,Q);
%     RR = blkdiag(RR,R);
% end
x = zeros(n,N+1);
x(:,1) = x0;

while norm(dHdu,'fro') > eps && i < max_num_iter
    
    % evolve x
    for j = 1:N
        x(:,j+1) = A(:,:,j)*x(:,j) + B(:,:,j)*u(:,j);
    end
    % calc total cost
    %J(i) = 0.5*(x(n+1:end)-r)*QQ*(x(n+1:end)-r)' + 0.5*u*RR*u';
    % evolve back lambda
    for j = N:-1:1
        lambda(:,j) = Q*(x(:,j+1) - r(:,j)) + A(:,:,j)'*lambda(:,j+1);
    end    
    % take a step
    for k = 1:N
        dHdu(:,k) = R*u(:,k) + B(:,:,k)'*lambda(:,k+1);
        H = @(u) deal((0.5*(x(:,k+1)-r(:,k))'*Q*(x(:,k+1)-r(:,k))) + ...
                 (0.5*u'*R*u) + (lambda(:,k+1)'*(A(:,:,k)*x(:,k+1)+B(:,:,k)*u)),...
                      R*u + B(:,:,k)'*lambda(:,k+1)); % grad
        alpha(k) = linesearch(H,-dHdu(:,k),u(:,k),0.5,10^(-4));
        u(:,k) = u(:,k) - alpha(k)*(dHdu(:,k));
    end
    i = i+1;
end
fprintf('Descended %d times.\n', i);
% if (verbose)
%     J %#ok
% end