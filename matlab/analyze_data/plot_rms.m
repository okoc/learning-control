% Plot rms error data for different adaptive ILC
% saved from C++

clc; clear; close all;

M = dlmread('../compare_adaptive_ilcs_rms.txt');
rms = zeros(11,5,3);
rms(:,:,1) = M(1:11,:);
rms(:,:,2) = M(12:22,:);
rms(:,:,3) = M(23:end,:);

rms_mean = sum(rms,2)/5;
rms_diff = rms - repmat(rms_mean,1,5,1);
rms_std = sqrt(1/4 * sum(rms_diff .* rms_diff,2));

figure;
hold on;
for i = 1:3
    errorbar(1:11,rms_mean(:,1,i),rms_std(:,1,i));
end
legend('Discrete','Continuous','Link')
hold off;