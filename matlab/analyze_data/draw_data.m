%% analyze real robot data

clc; clear; close all;
% load joints data
dyn.use_cpp = true; 
dyn.nom = 1; 
dyn.act = 3; %not needed here!
[wam,PD,Q0] = init_wam(dyn); 
plot_joints = 1; % 2 and 3 including in addition vel. and acc. resp.
%folder_name = '~/learning-control/data/2ndApril/'; 
%date = 'Mon Apr  2 12:37:19 2018'; %'2018:3:23:15:14';
folder_name = '~/learning-control/data/PONG-May5th/';
%date = 'Sat May  5 19:38:23 2018';
%date = 'Sat May  5 19:35:38 2018';
%date = 'Sat May  5 19:34:45 2018';
%date = 'Sat May  5 19:46:58 2018';
ndof = 7;
config_filename = [folder_name, 'config_', date, '.ini'];
config = ini2struct(config_filename)
num_ilc_iter = str2num(config.max_num_iter);
%num_ilc_iter = str2num(config.num_ilc_iter); % copy value from config_file
joint_filename = [folder_name, 'joints_', date, '.txt'];
M = dlmread(joint_filename);
N_ref = size(M,2)/(num_ilc_iter+1);

t = 0.002 * (1:N_ref*num_ilc_iter);
q_des = M(1:7,1:N_ref);
qd_des = M(8:14,1:N_ref);
qdd_des = M(15:end,1:N_ref);
ref = [q_des;qd_des];
% process q_act we only need striking part
Q_act = M(:,N_ref+1:end);
q_act = [];
qd_act = [];
qdd_act = [];
Q_iter = zeros(21,N_ref,num_ilc_iter);

rms = zeros(1,num_ilc_iter);
for i = 1:num_ilc_iter
    Q_iter(:,:,i) = Q_act(:,(i-1)*N_ref + (1:N_ref));
    q_act = [q_act, Q_iter(1:7,:,i)];
    qd_act = [qd_act, Q_iter(8:14,:,i)];
    qdd_act = [qdd_act, Q_iter(15:end,:,i)];
    q_diff = Q_iter(1:7,:,i) - q_des;
    qd_diff = Q_iter(8:14,:,i) - qd_des;
    qdd_diff = Q_iter(15:end,:,i) - qdd_des;
    rms(i) = norm([q_diff; qd_diff],'fro')/sqrt(2*ndof*N_ref);
    fin_cost(i) = norm([q_diff(:,end); qd_diff(:,end)]);
end
Q_des = repmat(q_des,1,num_ilc_iter);
Qd_des = repmat(qd_des,1,num_ilc_iter);
Qdd_des = repmat(qdd_des,1,num_ilc_iter);

if plot_joints == 3
    num_cols = 3;
elseif plot_joints == 2
    num_cols = 2;
elseif plot_joints == 1
    num_cols = 1;
end

% plot joint positions and velocities
figure;
for i = 1:ndof
    subplot(7,num_cols,(i-1)*num_cols + 1);
    if i == ndof
        xlabel('Time (s)');
    end
    plot(t,Q_des(i,:),'r',t,q_act(i,:),'b');
    ylabel(['$q_', num2str(i), '$'], 'Interpreter', 'latex');
    
    if plot_joints == 2
    subplot(7,2,(i-1)*num_cols + 2);
    plot(t,Qd_des(i,:),'r',t,qd_act(i,:),'b');    
    ylabel(['$\dot{q}_', num2str(i), '$'], 'Interpreter', 'latex');
    end
    
    if plot_joints == 3
        subplot(7,3,(i-1)*num_cols + 3);
        plot(t,Qdd_des(i,:),'r',t,qdd_act(i,:),'b');    
        ylabel(['$\ddot{q}_', num2str(i), '$'], 'Interpreter', 'latex');
    end
end

% plot iteration performance (rms error and final cost)
figure;
subplot(2,1,1);
plot(1:num_ilc_iter,rms);
legend('RMS traj. error');
subplot(2,1,2);
plot(1:num_ilc_iter,fin_cost);
legend('final cost');

%% Start ref. trajectory from actual positions

qf = ref(1:7,end);
qfdot = ref(8:end,end);
q0 = Q_iter(1:7,1,end);
q0dot = zeros(7,1);
dt = 0.002;
N = size(ref,2);
T = N*dt;
ref = calc_hit(dt,q0,q0dot,qf,qfdot,T);
ref = ref(1:14,:);

% remove some edges
%{
N = size(ref,2);
N_draw = N-6;
ref = ref(:,1:N_draw);
Q_iter = Q_iter(:,1:N_draw,:);
%}

%% plot cartesian positions
figure; % draw Cartesian output
y_des_cart = wam.kinematics(ref);
plot3(y_des_cart(1,:),y_des_cart(2,:),y_des_cart(3,:),'r--');
hold on;
grid on;
idx_draw = 1:num_ilc_iter;
for i = idx_draw
    y_cart = wam.kinematics(Q_iter(1:14,:,i));
    plot3(y_cart(1,:),y_cart(2,:),y_cart(3,:),'-');
end
xlabel('x');
ylabel('y');
zlabel('z');
labels_x{1} = 'desired';
for i = 1:num_ilc_iter
    labels_x{i+1} = ['trial ', int2str(i)];
end
if num_ilc_iter > 3
    legend(labels_x{1:4},'Location','northwest');
end

drawTimeIter = 40;
tLabel = t(1:drawTimeIter:N_ref);
precision = 4;
tLabelCell = num2cell(tLabel,precision);
for i = 1:length(tLabelCell)
    tLabelCell{i} = num2str(tLabelCell{i});
end
% annotate some of the ball positions
xDraw = y_des_cart(1,1:drawTimeIter:end);
yDraw = y_des_cart(2,1:drawTimeIter:end);
zDraw = y_des_cart(3,1:drawTimeIter:end);
%text(xDraw,yDraw,zDraw,tLabelCell)
scatter3(xDraw,yDraw,zDraw,20,'b','*');

%% Testing filtering

% filter_freq = 50;
% filter_perc = filter_freq/floor(N_ref/2);
% qtest = [q_des; qd_des]; %Q_iter(1:14,:,1);
% qfilt_own = filtfilt2(qtest,filter_perc);
% ytest = wam.kinematics(qtest);
% yfilt_own = wam.kinematics(qfilt_own);
% 
% figure;
% grid on; hold on;
% plot3(ytest(1,:),ytest(2,:),ytest(3,:),'k-');
% plot3(yfilt_own(1,:),yfilt_own(2,:),yfilt_own(3,:),'r-');
% if exist('filtfilt')
%     [B,A] = butter(2,filter_perc);
%     qfiltfilt = zeros(size(qtest,1),N_ref);
%     for i = 1:size(qtest,1)
%         qfiltfilt(i,:) = filtfilt(B,A,qtest(i,:));
%     end
%     yfiltfilt = wam.kinematics(qfiltfilt);
%     plot3(yfiltfilt(1,:),yfiltfilt(2,:),yfiltfilt(3,:),'b-');
%     legend('actual','myFiltfilt','matlabFiltFilt');
% else
%     legend('actual','myFiltfilt');
% end
% hold off;
