/**
 * @file ilc.cpp
 *
 * @brief ILC related functions (linearizing, discretizing, exponentiating etc.
 * are located here.
 *
 *  Created on: Aug 22, 2017
 *      Author: okoc
 */

#include <boost/program_options.hpp>
#include <armadillo>
#include "constants.h"
#include "traj.h"
#include "dynamics.h"
#include "filters.h"
#include "ilc_old.h"

using namespace arma;

/**
 * @brief Load ILC parameters and settings from config file.
 * @param config_file
 */
void ILC::load(const std::string & config_file) {

	namespace opt = boost::program_options;

    try {
		// Declare a group of options that will be
		// allowed in config file
		opt::options_description config("Configuration");
		config.add_options()
			("verbose", opt::value<bool>(&verbose), "verbosity on/off")
			("turn_on_filter", opt::value<bool>(&turn_on_filter),
					"Turn on zero phase filter")
			("use_lqr", opt::value<bool>(&use_fb),
					"Turn off SL feedback and add LQR feedback")
			("record_act_joints", opt::value<bool>(&save_joint_data),
					"Record actual joint data")
			("use_model", opt::value<bool>(&use_model),
					"Type of ILC: PD = 0, MODEL = 1")
		    ("ilc_iterations", opt::value<int>(&num_ilc_iter),
		    		"ILC iterations")
			("random_seed", opt::value<int>(&random_seed),
					"Random seed for generating trajectories")
			("downsample", opt::value<int>(&downsample),
					"Downsampling trajectories for model-based ILC")
			("step_length", opt::value<double>(&step_len),
					"Step length for model based ILC update")
			("pinv_tolerance", opt::value<double>(&pinv_tol),
					"Set tolerance for pseudoinverse based ILC (0 = use solve)")
			("p", opt::value<double>(&p),
					"Proportional gains for simple PD-ILC")
			("d", opt::value<double>(&d),
							"Derivative gains for simple PD-ILC")
			("relax_traj", opt::value<double>(&relax_traj),
					"Slow down trajectories")
			("cut_traj", opt::value<double>(&cut_traj),
					"Track some segment of strike")
			("cutoff_freq", opt::value<double>(&cutoff_freq),
					"Cutoff frequency for zero phase filter")
			("link_param_file", opt::value<std::string>(&link_param_file),
					"File to load link parameters");
        opt::variables_map vm;
        std::ifstream ifs(config_file.c_str());
        if (!ifs) {
            std::cout << "Can not open config file: " << config_file << "!!\n";
        }
        else {
        	std::cout << "Loaded config file options!\n";
            opt::store(parse_config_file(ifs, config), vm);
            notify(vm);
        }
    }
    catch (std::exception& e) {
        cout << e.what() << "\n";
    }

}

/**
 * @brief ILC initialization for REAL ROBOT experiments!
 *
 * Generates a random trajectory and initializes nominal
 * feedforward control commands for that trajectory.
 * Launched/detached as a separate thread
 */
void ILC::prepare(const traj & ref) {

	uff = ref.us;
	link link_param[NDOF+1];
	load_link_params(link_param_file, link_param);
	refs.push_back(ref);

	if (use_model) { // prepare model based
		mat F_nom = make_model(link_param,ref,downsample);
		learning_matrices.push_back(F_nom);
		if (verbose)
			cout << "Condition number of model matrix: " << cond(F_nom) << endl;
	}
	if (check_all_limits(ref))
		track_traj = true;
}

/**
 * @brief ILC update law is applied here
 *
 * IF model based update is turned on (use_model) then it calls model based update
 */
void ILC::update(const traj & traj_act) {

	trajs.push_back(traj_act);
	if (turn_on_filter) {
		filter_last_traj(traj_act);
	}
	if (use_model) { // apply model-based update
		model_based_update();
	}
	else { //apply pd-ilc
		mat e_pos = trajs.back().Q - refs.back().Q;
		mat e_vel = trajs.back().Qd - refs.back().Qd;
		uff -= p*e_pos + d*e_vel;
	}
	if (check_torque_limits(uff))
		track_traj = true;
}

/**
 * @brief Filter last trajectory with zero-phase 2nd order Butterworth filter.
 * @param traj_act
 */
void ILC::filter_last_traj(const traj & traj_act) {

	int num_elems = traj_act.Q.n_cols;
	int cutoff_perc = 100 * (cutoff_freq / (num_elems/2));
	trajs.back().Q = filtfilt2(trajs.back().Q,cutoff_perc);
	trajs.back().Qd = filtfilt2(trajs.back().Qd,cutoff_perc);
	//trajs.back().Qdd = filtfilt2(trajs.back().Qdd,cutoff_perc);
}

/**
 * @brief Apply model based ilc (pseudoinverse with certain tolerance)
 *
 * Only applying ILC on the striking segment of the whole reference trajectory.
 * The trajectory is downsampled and the corrections are made in downsampled space.
 * The total feedforward commands are then upsampled.
 *
 * @param traj Actual observed trajectories
 * @param ref Reference joint pos,vel,acc (and reference torques)
 * @param F The nominal linearized (downsampled) dynamics matrix
 * @param uff Feedforward controls to be updated
 */
void ILC::model_based_update() {

	static wall_clock clock;
	int N_full = refs.back().us.n_cols;
	int N_sample = N_full / downsample;
	vec correction = zeros<vec>(NDOF*N_sample);
	uvec idx = linspace<uvec>(downsample,N_sample*downsample,N_sample) - 1;

	// downsampling
	mat e = join_vert(trajs.back().Q.cols(idx) - refs.back().Q.cols(idx),
			          trajs.back().Qd.cols(idx) - refs.back().Qd.cols(idx));
	mat uff_ilc = uff.cols(idx);

	// applying ILC correction after vectorising
	clock.tic();
	if (pinv_tol == 0.0)
		correction = step_len * solve(learning_matrices.back(),vectorise(e));
	else
		correction = step_len * pinv(learning_matrices.back(),pinv_tol) * vectorise(e);
	if (verbose)
		cout << "ILC Inversion took " << clock.toc()*1000 << " ms.\n";

	// reshaping to undo vectorisation
	uff_ilc -= reshape(correction,NDOF,N_sample);

	// upsampling
	uff_ilc = upsample(uff_ilc,refs.back().Q.n_cols);
	uff.cols(0,N_full-1) = uff_ilc;
}

/**
 * @brief Upsampling the input matrix
 *
 * Armadillo does not support extrapolation (linear) so appending one element
 * in the beginning of the downsampled signal by reflecting the 2nd element around the first
 *
 * @param len Upsample total size
 * @param M downsampled signal
 * @return Upsampled signal
 */
mat upsample(mat & M,
		     int len) {

	int n = M.n_rows;
	int m = M.n_cols;
	int iter = len/m;
	vec v = zeros<vec>(len);
	mat out = zeros<mat>(n,len);
	vec tfull = linspace(1,len,len);
	vec origin = zeros<vec>(1);
	vec final = zeros<vec>(1);
	vec t = zeros<vec>(m+2);
	t(span(0,m)) = iter * linspace(0,m,m+1);
	t(m+1) = len;
	for (int i = 0; i < n; i++) {
		// reflect 2nd element around 1st element to get ``origin''
		origin(0) = 2*M(i,0) - M(i,1);
		final(0) = 2*M(i,m-1) - M(i,m-2);
		interp1(t,join_horiz(join_horiz(origin,M.row(i)),final).t(),tfull,v);
		out.row(i) = v.t();
	}
	return out;
}


/**
 * @brief Linearize, discretize and lift nominal plant dynamics around ref
 *
 * @param param The parameters of the dynamics model
 * @param ref Reference signal around which we linearize the dynamics
 * @param downsample Downsampling the reference signal
 *
 * @return Model matrix F
 */
mat make_model(const link param[NDOF+1],
		       const traj & ref,
			   const int downsample) {

	double dt = DT * downsample;
	cube A_nom, B_nom, Ad_nom, Bd_nom;
	linearize_wam_dynamics(param, ref, downsample,
			               1e-3, A_nom, B_nom);
	discretize_dynamics(dt,A_nom,B_nom,Ad_nom,Bd_nom);
	return lift_dynamics(Ad_nom,Bd_nom);
}

/**
 * @brief Linearize WAM dynamics around a reference trajectory
 *
 * Using 'TWO-SIDED-SECANT' to do a stable linearization
 * Ugly code to preserve simple syntax for forward dynamics
 *
 * @param link_param Link parameters for dynamics model
 * @param ref The reference trajectory around which we linearize WAM
 * @param downsample This is the 'DOWNSAMPLE' size for reference (to reduce inversion complexity)
 * @param h This is the stepsize used for linearization
 * @param A linearized LTV drift matrices around traj.
 * @param B linearized LTV control matrices around traj.
 */
void linearize_wam_dynamics(const link link_param[NDOF+1],
		                    const traj & ref,
		                    const int downsample,
							const double h,
		                    cube & A,
							cube & B) {

	int N = ref.Q.n_cols / downsample;
	joint qnow;
	A = zeros<cube>(2*NDOF,2*NDOF,N);
	B = zeros<cube>(2*NDOF,NDOF,N);
	mat df = zeros<mat>(NDOF,3*NDOF);
	mat qdd_plus = zeros<mat>(NDOF,3*NDOF);
	mat qdd_minus = zeros<mat>(NDOF,3*NDOF);
	for (int i = 1; i <= N; i++) {
		qnow.q = ref.Q.col(i*downsample - 1);
		qnow.qd = ref.Qd.col(i*downsample - 1);
		qnow.u = ref.us.col(i*downsample - 1);
		for (int j = 0; j < NDOF; j++) {
			qnow.q(j) += h; // modifying joint pos
			barrett_wam_dynamics_art(link_param,qnow);
			qdd_plus.col(j) = qnow.qdd;
			qnow.q(j) -= 2*h;
			barrett_wam_dynamics_art(link_param,qnow);
			qdd_minus.col(j) = qnow.qdd;
			qnow.q(j) += h;

			qnow.qd(j) += h; // modifying joint vel
			barrett_wam_dynamics_art(link_param,qnow);
			qdd_plus.col(j+NDOF) = qnow.qdd;
			qnow.qd(j) -= 2*h;
			barrett_wam_dynamics_art(link_param,qnow);
			qdd_minus.col(j+NDOF) = qnow.qdd;
			qnow.qd(j) += h;

			qnow.u(j) += h; // modifying joint inputs
			barrett_wam_dynamics_art(link_param,qnow);
			qdd_plus.col(j+2*NDOF) = qnow.qdd;
			qnow.u(j) -= 2*h;
			barrett_wam_dynamics_art(link_param,qnow);
			qdd_minus.col(j+2*NDOF) = qnow.qdd;
			qnow.u(j) += h;
		}
		df = (qdd_plus - qdd_minus) / (2*h);
		A.slice(i-1) = join_vert(join_horiz(zeros<mat>(NDOF,NDOF), eye<mat>(NDOF,NDOF)),
				               df.cols(0,2*NDOF-1));
		B.slice(i-1) = join_vert(zeros<mat>(NDOF,NDOF),df.cols(2*NDOF,3*NDOF-1));
	}
}

/**
 * @brief Discretize continuous matrices A,B for dt time step
 * @param dt time step
 * @param A Continuous drift matrices
 * @param B Continuous control matrices
 * @param Ad Output of discretization for drift term
 * @param Bd Output of discretization for control matrix
 */
void discretize_dynamics(const double dt,
		                 const cube & A,
						 const cube & B,
						 cube & Ad,
						 cube & Bd) {
	int N = A.n_slices;
	int n = A.n_cols;
	int m = B.n_cols;

	if (B.n_slices != N || A.n_rows != n || B.n_rows != n) {
		throw "Dimension of matrices are not consistent!";
	}

	Ad = zeros<cube>(n,n,N);
	Bd = zeros<cube>(n,m,N);
	mat M;
	for (int i = 0; i < N; i++) {
		M = join_vert(join_horiz(A.slice(i), B.slice(i)), zeros<mat>(m,n+m));
		M = expmat(dt * M);
		Ad.slice(i) = M(span(0,n-1),span(0,n-1));
		Bd.slice(i) = M(span(0,n-1),span(n,n+m-1));
	}
}

/**
 * @brief Lift the discretized dynamics matrices A(t), B(t) for each t
 * to form one big input-to-output plant matrix F
 */
mat lift_dynamics(const cube & A,
		          const cube & B) {

	int N = A.n_slices;
	int n = A.n_cols;
	int m = B.n_cols;

	if (B.n_slices != N || A.n_rows != n || B.n_rows != n) {
		throw "Dimension of matrices not consistent!";
	}

	mat F = zeros<mat>(N*n,N*m);
	mat M = zeros<mat>(n,m); // temporary matrix

	for (int i = 0; i < N; i++) {
		M = B.slice(i);
		for (int j = i; j < N-1; j++) {
			F(span(j*n,(j+1)*n-1),span(i*m,(i+1)*m - 1)) = M;
			M = A.slice(j+1) * M;
		}
		F(span((N-1)*n,N*n-1),span(i*m,(i+1)*m - 1)) = M;
	}
	return F;
}
