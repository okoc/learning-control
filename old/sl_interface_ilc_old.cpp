/**
 *
 * @file sl_interface_ilc.cpp
 *
 * @brief Interface of the ILC library to the SL real-time simulator and to
 * the robot.
 *
 *  Created on: Aug 23, 2017
 *      Author: okoc
 */

#include <armadillo>
#include <thread>
#include <chrono>
#include <string>
#include <iostream>
#include <fstream>
#include <mutex>
#include "constants.h"
#include "traj.h"
#include "dynamics.h"
#include "experiment_old.h"
#include "ilc_old.h"

using namespace arma;

/** The data structures from SL */

/**
 * @brief (actual) joint space state for each DOF
 */
struct SL_Jstate {
	double   th;   /*!< theta */
	double   thd;  /*!< theta-dot */
	double   thdd; /*!< theta-dot-dot */
	double   ufb;  /*!< feedback portion of command */
	double   u;    /*!< torque command */
	double   load; /*!< sensed torque */
};

/**
 * @brief (desired) joint space state commands for each DOF
 */
struct SL_DJstate { /*!< desired values for controller */
	double   th;   /*!< theta */
	double   thd;  /*!< theta-dot */
	double   thdd; /*!< theta-dot-dot */
	double   uff;  /*!< feedforward torque command */
	double   uex;  /*!< externally imposed torque */
};

Experiment exper = Experiment();

#include "sl_interface_ilc_old.h"

/**
 * @brief Set algorithm and options to initialize ILC with.
 *
 * The global variable RESET is set to true and
 * the experiment() function will use it to prepare ILC experiments.
 *
 */
void load_ilc_options() {

	exper.reset = true;
	const std::string home = std::getenv("HOME");
	const std::string config_file = home + "/learning-control/" + "ilc.cfg";
	exper.ilc.load(config_file);
}

/**
 * @brief Saves actual/desired joint data if save flag is set to TRUE
 *
 * If trajectory is being tracked
 * saves the time elapsed (sec), joint actual pos, vel, and
 * joint des pos, vel.
 *
 * Only appends during the robot operation. Otherwise will discard ilc.
 *
 */
static void save_joint_data(const SL_Jstate joint_state[NDOF+1],
		                    const SL_DJstate joint_des_state[NDOF+1]) {

	static std::ofstream stream;
	static const std::string home = std::getenv("HOME");
	static const std::string joint_file = home + "/learning-control/data/joints.txt";
	static vec joint_act = zeros<vec>(2*NDOF);
	static vec joint_des = zeros<vec>(2*NDOF);
	static bool firsttime = true;

	if (firsttime) {
		stream.open(joint_file,std::ofstream::out);
		firsttime = false;
	}

	for (int i = 0; i < NDOF; i++) {
		joint_act(i) = joint_state[i+1].th;
		joint_act(i+NDOF) = joint_state[i+1].thd;
		joint_des(i) = joint_des_state[i+1].th;
		joint_des(i+NDOF) = joint_des_state[i+1].thd;
	}

	if (exper.ilc.save_joint_data && exper.ilc.track_traj) {
		if (stream.is_open()) {
			stream << join_vert(joint_des,joint_act).t();
		}
		else {
			//cout << "OPENING STREAM!\n";
			stream.open(joint_file,std::ofstream::out | std::ofstream::app);
			stream << join_vert(joint_des,joint_act).t();
			//cout << "TIME ELAPSED: " << clock_t.toc() << "\n";
 		}
	}
	else {
		stream.close();
	}
}

/**
 * @brief C-Interface to the robot for making ILC experiments
 * @param joint_state Actual joint states coming from sensors
 * @param joint_des_state Desired joint states commanded
 */
void experiment(const SL_Jstate joint_state[NDOF+1], SL_DJstate joint_des_state[NDOF+1]) {

	static std::mutex mtx;
	static link link_param[NDOF+1];
	static joint rest_posture;
	static vec7 ufb = zeros<vec>(NDOF);
	static int idx_traj;
	static int idx_ilc_iter;

	if (exper.reset) {
		load_link_params("LinkParameters.cfg",link_param);
		idx_traj = 0;
		idx_ilc_iter = 0;
		for (int i = 0; i < NDOF; i++) {
			exper.qrest(i) = joint_state[i+1].th;
		}
		rest_posture.q = exper.qrest;
		barrett_wam_inv_dynamics_ne(link_param,rest_posture);
		exper.qact.q = exper.qrest;
		exper.prepare(true);
		exper.reset = false;
	}

	save_joint_data(joint_state,joint_des_state);

	mtx.lock();
	// observe state
	for (int i = 0; i < NDOF; i++) {
		exper.qact.q(i) = joint_state[i+1].th;
		exper.qact.qd(i) = joint_state[i+1].thd;
		exper.qact.qdd(i) = joint_state[i+1].thdd;
	}
	mtx.unlock();


	// track traj
	if (exper.ilc.track_traj) {
		if (idx_traj < exper.strike.Q.n_cols) {
			if (exper.ilc.use_lqr) {
				ufb = (exper.fb_pd-exper.fb_lqr) * (join_vert(exper.qact.q - exper.strike.Q.col(idx_traj),
						                          exper.qact.qd - exper.strike.Qd.col(idx_traj)));
			}
			for (int i = 0; i < NDOF; i++) {
				joint_des_state[i+1].th = exper.strike.Q(i,idx_traj);
				joint_des_state[i+1].thd = exper.strike.Qd(i,idx_traj);
				joint_des_state[i+1].thdd = exper.strike.Qdd(i,idx_traj);
				joint_des_state[i+1].uff = exper.ilc.uff(i,idx_traj) + ufb(i);
			}

			// save the actual joint data
			exper.strike_act.fill(exper.qact,idx_traj);
		}
		else if (idx_traj < (exper.strike.Q.n_cols + exper.ret.Q.n_cols)) {
			for (int i = 0; i < NDOF; i++) {
				joint_des_state[i+1].th = exper.ret.Q(i,idx_traj - exper.strike.Q.n_cols);
				joint_des_state[i+1].thd = exper.ret.Qd(i,idx_traj - exper.strike.Q.n_cols);
				joint_des_state[i+1].thdd = exper.ret.Qdd(i,idx_traj - exper.strike.Q.n_cols);
				joint_des_state[i+1].uff = exper.ret.us(i,idx_traj - exper.strike.Q.n_cols);
			}
		}
		else {
			exper.ilc.track_traj = false;
			for (int i = 0; i < NDOF; i++) {
				joint_des_state[i+1].th = joint_state[i+1].th;
				joint_des_state[i+1].thd = 0.0;
				joint_des_state[i+1].thdd = 0.0;
				joint_des_state[i+1].uff = rest_posture.u(i);
			}
			if (exper.ilc.verbose) {
				cout << "RMS error on strike: "
					 << calc_rms_error(exper.strike_act,exper.strike) << endl;
			}

			if (idx_ilc_iter < exper.ilc.num_ilc_iter) { //launch ilc
				exper.update(true);
				idx_traj = 0;
				idx_ilc_iter++;
			}
		}
		idx_traj++;
	}
}
