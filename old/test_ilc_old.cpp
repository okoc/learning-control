/**
 * @file test_ilc.cpp
 *
 * @brief Testing all sorts of ILC algorithms here.
 *
 *  Created on: Aug 16, 2017
 *      Author: okoc
 */

#include <boost/test/unit_test.hpp>
#include <boost/test/data/test_case.hpp>
#include <boost/test/data/monomorphic.hpp>
#include <armadillo>
#include "constants.h"
#include "traj.h"
#include "dynamics.h"
#include "ilc_old.h"

using namespace arma;
using namespace boost::unit_test;
namespace data = boost::unit_test::data;

void test_poly();
void test_filtfilt();
void test_inverse_dynamics();
void test_forward_dynamics();
void test_dynamics_cancellation();
void test_tracking_with_inv_dynamics();
void test_wam_pd_ilc();
void test_monotonicity_of_model_based_ilc();
void test_wam_model_based_ilc();

void check_learning_stability(const mat & nom, const mat & act);
mat perturb_matrix(const mat & M, double thresh);
void batch_model_based_ilc(const traj & act_traj,
		                   const traj & strike,
		                   const mat & F,
						   const int DOWNSAMPLE,
						   mat & uff);

/**
 * @brief Main function for boost unit testing.
 *
 * Unit tests are added here.
 */
test_suite* init_unit_test_suite( int /*argc*/, char* /*argv*/[] ) {


	test_suite* ts = BOOST_TEST_SUITE( "test_suite" );
	ts->add( BOOST_TEST_CASE( &test_poly ) );
	ts->add( BOOST_TEST_CASE( &test_filtfilt ));
	ts->add( BOOST_TEST_CASE( &test_forward_dynamics ));
	ts->add( BOOST_TEST_CASE( &test_inverse_dynamics ));
	ts->add( BOOST_TEST_CASE( &test_dynamics_cancellation ));
	ts->add( BOOST_TEST_CASE( &test_tracking_with_inv_dynamics ));
	ts->add( BOOST_TEST_CASE( &test_wam_pd_ilc ) );
	ts->add( BOOST_TEST_CASE( &test_monotonicity_of_model_based_ilc ) );
	ts->add( BOOST_TEST_CASE( &test_wam_model_based_ilc ) );

    //framework::master_test_suite().add( ts );
    return ts;
}

/**
 * @brief Testing ILC methods on Barrett WAM
 *
 * Refactored and general form of test_recursive_ilc_wam().
 * Settings are loaded by ILC class from a config file.
 */
void test_ilc_link_adaptive_wam() {

    BOOST_TEST_MESSAGE(
            "Testing ILC methods based on config file on Barrett WAM");
    //arma_rng::set_seed_random();
    arma_rng::set_seed(10);
    // Initialize ILC
    int m = NDOF;
    int n = 2 * NDOF;
    vec7 qrest;
    init_posture(1, qrest); // 0 = center, 1 = right, 2 = left
    joint qinit;
    qinit.q = qrest;
    traj strike, returning;
    gen_random_tabletennis_traj(qinit, qrest, 1.0, strike, returning);
    int N = strike.Q.n_cols;
    dyn::link link_param_nom[NDOF + 1];
    load_link_params("LinkParameters2.cfg", link_param_nom);
    // generate inv. dyn inputs
    calc_inv_dynamics_for_traj(barrett_wam_inv_dynamics_ne, link_param_nom,
                                strike);
    ILC ilc = ILC(barrett_wam_dynamics_art, link_param_nom, strike);
    BOOST_TEST_MESSAGE("Initialized ILC...");

    // initialize Experiment
    BOOST_TEST_MESSAGE("Initializing experiments...");

    calc_inv_dynamics_for_traj(barrett_wam_inv_dynamics_ne, link_param_nom,
                                returning);
    mat pd = zeros<mat>(m, n);
    load_PD_feedback_gains(pd);
    int N_ret = returning.Q.n_cols;
    cube Kpd = zeros<cube>(m, n, N_ret);
    Kpd.each_slice() = pd;
    cube Ktotal = join_slices(ilc.K, Kpd);

    // perform Experiment
    dyn::link link_param_act[NDOF + 1];
    load_link_params("LinkParameters1.cfg", link_param_act);
    mat uff = join_horiz(ilc.uff, returning.us);
    traj traj_act = strike;
    evolve(barrett_wam_dynamics_art, link_param_act, strike, uff, Ktotal, qinit,
            traj_act);
    int num_iter = 10;
    vec rms = zeros<vec>(num_iter + 1);
    rms(0) = calc_rms_error(traj_act, strike, N);

    BOOST_TEST_MESSAGE("Entering ILC loop...");

    for (unsigned j = 0; j < num_iter; j++) {
        ilc.update(traj_act);
        //BOOST_TEST_MESSAGE("ILC computation...");
        uff = join_horiz(ilc.uff, returning.us);
        evolve(barrett_wam_dynamics_art, link_param_act, strike, uff, Ktotal,
                qinit, traj_act);
        rms(j + 1) = calc_rms_error(traj_act, strike, N);
    }

    BOOST_TEST_MESSAGE("RMS of tracking errors for ILC: \n" << rms.t());
    BOOST_TEST(all(diff(rms) < 0.0));
}

/**
 * @brief Major Test script testing recursive ILC on Barrett WAM.
 */
void test_recursive_ilc_wam() {

    BOOST_TEST_MESSAGE("Testing recursive ILC on Barrett WAM");

    //arma_rng::set_seed_random();
    arma_rng::set_seed(20);
    bool adaptive = false;
    unsigned m = NDOF;
    unsigned n = 2 * NDOF;
    unsigned num_exp = 5;
    unsigned num_iter = 10;
    mat rms = zeros<mat>(num_exp, num_iter + 1);

    BOOST_TEST_MESSAGE("Initializing Barrett WAM dynamics...");
    vec7 qrest;
    init_posture(1, qrest); // 0 = center, 1 = right, 2 = left
    joint qinit;
    qinit.q = qrest;
    dyn::link link_param_nom[NDOF + 1];
    dyn::link link_param_act[NDOF + 1];
    const std::string filename_nom = "LinkParameters2.cfg";
    const std::string filename_act = "LinkParameters1.cfg";
    load_link_params(filename_nom, link_param_nom);
    load_link_params(filename_act, link_param_act);
    //BOOST_TEST_MESSAGE("Loading PD gains from file...");
    mat pd = zeros<mat>(m, n);
    load_PD_feedback_gains(pd);

    for (unsigned i = 0; i < num_exp; i++) {

        BOOST_TEST_MESSAGE("Experiment " << i+1);
        // lookup random strike
        traj strike, returning;
        gen_random_tabletennis_traj(qinit, qrest, 1.0, strike, returning);
        int N = strike.Q.n_cols;
        int N_ret = returning.Q.n_cols;

        // generate inv. dyn inputs
        calc_inv_dynamics_for_traj(barrett_wam_inv_dynamics_ne, link_param_nom,
                                    strike);
        calc_inv_dynamics_for_traj(barrett_wam_inv_dynamics_ne, link_param_nom,
                                    returning);
        BOOST_TEST_MESSAGE("Checking pos,vel,acc,torque limits for traj.");
        check_all_limits(strike);
        check_all_limits(returning);

        // linearize system dynamics
        cube A, B, Ad, Bd;
        linearize_dynamics(barrett_wam_dynamics_art, link_param_nom, strike, 1,
                            1e-3, A, B);
        discretize_dynamics(DT, A, B, Ad, Bd);
        /*int downsample = 10;
         linearize_dynamics(barrett_wam_dynamics_art,link_param_nom,strike,
         downsample,1e-3,A_nom,B_nom);
         discretize_dynamics(DT * downsample,A_nom,B_nom,Ad_nom,Bd_nom);
         mat F_nom = lift_dynamics(Ad_nom,Bd_nom);*/

        // generate lqr fb
        BOOST_TEST_MESSAGE("Generating LQR feedback to track traj.");
        cube Klqr = zeros<cube>(m, n, N);
        cube Kpd = zeros<cube>(m, n, N_ret);
        Kpd.each_slice() = pd;

        cube P = zeros<cube>(n, n, N + 1);
        mat Q = eye(n, n);
        cube Qs = zeros<cube>(n, n, N + 1);
        Qs.each_slice() = eye(n, n);
        mat R = 0.01 * eye(m, m);
        lqr_fin_horizon_ltv(Ad, Bd, Q, Q, R, Klqr, P);
        cube Ktotal = join_slices(Klqr, Kpd);

        // evolve system
        traj ref = strike;
        ref.join(returning);
        mat uff = ref.us;
        mat uff0 = uff;
        traj traj_act = ref;
        evolve(barrett_wam_dynamics_art, link_param_act, ref, uff, Ktotal,
                qinit, traj_act);
        traj traj_act_last = traj_act;
        mat ufull_last = traj_act.us;
        mat e_pos = traj_act.Q.cols(0, N - 1) - strike.Q;
        mat e_vel = traj_act.Qd.cols(0, N - 1) - strike.Qd;
        mat e_last = join_vert(e_pos, e_vel);
        mat e = e_last;
        rms(i, 0) = norm(e, "fro") / sqrt(e.n_elem);

        // create covariances for LBR/Broyden adaptation
        BOOST_TEST_MESSAGE("Initializing covariances of A,B mats...");
        cube Sigmas = zeros<cube>(n * (n + m), n * (n + m), N);
        double prec = 1e4;
        Sigmas.each_slice() = (1 / prec) * eye(n * (n + m), n * (n + m));

        BOOST_TEST_MESSAGE("Entering ILC loop...");

        for (unsigned j = 0; j < num_iter; j++) {

            // apply ILC

            //BOOST_TEST_MESSAGE("ILC computation...");
            recursive_ilc(Ad, Bd, P, Qs, R, e, uff);
            //recursive_cautious_ilc(Ad,Bd,Sigmas,Qs,R,e,Klqr,uff);
            //batch_ilc(e,F_nom,downsample,uff);

            BOOST_TEST(check_torque_limits(uff));

            // evolve system
            evolve(barrett_wam_dynamics_art, link_param_act, ref, uff, Ktotal,
                    qinit, traj_act);

            e_pos = traj_act.Q.cols(0, N - 1) - strike.Q;
            e_vel = traj_act.Qd.cols(0, N - 1) - strike.Qd;
            e_last = e;
            e = join_vert(e_pos, e_vel);

            // update A,B matrices if adaption is turned on
            if (adaptive) {
                mat du = traj_act.us - ufull_last;
                mat de = e - e_last;
                ufull_last = traj_act.us;
                double s2 = 0.001;
                LTV ltv;
                ltv.Ad = Ad;
                ltv.Bd = Bd;
                ltv.Sigmas = Sigmas;
                est_ltv_models_broyden(du, de, ltv);
                // NOTE: this will break if Gamma is not initialized
                //est_ltv_models_lbr(du,de,s2,Sigmas,Ad,Bd);

                // (optional) apply trust region weighting
                //trust_region_weighting(Ad_nom,Bd,Klqr,uff-uff0,qinit,
                //                     traj_act,traj_act_last,strike,Qs);
                traj_act_last = traj_act;
            }

            // calculate error along traj
            rms(i, j + 1) = norm(e, "fro") / sqrt(e.n_elem);
        }
    }
    // show or print to file for each experiment rms results
    BOOST_TEST_MESSAGE("RMS of tracking errors for ILC: \n" << rms);
    BOOST_TEST_MESSAGE("Checking for (mean of) improvement...");
    BOOST_TEST(mean(rms.col(num_iter-1)) < mean(rms.col(0)));
    BOOST_TEST_MESSAGE("Checking for (mean) monotonicity...");
    mat diff_rms = mean(diff(rms, 1, 1), 0);
    rowvec diff_row_rms = diff_rms.row(0);
    BOOST_TEST(all(diff_row_rms < 0.0));
}

/**
 * @brief Testing the simplest PD ILC.
 *
 * Testing PD ILC on Barrett WAM with
 * initial control inputs computed with inverse dynamics.
 * PD-feedback is applied in addition.
 */
void test_wam_pd_ilc() {

	arma_rng::set_seed_random();
	ILC ilc = ILC();
	ilc.num_ilc_iter = 10;
	ilc.use_model = false;
	ilc.p = 1.0;
	ilc.d = 0.1;
	BOOST_TEST_MESSAGE("Testing simple PD ILC with p = " << ilc.p << " and d = " << ilc.d);

	link link_param[NDOF+1];
	load_link_params("LinkParameters.cfg",link_param);
	//generate random reference trajectory (batch)
	vec7 qrest;
	init_right_posture(qrest);
	joint qinit;
	qinit.q = qrest;
	BOOST_TEST_MESSAGE("Loading a random striking trajectory.");
	traj strike, ret;
	gen_random_tabletennis_traj(qinit, qrest, 1.0, strike, ret);
	calc_inv_dynamics_for_traj(link_param,strike);

	BOOST_TEST_MESSAGE("Checking pos,vel,acc,torque limits for traj.");
	BOOST_TEST(check_all_limits(strike));
	BOOST_TEST_MESSAGE("Loading PD gains from file...");
	mat fb = zeros<mat>(NDOF,2*NDOF);
	load_PD_feedback_gains(fb);

	ilc.prepare(strike);
	BOOST_TEST(ilc.track_traj);
	traj traj_act;
	vec rms = zeros<vec>(ilc.num_ilc_iter);

	for (int i = 0; i < ilc.num_ilc_iter; i++) {

		// track traj.
		traj_act = evolve(link_param,strike,ilc.uff,fb,qinit);

		// apply ILC
		ilc.update(traj_act);
		BOOST_TEST(ilc.track_traj);

		rms(i) = calc_rms_error(traj_act,strike);
	}

	BOOST_TEST_MESSAGE("RMS of tracking errors for ILC: \n" << rms.t());
	BOOST_TEST(rms(ilc.num_ilc_iter-1) < rms(0));
	BOOST_TEST_MESSAGE("Checking for monotonicity...");
	BOOST_TEST(all(diff(rms,1) < 0.0));
}

/**
 * @brief Testing convergence of model based ilc for Barrett WAM.
 *
 * Using random table tennis striking movements.
 *
 * TODO: refactor to include ilc classes
 */
void test_wam_model_based_ilc() {

	arma_rng::set_seed_random();
	//arma_rng::set_seed(2);

	int DOWNSAMPLE = 10;
	int num_iter = 10;
	double time_return = 1.0;
	mat fb = zeros<mat>(NDOF,2*NDOF);
	vec rms = zeros<vec>(num_iter);
	joint qinit;
	vec7 qrest;
	traj strike, returning, traj_act;
	mat u_ff = zeros<mat>(NDOF,1);
	mat e_pos = zeros<mat>(NDOF,1);
	mat e_vel = zeros<mat>(NDOF,1);
	BOOST_TEST_MESSAGE("Testing model based ILC on Barrett WAM...");

	//generate random reference trajectory (batch)
	init_right_posture(qrest);
	qinit.q = qrest;
	gen_random_tabletennis_traj(qinit, qrest, time_return,strike,returning);

	link link_param_nom[NDOF+1];
	link link_param_act[NDOF+1];
	const std::string filename_nom = "LinkParametersNom.cfg";
	const std::string filename_act = "LinkParameters.cfg";
	load_link_params(filename_nom,link_param_nom);
	load_link_params(filename_act,link_param_act);
	int lqr_version = 5;
	BOOST_TEST_MESSAGE("Loading LQR matrix " << lqr_version << " from file...");
	load_LQR_feedback(fb,lqr_version);
	//BOOST_TEST_MESSAGE("Loading PD gains from file...");
	//load_PD_feedback_gains(fb);
	calc_inv_dynamics_for_traj(link_param_nom,strike);
	calc_inv_dynamics_for_traj(link_param_nom,returning);
	BOOST_TEST_MESSAGE("Checking pos,vel,acc,torque limits for traj.");
	check_all_limits(strike);
	check_all_limits(returning);

	// discretize and lift nominal dynamics
	double dt = DT * DOWNSAMPLE;
	cube A_nom, B_nom, Ad_nom, Bd_nom;
	BOOST_TEST_MESSAGE("ILC preparations to form big model matrix");
	BOOST_TEST_MESSAGE("Downsampling, Linearizing, discretizing, lifting...");
	linearize_wam_dynamics(link_param_nom,strike, DOWNSAMPLE, 1e-3, A_nom, B_nom);
	discretize_dynamics(dt,A_nom,B_nom,Ad_nom,Bd_nom);
	mat F_nom = lift_dynamics(Ad_nom,Bd_nom);
	BOOST_TEST_MESSAGE("Size of model learning matrix: "
			            << F_nom.n_rows << " x " << F_nom.n_cols);

	// join strike and return in one big traj
	traj ref = strike.join(returning);
	u_ff = ref.us;
	int N_strike = strike.Q.n_cols;
	BOOST_TEST_MESSAGE("Length of strike: " << N_strike);
	for (int i = 0; i < num_iter; i++) {

		// track traj.
		traj_act = evolve(link_param_act,ref,u_ff,fb,qinit);

		// apply ILC only on strike
		batch_model_based_ilc(traj_act,strike,F_nom,DOWNSAMPLE,u_ff);
		check_torque_limits(u_ff);

		e_pos = traj_act.Q.cols(0,N_strike-1) - strike.Q;
		e_vel = traj_act.Qd.cols(0,N_strike-1) - strike.Qd;
		rms(i) = norm(join_horiz(e_pos, e_vel),"fro");
		rms(i) /= sqrt(2*e_pos.n_elem);
	}
	BOOST_TEST_MESSAGE("RMS of tracking errors for ILC: \n" << rms.t());
	BOOST_TEST(rms(num_iter-1) < rms(0));
	BOOST_TEST_MESSAGE("Checking for monotonicity...");
	vec diff_rms = diff(rms,1);
	BOOST_TEST(all(diff_rms < 0.0));
}

/**
 * @brief Testing monotonicity property of model based ilc on a random system.
 *
 * Script is taken from MATLAB.
 */
void test_monotonicity_of_model_based_ilc() {

	BOOST_TEST_MESSAGE("Testing model based ILC on random systems...");
	arma_rng::set_seed_random();
	double tol = 0.01; // tolerance of pseudo-inverse
	int n = 2; // dim of x
	int m = 2; // dim of u
	double T = 1.0; // final time
	int K = 10; // number of ILC iterations
	int N = 50; // number of steps
	double dt = T/N; // time step
	vec rms = zeros<vec>(K);
	cube A_nom = randn<cube>(n,n,N);
	cube B_nom = randn<cube>(n,m,N);
	cube Ad_nom, Bd_nom;

	// discretize and lift nominal dynamics
	discretize_dynamics(dt,A_nom,B_nom,Ad_nom,Bd_nom);
	mat F_nom = lift_dynamics(Ad_nom,Bd_nom);
	vec sigmas = svd(F_nom);
	double thresh = 10 * sigmas(N*m-1); // smallest singular value of F_nom
	mat F_act = perturb_matrix(F_nom, thresh);

	check_learning_stability(F_nom,F_act);

	// generate a ramp reference
	vec refvec = zeros<vec>(n*N);
	vec evec = zeros<vec>(n*N);
	vec uvec = zeros<vec>(m*N);
	mat ref = zeros<mat>(n,N);
	ref.row(0) = 1 * linspace(0,N,N).t();
	ref.row(1) = 2 * linspace(0,N,N).t();
	refvec = vectorise(ref);

	for (int i = 0; i < K; i++) {
		// observe errors
		evec = F_act * uvec - refvec;
		// apply ILC
		uvec -= pinv(F_nom,tol) * evec;
		rms(i) = norm(evec)/sqrt(evec.n_elem);
	}
	BOOST_TEST_MESSAGE("RMS of errors should decrease!\n" << rms.t());
	BOOST_TEST(rms(K-1) < rms(0));
	BOOST_TEST_MESSAGE("Checking for monotonicity...");
	BOOST_TEST(all(diff(rms,1) <= 1e-4));

}

/**
 * @brief Function that checks for learning stability
 *
 * Monotonic convergence and asymptotic stability of ILC is checked.
 * @param nom
 * @param act
 */
void check_learning_stability(const mat & nom, const mat & act) {

	int n = nom.n_rows;
	int m = nom.n_cols;
	mat E = act - nom;
	vec sigmas = svd(nom);
	double sigma_min = sigmas(sigmas.n_elem-1);
	mat L = pinv(nom);
	mat M = eye<mat>(m,m) - L*act;
	cx_vec eigvals = eig_gen(M);
	if (norm(E) < sigma_min || abs(max(eigvals)) < 1) {
		BOOST_TEST_MESSAGE("Model is asymptotically stable!");
	}
	else {
		BOOST_TEST_MESSAGE("Model is NOT asymptotically stable!");
	}
	mat M2 = eye<mat>(n,n) - act*L;
	if (norm(M2) < 1) {
		BOOST_TEST_MESSAGE("Model is monotonically convergent!");
	}
	else {
		BOOST_TEST_MESSAGE("Model may not be monotonically convergent!");
	}
}

/**
 * @brief Perturb given matrix
 *
 * Perturb matrix with a random Gaussian lower triangular matrix
 * whose 2-norm is less than the given threshold
 */
mat perturb_matrix(const mat & M, double thresh) {

	int n = M.n_rows;
	int m = M.n_cols;
	mat E = randn<mat>(n,m); // noise matrix
	E = trimatl(E);
	int scale = (int)(norm(E)/thresh) + 1;
	return M + E/scale;
}

/**
 * @brief Batch model based ILC using direct lifted form.
 *
 * Model-based norm-optimal batch ILC, using direct lifted-vector form.
 * The implementation uses pseudoinverse with const set tolerance.
 *
 * Only applying ILC on the striking segment of the whole reference trajectory.
 * The trajectory is downsampled and the corrections are made in downsampled space.
 * The total feedforward commands are then upsampled.
 *
 * @param traj Actual observed trajectories
 * @param ref Reference joint pos,vel,acc (and reference torques)
 * @param F The nominal linearized (downsampled) dynamics matrix
 * @param uff Feedforward controls to be updated
 */
void batch_model_based_ilc(const traj & act_traj,
		                   const traj & strike,
		                   const mat & F,
						   const int DOWNSAMPLE,
						   mat & uff) {

	static wall_clock clock;
	const double alpha = 0.5;
	const int N_sample = strike.Q.n_cols / DOWNSAMPLE;
	const double tolerance = 0.01;
	uvec idx = linspace<uvec>(DOWNSAMPLE,N_sample*DOWNSAMPLE,N_sample) - 1;

	// downsampling
	mat e = join_vert(act_traj.Q.cols(idx) - strike.Q.cols(idx), act_traj.Qd.cols(idx) - strike.Qd.cols(idx));
	mat uff_ilc = uff.cols(idx);

	// applying ILC correction after vectorising
	clock.tic();
	//cout << "Condition number of mat: " << cond(F) << endl;
	vec correction = alpha * solve(F,vectorise(e));
	//vec correction = alpha * pinv(F,tolerance) * vectorise(e);
	cout << "ILC Inversion took " << clock.toc()*1000 << " ms.\n";

	// reshaping to undo vectorisation
	uff_ilc -= reshape(correction,NDOF,N_sample);

	// upsampling
	uff_ilc = upsample(uff_ilc,strike.Q.n_cols);
	uff.cols(0,strike.Q.n_cols-1) = uff_ilc; // hopefully this will check out!
	check_torque_limits(uff);

}
