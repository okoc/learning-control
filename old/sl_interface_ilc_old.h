/**
 * @file sl_interface_ilc.h
 *
 * @brief Interface to SL. Exposes experiment() for ILC experiments.
 *
 */

#ifndef SL_INTERF_ILC_H
#define SL_INTERF_ILC_H

#ifdef __cplusplus
extern "C" {
#endif

// Interface for Player
extern void experiment(const SL_Jstate joint_state[], SL_DJstate joint_des_state[]);
extern void reset_experiments(const SL_Jstate joint_state[]);

#ifdef __cplusplus
} // extern "C"
#endif

#ifdef __cplusplus

// internal C++ functions
static void save_joint_data(const SL_Jstate joint_state[],
		                    const SL_DJstate joint_des_state[]);

#endif

#endif /* SL_INTERF_ILC_H */
