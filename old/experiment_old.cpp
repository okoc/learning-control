/*! \mainpage Learning Control for Table Tennis
 *
 * \section intro_sec Introduction
 *
 * Welcome to Learning Control applied to Table Tennis trajectories!
 * This repository contains Learning Control algorithms
 * applied to Table Tennis striking trajectories for Barrett WAM.
 *
 * For your particular robot, you have to define/copy
 * forward and inverse dynamics functions (as well as DT, NDOF vars).
 *
 *
 * \section install_sec Installation
 *
 * For your convenience, an installation script is located in the main folder.
 * Simply run ./install.sh
 *
 * Otherwise, after pulling run 'make install DEBUG=0' after making directories:
 * 'mkdir obj/release' and 'mkdir obj/test' and 'mkdir lib/release'
 * This will allow us to run the unit tests
 * where we can validate the results found in the paper.
 * Make sure to type 'make test' and run ./unit_tests
 * For detailed output run ./unit_tests --log_level=message
 *
 *
 * \section test_sec Unit Tests
 *
 * The unit tests, use boost testing framework and do not only
 * consider 'unit' tests for each method, but also general scripts for various
 * test scenarios. For instance
 *
 * 1. Does the forward and inverse dynamics cancel eachother?
 * 2. Tracking reference errors with compute-torque control and
 *    feedback (PD and/or LQR)
 * 3. Zero-phase filtering with 2nd order Butterworth filters
 *    running forwards/backwards. Compatibility with MATLAB
 *    checked (transients are minimized).
 * 4. Checking performance of (Iterative) Learning Control approaches.
 *
 */

/**
 * @file experiment.cpp
 *
 * @brief Experiment file for interfacing with SL and running ILC experiments.
 *
 *  Created on: Aug 30, 2017
 *      Author: okoc
 */

#include <armadillo>
#include <thread>
#include "traj.h"
#include "dynamics.h"
#include "experiment_old.h"
#include "ilc_old.h"

using namespace arma;


/**
 * @brief Detach striking trajectory preparation if detach is set to true
 */
void Experiment::prepare(const bool detach) {

	if (!detach)
		prepare_traj();
	else {
		std::thread t(&Experiment::prepare_traj,this);
		t.detach();
	}
}

/**
 * @brief Update with ILC if detach is set to true.
 */
void Experiment::update(const bool detach) {

	if (!detach)
		ilc.update(strike_act);
	else {
		std::thread t(&ILC::update,&ilc,std::ref(strike_act));
		t.detach();
	}
}

/**
 * @brief Generate a random strike and initialize model based ILC.
 */
void Experiment::prepare_traj() {

	arma_rng::set_seed(ilc.random_seed);
	link link_param[NDOF+1];
	load_link_params("LinkParameters.cfg",link_param);
	load_PD_feedback_gains(fb_pd);
	load_LQR_feedback(fb_lqr,5);
	gen_random_tabletennis_traj(qact, qrest, 1.0, ilc.cut_traj, strike, ret);
	strike.relax(ilc.relax_traj);
	ret.relax(ilc.relax_traj);
	strike_act.enlarge(strike.Q.n_cols);
	calc_inv_dynamics_for_traj(link_param,strike);
	calc_inv_dynamics_for_traj(link_param,ret);
	//ilc.prepare(strike);
	if (check_all_limits(ret) && check_all_limits(strike))
		ilc.prepare(strike);
}


