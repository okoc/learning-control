CC=g++
LIBS=-larmadillo -lm -lboost_program_options -ladolc -lnlopt
ADOLCLIBDIR=$(HOME)/adolc_base/lib64
ADOLCPATH=$(HOME)/adolc_base/include
OPTIMINCPATH=$(HOME)/table-tennis/include/optim
TABLETENNISPATH=$(HOME)/table-tennis/include/player # for table tennis model
TABLETENNISLIBPATH=$(HOME)/table-tennis/lib
HOST=$(shell hostname)
ifeq ($(HOST),sill) # new machine in the new MPI building
	LIBPATH=$(HOME)/install/lib
else ifeq ($(HOST),lein) # new WAM machine
	LIBPATH=$(HOME)/software/lib
	ADOLCLIBDIR=$(HOME)/software/lib64
	ADOLCPATH=$(HOME)/software/lib64
else
	LIBPATH=/usr/local/lib
endif

I=include
DIR=$(HOME)/learning-control
HEADER=$(DIR)/$(I)
FLAGS=-pthread -std=c++11 -I$(HEADER) -I$(ADOLCPATH) -I$(TABLETENNISPATH) -I$(OPTIMINCPATH)
RELEASE_FLAGS=-O3 -DNDEBUG
DEBUG_FLAGS=-DDEBUG -g -Wall -pedantic -Wextra #$(EXTRA_WARNINGS) #-Werror
TEST_EXEC=./unit_tests
EXTRA_WARNINGS= -Wextra -Weffc++ -pedantic -pedantic-errors \
-Wcast-align -Wcast-qual -Wconversion \
-Wdisabled-optimization -Wfloat-equal\
-Wformat=2 -Wformat-nonliteral -Wformat-security -Wformat-y2k \
-Wimport  -Winline -Winvalid-pch -Wlong-long \
-Wmissing-field-initializers -Wmissing-format-attribute \
-Wmissing-include-dirs -Wmissing-noreturn \
-Wpacked  -Wpointer-arith \
-Wredundant-decls -Wshadow -Wstack-protector \
-Wstrict-aliasing=2 -Wswitch-default \
-Wswitch-enum -Wunreachable-code -Wunused -Wundef \
-Wvariadic-macros -Wwrite-strings

# not including the following warnings
# -Waggregate-return : aggregates are allocated on the stack so stack might overflow
# -Wpadded : padding means to add certain bytes to structures to align them

# for compiling everything, release and debug modes
DEBUG ?= 1
ifeq ($(DEBUG),1)
	FLAGS += $(DEBUG_FLAGS)
	SHARED_OBJECT = $(DIR)/lib/debug/liblc.so
	OBJDIR = $(DIR)/obj/debug
else
	FLAGS += $(RELEASE_FLAGS)
	SHARED_OBJECT = $(DIR)/lib/release/liblc.so
	OBJDIR = $(DIR)/obj/release
endif

SRC_LC = $(wildcard $(DIR)/src/*.cpp)
OBJS_LC = $(addprefix $(OBJDIR)/,$(basename $(notdir $(SRC_LC))))
OBJS = $(addsuffix .o,$(OBJS_LC))
TEST_OBJS = $(addsuffix .o, $(addprefix obj/test/,$(basename $(notdir $(wildcard $(DIR)/test/*.cpp)))))
#$(info $$OBJDIR is [${OBJDIR}])
#$(info $$TEST_OBJS is [${TEST_OBJS}])

#assuming constants.h never changes!
install: $(SHARED_OBJECT)
all: $(SHARED_OBJECT) $(TEST_EXEC)

$(SHARED_OBJECT) : $(OBJS)
	$(CC) -shared $(FLAGS) -o $@ $^ \
	-L$(LIBPATH) -L$(ADOLCLIBDIR) -L$(TABLETENNISLIBPATH) $(LIBS)
	
$(OBJDIR)/filters.o : src/filters.cpp $(I)/filters.h
	$(CC) -c -fPIC $(FLAGS) -o $@ $<
	
$(OBJDIR)/traj.o : src/traj.cpp $(I)/traj.h $(I)/filters.h
	$(CC) -c -fPIC $(FLAGS) -o $@ $<

$(OBJDIR)/dynamics.o : src/dynamics.cpp $(I)/dynamics.h $(I)/traj.h
	$(CC) -c -fPIC $(FLAGS) -o $@ $<	
	
$(OBJDIR)/forward_dynamics.o : src/forward_dynamics.cpp #$(DIR)/include/dynamics.h
	$(CC) -c -fPIC $(FLAGS) -o $@ $<
	
$(OBJDIR)/inverse_dynamics.o : src/inverse_dynamics.cpp #$(DIR)/include/dynamics.h
	$(CC) -c -fPIC $(FLAGS) -o $@ $<
	
$(OBJDIR)/adaptation.o : src/adaptation.cpp $(I)/adaptation.h $(I)/traj.h $(I)/dynamics.h
	$(CC) -c -fPIC $(FLAGS) -o $@ $<
	
$(OBJDIR)/ilc.o : src/ilc.cpp $(I)/ilc.h $(I)/adaptation.h
	$(CC) -c -fPIC $(FLAGS) -o $@ $<	
	
$(OBJDIR)/experiment.o : src/experiment.cpp $(I)/experiment.h $(I)/traj.h $(I)/ilc.h $(I)/dynamics.h
	$(CC) -c -fPIC $(FLAGS) -o $@ $<	
	
$(OBJDIR)/sl_interface_ilc.o : src/sl_interface_ilc.cpp $(I)/sl_interface_ilc.h $(I)/traj.h $(I)/ilc.h $(I)/experiment.h
	$(CC) -c -fPIC $(FLAGS) -o $@ $<	

test: $(TEST_EXEC)

$(TEST_EXEC) : $(TEST_OBJS)
	$(CC) $(FLAGS) -o $@ $^ $(SHARED_OBJECT) \
	-L$(LIBPATH) -L$(ADOLCLIBDIR) -L$(TABLETENNISLIBPATH) -lplayer $(LIBS) \
	$(LIBPATH)/libboost_unit_test_framework.a
	               
obj/test/test_filter.o : test/test_filter.cpp $(I)/filters.h
	$(CC) -c $(FLAGS) -o $@ $<
	
obj/test/test_traj.o : test/test_traj.cpp $(I)/traj.h $(I)/dynamics.h
	$(CC) -c $(FLAGS) -o $@ $<
	
obj/test/test_dynamics.o : test/test_dynamics.cpp $(I)/traj.h $(I)/dynamics.h
	$(CC) -c $(FLAGS) -o $@ $<
	
obj/test/test_lqr.o : test/test_lqr.cpp $(I)/traj.h $(I)/dynamics.h
	$(CC) -c $(FLAGS) -o $@ $<
	
obj/test/test_ilc.o : test/test_ilc.cpp $(I)/traj.h $(I)/dynamics.h $(I)/ilc.h $(I)/adaptation.h $(I)/experiment.h
	$(CC) -c $(FLAGS) -o $@ $<
	
obj/test/test_adapt.o : test/test_adapt.cpp $(I)/traj.h $(I)/dynamics.h $(I)/adaptation.h
	$(CC) -c $(FLAGS) -o $@ $<

obj/test/test_table_tennis.o : test/test_table_tennis.cpp $(I)/traj.h
	$(CC) -c $(FLAGS) -o $@ $<
	               
clean:
	rm -rf obj/debug/*.o obj/release/*.o obj/test/*.o lib/debug/*.so lib/release/*.so unit_tests.o

.PHONY: all release debug clean test
					
