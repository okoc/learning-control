/**
 * @file ilc.h
 *
 * @brief The main ILC class (model based and simple PD-type) is located here.
 *
 *  Created on: Aug 22, 2017
 *      Author: okoc
 */

#ifndef INCLUDE_ILC_H_OLD
#define INCLUDE_ILC_H_OLD

using namespace arma;

/**
 * @brief Actual trajectories in joint space for the robot.
 *
 * Cubes are storing the actual joint recordings over the iterations
 */
struct iterations {
	cube Q = zeros<cube>(NDOF,1,1);
	cube Qd = zeros<cube>(NDOF,1,1);
	cube Qdd = zeros<cube>(NDOF,1,1);
	cube us = zeros<cube>(NDOF,1,1); //!< ilc adjusted controls
};

/**
 * @brief ILC class for performing Learning Control updates.
 */
class ILC {

public:
	bool verbose = false;
	bool track_traj = false;
	bool use_fb = false;
	bool turn_on_filter = false; // zero-phase filter
	bool save_joint_data = false;
	bool use_model = 1; // pd or model-based
	int random_seed = 2; // for random traj generation
	int downsample = 10;
	int num_ilc_iter = 0;
	double cutoff_freq = 15; // zero-phase filter cutoff
	double cut_traj = 1.0; // cut part of strike
	double relax_traj = 1.0; // slow down traj
	double p = 0.1; // pd ilc learning gains
	double d = 0.01;
	double step_len = 1.0; // how much to apply update
	double pinv_tol = 0.01; // 0 = use solve
	std::string link_param_file = "LinkParameters.cfg";
	std::vector<mat> learning_matrices;
	std::vector<traj> refs;
	std::vector<traj> trajs;
	mat uff = zeros<mat>(NDOF,1);

	// loading data members from file
	void load(const std::string & config_file);

	// initialize model-based ILC and random trajectories
	void prepare(const traj & ref);

	// run ILC update once
	void update(const traj & traj_act);
	void model_based_update(); // apply model based inversion

	void filter_last_traj(const traj & traj_act);
};

void relax_reference(const double lambda, traj & ref);

// model based ILC routines
mat make_model(const link param[NDOF+1], const traj & ref, const int downsample);
void linearize_wam_dynamics(const link param[NDOF+1], const traj & ref,
		                    const int downsample, const double h,
		                    cube & A, cube & B);
void discretize_dynamics(const double dt,
		                 const cube & A, const cube & B,
						 cube & Ad, cube & Bd);
mat lift_dynamics(const cube & A, const cube & B);

mat upsample(mat & M, int len);

#endif /* INCLUDE_ILC_H_OLD */
