/**
 * @file experiment.h
 *
 * @brief File containing an experiment class, which orchestrates ILC.
 *
 *  Created on: Aug 30, 2017
 *      Author: okoc
 */

#ifndef INCLUDE_EXPERIMENT_H_
#define INCLUDE_EXPERIMENT_H_


/**
 * @brief Main orchestrator of Learning Control, sends/receives/updates trajectories.
 *
 * Means of communication between the main thread and ILC.
 */
class Experiment {

public:
	bool reset = true;
	mat fb_pd = zeros<mat>(NDOF,2*NDOF);
	mat fb_lqr = zeros<mat>(NDOF,2*NDOF);
	ILC ilc = ILC();
	vec7 qrest = zeros<vec>(NDOF);
	joint qact;
	traj strike;
	traj ret;
	traj strike_act;
	void prepare_traj();
	void prepare(const bool detach);
	void update(const bool detach);
};


#endif /* INCLUDE_EXPERIMENT_H_ */
