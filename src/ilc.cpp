/**
 * @file ilc.cpp
 *
 * @brief Recursive and batch model based ILC methods are located here.
 *
 *  Created on: Feb 16, 2018
 *      Author: okoc
 */

#include <boost/property_tree/ptree.hpp>
#include <boost/program_options.hpp>
#include <armadillo>

#include "ilc.h"
#include "constants.h"
#include "traj.h"
#include "dynamics.h"
#include "filters.h"
#include "adaptation.h"

using namespace arma;
using namespace dyn;
using namespace Traj;
using namespace adapt;
namespace pt = boost::property_tree;

ILC::ILC(f_dyn forward,
        dyn::link link_param[NDOF + 1],
        const traj & ref_,
        const std::string & config_file,
        const bool verbose_) :
                ref(ref_),
                adapt(config_file, link_param) {

    options.verbose = verbose_;
    unsigned m = NDOF;
    unsigned n = 2 * NDOF;
    unsigned N = ref.Q.n_cols;
    uff = zeros<mat>(NDOF,N);
    load(config_file);

    // linearize model
    cube A, B;
    //linearize_dynamics(forward, link_param, ref, 1, 1e-3, A, B);
    linearize_dynamics(link_param, ref, 1, A, B);
    discretize_dynamics(DT, A, B, ltv.A, ltv.B);
    // compute LQR
    K = zeros<cube>(m,n,N);
    P = zeros<cube>(n,n,N+1);
    mat Qf = eye(n,n);
    Q = zeros<cube>(n,n,N+1);
    Q.each_slice() = Qf;
    R = options.R_weight * eye(m,m);
    lqr_fin_horizon_ltv(ltv.A, ltv.B, Qf, Qf, R, K, P);
    // prepare covariances
    ltv.init_cov(options.init_ltv_precision);
    adapt.init_cts_ltv(A,B,options.init_ltv_precision);

    if (options.use_model && !options.recursive) { // batch ILC
        int downsample = 10;
        lift_dynamics(forward, link_param, ref, downsample, 1e-3, F);
    }
}

ILC::ILC(f_dyn forward,
        dyn::link link_param[NDOF + 1],
        const traj & ref_,
        const ilc_options & ilc_opt,
        const adapt_options & adapt_opt,
        const bool verbose_) :
                ref(ref_),
                adapt(adapt_opt, link_param),
                options(ilc_opt) {

    options.verbose = verbose_;
    unsigned m = NDOF;
    unsigned n = 2 * NDOF;
    unsigned N = ref.Q.n_cols;
    uff = zeros<mat>(NDOF,N);

    // linearize model
    cube A, B;
    //linearize_dynamics(forward, link_param, ref, 1, 1e-3, A, B);
    linearize_dynamics(link_param, ref, 1, A, B);
    discretize_dynamics(DT, A, B, ltv.A, ltv.B);
    // compute LQR
    K = zeros<cube>(m,n,N);
    P = zeros<cube>(n,n,N+1);
    mat Qf = eye(n,n);
    Q = zeros<cube>(n,n,N+1);
    Q.each_slice() = Qf;
    R = options.R_weight * eye(m,m);
    lqr_fin_horizon_ltv(ltv.A, ltv.B, Qf, Qf, R, K, P);
    // prepare covariances
    ltv.init_cov(options.init_ltv_precision);
    adapt.init_cts_ltv(A,B,options.init_ltv_precision);

    if (options.use_model && !options.recursive) { // batch ILC
        int downsample = 10;
        lift_dynamics(forward, link_param, ref, downsample, 1e-3, F);
    }
}

void ILC::load(const std::string & config_file) {

    namespace opt = boost::program_options;

    try {
        // Declare a group of options that will be
        // allowed in config file
        opt::options_description config("Configuration");
        config.add_options()
                ("use_model",
                 opt::value<bool>(&options.use_model),
                "Type of ILC: PD = 0, MODEL = 1")
                ("use_fb",
                 opt::value<bool>(&options.use_fb), "Add feedback in SL if true")
                ("recursive",
                 opt::value<bool>(&options.recursive),
                "Recursive model based ILC (LQR for dist. rejection)")
                ("cautious",
                 opt::value<bool>(&options.cautious),
                "Cautious options.recursive ILC using variances of models")
                ("adaptive",
                 opt::value<bool>(&options.adaptive),
                "Adaptive ILC updating LTV model matrices")
                ("step_length",
                opt::value<double>(&options.step_len),
                "Step length for model based ILC update")
                ("p",
                opt::value<double>(&options.pd(0)),
                "Proportional gains for simple PD-ILC")
                ("d",
                opt::value<double>(&options.pd(1)),
                "Derivative gains for simple PD-ILC")
                ("R_weight",
                opt::value<double>(&options.R_weight),
                "Scalar weighting for R penalty matrix (LQR)")
                ("mask",
                opt::value<std::vector<bool>>(&options.mask)->multitoken(),
                "options.masking certain joints during ILC update")
                ("init_precision",
                opt::value<double>(&options.init_ltv_precision),
                "initial precision used in LBR");

        opt::variables_map vm;
        std::ifstream ifs(config_file.c_str());
        bool allow_unregistered = true;
        if (!ifs) {
            std::cout << "Can not open config file: " << config_file << "!!\n";
        } else {
            std::cout << "Loaded config file options!\n";
            opt::store(parse_config_file(ifs, config, allow_unregistered), vm);
            notify(vm);
        }
    } catch (std::exception& e) {
        cout << e.what() << "\n";
    }
}

void ILC::dump(pt::ptree & tree) const {

    tree.put("ilc_iter", idx_update);
    tree.put("R_weight", options.R_weight);
    tree.put("adaptive", options.adaptive);
    tree.put("cautious", options.cautious);
    for (int i = 0; i < NDOF; i++) {
        tree.put("mask" + std::to_string(i + 1), options.mask[i]);
    }
    tree.put("p value", options.pd(0));
    tree.put("d_value", options.pd(1));
    tree.put("recursive", options.recursive);
    tree.put("step_len", options.step_len);
    tree.put("use_fb", options.use_fb);
    tree.put("use_model", options.use_model);
    tree.put("init_precision", options.init_ltv_precision);
    adapt.dump(tree);

}

void ILC::update_ref(const Traj::joint & qdes, const int idx) {
    ref.fill(qdes,idx);
}

void ILC::update(const std::vector<traj> & joint_data) {

    int N = ref.Q.n_cols;
    traj traj_act = joint_data.back();
    mat e = join_vert(traj_act.Q.cols(0, N - 1) - ref.Q,
                      traj_act.Qd.cols(0, N - 1) - ref.Qd);
    mat uff_last = uff;
    mat ufb_last = traj_act.us - uff_last;

    if (options.use_model) { // apply model-based update
        if (options.recursive) {
            if (options.adaptive && idx_update) {
                if (options.verbose)
                    cout << "Adapting LTV models...\n";
                adapt.learn(joint_data, ref, ltv);
            }
            if (options.cautious) {
                recursive_cautious_ilc(ltv, Q, R, e, K, uff, ufb_last);
            }
            else {
                recursive_ilc(ltv.A, ltv.B, P, Q, R, e, uff);
            }
        }
        else { // nonrecursive model based ILC does not have adaptation or caution implemented!
            int downsample = 10;
            batch_ilc(e, F, downsample, options.step_len, uff);
        }
    } else { //apply pd-ilc
        uff -= options.pd(0) * (traj_act.Q.cols(0, N - 1) - ref.Q)
                + options.pd(1) * (traj_act.Qd.cols(0, N - 1) - ref.Qd);
    }
    mask_update(uff_last);
    idx_update++;
}

void ILC::mask_update(const mat & uff_last) {

    for (int i = 0; i < NDOF; i++) {
        if (options.mask[i]) {
            uff.row(i) = uff_last.row(i);
        }
    }
}

bool ILC::add_feedback() const {

    return options.use_fb;
}

void recursive_ilc(const cube & Ad, const cube & Bd, const cube & P,
        const cube & Q, const mat & R, const mat & e, mat & uff) {

    int m = Bd.n_cols;
    int n = Ad.n_rows;
    int N = Ad.n_slices;
    mat nu = zeros<mat>(n, N + 1);
    cube K = zeros<cube>(m, n, N);
    mat Phi = zeros<mat>(m, m);
    mat Psi = zeros<mat>(m, n);

    for (int i = N - 1; i >= 0; i--) {
        Phi = R + Bd.slice(i).t() * P.slice(i + 1) * Bd.slice(i);
        Psi = Bd.slice(i).t() * P.slice(i + 1) * Ad.slice(i);
        K.slice(i) = -solve(Phi, Psi);
        nu.col(i) = (Ad.slice(i) + Bd.slice(i) * K.slice(i)).t() * nu.col(i + 1)
                     + Q.slice(i) * e.col(i);
        uff.col(i) += -solve(Phi, Bd.slice(i).t() * nu.col(i));
    }
}

void recursive_cautious_ilc(const LTV & ltv, const cube & Q, const mat & R,
                            const mat & e, cube & K, mat & uff, mat & ufb) {

    cube Am = ltv.A;
    cube Bm = ltv.B;
    int m = Bm.n_cols;
    int n = Am.n_rows;
    int N = Am.n_slices;

    cube P = zeros<cube>(n, n, N + 1);
    mat nu = zeros<mat>(n, N + 1);
    mat M = zeros<mat>(n, n);
    mat Phi = zeros<mat>(m, m);
    mat Psi = zeros<mat>(m, n);
    P.slice(N) = Q.slice(N);

    for (int i = N - 1; i >= 0; i--) {
        M = Am.slice(i).t() * P.slice(i + 1) * Am.slice(i);
        Phi = R + Bm.slice(i).t() * P.slice(i + 1) * Bm.slice(i);
        Psi = Bm.slice(i).t() * P.slice(i + 1) * Am.slice(i);
        estimate_cautious_covar(ltv.Sigmas.slice(i), P.slice(i + 1), Phi, Psi,
                M);
        K.slice(i) = -solve(Phi, Psi);
        P.slice(i) = Q.slice(i) + M + Psi.t() * K.slice(i);
        // this step ensures that Riccatti recursion is stable!
        P.slice(i) = (P.slice(i) + P.slice(i).t()) / 2.0;
        nu.col(i) = (Am.slice(i) + Bm.slice(i) * K.slice(i)).t() * nu.col(i + 1)
                + Q.slice(i) * e.col(i);
        uff.col(i) += -solve(Phi, Bm.slice(i).t() * nu.col(i));
        //uff.col(i) += ufb.col(i) - K.slice(i) * e.col(i);
    }

}

void estimate_cautious_covar(const mat & Sigma, const mat & P, mat & Phi,
                            mat & Psi, mat & M) {
    int n = M.n_cols;
    int m = Phi.n_cols;

    for (int i = 0; i < m; i++)
        for (int j = 0; j < m; j++)
            for (int k = 0; k < n; k++)
                for (int l = 0; k < n; k++)
                    Phi(i, j) += P(k, l)
                            * Sigma(n * n + i * n + k, n * n + j * n + l);

    for (int i = 0; i < m; i++)
        for (int j = 0; j < n; j++)
            for (int k = 0; k < n; k++)
                for (int l = 0; k < n; k++)
                    Psi(i, j) += P(k, l) * Sigma(n * n + i * n + k, j * n + l);

    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            for (int k = 0; k < n; k++)
                for (int l = 0; k < n; k++)
                    M(i, j) += P(k, l) * Sigma(i * n + k, j * n + l);
}

void batch_ilc(const mat & err,
               const mat & F,
               const int downsample,
               const double step_len,
               mat & uff) {

    int m = uff.n_rows;
    //static wall_clock clock;
    const int N = err.n_cols;
    const int N_sample = N / downsample;
    //const double tolerance = 0.01;
    uvec idx = linspace<uvec>(downsample, N_sample * downsample, N_sample) - 1;

    // downsampling
    mat e = err.cols(idx);
    mat uff_down = uff.cols(idx);

    // applying ILC correction after vectorising
    //clock.tic();
    //cout << "Condition number of mat: " << cond(F) << endl;
    //cout << F.n_rows << endl << F.n_cols << endl << e.n_rows << endl << e.n_cols << endl;
    vec correction = step_len * solve(F, vectorise(e));
    //vec correction = step_len * pinv(F,tolerance) * vectorise(e);
    //cout << "ILC Inversion took " << clock.toc()*1000 << " ms.\n";

    // reshaping to undo vectorisation
    uff_down -= reshape(correction, m, N_sample);

    // upsampling
    adapt::upsample(uff_down,uff);

}
