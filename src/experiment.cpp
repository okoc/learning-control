/**
 * @file experiment.cpp
 *
 * @brief Includes an experiment class that orchestrates Learning Control experiments.
 *
 *  Created on: Feb 21, 2018
 *      Author: okoc
 */

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/program_options.hpp>
#include <time.h>
#include <thread>
#include <mutex>
#include <armadillo>

#include "experiment.h" // learning control includes
#include "ilc.h"
#include "constants.h"
#include "traj.h"
#include "dynamics.h"
#include "filters.h"

#include "kinematics.hpp" // table tennis related includes
#include "tabletennis.h"

using namespace arma;
using namespace dyn;
using namespace Traj;

task::task() {
    double std = as_scalar(randu<mat>(1,1))/10;
    int ball_gun_side = (rand() % 3);
    tt.set_ball_gun(std,ball_gun_side);
}

vec6 task::get_state() {
    return tt.get_ball_state();
}

std::string task::evaluate_performance(traj & act) {

    double T_evolve = 2.0;
    vec6 init_state = get_state();
    player::racket robot_racket;
    optim::joint q_now; // one trial
    for (unsigned i = 0; i < act.Q.n_cols; i++) { // strike

        q_now.q = act.Q.col(i);
        q_now.qd = act.Qd.col(i);
        player::calc_racket_state(q_now,robot_racket);
        tt.integrate_ball_state(robot_racket,DT);
    }
    int N_evolve = (int) (T_evolve / DT);
    for (int i = 0; i < N_evolve; i++) { // return
        tt.integrate_ball_state(DT);
    }

    bool land = tt.has_legally_landed();
    tt.reset_stats();
    tt.set_ball_state(init_state);
    if (land)
        return "LAND";
    else
        return "FAIL";
}

Experiment::Experiment(const vec7 & qact,
                        f_dyn f_forward,
                        f_dyn f_inverse) :
                        f_forw(f_forward), f_inv(f_inverse) {
    if (!options.detach_ilc) {
        prepare(qact);
    } else {
        using memfunc_type = void (Experiment::*)(const vec7&);
        memfunc_type memfunc = &Experiment::prepare;
        std::thread t(memfunc, this, std::ref(qact));
        t.detach();
    }

}

Experiment::Experiment(const vec7 & qact,
                       const all_options & opts,
                       f_dyn f_forward,
                       f_dyn f_inverse) :
                                f_forw(f_forward),
                                f_inv(f_inverse),
                                options(opts.e_opt) {
    if (!options.detach_ilc) {
        prepare(qact,opts);
    } else {
        using memfunc_type = void (Experiment::*)(const vec7&, const all_options &);
        memfunc_type memfunc = &Experiment::prepare;
        std::thread t(memfunc, this,
                      std::ref(qact),
                      std::ref(opts));
        t.detach();
    }

}

Experiment::~Experiment() {

    save_data();
    delete ilc;
}

void Experiment::save_data() {

    namespace pt = boost::property_tree;
    if (options.save_joint_data) {

        std::time_t t = std::time(nullptr);
        char str[100];
        std::strftime(str, sizeof(str), "%c", std::localtime(&t));
        std::string time_now = str;
        options.save_file_name = home + "/learning-control/data/joints_"
                                      + time_now + ".txt";

        traj ref = joint_data[0];
        mat data = join_vert(join_vert(ref.Q,ref.Qd),ref.Qdd);
        for (unsigned i = 1; i < joint_data.size(); i++) {
            data = join_horiz(data,
                    join_vert(join_vert(joint_data[i].Q,joint_data[i].Qd),joint_data[i].Qdd));
        }
        data.save(options.save_file_name, raw_ascii);
        pt::ptree tree;
        dump(tree);
        pt::write_ini(home + "/learning-control/data/config_" + time_now + ".ini", tree);
    }
}

void Experiment::dump(pt::ptree & tree) const {

    tree.put("lookup", options.lookup);
    tree.put("check_torque", options.check_torque);
    tree.put("cut_traj", options.cut_traj);
    tree.put("cutoff_freq", options.cutoff_freq);
    tree.put("detach", options.detach_ilc);
    tree.put("link_param_file", options.link_param_file);
    tree.put("moving_threshold", options.moving_threshold);
    tree.put("max_num_iter", options.max_num_iter);
    tree.put("random_seed", options.random_seed);
    tree.put("relax_traj", options.relax_traj);
    tree.put("save_file_name", options.save_file_name);
    tree.put("time_to_return", options.time2return);
    tree.put("turn_on_filter", options.turn_on_filter);
    tree.put("filter_position", options.filter_position);

    ilc->dump(tree);
}

void Experiment::prepare(const vec7 & qact) {

    load(config_file);
    arma_rng::set_seed(options.random_seed);
    load_link_params(options.link_param_file, link_param);
	Traj::joint qinit;
    qrest.q = qact;
    qinit.q = qact;
    f_inv(link_param, qrest); // needed in real experiments to stabilize robot well

    // gen trajectory and calculate inv. dyn based uff to track
    traj ret(options.time2return/DT);
    if (options.lookup) {
        poly = gen_random_tabletennis_traj(qinit, qrest.q,
                                options.time2return, options.cut_traj, strike, ret);
    }
    else { // optimize
        int iter = 1;
        bool gen = false;
        while (!gen && iter++ < 10) {
            tt = task();
            vec6 ball_state = tt.get_state();
            double time_land_des = 0.8;
            vec2 ball_land_des = {0.0, dist_to_table - 3*table_length/4};
            gen = gen_random_tabletennis_traj(ball_state,qinit,qrest.q,ball_land_des,
                                    options.time2return,time_land_des,strike,ret,poly);
        }
        if (!gen) {
            if (options.verbose)
                cerr << "Tried too many times! Switching to lookup...\n";
            gen_random_tabletennis_traj(qinit, qrest.q,
                               options.time2return, options.cut_traj, strike, ret);
        }
    }
    strike.relax(options.relax_traj);
    calc_inv_dynamics_for_traj(f_inv, link_param, strike);

    // if trajectory is feasible construct ILC
    if (check_all_limits(strike)) {
        joint_data.push_back(strike);
        ilc = new ILC(f_forw, link_param, strike, config_file, options.verbose);
        track_traj = true;
        if (options.verbose) {
            cout << "Experiment setup (traj. gen. and ILC init.) complete!\n";
        }
    }
    fresh = true;
    load_PD_feedback_gains(pd);
}

void Experiment::prepare(const vec7 & qact, const all_options & opts) {

    arma_rng::set_seed(options.random_seed);
    load_link_params(options.link_param_file, link_param);
    Traj::joint qinit;
    qrest.q = qact;
    qinit.q = qact;
    f_inv(link_param, qrest); // needed in real experiments to stabilize robot well

    // gen trajectory and calculate inv. dyn based uff to track
    traj ret(options.time2return/DT);
    if (options.lookup) {
        poly = gen_random_tabletennis_traj(qinit, qrest.q,
                                options.time2return,
                                options.cut_traj, strike, ret);
    }
    else { // optimize
        int iter = 1;
        bool gen = false;
        while (!gen && iter++ < 10) {
        tt = task();
        vec6 ball_state = tt.get_state();
        double time_land_des = 0.8;
        vec2 ball_land_des = {0.0, dist_to_table - 3*table_length/4};
        gen = gen_random_tabletennis_traj(ball_state,qinit,qrest.q,ball_land_des,
                                options.time2return,time_land_des,strike,ret,poly);
        }
        if (!gen) {
            if (options.verbose)
                cerr << "Tried too many times! Switching to lookup...\n";
            gen_random_tabletennis_traj(qinit, qrest.q,
                               options.time2return, options.cut_traj, strike, ret);
        }
    }
    strike.relax(options.relax_traj);
    calc_inv_dynamics_for_traj(f_inv, link_param, strike);

    // if trajectory is feasible construct ILC
    if (check_all_limits(strike)) {
        joint_data.push_back(strike);
        ilc = new ILC(f_forw,
                        link_param,
                        strike,
                        opts.i_opt,
                        opts.a_opt,
                        options.verbose);
        track_traj = true;
        if (options.verbose) {
            cout << "Experiment setup (traj. gen. and ILC init.) complete!\n";
        }
    }
    fresh = true;

    load_PD_feedback_gains(pd);

    //calc_inv_dynamics_for_traj(f_inv,link_param,ret);
    // set feedback for strike + return trajectories
    /*int N_ret = ret.Q.n_cols;
     cube K_strike = ilc->K;
     cube K_return = zeros<cube>(NDOF,2*NDOF,N_ret);
     K_return.each_slice() = pd;
     K_ref = join_slices(ilc->K,K_return);*/

}

double Experiment::simulate(f_dyn f_forward,
                            const dyn::link link_param_act[NDOF + 1],
                            const Traj::joint & qinit,
                            traj & traj_act) const {
    evolve(f_forward, link_param_act, strike, strike.us + ilc->uff, ilc->K, qinit, traj_act);
    return traj_act.calc_rms_error(strike, ilc->uff.n_cols);
}

double Experiment::simulate(f_dyn f_forward,
                            const dyn::link link_param_act[NDOF + 1], const Traj::joint & qinit,
                            const cube & K, traj & traj_act) const {
    evolve(f_forward, link_param_act, strike, strike.us + ilc->uff, K, qinit, traj_act);
    return traj_act.calc_rms_error(strike, ilc->uff.n_cols);
}

double Experiment::simulate(f_dyn f_forward,
                            const dyn::link link_param_act[NDOF + 1], const Traj::joint & qinit,
                            const mat & fb, traj & traj_act) const {
    evolve(f_forward, link_param_act, strike, strike.us + ilc->uff, fb, qinit, traj_act);
    return traj_act.calc_rms_error(strike, ilc->uff.n_cols);
}

void Experiment::load(const std::string & config_file) {

    namespace opt = boost::program_options;

    try {
        // Declare a group of options that will be
        // allowed in config file
        opt::options_description config("Configuration");
        config.add_options()
                ("lookup",
                  opt::value<bool>(&options.lookup),
                 "lookup strike or optimize if false")
                ("detach",
                  opt::value<bool>(&options.detach_ilc),
                 "detach ILC for real robot experiments")
                ("verbose",
                  opt::value<bool>(&options.verbose),
                  "verbosity on/off")
                ("turn_on_filter",
                  opt::value<bool>(&options.turn_on_filter),
                 "Turn on zero phase filter")
                ("check_torque_limits",
                 opt::value<bool>(&options.check_torque),
                 "Check torque limits before applying next ILC update")
                ("record_act_joints",
                 opt::value<bool>(&options.save_joint_data),
                 "Record actual joint data")
                ("moving_threshold",
                 opt::value<double>(&options.moving_threshold),
                 "Robot will move if closer to rest posture than threshold")
                ("time_to_return",
                 opt::value<double>(&options.time2return),
                 "Time to return to rest posture after strike")
                ("ilc_iterations",
                 opt::value<int>(&options.max_num_iter), "ILC iterations")
                ("link_param_file",
                 opt::value<std::string>(&options.link_param_file),
                 "Link parameter file for dynamics model")
                ("random_seed",
                 opt::value<int>(&options.random_seed),
                 "Random seed for generating trajectories")
                ("relax_traj",
                 opt::value<double>(&options.relax_traj),
                "Slow down trajectories")
                ("cut_traj",
                 opt::value<double>(&options.cut_traj),
                "Track some segment of strike")
                ("cutoff_freq",
                 opt::value<double>(&options.cutoff_freq),
                "Cutoff frequency for zero phase filter")
                ("filter_pos",
                 opt::value<bool>(&options.filter_position),
                "Filter positions to calculate velocity");
        opt::variables_map vm;
        bool allow_unregistered = true;
        std::ifstream ifs(config_file.c_str());
        if (!ifs) {
            std::cout << "Can not open config file: " << config_file << "!!\n";
        } else {
        	//cout << options.link_param_file << endl;
            std::cout << "Loaded config file options!\n";
            opt::store(parse_config_file(ifs, config, allow_unregistered), vm);
            notify(vm);
        }
    } catch (std::exception& e) {
        cout << e.what() << "\n";
    }
}

void Experiment::rollout(Traj::joint & qact,
                         Traj::joint & qdes) {

    static int N_strike, N_return;
    static int idx_traj;
    static double t = 0.0;
    static vec ufb = zeros<vec>(NDOF);
    static vec ufull = zeros<vec>(NDOF);
    static traj traj_act(1000);

    if (fresh) { // initialize traj_act
        get_len_traj(N_strike, N_return);
        traj_act = traj(N_strike);
        idx_traj = 0;
        fresh = false;
    }

    // stabilize if not tracking traj
    if (!moving) {
        ufull = qrest.u;
        if (ilc->add_feedback()) {
            ufb = pd * join_vert(qact.q - qrest.q, qact.qd);
            ufull += ufb;
        }
        qdes.q = qrest.q;
        qdes.qd = zeros<vec>(NDOF);
        qdes.qdd = zeros<vec>(NDOF);
        qdes.u = ufull;
    }

    // track traj
    if (ready(qact)) {
        if (idx_traj == 0) {
            poly.a.col(3) = qact.q;
            poly.a.col(2) = qact.qd;
        }
        if (idx_traj < N_strike) {
            t = DT * (idx_traj+1);
            qdes.q = poly.a.col(0)*t*t*t + poly.a.col(1)*t*t + poly.a.col(2)*t + poly.a.col(3);
            qdes.qd = 3*poly.a.col(0)*t*t + 2*poly.a.col(1)*t + poly.a.col(2);
            qdes.qdd = 6*poly.a.col(0)*t + 2*poly.a.col(1);
            f_inv(link_param, qdes);
            ufull = qdes.u + ilc->uff.col(idx_traj);
            //cout << idx_traj << ": " << ufull.t();
            if (ilc->add_feedback()) {
                ufb = ilc->K.slice(idx_traj)
                        * (join_vert(qact.q - qdes.q, qact.qd - qdes.qd));
                ufull += ufb;
            }
            clamp_torque(ufull);
            qdes.u = ufull;
            // update reference in case of varying initial conditions
            ilc->update_ref(qdes,idx_traj);
            strike.fill(qdes,idx_traj);
            qact.u = ufull;
            // save the actual joint data
            traj_act.fill(qact, idx_traj);
            moving = true;
            idx_traj++;
            t += DT;
        }
        else if (idx_traj < (N_return + N_strike)) {
            return_to_rest(qact,idx_traj - N_strike,qdes);
            f_inv(link_param, qdes);
            ufull = qdes.u;
            if (ilc->add_feedback()) {
                ufb = pd * join_vert(qact.q - qdes.q, qact.qd - qdes.qd);
                ufull += ufb;
            }
            qdes.u = ufull;
            idx_traj++;
        } else if (idx_traj == (N_return + N_strike)) {
            update(traj_act);
            moving = false;
            idx_traj = 0;
        }
    }
}

void Experiment::return_to_rest(const Traj::joint & qact,
                                const int idx_return,
                                Traj::joint & qdes) {

    static mat params = zeros<mat>(NDOF, 2);
    double t = DT * idx_return;

    if (t == 0) {
        // update initial parameters from lookup table
        params.col(0) = (qrest.q - qact.q) / options.time2return;
        params.col(1) = qact.q;
    }
    if (t < options.time2return) {
        qdes.q = params.col(0) * t + params.col(1);
        qdes.qd = params.col(0);
        qdes.qdd = zeros<vec>(NDOF);
    } else {
        qdes.q = qrest.q;
        qdes.qd = zeros<vec>(NDOF);
        qdes.qdd = zeros<vec>(NDOF);
    }

    f_inv(link_param, qdes);

}

void Experiment::update(traj & traj_act) {

    track_traj = false;
    if (idx_ilc_iter < options.max_num_iter) {
        if (!options.detach_ilc) {
            run_ilc(traj_act);
        } else {
            std::thread t(&Experiment::run_ilc, this, std::ref(traj_act));
            t.detach();
        }
        idx_ilc_iter++;
    } else { //save data
        if (options.detach_ilc) {
            std::thread t(&Experiment::save_data, this);
            t.detach();
        } else {
            save_data();
        }
    }
}

void Experiment::run_ilc(traj & traj_act) {

    if (options.turn_on_filter) {
        traj_act.filter(options.filter_position,options.cutoff_freq);
    }
    double rms_error = traj_act.calc_rms_error(strike, strike.Q.n_cols);
    double fin_error = traj_act.calc_final_error(strike);
    std::string task_perf = tt.evaluate_performance(traj_act);
    if (options.verbose) {
        cout << "Launching ILC update!\n";
        cout << "RMS/Final error/Task perf. on strike: "
             << rms_error << " / " << fin_error << " / " << task_perf << endl;

    }
    joint_data.push_back(traj_act);
    ilc->update(joint_data);
    if (options.check_torque) {
        track_traj = check_torque_limits(ilc->uff);
    } else {
        track_traj = true;
    }
    if (options.verbose) {
        cout << "ILC update terminated!\n";
    }
}

void Experiment::set_detach(bool detach_) {
    options.detach_ilc = detach_;
}

bool Experiment::ready() const {
    return track_traj;
}

bool Experiment::ready(const Traj::joint & qact) const {

    bool ready = true;
    if (!moving)
        ready = posture_is_stable(qact, qrest, options.moving_threshold);
    return track_traj && ready;
}

int Experiment::get_num_exp() const {
    return options.max_num_iter;
}

void Experiment::get_len_traj(int & n_strike, int & n_return) const {

    n_strike = strike.Q.n_cols;
    n_return = options.time2return / DT;
}

bool posture_is_stable(const Traj::joint & qact,
                       const Traj::joint & qrest,
                       const double & thresh) {

    static mat diff = zeros<mat>(NDOF, 3);
    diff.col(0) = qact.q - qrest.q;
    diff.col(1) = qact.qd - qrest.qd;
    diff.col(2) = qact.qdd - qrest.qdd;
    double rms = norm(diff, "fro") / sqrt(diff.n_elem);
    //cout << "Resting pose rms: " << rms << endl;
    return rms < thresh;
}
