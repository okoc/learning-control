/**
 *
 * @file sl_interface_ilc.cpp
 *
 * @brief Interface of the ILC library to the SL real-time simulator and to
 * the robot.
 *
 *  Created on: Aug 23, 2017
 *      Author: okoc
 */

#include <armadillo>
#include <thread>
#include <chrono>
#include <string>
#include <iostream>
#include <fstream>
#include <mutex>
#include "constants.h"
#include "traj.h"
#include "dynamics.h"
#include "experiment.h"
#include "ilc.h"

using namespace arma;
using namespace Traj;

/** The data structures from SL */

/*
 * Actual joint state for each DOF
 */
struct SL_Jstate {
    double th; /*!< theta */
    double thd; /*!< theta-dot */
    double thdd; /*!< theta-dot-dot */
    double ufb; /*!< feedback portion of command */
    double u; /*!< torque command */
    double load; /*!< sensed torque */
};

/*
 * Desired joint state commands for each DOF
 */
struct SL_DJstate { /*!< desired values for controller */
    double th; /*!< theta */
    double thd; /*!< theta-dot */
    double thdd; /*!< theta-dot-dot */
    double uff; /*!< feedforward torque command */
    double uex; /*!< externally imposed torque */
};

#include "sl_interface_ilc.h"
Experiment* exper = nullptr;

void reset_experiments(const SL_Jstate joint_des_state[NDOF + 1]) {

    vec7 qrest;
    for (int i = 0; i < NDOF; i++) {
        qrest(i) = joint_des_state[i + 1].th;
    }
    delete exper;
    exper = new Experiment(qrest, dyn::barrett_wam_dynamics_art,
                           dyn::barrett_wam_inv_dynamics_ne);
    // make sure ILC updates will be detached
    exper->set_detach(true);
}

void experiment(const SL_Jstate joint_state[NDOF + 1],
        SL_DJstate joint_des_state[NDOF + 1]) {

    static std::mutex mtx;
    static joint qact;
    static joint qdes;
    static vec ufb = zeros<vec>(NDOF);
    static vec ufull = zeros<vec>(NDOF);
    static int N_strike, N_return, idx_traj;
    static traj traj_act(1000);

    // observe state
    mtx.lock();
    for (int i = 0; i < NDOF; i++) {
        qact.q(i) = joint_state[i + 1].th;
        qact.qd(i) = joint_state[i + 1].thd;
        qact.qdd(i) = joint_state[i + 1].thdd;
    }
    mtx.unlock();

    exper->rollout(qact,qdes);

    for (int i = 0; i < NDOF; i++) {
        joint_des_state[i + 1].th = qdes.q(i);
        joint_des_state[i + 1].thd = qdes.qd(i);
        joint_des_state[i + 1].thdd = qdes.qdd(i);
        joint_des_state[i + 1].uff = qdes.u(i);
    }
}
