/**
 * @file traj.cpp
 *
 * @brief This file includes trajectory generation functions
 * used by learning controllers
 *
 *  Created on: Jul 29, 2017
 *  Modified: August 14, 2017
 *      Author: okoc
 */

#include <armadillo>
#include <thread>
#include "traj.h"
#include "constants.h"
#include "lookup.h"
#include "dynamics.h"
#include "filters.h"
#include "optim.h"
#include "player.hpp"

using namespace arma;
namespace Traj {

static void gen_3rd_poly(const rowvec & times, const vec7 & a3, const vec7 & a2,
                         const vec7 & a1, const vec7 & a0, mat & Q, mat & Qd, mat & Qdd);
static void gen_strike(const vec::fixed<15> & strike_params,
                       const vec::fixed<8> & rest_params,
                       const joint & qact,
                       traj & ref);

joint::joint(const unsigned ndof) {
    q = zeros<vec>(ndof);
    qd = zeros<vec>(ndof);
    qdd = zeros<vec>(ndof);
    u = zeros<vec>(ndof);
}


traj::traj(const unsigned N) :
        Q(zeros<mat>(NDOF, N)), Qd(zeros<mat>(NDOF, N)), Qdd(
                zeros<mat>(NDOF, N)), us(zeros<mat>(NDOF, N)) {
}

void traj::enlarge(const unsigned N) {
    Q = zeros<mat>(NDOF, N);
    Qd = zeros<mat>(NDOF, N);
    Qdd = zeros<mat>(NDOF, N);
    us = zeros<mat>(NDOF, N);
}

void traj::cut(const unsigned N) {

    if (N < Q.n_cols) {
        Q = Q.cols(0, N);
        Qd = Qd.cols(0, N);
        Qdd = Qdd.cols(0, N);
        if (N < us.n_cols)
            us = us.cols(0, N);
        else
            us = zeros<mat>(NDOF, N);
    }
}

void traj::fill(const joint & qnow, unsigned elem) {
    Q.col(elem) = qnow.q;
    Qd.col(elem) = qnow.qd;
    Qdd.col(elem) = qnow.qdd;
    us.col(elem) = qnow.u;
}

void traj::join(const traj & traj2) {
    Q = join_horiz(Q, traj2.Q);
    Qd = join_horiz(Qd, traj2.Qd);
    Qdd = join_horiz(Qdd, traj2.Qdd);
    us = join_horiz(us, traj2.us);
}

void traj::filter(const bool filter_pos, const double & cutoff_freq) {

    int num_elems = Q.n_cols;
    int cutoff_perc = 100 * (cutoff_freq / (num_elems / 2));
    filtfilt2(Q, cutoff_perc, Q);

    // compute Qdd with same number of elements
    vec origin = zeros<vec>(1);
    vec final = zeros<vec>(1);
    mat q = zeros<mat>(NDOF,num_elems+2);
    mat qd = zeros<mat>(NDOF,num_elems);

    if (filter_pos) {
        for (int i = 0; i < NDOF; i++) {
            // reflect 2nd element around 1st element to get ``origin''
            origin(0) = 2 * Q(i,0) - Q(i,1);
            final(0) = 2 * Q(i,num_elems-1) - Q(i,num_elems-2);
            q.row(i) = join_horiz(join_horiz(origin, Q.row(i)), final);
            qd.row(i) = (q(i,span(2,num_elems+1)) - q(i,span(0,num_elems-1)))/DT;
        }
        filtfilt2(qd, cutoff_perc, Qd);
    }

    qd = zeros<mat>(NDOF,num_elems+2);
    mat qdd = zeros<mat>(NDOF,num_elems);
    for (int i = 0; i < NDOF; i++) {
        // reflect 2nd element around 1st element to get ``origin''
        origin(0) = 2 * Qd(i,0) - Qd(i,1);
        final(0) = 2 * Qd(i,num_elems-1) - Qd(i,num_elems-2);
        qd.row(i) = join_horiz(join_horiz(origin, Qd.row(i)), final);
        qdd.row(i) = (qd(i,span(2,num_elems+1)) - qd(i,span(0,num_elems-1)))/DT;
    }

    filtfilt2(qdd,cutoff_perc,Qdd);
    filtered = true;
}

double traj::calc_rms_error(const traj & ref,
                            const int N) const {

    mat e_pos = Q.cols(0, N - 1) - ref.Q.cols(0, N - 1);
    mat e_vel = Qd.cols(0, N - 1) - ref.Qd.cols(0, N - 1);
    double rms = norm(join_horiz(e_pos, e_vel), "fro");
    return rms / sqrt(2 * e_pos.n_elem);
}

double traj::calc_final_error(const traj & strike) const {
    return norm(join_vert(Q.tail_cols(1) - strike.Q.tail_cols(1),
                          Qd.tail_cols(1) - strike.Qd.tail_cols(1)));
}

void traj::subtract(const traj & traj_last, int oper, mat & error) const {

    switch (oper) {
    case 1: {
        error = this->Q - traj_last.Q;
        break; }
    case 2: {
        error = join_vert(this->Q - traj_last.Q, this->Qd - traj_last.Qd);
        break; }
    case 3: {
        error = join_vert(join_vert(this->Q - traj_last.Q, this->Qd - traj_last.Qd),
                          this->Qdd - traj_last.Qdd);
        break; }
    default:
        cout << "Wrong operation! Quitting...\n";
    }
}

void traj::relax(const double lambda) {

    if (lambda < 0.0)
        throw "Relaxation parameter lambda should be positive!\n";
    if (lambda != 1.0) {
        unsigned N = Q.n_cols;
        vec t = linspace<vec>(1, N, N);
        unsigned N_relax = (unsigned) (N * lambda);
        vec t_relax = linspace<vec>(1, N, N_relax);
        vec vs[NDOF * 4];
        for (unsigned i = 0; i < 4 * NDOF; i++)
            vs[i] = zeros<vec>(N_relax);
        for (unsigned i = 0; i < NDOF; i++) {
            interp1(t, Q.row(i).t(), t_relax, vs[3 * i + 0]);
            interp1(t, Qd.row(i).t(), t_relax, vs[3 * i + 1]);
            interp1(t, Qdd.row(i).t(), t_relax, vs[3 * i + 2]);
            if (us.n_cols == N)
                interp1(t, us.row(i).t(), t_relax, vs[3 * i + 3]);
        }
        mat temp_matrix = zeros<mat>(NDOF, N_relax);
        for (unsigned i = 0; i < NDOF; i++) {
            temp_matrix.row(i) = vs[3 * i + 0].t();
        }
        Q = temp_matrix;
        for (unsigned i = 0; i < NDOF; i++) {
            temp_matrix.row(i) = vs[3 * i + 1].t();
        }
        Qd = temp_matrix;
        for (unsigned i = 0; i < NDOF; i++) {
            temp_matrix.row(i) = vs[3 * i + 2].t();
        }
        Qdd = temp_matrix;
        if (us.n_cols == N) {
            for (unsigned i = 0; i < NDOF; i++) {
                temp_matrix.row(i) = vs[3 * i + 3].t();
            }
            us = temp_matrix;
        }
    }
}

void init_right_posture(vec7 & q0) {

    q0(0) = 1.0;
    q0(1) = -0.2;
    q0(2) = -0.1;
    q0(3) = 1.8;
    q0(4) = -1.57;
    q0(5) = 0.1;
    q0(6) = 0.3;
}

void init_posture(const int posture, vec7 & q0) {

    if (posture == 1) { // right
        vec qinit = { 1.0, -0.2, -0.1, 1.8, -1.57, 0.1, 0.3 };
        q0 = qinit;
    } else if (posture == 0) { // center
        vec qinit = { 0.0, 0.0, 0.0, 1.5, -1.75, 0.0, 0.0 };
        q0 = qinit;
    } else if (posture == 2) { // left
        vec qinit = { -1.0, 0.0, 0.0, 1.5, -1.57, 0.1, 0.3 };
        q0 = qinit;
    } else { // unrecognized input
        throw "Unrecognized posture. Quitting...";
    }
}

void gen_random_tabletennis_traj(const joint & qact,
                                 const vec7 & qrest,
                                 const double time2return,
                                 traj & ref) {
    vec6 ball_state;
    vec::fixed<15> strike_params;
    vec::fixed<8> rest_params;
    spline_params poly;

    // update initial parameters from lookup table
    //std::cout << "Looking up a random ball entry..." << std::endl;
    player::lookup_random_entry(ball_state, strike_params);

    rest_params.head(NDOF) = qrest;
    rest_params(NDOF) = time2return;
    calc_coeffs(qact, strike_params, rest_params, poly);
    gen_strike(strike_params, rest_params, qact, ref);
}

void gen_random_tabletennis_traj(const joint & qact,
                                 const vec7 & qrest,
                                 const double time2return,
                                 traj & strike,
                                 traj & returning) {

    int N_strike, N_total;
    vec6 ball_state;
    vec::fixed<15> strike_params;
    vec::fixed<8> rest_params;
    spline_params poly;
    traj ref; // output traj.

    // update initial parameters from lookup table
    //std::cout << "Looking up a random entry..." << std::endl;
    player::lookup_random_entry(ball_state, strike_params);
    rest_params.head(NDOF) = qrest;
    rest_params(NDOF) = time2return;
    calc_coeffs(qact, strike_params, rest_params, poly);
    gen_strike(strike_params, rest_params, qact, ref);
    N_strike = (int) (strike_params(2 * NDOF) / DT);
    N_total = ref.Q.n_cols;
    strike.Q = ref.Q.cols(0, N_strike - 1);
    strike.Qd = ref.Qd.cols(0, N_strike - 1);
    strike.Qdd = ref.Qdd.cols(0, N_strike - 1);
    returning.Q = ref.Q.cols(N_strike, N_total - 1);
    returning.Qd = ref.Qd.cols(N_strike, N_total - 1);
    returning.Qdd = ref.Qdd.cols(N_strike, N_total - 1);
}

spline_params gen_random_tabletennis_traj(const joint & qact,
                                         const vec7 & qrest,
                                         const double time2return,
                                         const double cut_strike,
                                         traj & strike,
                                         traj & returning) {
    if (cut_strike <= 0.0 || cut_strike > 1.0)
        throw "Cutting parameter should be between (0,1]!\n";
    int N_strike, N_total;
    vec6 ball_state;
    vec::fixed<15> strike_params;
    vec::fixed<8> rest_params;
    spline_params poly;
    traj ref; // output traj.

    // update initial parameters from lookup table
    std::cout << "Looking up a random entry..." << std::endl;
    player::lookup_random_entry(ball_state, strike_params);
    rest_params.head(NDOF) = qrest;
    rest_params(NDOF) = time2return;
    calc_coeffs(qact, strike_params, rest_params, poly);
    gen_strike(strike_params, rest_params, qact, ref);
    N_strike = (int) (strike_params(2 * NDOF) / DT);
    N_total = ref.Q.n_cols;

    if (cut_strike < 1.0) {
        ref.cut(cut_strike * N_strike);
        double T = cut_strike * strike_params(2 * NDOF);
        vec7 qf = ref.Q.tail_cols(1);
        vec7 qfdot = ref.Qd.tail_cols(1);
        strike_params(2 * NDOF) = T;
        strike_params(span(0, NDOF - 1)) = qf;
        strike_params(span(NDOF, 2 * NDOF - 1)) = qfdot;
        gen_strike(strike_params, rest_params, qact, ref);
        N_strike = (int) (T / DT);
        N_total = ref.Q.n_cols;
    }

    strike.Q = ref.Q.cols(0, N_strike - 1);
    strike.Qd = ref.Qd.cols(0, N_strike - 1);
    strike.Qdd = ref.Qdd.cols(0, N_strike - 1);
    returning.Q = ref.Q.cols(N_strike, N_total - 1);
    returning.Qd = ref.Qd.cols(N_strike, N_total - 1);
    returning.Qdd = ref.Qdd.cols(N_strike, N_total - 1);

    return poly;

}

bool gen_random_tabletennis_traj(const vec6 & ball_state,
                                 const joint & qact,
                                 const vec7 & qrest,
                                 const vec2 & ball_land_des,
                                 const double time2return,
                                 const double time_land_des,
                                 traj & strike,
                                 traj & returning,
                                 Traj::spline_params & poly) {

    bool generate = false;
    // optimize for robot movement
    double lb[2*NDOF+1], ub[2*NDOF+1];
    double SLACK = 0.01;
    double Tmax = 1.0;
    optim::set_bounds(lb,ub,SLACK,Tmax);
    player::EKF filter = player::init_filter();
    mat66 P;
    P.eye();
    filter.set_prior(ball_state,P);
    mat balls_pred = filter.predict_path(DT,1000);
    optim::optim_des racket_params;
    racket_params.Nmax = 1000;
    racket_params = optim::calc_racket_strategy(balls_pred,
                                         ball_land_des,
                                         time_land_des,
                                         racket_params);
    optim::spline_params poly_;
    optim::joint q_optim;
    q_optim.q = qact.q;
    q_optim.qd = qact.qd;
    optim::FocusedOptim opt = optim::FocusedOptim(q_optim.q.memptr(),lb,ub);
    opt.set_des_params(&racket_params);
    opt.update_init_state(q_optim);
    opt.set_detach(false);
    opt.set_verbose(false);
    opt.set_return_time(time2return);
    opt.run();
    generate = opt.get_params(q_optim,poly_);
    poly.a = poly_.a;
    poly.b = poly_.b;
    poly.time2hit = poly_.time2hit;

    // generate robot movement
    if (generate) {

        double t = 0.0;
        int N_strike = (int) (poly.time2hit / DT);
        int N_total = (int) ((time2return + poly.time2hit) / DT);
        traj ref(N_total);
        for (int i = 0; i < N_total; i++) { // one trial
            player::update_next_state(poly_,qrest,time2return,t,q_optim);
            ref.Q.col(i) = q_optim.q;
            ref.Qd.col(i) = q_optim.qd;
            ref.Qdd.col(i) = q_optim.qdd;
        }

        strike.Q = ref.Q.cols(0, N_strike - 1);
        strike.Qd = ref.Qd.cols(0, N_strike - 1);
        strike.Qdd = ref.Qdd.cols(0, N_strike - 1);
        returning.Q = ref.Q.cols(N_strike, N_total - 1);
        returning.Qd = ref.Qd.cols(N_strike, N_total - 1);
        returning.Qdd = ref.Qdd.cols(N_strike, N_total - 1);
    }
    return generate;
}

void calc_coeffs(const joint & qact,
                 const vec::fixed<15> & strike_params,
                 const vec::fixed<8> & rest_params,
                 spline_params & p) {

    vec7 qf = strike_params.head(NDOF);
    vec7 qfdot = strike_params.tail(NDOF + 1).head(NDOF);
    vec7 qrest = rest_params.head(NDOF);
    double T = strike_params(2 * NDOF);
    double time2return = rest_params(NDOF);

    vec7 qnow = qact.q;
    vec7 qdnow = qact.qd;
    p.a.col(0) = 2.0 * (qnow - qf) / pow(T, 3) + (qfdot + qdnow) / pow(T, 2);
    p.a.col(1) = 3.0 * (qf - qnow) / pow(T, 2) - (qfdot + 2.0 * qdnow) / T;
    p.a.col(2) = qdnow;
    p.a.col(3) = qnow;
    //cout << "A = \n" << p.a << endl;
    p.b.col(0) = 2.0 * (qf - qrest) / pow(time2return, 3)
            + (qfdot) / pow(time2return, 2);
    p.b.col(1) = 3.0 * (qrest - qf) / pow(time2return, 2)
            - (2.0 * qfdot) / time2return;
    p.b.col(2) = qfdot;
    p.b.col(3) = qf;
    p.time2hit = T;
}

/*
 * Generate BATCH 3rd order strike + return polynomials.
 *
 * Based on hitting and returning joint state parameters qf,qfdot
 * and hitting time T, calculates the relevant polynomial parameters
 * and generates BATCH polynomial values till time T.
 * @param strike_params qf, qfdot, T: Hitting parameters
 * @param rest_params qrest and T_return: Resting parameters
 * @param qact From actual joint state generate the joint des values
 * @param ref  Q,Qd,Qdd Generated joint pos., vel., acc.
 *
 */
static void gen_strike(const vec::fixed<15> & strike_params,
                       const vec::fixed<8> & rest_params,
                       const joint & qact,
                       traj & ref) {

    // first create hitting polynomials
    vec7 qnow = qact.q;
    vec7 qdnow = qact.qd;
    vec7 qf = strike_params.head(NDOF);
    vec7 qfdot = strike_params.tail(NDOF + 1).head(NDOF);
    vec7 qrest = rest_params.head(NDOF);
    double T = strike_params(2 * NDOF);
    double time2return = rest_params(NDOF);
    vec7 a3 = 2.0 * (qnow - qf) / pow(T, 3) + (qfdot + qdnow) / pow(T, 2);
    vec7 a2 = 3.0 * (qf - qnow) / pow(T, 2) - (qfdot + 2.0 * qdnow) / T;
    vec7 b3 = 2.0 * (qf - qrest) / pow(time2return, 3)
            + (qfdot) / pow(time2return, 2);
    vec7 b2 = 3.0 * (qrest - qf) / pow(time2return, 2)
            - (2.0 * qfdot) / time2return;

    int N_hit = (int) (T / DT);
    rowvec times_hit = linspace<rowvec>(DT, T, N_hit);
    int N_return = (int) (time2return / DT);
    rowvec times_ret = linspace<rowvec>(DT, time2return, N_return);

    mat Q_hit, Qd_hit, Qdd_hit, Q_ret, Qd_ret, Qdd_ret;
    Q_hit = Qd_hit = Qdd_hit = zeros<mat>(NDOF, N_hit);
    Q_ret = Qd_ret = Qdd_ret = zeros<mat>(NDOF, N_return);

    gen_3rd_poly(times_hit, a3, a2, qdnow, qnow, Q_hit, Qd_hit, Qdd_hit);
    gen_3rd_poly(times_ret, b3, b2, qfdot, qf, Q_ret, Qd_ret, Qdd_ret);
    ref.Q = join_horiz(Q_hit, Q_ret);
    ref.Qd = join_horiz(Qd_hit, Qd_ret);
    ref.Qdd = join_horiz(Qdd_hit, Qdd_ret);
}

bool update_next_state(const spline_params & poly,
                        const vec7 & q_rest_des,
                        const double time2return,
                        double & t,
                        joint & qdes) {
    mat a, b;
    double tbar;
    bool flag = true;

    if (t <= poly.time2hit) {
        a = poly.a;
        qdes.q = a.col(0) * t * t * t + a.col(1) * t * t + a.col(2) * t
                + a.col(3);
        qdes.qd = 3 * a.col(0) * t * t + 2 * a.col(1) * t + a.col(2);
        qdes.qdd = 6 * a.col(0) * t + 2 * a.col(1);
        t += DT;
        //cout << qdes.q << qdes.qd << qdes.qdd << endl;
    } else if (t <= poly.time2hit + time2return) {
        b = poly.b;
        tbar = t - poly.time2hit;
        qdes.q = b.col(0) * tbar * tbar * tbar + b.col(1) * tbar * tbar
                + b.col(2) * tbar + b.col(3);
        qdes.qd = 3 * b.col(0) * tbar * tbar + 2 * b.col(1) * tbar + b.col(2);
        qdes.qdd = 6 * b.col(0) * tbar + 2 * b.col(1);
        t += DT;
    } else {
        //printf("Hitting finished!\n");
        t = 0.0;
        flag = false;
        qdes.q = q_rest_des;
        qdes.qd = zeros<vec>(NDOF);
        qdes.qdd = zeros<vec>(NDOF);
    }
    return flag;
}

/*
 * Generate matrix of joint angles, velocities and accelerations
 */
static void gen_3rd_poly(const rowvec & times, const vec7 & a3, const vec7 & a2,
                         const vec7 & a1, const vec7 & a0, mat & Q, mat & Qd, mat & Qdd) {

    // IN MATLAB:
    //	qStrike(m,:) = a(1)*t.^3 + a(2)*t.^2 + a(3)*t + a(4);
    //	qdStrike(m,:) = 3*a(1)*t.^2 + 2*a(2)*t + a(3);
    //	qddStrike(m,:) = 6*a(1)*t + 2*a(2);

    for (int i = 0; i < NDOF; i++) {
        Q.row(i) = a3(i) * pow(times, 3) + a2(i) * pow(times, 2) + a1(i) * times
                + a0(i);
        Qd.row(i) = 3 * a3(i) * pow(times, 2) + 2 * a2(i) * times + a1(i);
        Qdd.row(i) = 6 * a3(i) * times + 2 * a2(i);
    }
}

bool check_all_limits(const traj & ref) {

    vec7 lb = zeros<vec>(NDOF, 1);
    vec7 ub = zeros<vec>(NDOF, 1);
    vec7 vel_lim = zeros<vec>(NDOF, 1);
    vec7 acc_lim = zeros<vec>(NDOF, 1);
    vec7 torque_lim = zeros<vec>(NDOF, 1);
    read_joint_limits(lb, ub, vel_lim, acc_lim, torque_lim);
    //cout << lb << ub << vel_lim << acc_lim << torque_lim;
    int N = ref.Q.n_cols;
    for (int i = 0; i < N; i++) {
        if (any(ref.Q.col(i) < lb) || any(ref.Q.col(i) > ub)) {
            cout << "Joint limits violated!";
            return false;
        }
        if (any(ref.Qd.col(i) < -vel_lim) || any(ref.Qd.col(i) > vel_lim)) {
            cout << "Velocity limits violated!";
            return false;
        }
        if (any(ref.Qdd.col(i) < -acc_lim) || any(ref.Qdd.col(i) > acc_lim)) {
            cout << "Acceleration limits violated!";
            return false;
        }
        if (any(ref.us.col(i) < -torque_lim)
                || any(ref.us.col(i) > torque_lim)) {
            cout << "Torque limits violated!";
            return false;
        }
    }
    return true;
}

bool check_torque_limits(const mat & u_ff) {

    vec7 lb = zeros<vec>(NDOF, 1);
    vec7 ub = zeros<vec>(NDOF, 1);
    vec7 vel_lim = zeros<vec>(NDOF, 1);
    vec7 acc_lim = zeros<vec>(NDOF, 1);
    vec7 torque_lim = zeros<vec>(NDOF, 1);
    read_joint_limits(lb, ub, vel_lim, acc_lim, torque_lim);
    int N = u_ff.n_cols;
    for (int i = 0; i < N; i++) {
        if (any(u_ff.col(i) < -torque_lim) || any(u_ff.col(i) > torque_lim)) {
            cout << "Torque limits violated! Idx = " << i << " , uff = "
                    << u_ff.col(i).t();
            return false;
        }
    }
    return true;
}

bool clamp_torque(vec & u_full) {

    static bool firsttime = true;
    bool clamped = false;
    static vec7 lb = zeros<vec>(NDOF, 1);
    static vec7 ub = zeros<vec>(NDOF, 1);
    static vec7 vel_lim = zeros<vec>(NDOF, 1);
    static vec7 acc_lim = zeros<vec>(NDOF, 1);
    static vec7 torque_lim = zeros<vec>(NDOF, 1);

    if (firsttime) {
        firsttime = false;
        read_joint_limits(lb, ub, vel_lim, acc_lim, torque_lim);
    }

    for (unsigned i = 0; i < u_full.n_elem; i++) {
        if (u_full(i) < -torque_lim(i)) {
            clamped = true;
            cout << "Clamping torque " << i << " from below!\n";
            u_full(i) = torque_lim(i);
        }
        if (u_full(i) > torque_lim(i)) {
            clamped = true;
            cout << "Clamping torque " << i << " from above!\n";
            u_full(i) = torque_lim(i);
        }
    }
    return clamped;

}

void read_joint_limits(vec7 & lb,
                        vec7 & ub,
                        vec7 & vel_lim,
                        vec7 & acc_lim,
                        vec7 & torque_lim) {

    using namespace std;
    string env = getenv("HOME");
    string filename = env + "/learning-control/config/Limits.cfg";
    mat joint_limits;
    joint_limits.load(filename);
    lb = joint_limits.col(0);
    ub = joint_limits.col(1);
    vel_lim = joint_limits.col(2);
    acc_lim = joint_limits.col(3);
    torque_lim = joint_limits.col(4);

}

}
