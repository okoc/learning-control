/**
 * @file adaptation.cpp
 *
 * @brief Adaptation of Model Matrices with LBR/Broyden methods
 *
 *  Created on: Feb 16, 2018
 *      Author: okoc
 */

#include <boost/property_tree/ptree.hpp>
#include <boost/program_options.hpp>
#include <armadillo>
#include "traj.h"
#include "dynamics.h"
#include "adaptation.h"

using namespace arma;
using namespace Traj;

namespace adapt {

Adapt::Adapt(const std::string & config_file) {
    load(config_file);
    ltv_cts.discrete = false;
}

Adapt::Adapt(const std::string & config_file, dyn::link link_params[NDOF+1]) {
    load(config_file);
    ltv_cts.discrete = false;
    mat link_mat = zeros<mat>(10,NDOF+1);
    vectorise_links(link_params,link_mat);
    this->link_params = vectorise(link_mat);
    link_precision = options.init_link_precision * eye<mat>((NDOF+1)*10,(NDOF+1)*10);
}

Adapt::Adapt(const adapt_options & opts,
             dyn::link link_params[NDOF+1]) : options(opts) {
    ltv_cts.discrete = false;
    mat link_mat = zeros<mat>(10,NDOF+1);
    vectorise_links(link_params,link_mat);
    this->link_params = vectorise(link_mat);
    link_precision = options.init_link_precision * eye<mat>((NDOF+1)*10,(NDOF+1)*10);
}

void Adapt::load(const std::string & config_file) {

    namespace opt = boost::program_options;
    int method = 0;

    try {
        // Declare a group of options that will be
        // allowed in config file
        opt::options_description config("Configuration");
        config.add_options()
                ("method",
                opt::value<int>(&method),
                "Set the method (DISC LTV/CTS LTV/LINK LTV/BROYDEN")
                ("use_window",
                opt::value<bool>(&options.use_window),
                "Use sliding window for LBR")
                ("trust_region_weighting",
                opt::value<bool>(&options.trust_region_weighting),
                "Trust region weighting to update Q matrices")
                ("forgetting_factor",
                opt::value<double>(&options.forgetting_factor),
                "Forgetting factor in (0,1) for LBR")
                ("size_window",
                opt::value<int>(&options.size_window),
                "Sliding window size")
                ("downsample",
                opt::value<int>(&options.downsample),
                "Downsample to adapt less LTV parameters (linearly interp. after)")
                ("init_link_precision",
                opt::value<double>(&options.init_link_precision),
                "Initial link parameter precision matrix scalar weight")
                ("mc_samples",
                opt::value<double>(&options.mc_samples),
                "Monte Carlo sample size to est. Ad,Bd mean/covs from link perturb.");

        opt::variables_map vm;
        std::ifstream ifs(config_file.c_str());
        bool allow_unregistered = true;
        if (!ifs) {
            std::cout << "Can not open config file: " << config_file << "!!\n";
        } else {
            std::cout << "Loaded config file options!\n";
            opt::store(parse_config_file(ifs, config, allow_unregistered), vm);
            notify(vm);
        }
    } catch (std::exception& e) {
        cout << e.what() << "\n";
    }

    switch (method) {
    case 0:
        options.method = DISC_LBR;
        break;
    case 1:
        options.method = CTS_LBR;
        break;
    case 2:
        options.method = LINK_LBR;
        break;
    case 3:
        options.method = BROYDEN;
        break;
    default:
        cout << "Unrecognized method. Initializing to discrete LTV adaptation with LBR...\n";
        options.method = DISC_LBR;
    }
}

void Adapt::dump(pt::ptree & tree) const {
    tree.put("use_window", options.use_window);
    tree.put("trust_region_weighting", options.trust_region_weighting);
    tree.put("forgetting_factor", options.forgetting_factor);
    tree.put("size_window", options.size_window);
    tree.put("downsample", options.downsample);
    tree.put("mc_samples", options.mc_samples);
    tree.put("init_link_precision", options.init_link_precision);
    tree.put("method", options.method);
}

void Adapt::learn(const std::vector<traj> & joint_data,
                  const traj & ref,
                  LTV & ltv) {

    int sz_win = options.size_window;
    int iter = joint_data.size();
    traj traj_cur = joint_data[iter-1];
    traj traj_last = joint_data[iter-2];
    mat du = traj_cur.us - traj_last.us;
    mat de(1,1,fill::zeros);

    if (options.method == LINK_LBR && traj_cur.filtered) {
        dyn::link link_param_str[NDOF+1];
        mat Xmat;
        vec y;
        form_regressor(traj_cur, link_params, Xmat, y);
        mat gamma_new = (Xmat.t() * Xmat) + options.forgetting_factor*link_precision;
        link_params = solve(gamma_new, link_precision*link_params + (Xmat.t() * y));
        link_precision = gamma_new;
        mat link_mat = reshape(link_params,10,NDOF+1);
        aggregate_links(link_mat,link_param_str);
        linearize_dynamics(link_param_str,
                inv(link_precision),
                ref,
                options.mc_samples,
                options.downsample,
                ltv);
    }
    else if (options.method == CTS_LBR && traj_cur.filtered && traj_last.filtered) {
        traj_cur.subtract(traj_last,3,de);
        est_ltv_cts_lbr(du, de, 1.0/(DT*DT),
                options.forgetting_factor,
                options.downsample,
                ltv_cts);
        if (options.use_window && iter > sz_win) {
            du = joint_data[iter-sz_win].us
                    - joint_data[iter-1-sz_win].us;
            joint_data[iter-sz_win].subtract(joint_data[iter-1-sz_win],3,de);
            est_ltv_cts_lbr(du, de, -1.0/(DT*DT),
                    options.forgetting_factor,
                    options.downsample,
                    ltv_cts);
        }
        ltv_cts.discretize(DT,ltv);
    }
    else if (options.method == DISC_LBR) {
        traj_cur.subtract(traj_last,2,de);
        est_ltv_discr_lbr(du, de, 1.0,
                options.forgetting_factor,
                options.downsample,
                ltv);
        if (options.use_window && iter > sz_win) {
            du = joint_data[iter-options.size_window].us
                    - joint_data[iter-1-options.size_window].us;
            joint_data[iter-sz_win].subtract(joint_data[iter-1-sz_win],2,de);
            est_ltv_discr_lbr(du, de, -1.0,
                    options.forgetting_factor,
                    options.downsample,
                    ltv);
        }
    }
    else if (options.method == BROYDEN){
        traj_cur.subtract(traj_last,2,de);
        est_ltv_discr_broyden(du, de, ltv);
    }
    else {
        cout << "NOT performing adaptation!\n";
    }

    //trust_region_weighting(ltv.Ad, ltv.Bd, K, uff - ref.us,
    //                       e_cur.col(0),
    //                       e_cur, e_last, Q);

}

void Adapt::init_cts_ltv(const cube & A,
                         const cube & B,
                         const double & discr_precision) {
    ltv_cts.A = A.tube(NDOF,0,2*NDOF-1,2*NDOF-1); //first row, first col, last row, last col
    ltv_cts.B = B.tube(NDOF,0,2*NDOF-1,NDOF-1);
    ltv_cts.init_cov(discr_precision*(DT*DT));
}

void est_ltv_discr_broyden(const mat & du, const mat & de, LTV & ltv) {

    int m = ltv.B.n_cols;
    int n = ltv.A.n_rows;
    int N = ltv.A.n_slices;
    // variation in IC is assumed to be zero
    vec de_last = zeros<vec>(n);
    mat D = zeros<mat>(n, n);
    mat Pz = zeros<mat>(n, n);
    vec z = zeros<vec>(n);
    vec mu = zeros<mat>(n);

    for (int i = 0; i < N; i++) {
        z = join_vert(de_last, du.col(i));
        Pz = eye(n * (n + m), n * (n + m))
            - kron((z * z.t() / (dot(z, z))), eye(n, n));
        ltv.Sigmas.slice(i) = Pz * ltv.Sigmas.slice(i) * Pz;
        D = (de.col(i) - (ltv.A.slice(i) * de_last + ltv.B.slice(i) * du.col(i)))
            * z.t();
        D /= dot(z, z);
        // update A,B matrices
        ltv.A.slice(i) += D.cols(0, n - 1);
        ltv.B.slice(i) += D.cols(n, n + m - 1);
        de_last = de.col(i);
    }

}

void est_ltv_discr_lbr(const mat & du,
                        const mat & de,
                        const double & s2,
                        const double & lambda,
                        const int & downsample,
                        LTV & ltv) {

    int m = ltv.B.n_cols;
    int n = ltv.A.n_rows;
    int N = ltv.A.n_slices;
    int p = ltv.Sigmas.n_cols;
    int N_sample = N / downsample;
    LTV ltv_down;
    uvec idx = linspace<uvec>(downsample, N_sample * downsample, N_sample) - 1;
    ltv.downsample(downsample,ltv_down);
    // variation in IC is assumed to be zero
    vec de_last = zeros<vec>(n);
    vec Xmat = zeros<vec>(n + m);
    mat M = zeros<mat>(n, p);
    vec mu = zeros<mat>(p);
    vec y = zeros<vec>(n);
    mat gamma_new = zeros<mat>(p, p);

    for (int i = 0; i < N_sample; i++) {

        de_last = de.col(fmax(idx(i)-1,0));
        Xmat = join_vert(de_last, du.col(idx(i)));
        M = kron(Xmat.t(), eye(n,n));
        mu = vectorise(join_horiz(ltv_down.A.slice(i),
                                  ltv_down.B.slice(i)));
        y = de.col(idx(i));
        // NEW CODE

        gamma_new = (1/s2)*(M.t()*M) + lambda * ltv_down.Gammas.slice(i);
        ltv_down.Sigmas.slice(i) = inv(gamma_new);
        mu = ltv_down.Sigmas.slice(i) *
                (lambda*ltv_down.Gammas.slice(i)*mu + (1/s2)*(M.t() * y));
        ltv_down.Gammas.slice(i) = gamma_new;

        // OLD CODE
        /* gamma_new = inv(Sigmas.slice(i));
         linear_bayes_regression(M,de.col(i),s2,gamma_new,mu);
         Sigmas.slice(i) = inv(gamma_new); */

        ltv_down.A.slice(i) = reshape(mu.rows(0, n * n - 1), n, n);
        ltv_down.B.slice(i) = reshape(mu.rows(n * n, n * (n + m) - 1), n, m);

    }
    ltv_down.upsample(ltv);

}

void est_ltv_cts_lbr(const mat & du,
                        const mat & de,
                        const double & s2,
                        const double & lambda,
                        const int & downsample,
                        LTV & ltv) {

    int m = ltv.B.n_cols; // 7 for wam
    int n = ltv.A.n_cols; // 14 for wam
    int k = ltv.A.n_rows; // 7 for wam
    int N = ltv.A.n_slices;
    int p = ltv.Sigmas.n_cols;
    int N_sample = N / downsample;
    LTV ltv_down;
    uvec idx = linspace<uvec>(downsample, N_sample * downsample, N_sample) - 1;
    ltv.downsample(downsample,ltv_down);
    // variation in IC is assumed to be zero
    vec de_last = zeros<vec>(n);
    mat Qs = de.rows(0,n-1);
    mat Qdds = de.rows(n,n+k-1);
    vec Xmat = zeros<vec>(n + m);
    mat M = zeros<mat>(n, p);
    vec mu = zeros<mat>(p);
    vec y = zeros<mat>(k);
    mat gamma_new = zeros<mat>(p, p);

    for (int i = 0; i < N_sample; i++) {

        de_last = Qs.col(idx(i));
        Xmat = join_vert(de_last, du.col(idx(i)));
        M = kron(Xmat.t(), eye(k,k));
        mu = vectorise(join_horiz(ltv_down.A.slice(i),
                                  ltv_down.B.slice(i)));
        y = Qdds.col(idx(i));

        gamma_new = (1/s2)*(M.t()*M) + lambda * ltv_down.Gammas.slice(i);
        ltv_down.Sigmas.slice(i) = inv(gamma_new);
        mu = ltv_down.Sigmas.slice(i) *
                (lambda*ltv_down.Gammas.slice(i)*mu + (1/s2)*(M.t() * y));
        ltv_down.Gammas.slice(i) = gamma_new;

        ltv_down.A.slice(i) = reshape(mu.rows(0,k*n-1),k,n);
        ltv_down.B.slice(i) = reshape(mu.rows(k*n,k*(n+m)-1),k,m);
    }
    ltv_down.upsample(ltv);

}

void linear_bayes_regression(const mat & Xmat, const vec & y, const double & s2,
        mat & Gamma, vec & mu) {

    mu += solve(Xmat.t() * Xmat + s2 * Gamma, Xmat.t() * (y - Xmat * mu));
    Gamma += (Xmat.t() * Xmat) / s2;
}

void trust_region_weighting(const cube & A, const cube & B, const cube & K,
                            const mat & uff, const vec & e0, const mat & e_cur, const mat & e_last,
                            cube & Q) {

    //const double q_max = 100;
    const double q_min = 0.01;
    const unsigned n = A.n_rows;
    const unsigned N = A.n_slices;

    vec e_minus = e0; // error in the previous time step
    vec e_pred_next = zeros<vec>(n);

    double step_cost_cur, step_cost_last, step_pred_cost_cur, rho;

    for (unsigned i = 0; i < N; i++) {
        step_cost_cur = dot(e_cur.col(i), e_cur.col(i));
        step_cost_last = dot(e_last.col(i), e_last.col(i));
        // compute predicted reduction of step cost
        e_pred_next = (A.slice(i) + B.slice(i) * K.slice(i)) * e_minus;
                        +B.slice(i) * uff.col(i);
        step_pred_cost_cur = dot(e_pred_next, e_pred_next);
        rho = (step_cost_last - step_cost_cur)
            / (step_cost_last - step_pred_cost_cur);

        if (rho < 0.25 && Q.slice(i).diag().max() > 4.0 * q_min) {
            Q.slice(i) /= 4.0;
        }
        /*else {
         if (rho > 0.75 && Q.slice(i).diag().min() < 0.5*q_max) {
         Q.slice(i) = 2.0*Q.slice(i);
         }
         }*/
        e_minus = e_cur.col(i);
    }
}

void upsample(const mat & M_down,
              mat & M_up) {

    int m = M_down.n_rows;
    int n = M_down.n_cols;
    int len = M_up.n_cols;
    int iter = len / n;
    vec v = zeros<vec>(len);
    vec t_up = linspace(1, len, len);
    vec origin = zeros<vec>(1);
    vec final = zeros<vec>(1);
    vec t_down = zeros<vec>(n + 2);
    t_down(0) = 0;
    t_down(span(1, n)) = iter * linspace(1,n,n);
    t_down(n + 1) = t_down(n) + iter;
    for (int i = 0; i < m; i++) {
        // reflect 2nd element around 1st element to get ``origin''
        origin(0) = 2 * M_down(i,0) - M_down(i,1);
        final(0) = 2 * M_down(i,n-1) - M_down(i,n-2);
        interp1(t_down, join_horiz(join_horiz(origin,
                                              M_down.row(i)),
                                   final).t(), t_up, v);
        M_up.row(i) = v.t();
    }
}

void upsample(const cube & C_down, cube & C_up) {

    int m = C_down.n_rows;
    int n = C_down.n_cols;
    int N = C_down.n_slices;
    int N_up = C_up.n_slices;
    mat M_down = reshape(C_down,m*n,N,1);
    mat M_up = zeros<mat>(m*n,N_up);
    upsample(M_down,M_up);
    cube C = cube(M_up.memptr(),m,n,N_up,false,false);
    C_up = C;
}

void form_regressor(const traj & traj_act,
                    vec & theta,
                    mat & Xmat,
                    vec & y) {

    int m = NDOF;
    int n = (NDOF+1)*10;
    int N = traj_act.Q.n_cols;
    y = zeros<vec>(N*NDOF);
    Xmat = zeros<mat>(N*NDOF,n);
    joint Q;
    mat jact = zeros<mat>(n,m); // jacabian transpose

    for (int i = 0; i < N; i++) {
        Q.q = traj_act.Q.col(i);
        Q.qd = traj_act.Qd.col(i);
        Q.qdd = traj_act.Qdd.col(i);
        dyn::generate_inv_dyn_tape(m,n,Q);
        dyn::auto_jacobian(TAG_INV_DYN_JAC,theta,jact);
        Xmat.rows(i*NDOF,(i+1)*NDOF-1) = jact.t();
        y.rows(i*NDOF,(i+1)*NDOF-1) = traj_act.us.col(i);
    }
}

void linearize_dynamics(const dyn::link link_param[NDOF + 1],
                        const mat & Sigma_link,
                        const traj & ref,
                        const int N_sample,
                        const int downsample,
                        LTV & ltv) {

    dyn::link link_perturb[NDOF+1];
    int N = ref.Q.n_cols / downsample;
    int n = 2 * NDOF;
    int m = NDOF;
    int k = NDOF;
    int n_par = (n+m)*k;
    joint qnow;
    cube dft = zeros<cube>(n+m, k, N_sample);
    mat dft_mean = zeros<mat>(n+m, k);
    mat dft_var = zeros<mat>(n_par,n_par);
    vec dft_diff = zeros<vec>(n_par);
    vec x = zeros<vec>(m+n);

    cube A = zeros<cube>(k, n, N);
    cube B = zeros<cube>(k, m, N);
    cube Sigma_dyn = zeros<cube>(n_par,n_par,N);
    for (int i = 1; i <= N; i++) {
        qnow.q = ref.Q.col(i * downsample - 1);
        qnow.qd = ref.Qd.col(i * downsample - 1);
        qnow.u = ref.us.col(i * downsample - 1);
        x = join_vert(join_vert(qnow.q,qnow.qd),qnow.u);
        for (int j = 0; j < N_sample; j++) {
            randomize_links(Sigma_link,link_param,link_perturb);
            generate_forw_dyn_tape(m,m+n,link_perturb,x);
            dyn::auto_jacobian(TAG_FORW_DYN_JAC,x,dft.slice(j));
        }
        dft_mean = mean(dft,2);
        dft_var = zeros<mat>(n_par,n_par);
        for (int j = 0; j < N_sample; j++) {
            dft_diff = vectorise(dft.slice(j) - dft_mean);
            dft_var += dft_diff*dft_diff.t();
        }
        dft_var /= N_sample;
        Sigma_dyn.slice(i-1) = dft_var;
        A.slice(i - 1) = dft_mean.rows(0,n-1).t();
        B.slice(i - 1) = dft_mean.rows(n,n+m-1).t();
    }
    LTV ltv_down(m,n,k,N);
    LTV ltv_cts(m,n,k,ref.Q.n_cols);
    ltv_down.A = A;
    ltv_down.B = B;
    ltv_down.Sigmas = Sigma_dyn;
    ltv_down.upsample(ltv_cts);
    ltv_cts.discrete = false;
    ltv_cts.discretize(DT,ltv);

}

} // namespace ends here

LTV::LTV(int m, int n, int k, int N) {

    A = zeros<cube>(k,n,N);
    B = zeros<cube>(k,m,N);
    int p = (n+m)*k;
    Sigmas = zeros<cube>(p,p,N);
    Gammas = zeros<cube>(p,p,N);
}

void LTV::init_cov(const double & init_precision) {

    int n = A.n_cols;
    int m = B.n_cols;
    int k = A.n_rows; // should be equal to B.n_rows
    int N = A.n_slices;
    int p = (n+m)*k;
    Sigmas = zeros<cube>(p, p, N);
    Gammas = zeros<cube>(p, p, N);
    Sigmas.each_slice() = (1/init_precision) * eye(p,p);
    Gammas.each_slice() = init_precision * eye(p,p);
}

void LTV::downsample(const int down, LTV & ltv_down) const {

    int m = this->B.n_cols;
    int n = this->A.n_cols;
    int k = this->A.n_rows;
    int N = this->A.n_slices;
    int N_down = N/down;
    uvec idx = linspace<uvec>(down, N_down * down, N_down) - 1;
    ltv_down = LTV(m,n,k,N_down);
    for (int i = 0; i < N_down; i++) {
        ltv_down.A.slice(i) = this->A.slice(idx(i));
        ltv_down.B.slice(i) = this->B.slice(idx(i));
        ltv_down.Sigmas.slice(i) = this->Sigmas.slice(idx(i));
        ltv_down.Gammas.slice(i) = this->Gammas.slice(idx(i));
    }
}

void LTV::upsample(LTV & ltv_up) const {

    adapt::upsample(this->A,ltv_up.A);
    adapt::upsample(this->B,ltv_up.B);
    adapt::upsample(this->Sigmas,ltv_up.Sigmas);
    adapt::upsample(this->Gammas,ltv_up.Gammas);
}

void LTV::discretize(const double & dt, LTV & ltv) const {

    if (discrete) {
        cout << "Warning! LTV is already discrete. Returning...\n";
        return;
    }

    int N = this->A.n_slices;
    int m = this->B.n_cols;
    int n = this->A.n_cols;
    int k = this->A.n_rows;
    int k_discr = 2*k;
    int p_discr = (n+m)*k_discr;

    ltv.Sigmas = zeros<cube>(p_discr,p_discr,N);
    // map [0,I;Ac] covariance to Ad covariance
    umat idx = linspace<umat>(1,k_discr*(n+m),k_discr*(n+m)) - 1;
    idx = reshape(idx,k,2*(k_discr+m));
    uvec ind = linspace<uvec>(2,2*(n+m),(n+m)) - 1;
    idx = idx.cols(ind);
    uvec idx_cts = vectorise(idx);

    // precision is not needed for control which needs only discrete Sigma values
    cube A = zeros<cube>(k_discr,n,N);
    cube B = zeros<cube>(k_discr,m,N);
    mat OI = join_horiz(zeros<mat>(k,k), eye<mat>(k,k));
    mat O = zeros<mat>(k,m);
    mat temp = zeros<mat>(p_discr,p_discr);
    // enlarge continuous A,B mats
    for (int i = 0; i < N; i++) {
        A.slice(i) = join_vert(OI,this->A.slice(i));
        B.slice(i) = join_vert(O,this->B.slice(i));
        temp.submat(idx_cts,idx_cts) = DT*DT * this->Sigmas.slice(i);
        ltv.Sigmas.slice(i) = temp;
    }
    dyn::discretize_dynamics(dt, A, B, ltv.A, ltv.B);

}
