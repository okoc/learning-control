/**
 * @file dynamics.cpp
 *
 * @brief Dynamics related functions (except for the math
 *        computations) are located here.
 *  Created on: Aug 15, 2017
 *      Author: okoc
 */

#include <adolc/adouble.h>            // use of active doubles
#include <adolc/drivers/drivers.h>    // use of "Easy to Use" drivers
#include <adolc/taping.h>             // use of taping
#include <armadillo>
#include <iostream>
#include <string>
#include "math.h"
#include "constants.h"
#include "traj.h"
#include "dynamics.h"

using namespace arma;
using namespace Traj;
namespace dyn {

vec link::vectorise() const {

    vec link_vec = zeros<vec>(10);
    link_vec(0) = this->mass;
    link_vec(span(1,3)) = this->mcm;
    link_vec(4) = this->inertia(0,0);
    link_vec(5) = this->inertia(0,1);
    link_vec(6) = this->inertia(0,2);
    link_vec(7) = this->inertia(1,1);
    link_vec(8) = this->inertia(1,2);
    link_vec(9) = this->inertia(2,2);
    return link_vec;
}

void vectorise_links(const link link_params[NDOF+1], mat & link_mat) {

    for (int i = 0; i < NDOF+1; i++) {
        link_mat.col(i) = link_params[i].vectorise();
    }
}

void aggregate_links(const mat & link_mat, link link_params[NDOF+1]) {

    mat inertia = zeros<mat>(3,3);

    for (int i = 0; i < NDOF+1; i++) {
        link_params[i].mass = link_mat(0,i);
        link_params[i].mcm = link_mat(span(1,3),i);
        inertia = zeros<mat>(3,3);

        inertia(0,0) = link_mat(4,i);
        inertia(0,1) = link_mat(5,i);
        inertia(0,2) = link_mat(6,i);
        inertia(1,1) = link_mat(7,i);
        inertia(1,2) = link_mat(8,i);
        inertia(2,2) = link_mat(9,i);
        link_params[i].inertia = symmatu(inertia);
    }

}

void evolve(f_dyn forward,
            const link link_param[NDOF + 1],
            const traj & ref,
            const mat & u_ff,
            const mat & fb,
            const joint & qinit,
            traj & traj_act) {

    if (fb.n_rows != NDOF && fb.n_cols != 2 * NDOF)
        throw "Feedback matrix size must be 7 x 14!";
    vec error = zeros<vec>(2 * NDOF, 1);
    int N = ref.Q.n_cols;
    joint qnow = qinit;
    for (int i = 0; i < N; i++) {
        error = join_vert(qnow.q - ref.Q.col(i), qnow.qd - ref.Qd.col(i));
        qnow.u = u_ff.col(i) + fb * error;
        // evolve system dynamics with symplectic euler
        forward(link_param, qnow);
        qnow.qd += DT * qnow.qdd;
        qnow.q += DT * qnow.qd;
        traj_act.fill(qnow, i);
    }
}

void evolve(f_dyn forward,
            const link link_param[NDOF + 1],
            const traj & ref,
            const mat & u_ff,
            const cube & fb,
            const joint & qinit,
            traj & traj_act) {

    if (fb.n_rows != NDOF && fb.n_cols != 2 * NDOF)
        throw "Feedback matrix size must be NDOF x 2*NDOF!";
    vec error = zeros<vec>(2 * NDOF, 1);
    unsigned N = ref.Q.n_cols;
    joint qnow = qinit;
    for (unsigned i = 0; i < N; i++) {
        error = join_vert(qnow.q - ref.Q.col(i), qnow.qd - ref.Qd.col(i));
        qnow.u = u_ff.col(i) + fb.slice(i) * error;
        //cout << "u:\n" << qnow.u.t();
        // evolve system dynamics with symplectic euler
        forward(link_param, qnow);
        //cout << "acc:\n" << qnow.qdd.t();
        qnow.qd += DT * qnow.qdd;
        qnow.q += DT * qnow.qd;
        //cout << "pos:\n" << qnow.q.t();
        traj_act.fill(qnow, i);
    }
}

void calc_inv_dynamics_for_traj(f_dyn f_inv,
                                const link link_param[NDOF + 1],
                                traj & ref) {

    const int N = ref.Q.n_cols;
    ref.us = zeros<mat>(NDOF, N);
    joint qnow;
    for (int i = 0; i < N; i++) {
        qnow.q = ref.Q.col(i);
        qnow.qd = ref.Qd.col(i);
        qnow.qdd = ref.Qdd.col(i);
        f_inv(link_param, qnow);
        ref.us.col(i) = qnow.u;
    }
}

void linearize_dynamics(f_dyn f,
                        const link link_param[NDOF + 1],
                        const traj & ref,
                        const int downsample,
                        const double h,
                        cube & A,
                        cube & B) {

    unsigned N = ref.Q.n_cols / downsample;
    unsigned n = 2 * NDOF;
    unsigned m = NDOF;
    joint qnow;
    A = zeros<cube>(n, n, N);
    B = zeros<cube>(n, m, N);
    mat df = zeros<mat>(NDOF, n + m);
    mat qdd_plus = zeros<mat>(NDOF, n + m);
    mat qdd_minus = zeros<mat>(NDOF, n + m);
    for (unsigned i = 1; i <= N; i++) {
        qnow.q = ref.Q.col(i * downsample - 1);
        qnow.qd = ref.Qd.col(i * downsample - 1);
        qnow.u = ref.us.col(i * downsample - 1);
        for (unsigned j = 0; j < NDOF; j++) {
            qnow.q(j) += h; // modifying joint pos
            f(link_param, qnow);
            qdd_plus.col(j) = qnow.qdd;
            qnow.q(j) -= 2 * h;
            f(link_param, qnow);
            qdd_minus.col(j) = qnow.qdd;
            qnow.q(j) += h;

            qnow.qd(j) += h; // modifying joint vel
            f(link_param, qnow);
            qdd_plus.col(j + NDOF) = qnow.qdd;
            qnow.qd(j) -= 2 * h;
            f(link_param, qnow);
            qdd_minus.col(j + NDOF) = qnow.qdd;
            qnow.qd(j) += h;

            qnow.u(j) += h; // modifying joint inputs
            f(link_param, qnow);
            qdd_plus.col(j + 2*NDOF) = qnow.qdd;
            qnow.u(j) -= 2 * h;
            f(link_param, qnow);
            qdd_minus.col(j + 2*NDOF) = qnow.qdd;
            qnow.u(j) += h;
        }
        df = (qdd_plus-qdd_minus)/(2*h);
        A.slice(i-1) = join_vert(
                        join_horiz(zeros<mat>(NDOF, NDOF), eye<mat>(NDOF, NDOF)),
                        df.cols(0, n-1));
        B.slice(i-1) = join_vert(zeros<mat>(NDOF, NDOF),
                                    df.cols(n, n+m-1));
    }
}

void linearize_dynamics(const link link_param[NDOF + 1],
                        const traj & ref,
                        const int downsample,
                        cube & A,
                        cube & B) {

    unsigned N = ref.Q.n_cols / downsample;
    unsigned n = 2 * NDOF;
    unsigned m = NDOF;
    joint qnow;
    A = zeros<cube>(n, n, N);
    B = zeros<cube>(n, m, N);
    mat dft = zeros<mat>(n+m, m);
    vec x = zeros<vec>(m+n);
    generate_forw_dyn_tape(m,m+n,link_param,x);
    for (unsigned i = 1; i <= N; i++) {
        qnow.q = ref.Q.col(i * downsample - 1);
        qnow.qd = ref.Qd.col(i * downsample - 1);
        qnow.u = ref.us.col(i * downsample - 1);
        x = join_vert(join_vert(qnow.q,qnow.qd),qnow.u);
        auto_jacobian(TAG_FORW_DYN_JAC,x,dft);
        A.slice(i-1) = join_vert(
                        join_horiz(zeros<mat>(NDOF,NDOF), eye<mat>(NDOF,NDOF)),
                        dft.rows(0,n-1).t());
        B.slice(i-1) = join_vert(zeros<mat>(NDOF, NDOF),
                                    dft.rows(n,n+m-1).t());
    }
}

void generate_forw_dyn_tape(const int m,
                            const int n,
                            const link link_param[NDOF+1],
                            const vec & x) {

    adouble *x_auto = new adouble[n];
    adouble *qdd_auto = new adouble[m];
    double dummy;

    trace_on(TAG_FORW_DYN_JAC,1);
    for (int i = 0; i < n; i++)
        x_auto[i] <<= x(i);

    barrett_wam_dynamics_art_auto(link_param,x_auto,qdd_auto);
    for(int i = 0; i < m; i++)
        qdd_auto[i] >>= dummy;
    trace_off();

    delete[] x_auto;
    delete[] qdd_auto;
}

void generate_inv_dyn_tape(const int m, const int n, const joint & Q) {

    adouble *x_auto = new adouble[n];
    adouble *u_auto = new adouble[m];
    double *x = new double[n];
    double dummy;

    trace_on(TAG_INV_DYN_JAC,1);
    for (int i = 0; i < n; i++)
        x_auto[i] <<= x[i];

    inv_dyn_ne_auto(x_auto,Q.q,Q.qd,Q.qdd,u_auto);
    for(int i = 0; i < m; i++)
        u_auto[i] >>= dummy;
    trace_off();

    delete[] x_auto;
    delete[] x;
    delete[] u_auto;
}

void auto_jacobian(const int & tag, vec & x, mat & jact) {
    const int m = 7; //jact.n_cols;
    const int n = jact.n_rows;
    const int k = 1;
    double y[n];
    //cout << "Initializing jac...\n";

    /*double **cjac = new double*[m];
    for (int i = 0; i < m; i++) {
        cjac[i] = new double[n];
    }
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            cjac[i][j] = jact(j,i);
        }
    }*/

    double *cjac[m] = {jact.colptr(0), jact.colptr(1), jact.colptr(2),
                         jact.colptr(3), jact.colptr(4), jact.colptr(5),
                         jact.colptr(6)};
    //cout << "Before jac...\n";
    large_jacobian(tag,m,n,k,x.memptr(),y,cjac);

    /*for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            jact(j,i) = cjac[i][j];;
        }
    }

    for (int i = 0; i < m; i++) {
        delete cjac[i];
    }
    delete cjac;*/
    //cout << "After jac...\n";
}

void discretize_dynamics(const double dt,
                        const cube & A,
                        const cube & B,
                        cube & Ad,
                        cube & Bd) {
    unsigned N = A.n_slices;
    unsigned n = A.n_cols;
    unsigned m = B.n_cols;

    if (B.n_slices != N || A.n_rows != n || B.n_rows != n) {
        throw "Dimension of matrices are not consistent!";
    }

    Ad = zeros<cube>(n, n, N);
    Bd = zeros<cube>(n, m, N);
    mat M;
    for (unsigned i = 0; i < N; i++) {
        M = join_vert(join_horiz(A.slice(i), B.slice(i)), zeros<mat>(m, n + m));
        M = expmat(dt * M);
        Ad.slice(i) = M(span(0, n - 1), span(0, n - 1));
        Bd.slice(i) = M(span(0, n - 1), span(n, n + m - 1));
    }
}

void lift_dynamics(f_dyn forward,
                   const link link_param[NDOF + 1],
                   const traj & ref,
                   const int downsample,
                   const double h,
                   mat & F) {

    cube A, B, Ad, Bd;
    double dt = DT * downsample;
    linearize_dynamics(forward, link_param, ref, downsample, h, A, B);
    discretize_dynamics(dt, A, B, Ad, Bd);

    unsigned N = Ad.n_slices;
    unsigned n = Ad.n_cols;
    unsigned m = Bd.n_cols;

    F = zeros<mat>(N * n, N * m);
    mat M = zeros<mat>(n, m); // temporary matrix

    for (unsigned i = 0; i < N; i++) {
        M = Bd.slice(i);
        for (unsigned j = i; j < N - 1; j++) {
            F(span(j * n, (j + 1) * n - 1), span(i * m, (i + 1) * m - 1)) = M;
            M = Ad.slice(j + 1) * M;
        }
        F(span((N - 1) * n, N * n - 1), span(i * m, (i + 1) * m - 1)) = M;
    }
}

void lqr_fin_horizon_ltv(const cube & Ad,
                        const cube & Bd,
                        const mat & Q,
                        const mat & Qf,
                        const mat & R,
                        cube & K,
                        cube & P) {
    int m = Bd.n_cols;
    int n = Ad.n_rows;
    int N = Ad.n_slices;

    P.slice(N) = Qf;
    mat M = zeros<mat>(n, n);
    mat Phi = zeros<mat>(m, m);
    mat Psi = zeros<mat>(m, n);

    for (int i = N - 1; i >= 0; i--) {
        M = Ad.slice(i).t() * P.slice(i + 1) * Ad.slice(i);
        Phi = R + Bd.slice(i).t() * P.slice(i + 1) * Bd.slice(i);
        Psi = Bd.slice(i).t() * P.slice(i + 1) * Ad.slice(i);
        K.slice(i) = -solve(Phi, Psi);
        P.slice(i) = Q + M + Psi.t() * K.slice(i);
        // this step ensures that riccatti recursion is stable!
        P.slice(i) = (P.slice(i) + P.slice(i).t()) / 2;
    }
}

void load_PD_feedback_gains(mat & fb) {

    using namespace std;
    string env = getenv("HOME");
    string filename = env + "/learning-control/config/Gains.cfg";
    mat pd_gains;
    pd_gains.load(filename);
    for (int i = 0; i < NDOF; i++) {
        fb(i, i) = -pd_gains(i, 0); //
        fb(i, i + NDOF) = -pd_gains(i, 1);
    }

}

void load_LQR_feedback(mat & fb, int version) {

    using namespace std;
    string env = getenv("HOME");
    string filename = env + "/learning-control/config/LQR" + to_string(version)
            + ".txt";
    fb.load(filename);
}

void load_link_params(const std::string & filename,
                        link link_param[NDOF + 1]) {

    using namespace std;
    string env = getenv("HOME");
    string fullname = env + "/learning-control/config/" + filename;
    static mat link_mat = zeros<mat>(NDOF+1,10);
    link_mat.load(fullname);
    //cout << link_mat;
    mat inertia = zeros<mat>(3,3);

    for (unsigned i = 0; i < link_mat.n_rows; i++) {
        link_param[i].mass = link_mat(i, 0);
        link_param[i].mcm(0) = link_mat(i, 1);
        link_param[i].mcm(1) = link_mat(i, 2);
        link_param[i].mcm(2) = link_mat(i, 3);
        inertia(0, 0) = link_mat(i, 4); //element 11
        inertia(0, 1) = link_mat(i, 5); //12 and 21
        inertia(0, 2) = link_mat(i, 6);
        inertia(1, 1) = link_mat(i, 7);
        inertia(1, 2) = link_mat(i, 8);
        inertia(2, 2) = link_mat(i, 9);
        link_param[i].inertia = symmatu(inertia);
    }
}

void randomize_links(const double & std,
                    const link link_param[NDOF + 1],
                    link new_links[NDOF + 1]) {
    mat33 rand_mat;
    for (int i = 0; i < NDOF + 1; i++) {
        new_links[i].mass = (1 + std * randu()) * link_param[i].mass;
        new_links[i].mcm = (1 + std * randu(3, 1)) % link_param[i].mcm;
        rand_mat = 1 + std * randu(3, 3);
        rand_mat = (rand_mat + rand_mat.t()) / 2.0;
        new_links[i].inertia = rand_mat % link_param[i].inertia;
    }

}

void randomize_links(const mat & Sigma,
                    const link link_param[NDOF + 1],
                    link new_links[NDOF + 1]) {
    uvec inertia_idx = {0,3,6,4,7,8};
    mat33 rand_mat;
    vec perturb = chol(Sigma) * randn(Sigma.n_cols,1); // of length 80
    for (int i = 0; i < NDOF + 1; i++) {
        new_links[i].mass = link_param[i].mass + perturb(i*10 + 0);
        new_links[i].mcm = link_param[i].mcm + perturb(span(i*10+1,i*10+3));
        rand_mat = zeros<mat>(3,3);
        rand_mat.elem(inertia_idx) = perturb(span(i*10+4,i*10+9));
        rand_mat += rand_mat.t();
        new_links[i].inertia = rand_mat + link_param[i].inertia;
    }

}

}
