/*
 * test_table_tennis.cpp
 *
 * Here we are testing the application of ILC to improving
 * table tennis performance of Barrett WAM.
 *
 * In particular, trajectories that are optimized on the kinematics
 * level (3rd order polynomials for hitting the ball with minimal
 * accelerations throughout) need to be TRACKED well.
 *
 * NOTE:
 * The optimizer for trajectories is running only ONCE.
 *
 *  Created on: Apr 8, 2018
 *      Author: okoc
 */

#include <boost/test/unit_test.hpp>
#include <armadillo>
#include "optim.h"
#include "constants.h"
#include "lookup.h"
#include "tabletennis.h"
#include "kinematics.hpp"
#include "traj.h"
#include "player.hpp"
#include "kalman.h"

using namespace arma;
using namespace Traj;
using namespace player;
using namespace optim;

/*
 * Testing OPTIMIZATION from external libplayer as opposed to lookup.
 * Checking for legal landing of ball.
 */
void test_table_tennis() {

    BOOST_TEST_MESSAGE("Testing for ball landing...");

    double lb[2*NDOF+1], ub[2*NDOF+1];

    double SLACK = 0.01;
    double Tmax = 1.0;
    set_bounds(lb,ub,SLACK,Tmax);

    // update initial parameters from lookup table
    BOOST_TEST_MESSAGE("Looking up a random ball entry...");
    //arma_rng::set_seed(5);
    arma_rng::set_seed_random();
    vec::fixed<15> strike_params;
    vec6 ball_state;
    lookup_random_entry(ball_state,strike_params);
    optim::joint qact;
    vec7 qrest;
    init_right_posture(qrest);
    qact.q = qrest;

    EKF filter = player::init_filter();
    mat66 P;
    P.eye();
    filter.set_prior(ball_state,P);
    double time_land_des = 0.8;
    double time2return = 1.0;
    mat balls_pred = filter.predict_path(DT,1000);
    vec2 ball_land_des = {0.0, dist_to_table - 3*table_length/4};
    optim_des racket_params;
    racket_params.Nmax = 1000;
    racket_params = calc_racket_strategy(balls_pred,
                                         ball_land_des,
                                         time_land_des,
                                         racket_params);

    FocusedOptim opt = FocusedOptim(qact.q.memptr(),lb,ub);
    opt.set_des_params(&racket_params);
    opt.update_init_state(qact);
    opt.set_return_time(time2return);
    opt.set_verbose(true);
    opt.set_detach(false);
    opt.run();
    optim::spline_params poly;
    bool update = opt.get_params(qact,poly);

    BOOST_TEST(update);

    vec::fixed<8> rest_params;

    BOOST_TEST(time2return > time_land_des); // ball should land before rest

    rest_params.head(NDOF) = qrest;
    rest_params(NDOF) = time2return;
    int N = (poly.time2hit + time2return)/DT;
    racket robot_racket;
    bool spin = false;
    TableTennis tt = TableTennis(ball_state,spin,true);
    double t = 0.0;

    for (int i = 0; i < N; i++) { // one trial

        player::update_next_state(poly,qrest,time2return,t,qact);
        calc_racket_state(qact,robot_racket);
        tt.integrate_ball_state(robot_racket,DT);
    }

    BOOST_TEST(tt.has_legally_landed());

}

/*
 * Refactored version of table tennis test.
 */
void test_table_tennis_refactored() {

    BOOST_TEST_MESSAGE("Testing for ball landing with ballgun...");
    TableTennis tt = TableTennis(false,true);
    traj strike, ret;
    bool gen = false;
    int trial = 1;
    while (!gen) {
        BOOST_TEST_MESSAGE("Optimizing traj. Trial: " << trial++);
        BOOST_TEST_MESSAGE("Setting seed to random");
        arma_rng::set_seed_random();
        double std = as_scalar(randu<mat>(1,1))/10;
        int ballgun_side = rand() % 3;
        tt.set_ball_gun(std,ballgun_side);
        vec6 ball_state = tt.get_ball_state();
        double time_land_des = 0.8;
        double time2return = 1.0;
        vec2 ball_land_des = {0.0, dist_to_table - 3*table_length/4};

        Traj::joint qact;
        Traj::spline_params poly;
        vec7 qrest;
        init_right_posture(qrest);
        qact.q = qrest;
        gen = gen_random_tabletennis_traj(ball_state,qact,qrest,ball_land_des,
                                time2return,time_land_des,strike,ret,poly);
    }
    traj ref = strike;
    ref.join(ret);
    racket robot_racket;
    optim::joint q_now;

    for (unsigned i = 0; i < ref.Q.n_cols; i++) { // one trial

        q_now.q = ref.Q.col(i);
        q_now.qd = ref.Qd.col(i);
        calc_racket_state(q_now,robot_racket);
        tt.integrate_ball_state(robot_racket,DT);
    }

    BOOST_TEST(tt.has_legally_landed());
}
