/*
 * test_filter.cpp
 *
 * Unit test for zero-phase filtering, a.k.a. 'filtfilt'
 * and (Extended) Kalman Smoothing
 */

#include <boost/test/unit_test.hpp>
#include <armadillo>
#include "filters.h"
using namespace arma;

/*
 * Testing the filtfilt.
 *
 * We expect the filtered signal to resemble the original,
 * i.e. low rms error
 *
 */
void test_filtfilt() {

    BOOST_TEST_MESSAGE("Testing filtfilt...");

    /*vec X = randu<vec>(5);
     cout << X << endl << flipud(X) << endl;*/

    const int seed = 1;
    const int N = 50;
    const double pi = 3.1416;
    const double dt = 0.01;
    const double std_noise = 0.1;
    arma_rng::set_seed(seed);
    // create a sinusoid mixed with some cosine
    vec t = dt * linspace<vec>(1,N,N);
    vec signal = sin(2*pi*5*t) + 0.2*cos(2*pi*6*t);

    // add noise
    vec noise = std_noise * randn<vec>(N);
    vec noisy_signal = signal + noise;
    //cout << noisy_signal;

    // filtfilt
    double cutoff = 6.0;
    unsigned cutoff_perc = 100 * cutoff / (N / 2);
    vec filt_signal = noisy_signal;
    filtfilt2(noisy_signal, cutoff_perc, filt_signal);
    //cout << filt_signal.t();
    //cout << signal.t() << endl << noise.t() << endl << filt_signal.t();
    double rms_noise = norm(noise, 2);
    double rms_filt = norm(filt_signal - signal, 2);
    double rms_matlab = 0.2754;
    BOOST_TEST_MESSAGE(
            "Filtered signal should have zero phase!\n" << "and lower rms error: " << rms_filt << " < " << rms_noise);
    BOOST_TEST(rms_filt < rms_noise);
    BOOST_TEST_MESSAGE("Checking with MATLAB (using arma randn on matlab).");
    BOOST_TEST(rms_filt == rms_matlab, boost::test_tools::tolerance(0.001));
}
