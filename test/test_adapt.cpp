/*
 * test_adapt.cpp
 *
 *  Created on: Mar 28, 2018
 *      Author: okoc
 */

#include <boost/test/unit_test.hpp>
#include <adolc/adouble.h>            // use of active doubles
#include <adolc/drivers/drivers.h>    // use of "Easy to Use" drivers
#include <adolc/taping.h>             // use of taping
#include <armadillo>
#include "constants.h"
#include "traj.h"
#include "dynamics.h"
#include "experiment.h"
#include "adaptation.h"

using namespace dyn;
using namespace Traj;
using namespace adapt;

/*
 * Testing incremental system identification with Inverse Dynamics parameters
 */
void test_sys_id() {

    BOOST_TEST_MESSAGE("Checking convergence of sys. id. of inv. dyn param...");

    arma_rng::set_seed_random();
    vec7 qrest;
    init_right_posture(qrest);
    joint qinit;
    qinit.q = qrest;

    BOOST_TEST_MESSAGE("Looking up a random trajectory...");
    traj strike, returning;
    gen_random_tabletennis_traj(qinit, qrest, 1.0, strike, returning);
    int N = strike.Q.n_cols;
    //links link_param_nom[NDOF+1];
    dyn::link link_param_act[NDOF + 1];
    dyn::link link_param_nom[NDOF + 1];
    const std::string filename_nom = "LinkParameters2.cfg";
    const std::string filename_act = "LinkParameters1.cfg";
    load_link_params(filename_nom, link_param_nom);
    load_link_params(filename_act, link_param_act);
    calc_inv_dynamics_for_traj(barrett_wam_inv_dynamics_ne,
                               link_param_nom,
                               strike);
    check_all_limits(strike);

    BOOST_TEST_MESSAGE("Calculating LQR feedback mats...");
    cube A_nom, B_nom, Ad_nom, Bd_nom;
    linearize_dynamics(link_param_nom, strike, 1, A_nom, B_nom);
    discretize_dynamics(DT, A_nom, B_nom, Ad_nom, Bd_nom);
    cube Klqr = zeros<cube>(NDOF, 2*NDOF, N);
    cube P = zeros<cube>(2*NDOF, 2*NDOF, N + 1);
    mat Q = eye(2*NDOF,2*NDOF);
    cube Qs = zeros<cube>(2*NDOF, 2*NDOF, N);
    Qs.each_slice() = eye(2*NDOF, 2*NDOF);
    mat R = 0.05 * eye(NDOF, NDOF);
    lqr_fin_horizon_ltv(Ad_nom, Bd_nom, Q, Q, R, Klqr, P);

    traj traj_act = strike;
    evolve(barrett_wam_dynamics_art,
            link_param_act, strike, strike.us,
            Klqr, qinit, traj_act);

    int num_iter = 10;
    vec rms = zeros<vec>(num_iter+1);
    rms(0) = traj_act.calc_rms_error(strike,strike.Q.n_cols);

    // prepare regression
    int m = NDOF;
    int n = (NDOF+1)*10;
    mat Xmat;
    vec y;
    mat link_mat = zeros<mat>(10,NDOF+1); //regressed params
    double lambda = 1.0; // forgetting factor
    double prec = 0.1;
    mat Gamma(n,n);
    Gamma.eye();
    Gamma *= prec;
    mat gamma_new = Gamma;
    vectorise_links(link_param_nom,link_mat);
    vec theta = vectorise(link_mat);

    BOOST_TEST_MESSAGE("Entering the regression loop...");
    for (int i = 0; i < num_iter; i++) {
        form_regressor(traj_act, theta, Xmat, y);

        gamma_new = (Xmat.t() * Xmat) + lambda*Gamma;
        theta = solve(gamma_new, Gamma * theta + (Xmat.t() * y));
        Gamma = gamma_new;

        link_mat = reshape(theta,10,NDOF+1);
        aggregate_links(link_mat,link_param_nom);
        calc_inv_dynamics_for_traj(barrett_wam_inv_dynamics_ne,
                                    link_param_nom,
                                    strike);
        evolve(barrett_wam_dynamics_art,
                link_param_act, strike, strike.us,
                Klqr, qinit, traj_act);

        rms(i+1) = traj_act.calc_rms_error(strike,strike.Q.n_cols);
    }
    BOOST_TEST_MESSAGE("RMS error on traj: \n" << rms.t());
    BOOST_TEST(rms(num_iter) < rms(0));
}

/*
 * Test calculating covariance of forward dynamics derivatives
 */
void check_calc_dyn_derivative_cov() {

    BOOST_TEST_MESSAGE("Calculating forward dyn derivative mean and covar...");
    BOOST_TEST_MESSAGE("Using Monte Carlo sampling and autodiff...");

    arma_rng::set_seed_random();
    vec7 qrest;
    init_right_posture(qrest);
    joint qinit;
    qinit.q = qrest;
    traj strike, returning;
    gen_random_tabletennis_traj(qinit, qrest, 1.0, strike, returning);
    cube A_num, B_num, A_auto, B_auto;
    dyn::link link_param[NDOF + 1];
    const std::string filename = "LinkParameters1.cfg";
    load_link_params(filename, link_param);
    calc_inv_dynamics_for_traj(barrett_wam_inv_dynamics_ne,
                               link_param,
                               strike);

    // create random covariance
    mat Sigma_link = randn<mat>(80,80);
    Sigma_link = (Sigma_link + Sigma_link.t()) / 2.0;
    Sigma_link += 80 * eye<mat>(80,80);
    Sigma_link = 0.01 * Sigma_link/80;

    LTV ltv(NDOF,2*NDOF,NDOF,strike.Q.n_cols);
    int N_sample = 100;
    int downsample = 20;
    BOOST_TEST_MESSAGE("Numerical differentiation of forward dyn...");
    wall_clock timer;
    timer.tic();
    linearize_dynamics(link_param, Sigma_link, strike, N_sample, downsample, ltv);
    double t = timer.toc();
    BOOST_TEST_MESSAGE("Time elapsed (ms) : " << 1000*t);
}
