/*
 * test_lqr.cpp
 *
 * Testing performance of LQR feedback matrices
 * on Barrett WAM.
 *
 *  Created on: Feb 17, 2018
 *      Author: okoc
 */

#include <boost/test/unit_test.hpp>
#include <armadillo>
#include "constants.h"
#include "traj.h"
#include "dynamics.h"

using namespace arma;
using namespace dyn;
using namespace Traj;

/*
 * Testing LQR on Barrett WAM. Comparing with PD and another fb matrix.
 *
 * Using linearized Barrett WAM dynamics around a random reference
 * trajectory to compute LQR matrices. The comparison is made to PD
 * and another constant matrix (loaded LQR matrix computed in MATLAB).
 */
void test_compare_lqr_wam() {

    BOOST_TEST_MESSAGE("Testing LQR with linearized WAM dynamics...");
    arma_rng::set_seed_random();
    //arma_rng::set_seed(2);
    int m = NDOF;
    int n = 2 * NDOF;

    BOOST_TEST_MESSAGE("Loading PD gains from file...");
    mat pd = zeros<mat>(m, n);
    load_PD_feedback_gains(pd);
    mat lqr = zeros<mat>(m, n);
    int lqr_version = 5;
    BOOST_TEST_MESSAGE(
            "Loading const LQR matrix " << lqr_version << " from file...");
    load_LQR_feedback(lqr, lqr_version);
    vec7 qrest;
    init_right_posture(qrest);
    joint qinit;
    qinit.q = qrest;

    BOOST_TEST_MESSAGE("Looking up a random trajectory...");
    traj strike, returning;
    gen_random_tabletennis_traj(qinit, qrest, 1.0, strike, returning);
    int N = strike.Q.n_cols;
    //links link_param_nom[NDOF+1];
    dyn::link link_param_act[NDOF + 1];
    //const std::string filename_nom = "LinkParameters2.cfg";
    const std::string filename_act = "LinkParameters1.cfg";
    //load_link_params(filename_nom,link_param_nom);
    load_link_params(filename_act, link_param_act);
    calc_inv_dynamics_for_traj(barrett_wam_inv_dynamics_ne, link_param_act,
                                strike);
    check_all_limits(strike);

    BOOST_TEST_MESSAGE("Calculating LQR feedback mats...");
    cube A_nom, B_nom, Ad_nom, Bd_nom;
    linearize_dynamics(barrett_wam_dynamics_art, link_param_act, strike, 1,
            1e-6, A_nom, B_nom);
    discretize_dynamics(DT, A_nom, B_nom, Ad_nom, Bd_nom);
    cube Klqr = zeros<cube>(m, n, N);
    cube P = zeros<cube>(n, n, N + 1);
    mat Q = eye(n, n);
    cube Qs = zeros<cube>(n, n, N);
    Qs.each_slice() = eye(n, n);
    mat R = 1e-2 * eye(m, m);
    lqr_fin_horizon_ltv(Ad_nom, Bd_nom, Q, Q, R, Klqr, P);

    // track traj. with PD fb
    traj act_pd = strike;
    traj act_load_lqr = strike;
    traj act_comp_lqr = strike;
    evolve(barrett_wam_dynamics_art, link_param_act, strike, strike.us, pd,
            qinit, act_pd);
    evolve(barrett_wam_dynamics_art, link_param_act, strike, strike.us, lqr,
            qinit, act_load_lqr);
    // track traj. with LQR fb
    // interesting to note that applying only the first LQR matrix
    // leads to smaller rms error!
    evolve(barrett_wam_dynamics_art, link_param_act, strike, strike.us,
            Klqr.slice(0), qinit, act_comp_lqr);
    double rms_pd = norm(act_pd.Q - strike.Q, "fro") / sqrt(strike.Q.n_elem);
    double rms_comp_lqr = norm(act_comp_lqr.Q - strike.Q, "fro")
                            / sqrt(strike.Q.n_elem);
    double rms_load_lqr = norm(act_load_lqr.Q - strike.Q, "fro")
                            / sqrt(strike.Q.n_elem);

    BOOST_TEST_MESSAGE("Tracking error with PD-feedback = " << rms_pd);
    BOOST_TEST_MESSAGE(
            "Tracking error with loaded LQR-feedback = " << rms_load_lqr);
    BOOST_TEST_MESSAGE(
            "Tracking error with computed LQR-feedback = " << rms_comp_lqr);
    BOOST_TEST(rms_comp_lqr < rms_load_lqr,
            boost::test_tools::tolerance(0.001));
}

/*
 * Testing performance of LQR computed with a nominal model.
 *
 * We compare the cross-performance of three different nominal models
 * and the LQR matrices computed as a result.
 *
 */
void test_lqr_wam_nominal() {

    BOOST_TEST_MESSAGE("Testing LQR computed with wrong models...");
    dyn::link link_param_nom[NDOF + 1];
    dyn::link link_param_act[NDOF + 1];
    arma_rng::set_seed_random();
    int m = NDOF;
    int n = 2 * NDOF;
    std::vector<int> models = { 1, 2, 3 };
    mat r_vals = { { 0.01, 20, 0.05 },
                   { 0.01, 0.01, 0.01 },
                   { 20, 200, 0.01 } };
    BOOST_TEST_MESSAGE("Minimum penalties for stability:\n" << r_vals);
    BOOST_TEST_MESSAGE("rows = nominal, cols = actual\n");
    vec7 qrest;
    init_right_posture(qrest);
    joint qinit;
    qinit.q = qrest;

    for (unsigned i = 0; i < models.size(); i++) {
        for (unsigned j = 0; j < models.size(); j++) {

            traj strike, returning;
            gen_random_tabletennis_traj(qinit, qrest, 1.0, strike, returning);
            //strike.relax(5);
            int N = strike.Q.n_cols;
            traj traj_act(N);
            std::string filename_nom = "LinkParameters"
                    + std::to_string(models[i]) + ".cfg";
            std::string filename_act = "LinkParameters"
                    + std::to_string(models[j]) + ".cfg";
            load_link_params(filename_nom, link_param_nom);
            load_link_params(filename_act, link_param_act);
            strike.us = zeros<mat>(NDOF, N);
            //calc_inv_dynamics_for_traj(barrett_wam_inv_dynamics_ne,
            //						   link_param_nom,strike);

            cube A_nom, B_nom, Ad_nom, Bd_nom;
            BOOST_TEST_MESSAGE("Linearizing dynamics...");
            //linearize_dynamics(barrett_wam_dynamics_art, link_param_nom, strike,
            //                   1, 1e-6, A_nom, B_nom);
            linearize_dynamics(link_param_nom,strike,1,A_nom,B_nom);
            BOOST_TEST_MESSAGE("Discretizing dynamics...");
            discretize_dynamics(DT, A_nom, B_nom, Ad_nom, Bd_nom);
            cube Klqr = zeros<cube>(m, n, N);
            cube P = zeros<cube>(n, n, N + 1);
            mat Q = eye(n, n);
            cube Qs = zeros<cube>(n, n, N);
            Qs.each_slice() = eye(n, n);
            mat R = r_vals(i, j) * eye(m, m);
            //BOOST_TEST_MESSAGE("Calculating LQR feedback mats...");
            lqr_fin_horizon_ltv(Ad_nom, Bd_nom, Q, Q, R, Klqr, P);

            //BOOST_TEST_MESSAGE("Evolving dynamics...");
            evolve(barrett_wam_dynamics_art, link_param_act, strike, strike.us,
                    Klqr, qinit, traj_act);
            double rms = norm(traj_act.Q - strike.Q, "fro")
                            / sqrt(strike.Q.n_elem);
            BOOST_TEST_MESSAGE(
                    "Tracking error with nom = " << models[i] <<
                    ", act = " << models[j] << " : " << rms);
            BOOST_TEST(is_finite(rms));
        }
    }
}
