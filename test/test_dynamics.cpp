/*
 * test_dynamics.cpp
 *
 * Forward/inverse dynamics checks
 *
 * Tests for checking the correctness of forward and inverse dynamics functions
 * as well as loading the dynamics model parameters
 *
 *  Created on: Aug 15, 2017
 *      Author: okoc
 */

#include <boost/test/unit_test.hpp>
#include <adolc/adouble.h>            // use of active doubles
#include <adolc/drivers/drivers.h>    // use of "Easy to Use" drivers
#include <adolc/taping.h>             // use of taping
#include <armadillo>
#include "constants.h"
#include "traj.h"
#include "dynamics.h"
#include "experiment.h"

using namespace arma;
using namespace dyn;
using namespace Traj;

/*
 * Testing forward dynamics, comparing with MATLAB
 *
 * Time elapsed is also given in output.
 */
void test_forward_dynamics() {

    BOOST_TEST_MESSAGE("Testing forward dynamics");

    BOOST_TEST_MESSAGE("Loading dynamics model parameters");
    dyn::link link_param[NDOF + 1];
    const std::string filename = "LinkParameters1.cfg";
    load_link_params(filename, link_param);

    joint Q;
    Q.u(1) = -0.10;
    Q.u(3) = -1.71;
    wall_clock timer;
    timer.tic();
    barrett_wam_dynamics_art(link_param, Q);
    double t_elapsed = timer.toc();
    vec7 qdd_matlab = { 1.210915593859586, -1.736650691007738,
            0.711975303190104, 7.973565526107187, -1.152732404657257,
            -9.408056049294412, 9.406444931694216 };
    BOOST_TEST_MESSAGE(
            "Forward Dynamics Computation took " << 1000*t_elapsed << " ms.");
    BOOST_TEST_MESSAGE("Comparing with MATLAB");
    //cout << Q.qdd;
    BOOST_TEST(approx_equal(Q.qdd, qdd_matlab, "absdiff", 0.001));
}

/*
 * Testing inverse dynamics, comparing with MATLAB.
 *
 * Time elapsed is also given.
 */
void test_inverse_dynamics() {

    arma_rng::set_seed(1);
    BOOST_TEST_MESSAGE("Testing inverse dynamics");

    BOOST_TEST_MESSAGE("Loading dynamics model parameters");
    dyn::link link_param[NDOF + 1];
    const std::string filename = "LinkParameters1.cfg";
    load_link_params(filename, link_param);

    joint Q;
    Q.q = randn<vec>(7);
    Q.qd = randn<vec>(7);
    Q.qdd = randn<vec>(7);
    wall_clock timer;
    timer.tic();
    barrett_wam_inv_dynamics_ne(link_param, Q);
    double t_elapsed = timer.toc();
    vec7 u_matlab =
            { 0.0101, -4.8172, -0.1534, -4.7213, 0.0005, -0.4588, 0.0009 };
    BOOST_TEST_MESSAGE(
            "Inverse Dynamics Computation took " << 1000*t_elapsed << " ms.");
    BOOST_TEST_MESSAGE("Comparing with MATLAB");
    BOOST_TEST(approx_equal(Q.u, u_matlab, "absdiff", 0.001));
}

/*
 * Checking for dynamics cancellation with three diff. nominal models.
 *
 * Checking for approximate equality of forward "o" inverse dynamics,
 * with tolerance 0.001. For each set of model parameters:
 * 1. Fix arbitrary q,qd,u
 * 2. calc qdd with forward dynamics
 * 3. check cancellation with inverse dynamics.
 */
void test_dynamics_cancellation() {

    BOOST_TEST_MESSAGE("Checking for dynamics cancellation");
    //arma_rng::set_seed(1);
    arma_rng::set_seed_random();

    dyn::link link_param[NDOF + 1];
    std::vector<int> models = { 1, 2, 3 };

    for (int i = 0; i < models.size(); i++) {
        std::string filename = "LinkParameters" + std::to_string(models[i]) + ".cfg";
        load_link_params(filename, link_param);

        joint Q;
        vec7 u = randn<vec>(7);
        // start by checking forw + inv
        Q.q = randn<vec>(7);
        Q.qd = randn<vec>(7);
        Q.u = u;
        barrett_wam_dynamics_art(link_param, Q);
        barrett_wam_inv_dynamics_ne(link_param, Q);
        //cout << Q.u - u << endl;
        BOOST_TEST(approx_equal(Q.u, u, "absdiff", 0.001));
        // finish with inv + forw
        vec7 qdd = randn<vec>(7);
        Q.q = randn<vec>(7);
        Q.qd = randn<vec>(7);
        Q.qdd = qdd;
        barrett_wam_inv_dynamics_ne(link_param, Q);
        barrett_wam_dynamics_art(link_param, Q);
        //cout << Q.qdd - qdd << endl;
        BOOST_TEST(approx_equal(Q.qdd, qdd, "absdiff", 0.001));
    }
}

/*
 * Testing autodifferentiation of InvDyn w.r.t. link parameters
 *
 * Since InvDyn is linear w.r.t. link parameters, we want to make sure that
 * the regressor is the same for fixed q,qd,qdd
 */
void test_autodiff_inv_dyn() {

    BOOST_TEST_MESSAGE("Checking autodiff of Inverse Dynamics...");

    dyn::link link_param[NDOF+1];
    std::string filename1 = "LinkParameters1.cfg";
    std::string filename2 = "LinkParameters2.cfg";
    load_link_params(filename1, link_param);

    int m = NDOF;
    int n = (NDOF+1)*10;
    joint Q;
    generate_inv_dyn_tape(m,n,Q);

    // evaluate at the first link parameter set
    mat link_mat = zeros<mat>(10,NDOF+1);
    vectorise_links(link_param,link_mat);
    mat jact1 = zeros<mat>(n,m); //jacobian transpose
    vec theta = vectorise(link_mat);
    auto_jacobian(TAG_INV_DYN_JAC,theta,jact1);

    // evaluate at second set
    load_link_params(filename2, link_param);
    vectorise_links(link_param, link_mat);
    theta = vectorise(link_mat);
    mat jact2 = zeros<mat>(n,m);
    auto_jacobian(TAG_INV_DYN_JAC,theta,jact2);

    // compare two jacobians
    BOOST_TEST(approx_equal(jact1,jact2,"absdiff",0.001));

}

/*
 * Comparing autodifferentiation of forward dynamics w.r.t. joints
 * to numeric diff.
 */
void test_autodiff_linearize_dyn() {

    BOOST_TEST_MESSAGE("Comparing forward dyn auto-diff to num-diff...");

    arma_rng::set_seed_random();
    vec7 qrest;
    init_right_posture(qrest);
    joint qinit;
    qinit.q = qrest;
    traj strike, returning;
    gen_random_tabletennis_traj(qinit, qrest, 1.0, strike, returning);
    cube A_num, B_num, A_auto, B_auto;
    dyn::link link_param[NDOF + 1];
    const std::string filename = "LinkParameters1.cfg";
    load_link_params(filename, link_param);
    calc_inv_dynamics_for_traj(barrett_wam_inv_dynamics_ne,
                               link_param,
                               strike);
    BOOST_TEST_MESSAGE("Numerical differentiation of forward dyn...");
    wall_clock timer;
    timer.tic();
    linearize_dynamics(barrett_wam_dynamics_art,
                        link_param, strike, 1,
                        1e-6, A_num, B_num);
    double t = timer.toc();
    BOOST_TEST_MESSAGE("Time elapsed (ms) : " << 1000*t);
    BOOST_TEST_MESSAGE("Automatic differentiation of forward dyn...");
    timer.tic();
    linearize_dynamics(link_param, strike, 1, A_auto, B_auto);
    t = timer.toc();
    BOOST_TEST_MESSAGE("Time elapsed (ms) : " << 1000*t);
    BOOST_TEST(approx_equal(A_num,A_auto,"absdiff",0.001));
    BOOST_TEST(approx_equal(B_num,B_auto,"absdiff",0.001));
}
