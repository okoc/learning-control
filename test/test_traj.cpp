/*
 * test_traj.cpp
 *
 * Testing trajectory generation (polynomials, dmp, etc.).
 */

#include <boost/test/unit_test.hpp>
#include <armadillo>
#include "traj.h"
#include "lookup.h"
#include "constants.h"
#include "dynamics.h"

using namespace arma;
using namespace dyn;
using namespace Traj;

/*
 * Testing for strike & return polynomial generation.
 *
 * We check if the trajectories are coming back to initial state.
 */
void test_poly() {

    BOOST_TEST_MESSAGE("Testing for strike & return polynomial generation");

    BOOST_TEST_MESSAGE("Looking up a random trajectory...");
    arma_rng::set_seed_random();
    vec::fixed<15> strike_params;
    vec6 ball_state;
    player::lookup_random_entry(ball_state, strike_params);

    vec7 qinit;
    double time2return = 1.0;
    init_right_posture(qinit);
    vec::fixed<8> rest_params;
    rest_params.head(NDOF) = qinit;
    rest_params(NDOF) = time2return;
    joint qact;
    qact.q = qinit;
    spline_params poly;
    calc_coeffs(qact, strike_params, rest_params, poly);

    double T_total = time2return + strike_params(14);
    int N_total = (int) (T_total / DT);
    double t = 0.0;
    BOOST_TEST_MESSAGE("Generating trajectory iteratively...");
    for (int i = 0; i < N_total; i++) {
        update_next_state(poly, qinit, time2return, t, qact);
    }
    BOOST_TEST_MESSAGE(
            "Checking if trajectory comes back to initial posture...");
    BOOST_TEST(approx_equal(qact.q, qinit, "absdiff", 0.001));
}

/*
 * Testing tracking hitting movements with inverse dynamics.
 *
 * PD feedback is also additionally applied to stabilize the system.
 */
void test_tracking_with_inv_dynamics() {

    BOOST_TEST_MESSAGE("Testing tracking hitting movements with inv.dyn...");
    arma_rng::set_seed(2);

    BOOST_TEST_MESSAGE("Loading PD gains from file...");
    mat fb = zeros<mat>(NDOF, 2 * NDOF);
    load_PD_feedback_gains(fb);
    //cout << fb;
    vec7 qrest;
    init_right_posture(qrest);
    joint qinit;
    qinit.q = qrest;
    //generate random reference trajectory (batch)

    BOOST_TEST_MESSAGE("Looking up a random trajectory...");
    traj ref;
    gen_random_tabletennis_traj(qinit, qrest, 1.0, ref);
    //cout << ref.Q.col(0) << endl << qinit.q << endl;
    dyn::link link_param[NDOF + 1];
    const std::string filename = "LinkParameters1.cfg";
    load_link_params(filename, link_param);
    calc_inv_dynamics_for_traj(barrett_wam_inv_dynamics_ne, link_param, ref);

    BOOST_TEST_MESSAGE("Checking pos,vel,acc,torque limits for traj.");
    check_all_limits(ref);
    // track traj with only inv.dyn
    traj act0 = ref;
    evolve(barrett_wam_dynamics_art, link_param, ref, ref.us,
            zeros<mat>(NDOF, 2 * NDOF), qinit, act0);
    // track traj. with fb and inv.dyn
    traj act1 = ref;
    evolve(barrett_wam_dynamics_art, link_param, ref, ref.us, fb, qinit, act1);
    // track traj. only with fb
    traj act2 = ref;
    evolve(barrett_wam_dynamics_art, link_param, ref,
            zeros<mat>(NDOF, ref.Q.n_cols), fb, qinit, act2);
    double rms0 = norm(act0.Q - ref.Q, "fro") / sqrt(ref.Q.n_elem);
    double rms1 = norm(act1.Q - ref.Q, "fro") / sqrt(ref.Q.n_elem);
    double rms2 = norm(act2.Q - ref.Q, "fro") / sqrt(ref.Q.n_elem);
    //pow(norm(traj_act.Qd - ref.Qd,2),2));
    BOOST_TEST_MESSAGE("Tracking error with only inv.dyn = " << rms0);
    BOOST_TEST_MESSAGE("Tracking error with inv.dyn + fb = " << rms1);
    BOOST_TEST_MESSAGE("Tracking error with only fb = " << rms2);
    BOOST_TEST(rms1 < rms2);
}
