/*
 * test_ilc.cpp
 *
 * Testing all sorts of ILC algorithms here.
 *
 *  Created on: Aug 16, 2017
 *      Author: okoc
 */

#include <boost/test/unit_test.hpp>
#include <armadillo>

#include "ilc.h"
#include "constants.h"
#include "traj.h"
#include "dynamics.h"
#include "adaptation.h"
#include "experiment.h"

using namespace arma;
using namespace boost::unit_test;
//namespace data = boost::unit_test::data;

void test_poly();
void test_filtfilt();
void test_inverse_dynamics();
void test_forward_dynamics();
void test_dynamics_cancellation();
void test_tracking_with_inv_dynamics();
void test_compare_lqr_wam();
void test_lqr_wam_nominal();
void test_autodiff_inv_dyn();
void test_autodiff_linearize_dyn();
void check_calc_dyn_derivative_cov();
void test_sys_id();
void test_ilc_link_adaptive_wam();
void test_ilc_config_wam();
void test_compare_adaptive_ilc_wam();
void test_table_tennis();
void test_table_tennis_refactored();

using namespace dyn;
using namespace Traj;
using namespace adapt;

/*
 * Main function for boost unit testing.
 *
 * Unit tests are added here.
 */
test_suite* init_unit_test_suite(int /*argc*/, char* /*argv*/[]) {

    test_suite* ts = BOOST_TEST_SUITE("test_suite");
    ts->add(BOOST_TEST_CASE(&test_poly));
    ts->add(BOOST_TEST_CASE(&test_filtfilt));
    ts->add(BOOST_TEST_CASE(&test_forward_dynamics));
    ts->add(BOOST_TEST_CASE(&test_inverse_dynamics));
    ts->add(BOOST_TEST_CASE(&test_dynamics_cancellation));
    ts->add(BOOST_TEST_CASE(&test_tracking_with_inv_dynamics));
    ts->add(BOOST_TEST_CASE(&test_autodiff_inv_dyn));
    ts->add(BOOST_TEST_CASE(&test_autodiff_linearize_dyn));
    //ts->add(BOOST_TEST_CASE(&check_calc_dyn_derivative_cov));
    //ts->add(BOOST_TEST_CASE(&test_compare_lqr_wam));
    //ts->add(BOOST_TEST_CASE( &test_lqr_wam_nominal ));
    //ts->add(BOOST_TEST_CASE(&test_sys_id ));
    //ts->add(BOOST_TEST_CASE(&test_ilc_link_adaptive_wam));
    ts->add(BOOST_TEST_CASE(&test_ilc_config_wam));
    //ts->add(BOOST_TEST_CASE(&test_compare_adaptive_ilc_wam));
    //ts->add(BOOST_TEST_CASE(&test_table_tennis));
    //ts->add(BOOST_TEST_CASE(&test_table_tennis_refactored));
    return ts;
}

/*
 * Compare different adaptive ILCs on Barrett WAM and average over multiple traj.
 */
void test_compare_adaptive_ilc_wam() {

    const int num_algs = 3; // LINK, DISCRETE AND CTS LTV ADAPTATION
    const int num_exp = 5; // each one is learning over a diff traj.
    const int num_iter = 10;
    int seeds[num_exp] = {1,12,11,8,9};
    algo methods[num_algs] = {DISC_LBR, CTS_LBR, LINK_LBR};
    std::string method_names[num_algs] = {"Discrete LBR", "Continuous LBR", "Link adaptation with LBR"};
    cube rms = zeros<cube>(num_iter+1, num_exp, num_algs);

    joint qinit;
    init_posture(1, qinit.q);
    all_options options[num_algs];
    dyn::link link_param_act[NDOF+1];

    for (int i = 0; i < num_algs; i++) {

        BOOST_TEST_MESSAGE("Testing " + method_names[i]);

        options[i].e_opt.check_torque = false;
        options[i].e_opt.max_num_iter = num_iter;
        options[i].e_opt.link_param_file = "LinkParameters2.cfg";
        options[i].e_opt.turn_on_filter = true;
        options[i].i_opt.adaptive = true;
        options[i].a_opt.forgetting_factor = 0.8;
        options[i].a_opt.method = methods[i];

        for (int j = 0; j < num_exp; j++) {
            options[i].e_opt.random_seed = seeds[j];
            Experiment exp = Experiment(qinit.q, options[i], barrett_wam_dynamics_art,
                                                          barrett_wam_inv_dynamics_ne);
            randomize_links(50.0,exp.link_param,link_param_act);
            traj traj_act = exp.strike;
            rms(0,j,i) = exp.simulate(barrett_wam_dynamics_art,
                                      link_param_act, qinit, traj_act);
            for (int k = 0; k < num_iter; k++) {
                exp.update(traj_act);
                if (exp.ready())
                    rms(k+1,j,i) = exp.simulate(barrett_wam_dynamics_art,
                                               link_param_act, qinit, traj_act);
            }
        }
    }

    // show or print to file for each experiment rms results
    BOOST_TEST_MESSAGE("RMS of tracking errors for different ILC: \n" << rms);
    rms.save("/home/okoc/learning-control/compare_adaptive_ilcs_rms.txt",raw_ascii);
}

/*
 * Testing Link Adaptive ILC on Barrett WAM using Experiments class.
 *
 */
void test_ilc_link_adaptive_wam() {

    BOOST_TEST_MESSAGE("Testing ADAPTIVE ILC on Barrett WAM...");
    joint qinit;
    init_posture(1, qinit.q);
    all_options options;
    options.e_opt.lookup = true;
    options.e_opt.check_torque = false;
    options.e_opt.max_num_iter = 5;
    options.e_opt.link_param_file = "LinkParameters2.cfg";
    options.e_opt.turn_on_filter = true;
    options.e_opt.random_seed = 5;
    options.i_opt.adaptive = true;
    options.a_opt.forgetting_factor = 0.8;
    options.a_opt.method = LINK_LBR;
    Experiment exp = Experiment(qinit.q, options, barrett_wam_dynamics_art,
                                                  barrett_wam_inv_dynamics_ne);
    dyn::link link_param_act[NDOF + 1];
    load_link_params("LinkParameters1.cfg", link_param_act);
    // create random multiplicative dist. to link params
    //randomize_links(50.0,exp.link_param,link_param_act);

    traj traj_act = exp.strike;
    vec rms = zeros<vec>(exp.get_num_exp() + 1);
    rms(0) = exp.simulate(barrett_wam_dynamics_art,
                          link_param_act, qinit, traj_act);
    for (int j = 0; j < exp.get_num_exp(); j++) {
        exp.update(traj_act);
        if (exp.ready())
            rms(j + 1) = exp.simulate(barrett_wam_dynamics_art,
                                      link_param_act, qinit, traj_act);
    }
    BOOST_TEST_MESSAGE("RMS of tracking errors for ILC: \n" << rms.t());
    BOOST_TEST(all(diff(rms) < 0.0));
}

/*
 * Loading ILC experiments from config file
 *
 */
void test_ilc_config_wam() {

    BOOST_TEST_MESSAGE(
            "Testing ILC on Barrett WAM based on ilc.cfg file...");
    joint qinit;
    init_posture(1, qinit.q);
    Experiment exp = Experiment(qinit.q, barrett_wam_dynamics_art,
    	                                     barrett_wam_inv_dynamics_ne);
    dyn::link link_param_act[NDOF + 1];
    load_link_params("LinkParameters1.cfg", link_param_act);

    // create random multiplicative dist. to link params
    //randomize_links(50.0,exp.link_param,link_param_act);

    // create random additive dist. (UNSTABLE!)
    /*mat Sigma_link = randn<mat>(80,80);
    Sigma_link = (Sigma_link + Sigma_link.t()) / 2.0;
    Sigma_link += 80 * eye<mat>(80,80);
    Sigma_link = 1e-10 * Sigma_link/80;
    randomize_links(Sigma_link,exp.link_param,link_param_act);
    mat link_mat = zeros<mat>(10,NDOF+1);
    vectorise_links(link_param_act,link_mat);
    cout << link_mat;*/

    traj traj_act = exp.strike;
    vec rms = zeros<vec>(exp.get_num_exp() + 1);
    rms(0) = exp.simulate(barrett_wam_dynamics_art,
                            link_param_act, qinit, traj_act);
    for (int j = 0; j < exp.get_num_exp(); j++) {
        exp.update(traj_act);
        if (exp.ready())
            rms(j + 1) = exp.simulate(barrett_wam_dynamics_art,
                                      link_param_act, qinit, traj_act);
    }
    BOOST_TEST_MESSAGE("RMS of tracking errors for ILC: \n" << rms.t());
    BOOST_TEST(all(diff(rms) < 0.0));
}
