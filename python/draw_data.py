# analyze real robot data

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy import signal
import barrett_wam_kinematics as wam
 
# load joints data
date = 'Sat May  5 18:51:14 2018'
#folder_name = '/home/okoc/learning-control/data/2ndApril/'
folder_name = '/home/robolab/learning-control/data/'
num_ilc_iter = 1
ndof = 7
joint_filename = folder_name + 'joints_' + date + '.txt'
M = np.fromfile(joint_filename, sep=" ")
#print M.shape
#print M[0:14]
M = np.reshape(M,[3*ndof,M.size/(3*ndof)])
N_ref = M.shape[1]/(num_ilc_iter+1)

dt = 0.002
t = dt * np.arange(0,N_ref*num_ilc_iter)
q_des = M[0:7,0:N_ref]
qd_des = M[7:14,0:N_ref]
q_act = M[0:7,N_ref:]
qd_act = M[7:14,N_ref:]

Q_des = np.tile(q_des,num_ilc_iter)
Qd_des = np.tile(qd_des,num_ilc_iter)

f, axs = plt.subplots(7,2,sharex=False)

#print q_des.shape
#print q_act.shape

for i in range(7):
	axs[i,0].plot(t,Q_des[i,:])
	axs[i,0].plot(t,q_act[i,:])
	axs[i,1].plot(t,Qd_des[i,:])
	axs[i,1].plot(t,qd_act[i,:]) 

#plt.show()

###### FILTER THE DESIRED JOINTS

'''
filter_freq = 50
filter_perc = filter_freq/np.floor(N_ref/2)
print "filter_perc: ", filter_perc
b,a = signal.butter(2,filter_perc)
f,axs = plt.subplots(7,1,sharex=False)
t_short = dt * np.arange(0,N_ref)
q_pad = np.zeros((7,N_ref))

for i in range(7):
    q_pad[i,:] = signal.filtfilt(b,a,q_des[i,:],padlen=50)
    #q_pad[i,:] = signal.filtfilt(b,a,q_act[i,0:N_ref],padlen=50)
    #fgust = signal.filtfilt(b,a,q_des[i,:],method="gust")
    axs[i].plot(t_short,q_des[i,:])
    #axs[i].plot(t_short,q_act[i,0:N_ref])
    axs[i].plot(t_short,q_pad[i,:])
    #axs[i].plot(t_short,fgust)
#plt.show()

x = np.zeros((3,N_ref))
x_pad = np.zeros((3,N_ref))
for i  in range(N_ref):
    q1 = q_des[:,i]
    #q = q_act[:,i]
    As = wam.barrett_wam_kinematics(q1)
    x[:,i] = np.transpose(As[-1])[-1][0:3]
    q2 = q_pad[:,i]
    As2 = wam.barrett_wam_kinematics(q2)
    x_pad[:,i] = np.transpose(As2[-1])[-1][0:3]

## KINEMATICS PLOT
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
for i in range(3):
    ax.scatter(x[0,:], x[1,:], x[2,:], c="b")
    ax.scatter(x_pad[0,:], x_pad[1,:], x_pad[2,:], c="r")

print 'Norm of diff:', np.linalg.norm(x-x_pad)
'''
plt.show()
