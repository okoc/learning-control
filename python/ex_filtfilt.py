##### TESTING FILTER

import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
import barrett_wam_kinematics as wam

###### EXAMPLE 1

# create signal
#t = np.linspace(0,1.0,2001)
#xlow = np.sin(2*np.pi*5*t)
#xhigh = np.sin(2*np.pi*250*t)
#x = xlow + xhigh

#b,a = signal.butter(8,0.125)
#y = signal.filtfilt(b,a,x,padlen=150)
#print np.abs(y-xlow).max()

###### MODIFICATION OF EXAMPLE 2

b,a = signal.butter(2,0.25)
np.random.seed(123456)
n = 60
t = np.linspace(0,1.0,n)
sig = np.sin(2*np.pi*2*t)
sig_noise = sig + 0.1*np.random.randn(n)

fgust = signal.filtfilt(b,a,sig_noise,method="gust")
fpad = signal.filtfilt(b,a,sig_noise,padlen=50)
plt.plot(sig,'k-',label='input')
plt.plot(fgust, 'b-', linewidth=4, label='gust')
plt.plot(fpad, 'c-', linewidth=1.5, label='pad')
plt.legend(loc='best')
plt.show()
